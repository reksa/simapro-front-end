import { JenisDayaGunaService } from "./services/data-services/jenis-daya-guna.service";
import { OnProgressGuardService } from "./services/auth/on-progress-guard.service";
import { CommonModule, UpperCasePipe, DatePipe } from "@angular/common";
// import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ErrorHandler } from "@angular/core";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";

// Import containers
import { FullLayoutComponent, SimpleLayoutComponent } from "./containers";

const APP_CONTAINERS = [FullLayoutComponent, SimpleLayoutComponent];

// Import components
import {
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV,
  AppListErrorsComponent
} from "./components";

import { AuthComponent } from "./views/auth/auth.component";
// Import Error Pages
import {
  P401Component,
  P403Component,
  P404Component,
  P500Component
} from "app/views/error-pages";

const APP_COMPONENTS = [
  AppAsideComponent,
  AppBreadcrumbsComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarFooterComponent,
  AppSidebarFormComponent,
  AppSidebarHeaderComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
];

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from "./directives";

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
];

// Import routing module
import { AppRoutingModule } from "./app.routing";

// Import 3rd party components
import {
  BsDropdownModule,
  ModalModule,
  TabsModule,
  AlertModule,
  CollapseModule
} from "ngx-bootstrap";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { NgxSpinnerModule } from "ngx-spinner";

import { AppErrorHandler } from "app/error-handler/app-error-handler";

// Import Services
import { JwtService } from "./services/jwt.service";
import { ApiService } from "./services/data-services/api.service";
import {
  NoAuthGuard,
  AuthGuard,
  PermissionGuard,
  LoginSSOService
} from "./services/auth";
import { AppErrorHandlerService } from "app/error-handler/app-error-handler.service";
import { AlertService } from "app/services/alert.service";
import { YagaModule } from "@yaga/leaflet-ng2";
import {
  PenggunaService,
  PermissionService,
  NotifikasiService,
  ProvinsiService,
  CompanyCodeService,
  BusinessAreaService,
  LokasiService,
  KabupatenService,
  AsetTanahService,
  AsetPropertiService,
  JsonService
} from "app/services/data-services";
import { CreditsComponent } from "./views/credits/credits.component";
import { TimeAgoPipe } from "time-ago-pipe";
import { OnProgressService } from "./services/on-progress.service";
import { SelectModule } from "ng2-select";
import { ModalFilterContentComponent } from "./views/modal-filter-content/modal-filter-content.component";
import { TextToSlugPipe } from "./services/text-to-slug.service";
import { RouterModule } from "@angular/router";
import { ModalContentSelectAsetComponent } from "./views/select-aset/modal-content-select-aset/modal-content-select-aset.component";
import { InputNumberModule } from "./views/input-number/input-number.module";
import { EditPermissionGuard } from "./services/auth/edit-permission-guard.service";
import { HttpClientModule } from "@angular/common/http";
import { LoginWithSsoComponent } from "./views/auth/login-with-sso/login-with-sso.component";
import { FaqComponent } from "./views/faq/faq.component";
import { DownloadPageComponent } from "./views/download-page/download-page.component";

@NgModule({
  imports: [
    RouterModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    ChartsModule,
    YagaModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    SelectModule,
    InputNumberModule,
    NgxSpinnerModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
    P401Component,
    P403Component,
    P404Component,
    P500Component,
    AuthComponent,
    AppListErrorsComponent,
    CreditsComponent,
    TimeAgoPipe,
    ModalFilterContentComponent,
    ModalContentSelectAsetComponent,
    LoginWithSsoComponent,
    FaqComponent,
    DownloadPageComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    ApiService,
    AuthGuard,
    NoAuthGuard,
    PermissionGuard,
    EditPermissionGuard,
    JwtService,
    PenggunaService,
    PermissionService,
    AppErrorHandlerService,
    AlertService,
    NotifikasiService,
    OnProgressService,
    OnProgressGuardService,
    ProvinsiService,
    KabupatenService,
    LokasiService,
    CompanyCodeService,
    AsetTanahService,
    AsetPropertiService,
    BusinessAreaService,
    DatePipe,
    UpperCasePipe,
    TextToSlugPipe,
    TimeAgoPipe,
    JenisDayaGunaService,
    LoginSSOService,
    JsonService,
    {
      provide: ErrorHandler,
      useClass: AppErrorHandler
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalFilterContentComponent,
    ModalContentSelectAsetComponent
  ]
})
export class AppModule {}
