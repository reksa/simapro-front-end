import { Component, AfterViewInit } from '@angular/core';
import { AlertService, AlertMessage } from 'app/services/alert.service';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements AfterViewInit{
  dismissible = true;
  alerts: any = [];

  constructor(
    private alertService: AlertService,
    router: Router,
    spinner: NgxSpinnerService
  ) {
    router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        spinner.show();
      } else if (e instanceof NavigationEnd) {
        spinner.hide();
      } else if (e instanceof NavigationError) {
        // console.log(e.error);
        spinner.hide();
      }
    });
  }

  ngAfterViewInit() {
    this.alertService.getAlert().subscribe( (alert: AlertMessage) => {
      if (typeof alert !== 'undefined') {
        this.alerts.push(alert);
      }
    });
  }
 }
