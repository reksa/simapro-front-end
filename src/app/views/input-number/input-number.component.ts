import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
// creates an ngModel accessor to be used in components providers
const INPUT_NUMBER_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputNumberComponent),
  multi: true
};

@Component({
  selector: 'input-number',
  providers: [ INPUT_NUMBER_CONTROL_VALUE_ACCESSOR ],
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss']
})
export class InputNumberComponent implements OnInit {
  private _value = 0;
  editable = false;

  @Input('placeholder') _placeholder: number;
  @Output() myValue = new EventEmitter();
  @ViewChild('edit') _edit: ElementRef;
  
  get value(): number{
    return this._value;
  };
  // set the value (emit) to the parent model
  set value(v: number) {
    if (v !== this._value) {
      this._value = v;
      this.onChange(v);
    }
  }
  // write the value to the input
  writeValue(value: any) {
    this._value = value;
    this.onChange(value);
  }

  onChange = (_) => {};
  onTouched = () => {};
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  constructor() { }

  ngOnInit() {
  }

  toggleEditable(status) {
    this.editable = status;
    const obj = this;
    setTimeout(() => {
      if(obj && obj._edit && obj._edit.nativeElement) {
        obj._edit.nativeElement.focus();
      }
    }, 100);
  }

  emitValue ($event) {
    this.myValue.emit($event);
  }

}
