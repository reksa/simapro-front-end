import { InputNumberComponent } from './input-number.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [InputNumberComponent],
  exports: [InputNumberComponent]
})
export class InputNumberModule { }
