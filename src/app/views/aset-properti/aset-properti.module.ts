import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { AppGeometryModule } from 'app/components/app-geometry/app-geometry.module';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe, DatePipe, CurrencyPipe, UpperCasePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AsetPropertiRoutingModule } from './aset-properti-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';

import { AsetPropertiComponent } from './aset-properti/aset-properti.component';
import { AsetPropertiDetailComponent } from './aset-properti-detail/aset-properti-detail.component';
import { AsetPropertiEditComponent } from './aset-properti-edit/aset-properti-edit.component';
import {
  AsetPropertiService, ProvinsiService, CompanyCodeService,
  LokasiService, KabupatenService, BusinessAreaService,
  SumberPerolehanService, StatusKelayakanService,
  JenisDayaGunaService,
  JenisPropertiService, AsetTanahService
} from 'app/services/data-services';
import { AlertService } from 'app/services/alert.service';
import { YagaModule } from '@yaga/leaflet-ng2';
import { TabsModule } from 'ngx-bootstrap/tabs/tabs.module';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { DataMapService } from '../../services/data-services/data-map.service';
import { AsetPropertiRevisiComponent } from './aset-properti-revisi/aset-properti-revisi.component';
import { SelectModule } from 'ng2-select';
import { ModalModule } from 'ngx-bootstrap';
import { TimelineModule } from '../timeline/timeline.module';
import { ValidasiAsetPropertiComponent } from './validasi-aset-properti/validasi-aset-properti.component';
import { InputNumberModule } from '../input-number/input-number.module';
import { MapLegendModule } from '../map-legend/map-legend.module';

@NgModule({
  imports: [
    CommonModule,
    AsetPropertiRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    HttpModule,
    YagaModule,
    TabsModule,
    AppGeometryModule,
    AppDatatableModule,
    SelectModule,
    ModalModule,
    TimelineModule,
    InputNumberModule,
    MapLegendModule
  ],
  providers: [
    AsetPropertiService, AlertService, ProvinsiService,
    KabupatenService, LokasiService, CompanyCodeService,
    BusinessAreaService, JenisDayaGunaService, UpperCasePipe,
    StatusKelayakanService, JenisPropertiService,
    SumberPerolehanService, LokasiService, AsetTanahService,
    DecimalPipe, DatePipe, CurrencyPipe, TextToSlugPipe, DataMapService
  ],
  declarations: [
    AsetPropertiComponent, AsetPropertiDetailComponent, AsetPropertiEditComponent,
    AsetPropertiRevisiComponent,
    ValidasiAsetPropertiComponent
  ]
})
export class AsetPropertiModule { }
