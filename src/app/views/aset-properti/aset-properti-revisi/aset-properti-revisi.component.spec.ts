import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetPropertiRevisiComponent } from './aset-properti-revisi.component';

describe('AsetPropertiRevisiComponent', () => {
  let component: AsetPropertiRevisiComponent;
  let fixture: ComponentFixture<AsetPropertiRevisiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetPropertiRevisiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetPropertiRevisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
