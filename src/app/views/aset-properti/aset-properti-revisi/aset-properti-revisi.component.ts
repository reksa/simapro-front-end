import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {
  AsetPropertiService, ProvinsiService, KabupatenService, BusinessAreaService, LokasiService,
  AsetTanahService, SumberPerolehanService, CompanyCodeService, JenisPropertiService
} from 'app/services/data-services';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { environment } from 'environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  PerpindahanAsetProperti, AsetPropertiDetail, AsetProperti, DokumenLegal, NilaiAsetProperti,
  TahapDokumenLegal, Pengguna, PotensiAset, AsetTanah, JenisAsetProperti, SubJenisAsetProperti,
  SumberPerolehan
} from 'app/models';
import { Location, UpperCasePipe } from '@angular/common';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { LatLng, LatLngBounds, Map } from 'leaflet';
import { TabsetComponent } from 'ngx-bootstrap';
import { SelectComponent, SelectItem } from 'ng2-select';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-properti-revisi',
  templateUrl: './aset-properti-revisi.component.html',
  styleUrls: ['./aset-properti-revisi.component.scss']
})
export class AsetPropertiRevisiComponent implements OnInit {
  upload = environment.upload_url+'/aset-properti/';
  oldFoto = null; //foto
  foto;
  documentReady = false;
  isSubmitted = false;
  isRevisi = false;
  jenisMutasi;
  fragment = 'detail';
  formDetail: FormGroup;

  formLegal: FormGroup;
  tahapDokumenLegal: TahapDokumenLegal;

  formMutasi: FormGroup;
  asetTanahs: AsetTanah[] = [];
  asetTanah: AsetTanah;
  jenisAsetPropertis: JenisAsetProperti[] = [];
  jenisAsetProperti: JenisAsetProperti;
  subJenisAsetPropertisX: SubJenisAsetProperti[] = [];
  pengguna: Pengguna;

  formAsetProperti: FormGroup;
  asetPropertiPerubahan: AsetProperti;
  sumberPerolehans: SumberPerolehan[] = [];

  formNilai: FormGroup;
  min: number;
  max: number;

  formPotensi: FormGroup;
  potensiAset: PotensiAset;

  tab = 'detail';
  asetProperti = new AsetProperti();
  detailAsetProperti: AsetPropertiDetail;
  detailAsetPropertiPerubahan: AsetPropertiDetail;
  perpindahanAsetProperti: PerpindahanAsetProperti;
  perpindahanAsetPropertiPerubahan: PerpindahanAsetProperti;
  dokumen: DokumenLegal;
  dokumenPerubahan: DokumenLegal;
  nilai: NilaiAsetProperti;
  nilaiPerubahan: NilaiAsetProperti;

  is_validator = false;
  editable = true;
  name = 'Passau';

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 18;
  memo = '';
  poly: LatLng[][] = [[]];
  polyOld: any;

  namaAsetProperti = '';
  namaCompanyCode = '';
  root = '/aset-properti'
  path = '';
  modalRef: BsModalRef;

  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  @ViewChild('tabRevisi') tabRevisi: TabsetComponent;
  
  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;
  tanah: AsetTanah;

  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];
  itemsT = [];

  preview = '';

  @ViewChild('tabset') tabset: TabsetComponent;
  @ViewChild('templatePoint') templatePoint: TemplateRef<any>;
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('selectTanah') selectTanah: SelectComponent;

  @ViewChild('map') map: Map;

  constructor(
    private route: ActivatedRoute,
    private asetPropertiService: AsetPropertiService,
    private asetTanahService: AsetTanahService,
    private sumberPerolehanService: SumberPerolehanService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private lokasiService: LokasiService,
    private jenisPropertiService: JenisPropertiService,
    private _location: Location,
    private _fb: FormBuilder,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    Observable.combineLatest(this.route.url, this.route.paramMap, this.route.fragment)
    .subscribe(obs => {
      this.isRevisi = obs[0][0].path === 'revisi'; // edit or revisi
      const params = obs[1];

      this.jenisMutasi = params.get('jenisMutasi') || 'internal'; // edit or revisi

      const fragment = params.get('fragment');
      this.fragment = fragment;

      const id = parseInt(params.get('idAsetProperti'), 10);

      this.path = `${this.root}/${obs[0][0].path}/${id}/aset/${fragment}`;

      if(this.jenisMutasi === 'eksternal' || this.jenisMutasi === 'penerimaan') {
        this.path += `/${this.jenisMutasi}`;
      }

      this.asetPropertiService.getById(id).subscribe((asetProperti: AsetProperti) => {
        asetProperti = new AsetProperti(asetProperti);
        
        this.oldFoto = asetProperti.detail ? `${this.upload}${asetProperti.detail.foto}` : null;

        if (['detail', 'legal', 'nilai', 'mutasi'].indexOf(fragment) === -1) {
          this.cancel();
        }
        
        if ( ! (
          (
            fragment === 'detail' && (
              ( ! this.isRevisi && asetProperti.detail && asetProperti.detail.is_editable_detail) || 
              ( this.isRevisi && asetProperti.detail_perubahan &&
                ( asetProperti.detail_perubahan.action_button.type === 'revisi' && asetProperti.detail_perubahan.action_button.button)
              )
            )
          ) ||
          (
            fragment === 'nilai' && (
              ( ! this.isRevisi && asetProperti.nilai && asetProperti.nilai.is_editable_nilai) || 
              ( this.isRevisi && asetProperti.nilai_perubahan && ( 
                  asetProperti.nilai_perubahan.action_button.type === 'revisi' && asetProperti.nilai_perubahan.action_button.button
                )
              ) || 
              ( ! asetProperti.nilai && ! asetProperti.nilai_perubahan )
            )
          ) ||
          (
            fragment === 'mutasi' && (
              ( ! this.isRevisi && asetProperti.perpindahan && asetProperti.perpindahan.is_editable_perpindahan) || 
              ( this.isRevisi && asetProperti.perpindahan_perubahan && (
                  asetProperti.perpindahan_perubahan.action_button.type === 'revisi' && asetProperti.perpindahan_perubahan.action_button.button
                )
              )
            )
          )
        )) {
          alert('Anda tidak memiliki izin atau Masih ada pengajuan yang sedang diproses!');
          this._location.back();
        }
        this.asetProperti = asetProperti;
        this.detailAsetProperti = asetProperti.detail;
        this.perpindahanAsetProperti = asetProperti.perpindahan;
        this.nilai = asetProperti.nilai;

        if (!this.detailAsetProperti || !this.perpindahanAsetProperti) {
          this._initFormDataStatis();
        }

        const perpindahanX = asetProperti.perpindahan || asetProperti.perpindahan_perubahan;
        this.asetTanah = new AsetTanah(perpindahanX.tanah);
        
        switch (fragment) {
          case 'mutasi': this._initFormMutasi();
            break;
          case 'nilai': this._initFormNilai();
            break;
          case 'detail':
          default: this._initFormDetail();
        }

        this.namaAsetProperti = this.detailAsetProperti ? this.detailAsetProperti.nama_properti : this.asetProperti.detail_perubahan.nama_properti;
        this.namaCompanyCode = perpindahanX.company_code.nama;
        
        if (this.detailAsetProperti && this.detailAsetProperti.polygon && this.detailAsetProperti.polygon[0]) {
          this.polyOld = this.detailAsetProperti.polygon[0].area;
          const center = this._getCenter(this.detailAsetProperti.polygon[0].area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.polyOld.coordinates[0]);
        } 
        if (this.asetProperti.detail_perubahan && this.asetProperti.detail_perubahan.polygon && this.asetProperti.detail_perubahan.polygon[0]) {
          const center = this._getCenter(this.asetProperti.detail_perubahan.polygon[0].area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.asetProperti.detail_perubahan.polygon[0].coordinates[0]);
        }
        if (this.tabRevisi.tabs[1]) {
          this.tabRevisi.tabs[1].active = true;
        }
        this.documentReady = true;
      });
    });
  }

  log(data: any) {
    // console.log(data);
    return false;
  }

  chooseTab(tab: string) {
    this.tab = tab;
    return false;
  }

  private _initFormDataStatis() {
    this.sumberPerolehanService.getAll().subscribe(sumberPerolehans => {
      this.sumberPerolehans = sumberPerolehans;
    })
    this.asetPropertiPerubahan = Object.assign({}, this.asetProperti);
    this.formAsetProperti = this._fb.group({
      id_sumber_perolehan: ['', Validators.required],
      tahun_perolehan: ['', [Validators.required, Validators.min(1800), Validators.max(this.max)]],
      nilai_perolehan: ['', Validators.required],
    });
  }

  private _initFormDetail() {
    this.jenisPropertiService.getAll().subscribe(jenisAsetPropertis => {
      this.jenisAsetPropertis = jenisAsetPropertis;
      jenisAsetPropertis.forEach(jenisAset => {
        if(this.detailAsetPropertiPerubahan.id_jenis_aset == jenisAset.id_jenis_aset) {
          this.jenisAsetProperti = jenisAset;
          this.subJenisAsetPropertisX = jenisAset.sub_jenis_aset;
        }
      });
    });
    if (this.isRevisi) {
      this.detailAsetPropertiPerubahan = this.asetProperti.detail_perubahan || new AsetPropertiDetail();
    } else {
      this.detailAsetPropertiPerubahan = Object.assign({}, this.detailAsetProperti);

      this.detailAsetPropertiPerubahan.id_properti_detail = null;
      this.detailAsetPropertiPerubahan.created_on = null;
      this.detailAsetPropertiPerubahan.changed_on = null;
      this.detailAsetPropertiPerubahan.created_by = null;
      this.detailAsetPropertiPerubahan.changed_by = null;
      this.detailAsetPropertiPerubahan.is_use = null;
    }
    this.foto = this.detailAsetPropertiPerubahan.foto ? `${this.upload}${this.detailAsetPropertiPerubahan.foto}` : null;

    this.formDetail = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]],
      alamat: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255)]],
      polygon: this._fb.group({
        points: this._fb.array([], Validators.minLength(4))
      }),
      id_jenis_aset: ['', [Validators.required]],
      id_sub_jenis_aset: ['', []],
      luas_properti: ['', [Validators.required]],
      foto: ['', ''],
      no_imb: ['', [Validators.required]],
      no_sap: ['', []],
      tanggal_terbit_imb: ['', [Validators.required]],
    });

    const defaultFoto = this.detailAsetPropertiPerubahan ? this.detailAsetPropertiPerubahan.foto : null;
    this.formDetail.get('foto').setValue(defaultFoto);

    const p = this.detailAsetPropertiPerubahan.polygon && this.detailAsetPropertiPerubahan.polygon[0] ? this.detailAsetPropertiPerubahan.polygon[0].area.coordinates[0] : [];
    for (let i = 0; i < (p.length - 1); i++) {
      this.addPoint(true, new LatLng(p[i][0], p[i][1]));
    }
  }

  private _initPoint(withModel = true, latLng: LatLng) {
    if (withModel) {
      const newData = latLng;
      this.poly[0].splice(this.poly[0].length, 0, newData);
    }
    return this._fb.group({
      lat: ['', [Validators.required]],
      lng: ['', [Validators.required]]
    });
  }

  public addPoint(withModel = true, latLng?: LatLng) {
    const polygon = <FormGroup> this.formDetail.get('polygon');
    const points = <FormArray> polygon.get('points');
    latLng = latLng || new LatLng(this.lat, this.lng);
    points.push(this._initPoint(withModel, latLng));
    return false;
  }

  public removePoint(index: number) {
    const polygon = <FormGroup> this.formDetail.get('polygon');
    const points = <FormArray> polygon.get('points');
    points.removeAt(index);
    this.poly[0].splice(index, 1);
    return false;
  }

  private _initFormMutasi() {
    if (this.isRevisi) {
      this.perpindahanAsetPropertiPerubahan = this.asetProperti.perpindahan_perubahan;
    } else {
      this.perpindahanAsetPropertiPerubahan = Object.assign({}, this.perpindahanAsetProperti);

      this.perpindahanAsetPropertiPerubahan.id_perpindahan_properti = null;
      this.perpindahanAsetPropertiPerubahan.created_on = null;
      this.perpindahanAsetPropertiPerubahan.changed_on = null;
      this.perpindahanAsetPropertiPerubahan.created_by = null;
      this.perpindahanAsetPropertiPerubahan.changed_by = null;
      this.perpindahanAsetPropertiPerubahan.is_use = null;
      this.perpindahanAsetPropertiPerubahan.start_date_perpindahan = null;
      this.perpindahanAsetPropertiPerubahan.end_date_perpindahan = null;
    }
    this.idCompanyCode = this.perpindahanAsetPropertiPerubahan.id_company_code;
    this.idBusinessArea = this.perpindahanAsetPropertiPerubahan.id_business_area;
    const tanah = new AsetTanah(this.perpindahanAsetPropertiPerubahan.tanah);
    this.idProvinsi = tanah.perpindahan.lokasi.kabupaten.id_provinsi;
    this.idKabupaten = tanah.perpindahan.lokasi.id_kabupaten;
    this.idLokasi = tanah.perpindahan.id_lokasi;
    this.tanah = tanah;
    this._getAsetTanah();
    this.formMutasi = this._fb.group({
      idProvinsi: ['', [Validators.required]],
      idKabupaten: ['', [Validators.required]],
      idCompanyCode: ['', [Validators.required]],
      idBusinessArea: ['', [Validators.required]],
      idLokasi: ['', [Validators.required]],
      tanah: ['', [Validators.required]],
      no_bast: ['', [Validators.maxLength(50)]],
      keterangan: ['', [Validators.maxLength(255)]]
    });
    if (this.jenisMutasi === 'eksternal') {
      const all = true;
      this.getCompanyCode(all);
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').disable();
      this.formMutasi.get('idProvinsi').disable();
      this.formMutasi.get('idKabupaten').disable();
      this.formMutasi.get('idLokasi').disable();
      this.formMutasi.get('tanah').disable();
    } else if(this.jenisMutasi === 'penerimaan') {
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').setValue(this.idBusinessArea);
      this.formMutasi.get('idProvinsi').disable();
      this.formMutasi.get('idKabupaten').disable();
      this.formMutasi.get('idLokasi').disable();
      this.formMutasi.get('tanah').disable();
      this.getBusinessArea();
    } else {
      this.getProvinsi();
      this.getKabupaten();
      this.getLokasi();
      this.getTanah();
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').setValue(this.idBusinessArea);
      this.getBusinessArea();
    }
  }

  private _getAsetTanah() {
    if (! this.idLokasi) {
      return false;
    }
    const params = new URLSearchParams();
    params.append('id_lokasi', this.idLokasi.toString());
    this.asetTanahService.getAll(params).subscribe(asetTanahs => {
      this.asetTanahs = asetTanahs;
    });
  }

  setStatusProgress(selesai) {
    this.dokumenPerubahan.status_progress = selesai;
    if (selesai) {
      this.formLegal.controls['formTahapDokumen'].disable();
    } else {
      this.formLegal.controls['formTahapDokumen'].enable();
    }
  }

  private _initFormNilai() {
    if (this.isRevisi) {
      this.nilaiPerubahan = this.asetProperti.nilai_perubahan || new NilaiAsetProperti();
    } else {
      this.nilaiPerubahan = this.nilai ? Object.assign({}, this.nilai) : new NilaiAsetProperti();

      this.nilaiPerubahan.id_nilai_properti = null;
      this.nilaiPerubahan.created_on = null;
      this.nilaiPerubahan.changed_on = null;
      this.nilaiPerubahan.created_by = null;
      this.nilaiPerubahan.changed_by = null;
      this.nilaiPerubahan.is_use = null;
    }

    this.min = this.nilai ? this.nilai.tahun_penilaian : this.asetProperti.tahun_perolehan;
    this.max = new Date().getFullYear();
    this.formNilai = this._fb.group({
      tahun_penilaian: ['', [Validators.required, Validators.min(this.min), Validators.max(this.max)]],
      nilai_properti: ['', [Validators.required]],
      biaya_penyusutan: ['', [Validators.required]],
      keterangan: ['', [Validators.required, Validators.maxLength(254)]],
    });
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  cancel() {
    this._backToDetail();
    return false;
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      if(file.size > environment.max_file_upload_size) {
        alert(`Ukuran foto maksimal ${environment.max_file_upload_size/1000000} MB`);
        return false;
      }
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.formDetail.get('foto').setValue({
          filename: file.name,
          filetype: file.type,
          // @ts-ignore
          value: reader.result.split(',')[1]
        });
        // @ts-ignore
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    const defaultFoto = this.detailAsetPropertiPerubahan ? this.detailAsetPropertiPerubahan.foto : null;
    this.preview = null;
    this.formDetail.get('foto').setValue(defaultFoto);
    return false;
  }

  saveDataStatis() {
    this.spinner.show();
    this.asetPropertiPerubahan.path = '/aset-tanah/add';
    this.isSubmitted = true;
    this.asetPropertiService.saveDataStatis(this.asetPropertiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  saveDetail() {
    this.spinner.show();
    this.detailAsetPropertiPerubahan.path = this.path;
    this.isSubmitted = true;
    this.detailAsetPropertiPerubahan.polygon = this.poly;
    if (this.preview) {
      this.detailAsetPropertiPerubahan.foto = this.formDetail.get('foto').value;
    }
    if (this.isRevisi) {
      this.revisiDetail();
    } else {
      this.addDetail();
    }
    return false;
  }

  private addDetail() {
    this.asetPropertiService.saveDetail(this.detailAsetPropertiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiDetail() {
    this.asetPropertiService.revisiDetail(this.detailAsetPropertiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  saveNilaiAset() {
    this.spinner.show();
    this.nilaiPerubahan.path = this.path;
    this.isSubmitted = true;
    this.nilaiPerubahan.id_properti = this.asetProperti.id_properti;
    if(this.isRevisi){
      this.revisiNilaiAset();
    } else {
      this.addNilaiAset();
    }
    return false;
  }

  private addNilaiAset() {
    this.asetPropertiService.saveNilaiAset(this.nilaiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiNilaiAset() {
    this.asetPropertiService.revisiNilaiAset(this.nilaiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  saveMutasi() {
    this.spinner.show();
    if (this.jenisMutasi === 'eksternal' ) {
      this.perpindahanAsetPropertiPerubahan.id_business_area = null;
    } else {
      this.perpindahanAsetPropertiPerubahan.id_business_area = this.idBusinessArea;
    }
    this.perpindahanAsetPropertiPerubahan.path = this.path;
    this.perpindahanAsetPropertiPerubahan.id_tanah = this.tanah.id_tanah;
    this.perpindahanAsetPropertiPerubahan.id_company_code = this.idCompanyCode;
    this.isSubmitted = true;
    if(this.isRevisi) {
      this.revisiMutasi();
    } else{
      this.addMutasi();
    }
    return false;
  }

  private addMutasi() {
    this.asetPropertiService.saveMutasi(this.perpindahanAsetPropertiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiMutasi() {
    this.asetPropertiService.revisiMutasi(this.perpindahanAsetPropertiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._backToDetail();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private _backToDetail() {
    if (confirm('Kembali ke halaman detail aset?')) {
      this._location.back();
    }
    return false;
  }

  dblClick(e) {
    if(this.fragment === 'detail' && e.latlng && this.poly[0].length) {
      this.addPoint(true, e.latlng);
    }
  }

  private getCompanyCode(all?: boolean) {
    const params = new URLSearchParams();
    all = all || false;
    this.itemsCC = [];
    this.companyCodeService.getAll(params, all).subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
          // this.getBusinessArea();
        }
      });
    });
  }

  private getBusinessArea() {
    this.itemsBA = [];
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
      // this.companyCode.business_area = business_areas;
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id === this.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  private getProvinsi() {
    this.itemsP = [];
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
          // this.getKabupaten();
        }
      });
    });
  }

  private getKabupaten() {
    this.itemsK = [];
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
          // this.getLokasi();
        }
      });
    });
  }

  private getLokasi() {
    this.itemsL = [];
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id === this.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
          // this.getTanah();
        }
      });
    });
  }

  private getTanah() {
    this.itemsT = [];
    this.asetTanahService.getByIdLokasi(this.idLokasi).subscribe(tanahs => {
        this.itemsT = tanahs.map(tanah => {
        tanah = new AsetTanah(tanah);
        return {id: tanah, text: this.uppercasePipe.transform(tanah.detail.nama_tanah)};
      });
      this.itemsT.forEach((item,index) => {
        if (item.id.id_tanah == this.tanah.id_tanah) {
          this.selectTanah.active = [this.itemsT[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formMutasi.get('idCompanyCode').setValue(e.id);
    this.formMutasi.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      // this.formAsetProperti.get('idCompanyCode').disable();
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formMutasi.get('idCompanyCode').setValue(e.id);
    this.formMutasi.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      // this.formMutasi.get('idCompanyCode').disable();
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.formMutasi.get('idBusinessArea').setValue(e.id);
    this.formMutasi.get('idBusinessArea').updateValueAndValidity();
  }

  selectedBA(e: SelectItem) {
    this.formMutasi.get('idBusinessArea').setValue(e.id);
    this.formMutasi.get('idBusinessArea').updateValueAndValidity();
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formMutasi.get('idProvinsi').setValue(e.id);
    this.formMutasi.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formMutasi.get('idProvinsi').setValue(e.id);
    this.formMutasi.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formMutasi.get('idKabupaten').setValue(e.id);
    this.formMutasi.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formMutasi.get('idKabupaten').setValue(e.id);
    this.formMutasi.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    const old = this.idLokasi;
    this.formMutasi.get('idLokasi').setValue(e.id);
    this.formMutasi.get('idLokasi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getTanah();
    }
  }

  selectedL(e: SelectItem) {
    const old = this.idLokasi;
    this.formMutasi.get('idLokasi').setValue(e.id);
    this.formMutasi.get('idLokasi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getTanah();
    }
  }

  refreshValueT(e) {
    this.formMutasi.get('tanah').setValue(e.id);
    this.formMutasi.get('tanah').updateValueAndValidity();
  }

  selectedT(e) {
    this.formMutasi.get('tanah').setValue(e.id);
    this.formMutasi.get('tanah').updateValueAndValidity();
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}

