import { Observable } from 'rxjs/Observable';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';

import { DataSetting, AppTableConfig, PaginationData } from 'app/components/pagination/page/page.component';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import {
  AsetPropertiService, PenggunaService
} from 'app/services/data-services';

import { environment } from 'environments/environment';
import { AsetProperti, Pengguna } from 'app/models';
import { TextToSlugPipe } from '../../../services/text-to-slug.service';
import { DataMapService } from '../../../services/data-services/data-map.service';
import { ModalFilterContentComponent } from '../../modal-filter-content/modal-filter-content.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-aset-properti',
  templateUrl: './aset-properti.component.html',
  styleUrls: ['./aset-properti.component.scss']
})
export class AsetPropertiComponent implements OnInit {
  // page setting
  tab = 'map';
  root = 'aset-properti/';
  showFilter = {
    wilayah: 'provinsi',
    unit: 'company-code'
  };
  dataSubscription: Subscription;
  mapSubscription: Subscription;

  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;

  fotoDir = `${environment.upload_url}/aset-properti/`;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  public lat = -2.964984369333955;
  public lng = 114.08203125000001;
  zoom = 5;
  @ViewChild('map') map;
  markersProperti = [];
  lastRequestParams = {
    zoom: null,
    bounds: null
  }

  public modalRef: BsModalRef;

  private editMode = false;
  private viewMode = false;

  public tableConfig = new AppTableConfig();
  public pengguna = new Pengguna();

  constructor(
    private asetPropertiService: AsetPropertiService,
    private route: ActivatedRoute,
    private router: Router,
    private penggunaService: PenggunaService,
    private modalService: BsModalService,
    private slugPipe: TextToSlugPipe,
    private mapService: DataMapService
  ) {}

  ngOnInit() {
    this.penggunaService.getRoleForModule('properti').subscribe(pengguna => this.pengguna = pengguna);
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      if (this.tab === 'map') {
        if(this.dataSubscription) {
          this.dataSubscription.unsubscribe();
        }
        this._map();
      } else {
        if(this.mapSubscription) {
          this.mapSubscription.unsubscribe();
        }
        this._data();
      }
    });
  }

  ngOnDestroy() {
    if(this.mapSubscription){
      this.mapSubscription.unsubscribe();
    }
  }

  private _map() {
    this._getMapData();
  }

  _getMapData() {
    if(this.tab !== 'map') {
      return false;
    }
    
    if(this.mapSubscription){
      this.mapSubscription.unsubscribe();
    }
    if (this.lastRequestParams.zoom !== this.zoom || this.lastRequestParams.bounds !== this.map.getBounds()) {
      if(this.lastRequestParams.zoom !== this.zoom) {
        this.map.closePopup();  
      }
      const params = {};
      if (this.dataSetting.idKabupaten && this.dataSetting.idKabupaten != 0) {
        params['id_kabupaten'] = this.dataSetting.idKabupaten;
      }
      if (this.dataSetting.idProvinsi && this.dataSetting.idProvinsi != 0) {
        params['id_provinsi'] = this.dataSetting.idProvinsi;
      }
      if (this.dataSetting.idBusinessArea && this.dataSetting.idBusinessArea != 0){
        params['id_business_area'] = this.dataSetting.idBusinessArea;
      }
      if (this.dataSetting.idCompanyCode && this.dataSetting.idCompanyCode != 0){
        params['id_company_code'] = this.dataSetting.idCompanyCode;
      }
      if (this.dataSetting.idLokasiAset && this.dataSetting.idLokasiAset != 0){
        params['id_lokasi'] = this.dataSetting.idLokasiAset;
      }
      if (this.dataSetting.nama) {
        params['nama'] = this.dataSetting.nama;
      }
      if (this.dataSetting.no_imb) {
        params['no_imb'] = this.dataSetting.no_imb;
      }
      if (this.dataSetting.start_range_luas) {
        params['start_range_luas'] = this.dataSetting.start_range_luas;
      }
      if (this.dataSetting.end_range_luas) {
        params['end_range_luas'] = this.dataSetting.end_range_luas;
      }
      this.lastRequestParams = {zoom: this.zoom, bounds: this.map.getBounds()};
      this.mapSubscription = this.mapService.get(this.zoom, this.map.getBounds(), ['properti'], params)
        .subscribe(markers => {
          this.markersProperti = markers.properti || [];
        });
    }
  }

  private _data() {
    if(this.dataSubscription){
      this.dataSubscription.unsubscribe();
    }

    this.dataSubscription = this.route.queryParamMap.switchMap((params) => {
      this.pageData = [];
      this.isLoading = true;
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      if (params.get('idKabupaten')) {
        parameters.append('id_kabupaten', params.get('idKabupaten'));
        this.dataSetting.idKabupaten = parseInt(params.get('idKabupaten'));
      }
      if (params.get('idProvinsi')) {
        parameters.append('id_provinsi', params.get('idProvinsi'));
        this.dataSetting.idProvinsi = parseInt(params.get('idProvinsi'));
      }
      if (params.get('idBusinessArea')) {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.dataSetting.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      if (params.get('idCompanyCode')) {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.dataSetting.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('idLokasiAset')) {
        parameters.append('id_lokasi', params.get('idLokasiAset'));
        this.dataSetting.idLokasiAset = parseInt(params.get('idLokasiAset'));
      }
      if (params.get('nama') !== '') {
        parameters.append('nama', params.get('nama'));
        this.dataSetting.nama = params.get('nama');
      }
      if (params.get('no_imb') && params.get('no_imb') !== '') {
        parameters.append('no_imb', params.get('no_imb'));
        this.dataSetting.no_imb = params.get('no_imb');
      }
      if (params.get('start_range_luas') && params.get('start_range_luas') !== '') {
        parameters.append('start_range_luas', params.get('start_range_luas'));
        this.dataSetting.start_range_luas = parseFloat(params.get('start_range_luas'));
      }
      if (params.get('end_range_luas') && params.get('end_range_luas') !== '') {
        parameters.append('end_range_luas', params.get('end_range_luas'));
        this.dataSetting.end_range_luas = parseFloat(params.get('end_range_luas'));
      }

      let data: Observable<PaginationData>;
      switch(this.tab) {
        case 'data-perubahan': data = this.asetPropertiService.getPaginateNonValid(parameters);
          break;
        case 'data-penerimaan': data = this.asetPropertiService.getPaginatePenerimaan(parameters);
          break;
        default: data = this.asetPropertiService.getPaginateValid(parameters);
      }
                                  
      return data;
    }).subscribe((res) => {
      this.dataSetting.dataPerPage = <number> res.per_page;
      this.pageData = res.data.map(d => {
        return new AsetProperti(d);
      });
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      if (this.modalRef) {
        this.modalRef.hide();
      }

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  openModal() {
    const initialState = {
      dataSetting: Object.assign({}, this.dataSetting),
      isAsetProperti: true
    }
    this.modalRef = this.modalService.show(ModalFilterContentComponent, { initialState, class: 'modal-lg', ignoreBackdropClick: true });
    const content = <ModalFilterContentComponent> this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
      if (content.filter) {
        this.dataSetting = content.dataSetting;
        if(this.tab !== 'map') {
          this.search();
        } else {
          this._getMapData();
        }
      }
      subscribeModal.unsubscribe();
    });
  }

  show(asetProperti: AsetProperti) {
    const detail = asetProperti.detail || asetProperti.detail_perubahan;
    const nama = this.slugPipe.transform(detail.nama_properti);
    this.router.navigate([`${this.root}show/${asetProperti.id_properti}/${nama}`]);
    return false;
  }

  validasiPengajuan(asetProperti: AsetProperti) {
    const detail = asetProperti.detail_perubahan;
    const nama = this.slugPipe.transform(detail.nama_properti);
    this.router.navigate([`${this.root}validasi/${asetProperti.id_properti}/${nama}`]);
    return false;
  }

  showFromMap(id: number, nama: string) {
    nama = this.slugPipe.transform(nama);
    const url = `/${this.root}show/${id}/${nama}`;
    return url;
  }

  mutasiPenerimaan(asetProperti: AsetProperti) {
    const detail = asetProperti.detail || asetProperti.detail_perubahan;
    const nama = this.slugPipe.transform(detail.nama_properti);
    this.router.navigate([`${this.root}edit/${asetProperti.id_properti}/${nama}/mutasi/penerimaan`]);
    return false;
  }

}
