import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetPropertiComponent } from './aset-properti.component';

describe('AsetPropertiComponent', () => {
  let component: AsetPropertiComponent;
  let fixture: ComponentFixture<AsetPropertiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetPropertiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetPropertiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
