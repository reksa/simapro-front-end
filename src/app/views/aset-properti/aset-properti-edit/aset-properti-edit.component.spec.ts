import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetPropertiEditComponent } from './aset-properti-edit.component';

describe('AsetPropertiEditComponent', () => {
  let component: AsetPropertiEditComponent;
  let fixture: ComponentFixture<AsetPropertiEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetPropertiEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetPropertiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
