import { AppGeoPolygon } from './../../../models/app-geo.model';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Location, UpperCasePipe } from '@angular/common';

import { TabsetComponent } from 'ngx-bootstrap/tabs/tabset.component';

import {
  SumberPerolehanService, ProvinsiService,
  BusinessAreaService, KabupatenService, LokasiService, AsetPropertiService, JenisPropertiService, AsetTanahService, CompanyCodeService,
} from 'app/services/data-services';

import { environment } from 'environments/environment';

import {
  JenisPenghapusan, SumberPerolehan, AsetProperti, Provinsi, AsetPropertiDetail,
  PerpindahanAsetProperti, BusinessArea,
  Kabupaten, Lokasi, JenisAsetProperti, AsetTanah } from 'app/models';
import { LatLng, LatLngBounds, Map } from 'leaflet';
import { SelectComponent, SelectItem } from 'ng2-select';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-properti-edit',
  templateUrl: './aset-properti-edit.component.html',
  styleUrls: ['./aset-properti-edit.component.scss']
})
export class AsetPropertiEditComponent implements OnInit {
  // page setting
  action = '';
  title = '';
  ready = false;
  maxYear = new Date().getFullYear();
  oldFoto: string;
  uploadPath = environment.upload_url + '/aset-properti/';

  // form
  isSubmitted: boolean;
  formAsetProperti: FormGroup;
  jenisPenghapusans: JenisPenghapusan[] = [];
  sumberPerolehans: SumberPerolehan[] = [];
  provinsis: Provinsi[] = [];
  kabupatens: Kabupaten[] = [];
  businessAreas: BusinessArea[] = [];
  lokasis: Lokasi[] = [];
  jenisAsetPropertis: JenisAsetProperti[] = [];
  asetTanahs: AsetTanah[] = [];
  asetTanah: AsetTanah;

  jenisAsetProperti: JenisAsetProperti;
  idSubJenisAset: number;

  asetProperti: AsetProperti;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 15;

  poly: LatLng[][]  = [[]];
  
  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;
  tanah: AsetTanah;

  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];
  itemsT = [];

  preview = '';

  @ViewChild('tabset') tabset: TabsetComponent;
  @ViewChild('templatePoint') templatePoint: TemplateRef<any>;
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('selectTanah') selectTanah: SelectComponent;

  @ViewChild('map') map: Map;

  hasPoint = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private asetPropertiService: AsetPropertiService,
    private asetTanahService: AsetTanahService,
    private sumberPerolehanService: SumberPerolehanService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private lokasiService: LokasiService,
    private jenisPropertiService: JenisPropertiService,
    private _location: Location,
    private _fb: FormBuilder,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.sumberPerolehanService.getAll().subscribe(sumberPerolehans => {
      this.sumberPerolehans = sumberPerolehans;
    });
    
    this.formAsetProperti = this._fb.group({
      idProvinsi: ['', [Validators.required]],
      idKabupaten: ['', [Validators.required]],
      idCompanyCode: ['', [Validators.required]],
      idBusinessArea: ['', [Validators.required]],
      idLokasi: ['', [Validators.required]],
      id_sumber_perolehan: ['', [Validators.required]],
      // is_active: ['', [Validators.required]],
      polygon: this._fb.group({
        points: this._fb.array([])
      }, Validators.minLength(4)),
      foto: ['', []],
      tanah: ['', Validators.required],
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]],
      alamat: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255)]],
      id_jenis_aset: ['', Validators.required],
      id_sub_jenis_aset: ['', []],
      luas_properti: ['', Validators.required],
      nilai_perolehan: ['', Validators.required],
      tahun_perolehan: ['', [Validators.required, Validators.min(1800), Validators.max(this.maxYear)]],
      no_imb: ['', Validators.required],
      no_sap: ['', ],
      tanggal_terbit_imb: ['', Validators.required],
    });
    // get list Kabupaten by Selected Id Provinsi
    this.route.url.subscribe(url => {
      this.asetProperti = new AsetProperti();
      this.asetProperti.detail = new AsetPropertiDetail();
      this.action = url[0].path;
      if (this.action === 'revisi-pengajuan'){
        this.title = 'Revisi Aset Properti';
        this._revisi();
      } else {
        this.getProvinsi();
        this.getCompanyCode();
        this.getJenisAsetProperti();
        this.title = 'Tambah Aset Properti';
        this.asetProperti.perpindahan_aktif.push(new PerpindahanAsetProperti());
        this.ready = true;
      }
    });
  }

  private _revisi() {
    this.route.paramMap.subscribe(params => {
      const id =  parseInt(params.get('idAsetProperti'));
      this.asetPropertiService.getById(id).subscribe(asetProperti => {
        this.asetProperti = new AsetProperti(asetProperti);
        this.asetTanah = new AsetTanah(this.asetProperti.perpindahan_perubahan.tanah);

        this.asetProperti.detail = this.asetProperti.detail_perubahan;
        this.asetProperti.perpindahan = this.asetProperti.perpindahan_perubahan;
        this.idCompanyCode = this.asetProperti.perpindahan.id_company_code; 
        this.idBusinessArea = this.asetProperti.perpindahan.id_business_area; 
        this.idProvinsi = this.asetTanah.perpindahan.lokasi ? this.asetTanah.perpindahan.lokasi.kabupaten.id_provinsi : null; 
        this.idKabupaten = this.asetTanah.perpindahan.lokasi ? this.asetTanah.perpindahan.lokasi.id_kabupaten : null; 
        this.idLokasi = this.asetTanah.perpindahan.id_lokasi; 
        this.tanah = this.asetTanah;
        this.asetProperti.id_jenis_aset = this.asetProperti.detail.id_jenis_aset;
        this.asetProperti.id_sub_jenis_aset = this.asetProperti.detail.id_sub_jenis_aset;

        if(this.asetProperti.detail && this.asetProperti.detail.polygon && this.asetProperti.detail.polygon[0] && this.asetProperti.detail.polygon[0].area) { 
          const latlngs: number[] = this.asetProperti.detail.polygon[0].area.coordinates[0]; 
          for(let i = 0; i < (latlngs.length -1); i++) { 
            this.addPoint(true, new LatLng(latlngs[i][0], latlngs[i][1])); 
          } 
          this.hasPoint = true;
          const center = this._getCenter(latlngs);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.poly[0]);
        } 
        if(this.asetProperti.detail.foto) { 
          this.oldFoto = this.uploadPath + this.asetProperti.detail.foto; 
          this.formAsetProperti.get('foto').setValue(this.oldFoto); 
        } 
        this.getJenisAsetProperti();
        this.getProvinsi(); 
        this.getCompanyCode(); 
        this.getKabupaten(); 
        this.getBusinessArea(); 
        this.getLokasi(); 
        this.getTanah(); 
        this.ready = true; 
      });
    });
  }

  // private _getAsetProperti() {
  //   this.route.paramMap.subscribe(params => {
  //     const id = parseInt(params.get('idAsetProperti'), 10);
  //     this.asetPropertiService.getById(id).subscribe(asetProperti => {
  //       this.asetProperti = asetProperti;
  //     });
  //   });
  // }

  private _initPoint(withModel = true, latLng: LatLng) {
    if (withModel) {
      const newData = latLng;
      this.poly[0].splice(this.poly[0].length, 0, newData);
    }
    return this._fb.group({
      lat: ['', Validators.required],
      lng: ['', Validators.required]
    });
  }

  private _getKabupaten() {
    this.kabupatenService.getByIdProvinsi(this.idProvinsi)
      .subscribe(kabupatens => {
        this.kabupatens = kabupatens;
      });
  }

  private _getLokasi() {
    this.lokasiService.getByIdKabupaten(this.idKabupaten)
      .subscribe(lokasis => {
        this.lokasis = lokasis;
    });
  }

  public addPoint(withModel = true, latLng?: LatLng) {
    const polygon = <FormGroup> this.formAsetProperti.get('polygon');
    const points = <FormArray> polygon.get('points');
    latLng = latLng || new LatLng(this.lat, this.lng);
    points.push(this._initPoint(withModel, latLng));
    return false;
  }

  public removePoint(index: number) {
    const polygon = <FormGroup> this.formAsetProperti.get('polygon');
    const points = <FormArray> polygon.get('points');
    points.removeAt(index);
    this.poly[0].splice(index, 1);
    return false;
  }

  _selectJenisAsetProperti () {
    this.asetProperti.id_jenis_aset = this.jenisAsetProperti.id_jenis_aset;
    this.asetProperti.id_sub_jenis_aset = null;
  }

  private selectAsetTanah() {
    this.asetProperti.perpindahan.id_tanah = this.tanah.id_tanah;

    const polygon = <FormGroup> this.formAsetProperti.get('polygon');
    const points = <FormArray> polygon.get('points');
    const length = points.controls.length;
    for (let i = 0; i < length; i++) {
      points.removeAt(0);
    }

    const coordinates = (this.tanah.detail_aktif[0].polygon[0] as AppGeoPolygon).area.coordinates[0];
    const center = this._getCenter(coordinates);
    this.lat = center.lat;
    this.lng = center.lng;

    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng - 0.0005));
    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng - 0.0005));
    this.poly[0].push(new LatLng(this.lat - 0.0005, this.lng - 0.0005));
    return false;
  }

  cancel() {
    if (confirm('Batal menambah Aset Properti?')) {
      this._location.back();
    }
    return false;
  }

  log(obj) {
    // console.log(obj);
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      if(file.size > environment.max_file_upload_size) {
        alert(`Ukuran foto maksimal ${environment.max_file_upload_size/1000000} MB`);
        this.formAsetProperti.get('foto').setValue(this.asetProperti.detail.foto);
        return false;
      }
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.oldFoto = null;
        this.formAsetProperti.get('foto').setValue({
          filename: file.name,
          filetype: file.type,
          // @ts-ignore
          value: reader.result.split(',')[1]
        });
        this.asetProperti.detail.foto = this.formAsetProperti.get('foto').value;
        // @ts-ignore
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    this.asetProperti.detail.foto = null;
    this.preview = null;
    this.formAsetProperti.get('foto').setValue(null);
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.asetProperti.path = '/aset-properti/add';
    this.asetProperti.id_tanah = this.tanah.id_tanah;
    this.asetProperti.id_business_area = this.idBusinessArea;
    this.asetProperti.nama_properti = this.asetProperti.detail.nama_properti;
    this.asetProperti.alamat_properti = this.asetProperti.detail.alamat_properti;
    this.asetProperti.luas_properti = this.asetProperti.luas_properti;
    this.asetProperti.foto = this.asetProperti.foto;
    this.asetProperti.no_imb = this.asetProperti.detail.no_imb;
    this.asetProperti.no_sap = this.asetProperti.detail.no_sap;
    this.asetProperti.tanggal_terbit_imb = this.asetProperti.detail.tanggal_terbit_imb;
    this.isSubmitted = true;
    this.asetProperti.polygon = this.poly;
    this.asetPropertiService.save(this.asetProperti)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this.router.navigate(['/aset-properti']);
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  dblClick(e) {
    if(e.latlng && this.poly[0].length) {
      this.addPoint(true, e.latlng);
    }
  }

  private getCompanyCode() {
    this.itemsCC = [];
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
        }
      });
    });
  }

  private getJenisAsetProperti() {
    this.jenisPropertiService.getAll().subscribe(jenisPropertis => {
      this.jenisAsetPropertis = jenisPropertis;
      jenisPropertis.forEach(jenisAset => {
        if(this.asetProperti.id_jenis_aset == jenisAset.id_jenis_aset) {
          this.jenisAsetProperti = jenisAset;
        }
      });
    });
  }

  private getBusinessArea() {
    this.itemsBA = [];
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id === this.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  private getProvinsi() {
    this.itemsP = [];
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
        }
      });
    });
  }

  private getKabupaten() {
    this.itemsK = [];
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
          // this.getLokasi();
        }
      });
    });
  }

  private getLokasi() {
    this.itemsL = [];
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id === this.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
        }
      });
    });
  }

  private getTanah() {
    this.itemsT = [];
    this.asetTanahService.getByIdLokasi(this.idLokasi).subscribe(tanahs => {
      this.itemsT = tanahs.map(tanah => {
        tanah = new AsetTanah(tanah);
        return {id: tanah, text: tanah.perpindahan.kode_tanah + ' - ' + this.uppercasePipe.transform(tanah.detail.nama_tanah)};
      });
      this.itemsT.forEach((item,index) => {
        if (this.tanah && item.id.id_tanah == this.tanah.id_tanah) {
          this.selectTanah.active = [this.itemsT[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAsetProperti.get('idCompanyCode').setValue(e.id);
    this.formAsetProperti.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAsetProperti.get('idCompanyCode').setValue(e.id);
    this.formAsetProperti.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.formAsetProperti.get('idBusinessArea').setValue(e.id);
    this.formAsetProperti.get('idBusinessArea').updateValueAndValidity();
  }

  selectedBA(e: SelectItem) {
    this.formAsetProperti.get('idBusinessArea').setValue(e.id);
    this.formAsetProperti.get('idBusinessArea').updateValueAndValidity();
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formAsetProperti.get('idProvinsi').setValue(e.id);
    this.formAsetProperti.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formAsetProperti.get('idProvinsi').setValue(e.id);
    this.formAsetProperti.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formAsetProperti.get('idKabupaten').setValue(e.id);
    this.formAsetProperti.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formAsetProperti.get('idKabupaten').setValue(e.id);
    this.formAsetProperti.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    const old = this.idLokasi;
    this.formAsetProperti.get('idLokasi').setValue(e.id);
    this.formAsetProperti.get('idLokasi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getTanah();
    }
  }

  selectedL(e: SelectItem) {
    const old = this.idLokasi;
    this.formAsetProperti.get('idLokasi').setValue(e.id);
    this.formAsetProperti.get('idLokasi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getTanah();
    }
  }

  refreshValueT(e) {
    const old = this.tanah;
    this.formAsetProperti.get('tanah').setValue(e.id);
    this.formAsetProperti.get('tanah').updateValueAndValidity();
    if (old != e.id) {
      this.selectAsetTanah();
    }
  }

  selectedT(e) {
    const old = this.tanah;
    this.formAsetProperti.get('tanah').setValue(e.id);
    this.formAsetProperti.get('tanah').updateValueAndValidity();
    if (old != e.id) {
      this.selectAsetTanah();
    }
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}
