import { AsetPropertiEditComponent } from './aset-properti-edit/aset-properti-edit.component';
import { AsetPropertiDetailComponent } from './aset-properti-detail/aset-properti-detail.component';
import { AsetPropertiComponent } from './aset-properti/aset-properti.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { AsetPropertiRevisiComponent } from './aset-properti-revisi/aset-properti-revisi.component';
import { ValidasiAsetPropertiComponent } from './validasi-aset-properti/validasi-aset-properti.component';

const routes: Routes = [
  { path: '', component: AsetPropertiComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'add', component: AsetPropertiEditComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'revisi-pengajuan/:idAsetProperti/:nama', component: AsetPropertiEditComponent, canActivate: []  },
  { path: 'show/:idAsetProperti/:nama', component: AsetPropertiDetailComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'edit/:idAsetProperti/:nama/:fragment', component: AsetPropertiRevisiComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'edit/:idAsetProperti/:nama/:fragment/:jenisMutasi', component: AsetPropertiRevisiComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'revisi/:idAsetProperti/:nama/:fragment', component: AsetPropertiRevisiComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'revisi/:idAsetProperti/:nama/:fragment/:jenisMutasi', component: AsetPropertiRevisiComponent, canActivate: [AuthGuard, PermissionGuard]  },
  { path: 'validasi/:idAsetProperti/:nama', component: ValidasiAsetPropertiComponent, canActivate: [AuthGuard, PermissionGuard]  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsetPropertiRoutingModule { }
