import { ViewChild } from '@angular/core';
import { DatatablesContent } from 'app/components/app-datatable/datatables-content.model';
import { Observable, Subscription } from 'rxjs/Rx';
import { AsetPropertiService, PenggunaService } from 'app/services/data-services';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  PerpindahanAsetProperti, AsetPropertiDetail, AsetProperti, NilaiAsetProperti, PendayagunaanAset, PotensiAset, Pengguna, AsetTanah
} from 'app/models';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { TemplateRef } from '@angular/core/src/linker/template_ref';
import { DecimalPipe, DatePipe, CurrencyPipe, Location, UpperCasePipe } from '@angular/common';
import { TextToSlugPipe } from '../../../services/text-to-slug.service';
import { Disposisi } from '../../../models/disposisi.model';
import { TabsetComponent } from 'ngx-bootstrap';
import { Map, LatLng, LatLngBounds } from 'leaflet';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-properti-detail',
  templateUrl: './aset-properti-detail.component.html',
  styleUrls: ['./aset-properti-detail.component.scss']
})
export class AsetPropertiDetailComponent implements OnInit {
  showMap = false;
  tab = 'detail';
  uploadUrl = environment.upload_url;
  upload = this.uploadUrl + '/aset-properti/';
  foto;
  fotoPerubahan;
  asetProperti = new AsetProperti();
  asetTanah: AsetTanah;
  detailAsetProperti: AsetPropertiDetail;
  detailAsetPropertiPerubahan: AsetPropertiDetail;
  perpindahanAsetProperti: PerpindahanAsetProperti;
  perpindahanAsetPropertiPerubahan: PerpindahanAsetProperti;
  nilai: NilaiAsetProperti;
  nilaiPerubahan: NilaiAsetProperti;
  potensi: PotensiAset;
  disposisiDetail: Disposisi;
  disposisiNilai: Disposisi;
  disposisiMutasi: Disposisi;
  history = new DatatablesContent();
  historyPerpindahan = new DatatablesContent();
  historyNilai = new DatatablesContent();
  historyPotensi = new DatatablesContent();
  historyPendayagunaan = new DatatablesContent();

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.asetProperti;
  markerTanah = environment.map.marker.asetTanah;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 18;
  memo = '';
  poly: any;
  polyNonValid: any;

  namaAsetProperti = '';
  namaCompanyCode = '';
  root = 'aset-properti/'
  modalRef: BsModalRef;

  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  pengguna = new Pengguna();

  subsRole: Subscription;

  historisDisposisi: any[] = [];

  @ViewChild('map') map: Map;
  @ViewChild('tabset') tabset: TabsetComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private asetPropertiService: AsetPropertiService,
    private modalService: BsModalService,
    private numberPipe: DecimalPipe,
    private datePipe: DatePipe,
    private penggunaService: PenggunaService,
    private slugPipe: TextToSlugPipe,
    private uppercasePipe: UpperCasePipe,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this._init();
  }

  private _init(){
    this.spinner.show();
    if(this.subsRole) {
      this.subsRole.unsubscribe();
    }
    this.route.fragment.subscribe(fragment => {
      this.chooseTab(fragment);
    });
    this.subsRole = this.penggunaService.getRoleForModule('properti').subscribe(pengguna => {
      this.pengguna = pengguna;
      this.route.paramMap.subscribe(params => {
        const id = parseInt(params.get('idAsetProperti'), 10);
        const canViewChange = this.pengguna.is_user_input || this.pengguna.is_validator_pusat || this.pengguna.is_validator_unit;
        this.asetPropertiService.getById(id).subscribe((asetProperti: AsetProperti) => {
          this.asetProperti = new AsetProperti(asetProperti);
          
          const perpindahanX = this.asetProperti.perpindahan || this.asetProperti.perpindahan_perubahan; 
          this.asetTanah = new AsetTanah(perpindahanX.tanah);

          this.detailAsetProperti = this.asetProperti.detail;
          this.foto = this.detailAsetProperti && this.detailAsetProperti.foto ? `${this.upload}${this.detailAsetProperti.foto}` : 'assets/img/no-image.jpg';
          this.perpindahanAsetProperti = this.asetProperti.perpindahan;
          this.nilai = this.asetProperti.nilai;
          this.potensi = this.asetProperti.potensi;

          if (canViewChange) {
            this.detailAsetPropertiPerubahan = this.asetProperti.detail_perubahan;
            this.perpindahanAsetPropertiPerubahan = this.asetProperti.perpindahan_perubahan;
            this.fotoPerubahan = this.detailAsetPropertiPerubahan && this.detailAsetPropertiPerubahan.foto ? `${this.upload}${this.detailAsetPropertiPerubahan.foto}` : 'assets/img/no-image.jpg';
            this.nilaiPerubahan = this.asetProperti.nilai_perubahan;
          }

          // map list to datatables
          this._mapDetailToDatatable(this.asetProperti.histori_detail || []);
          this._mapPerpindahanToDatatable(this.asetProperti.histori_perpindahan || []);
          this._mapNilaiToDatatable(this.asetProperti.histori_nilai || []);
          this._mapPotensiToDatatable(this.asetProperti.histori_potensi || []);
          this._mapPendayagunaanToDatatable(this.asetProperti.pendayagunaan_aktif || []);

          this.namaAsetProperti = this.detailAsetProperti ? this.detailAsetProperti.nama_properti : this.detailAsetPropertiPerubahan.nama_properti;
          this.namaCompanyCode = perpindahanX.company_code.nama;

          if (this.detailAsetProperti && this.detailAsetProperti.polygon && this.detailAsetProperti.polygon[0]) {
            this.poly = this.detailAsetProperti.polygon[0];
            const center = this._getCenter(this.poly.area.coordinates[0]);
            this.map.panTo(center);
            this.fitMap(this.poly.area.coordinates[0])
          }
          if (this.detailAsetPropertiPerubahan && this.detailAsetPropertiPerubahan.polygon && this.detailAsetPropertiPerubahan.polygon[0]) {
            this.polyNonValid = this.detailAsetPropertiPerubahan.polygon[0];
            const center = this._getCenter(this.polyNonValid.area.coordinates[0]);
            this.map.panTo(center);
            this.fitMap(this.polyNonValid.area.coordinates[0]);
          }
          this.spinner.hide();
        })
      });
    });
  }

  ngAfterContentInit() {
    this.showMap = true;
  }

  log(data: any) {
    // console.log(data);
  }

  chooseTab(tab: string) {
    const tabs = ['detail', 'nilai', 'mutasi'];
    this.tab = tab;
    const index = tabs.indexOf(tab);
    if(index > -1) {
      this.tabset.tabs[index].active = true;
    }
    return false;
  }

  edit(fragment?: string, isRevisi = false) {
    const nama = this.slugPipe.transform(this.namaAsetProperti);
    fragment = fragment || 'detail';
    const tipe = isRevisi ? 'revisi' : 'edit';
    if (!this.asetProperti.is_use) {
      this.router
        .navigate([
          `${this.root}revisi-pengajuan/${this.asetProperti.id_properti}/${nama}`]);
    } else {
      this.router
        .navigate([
          `${this.root}${tipe}/${this.asetProperti.id_properti}/${nama}/${fragment}`]);
    }
    return false;
  }

  mutasiEksternal(isRevisi = false) {
    const nama = this.slugPipe.transform(this.namaAsetProperti);
    const tipe = isRevisi ? 'revisi' : 'edit';
    this.router
      .navigate([
        `${this.root}${tipe}/${this.asetProperti.id_properti}/${nama}/mutasi/eksternal`]);
    return false;
  }

  revisi(fragment: string) {
    this.edit(fragment, true);
    return false;
  }

  revisiMutasi() {
    const perpindahan = this.asetProperti.perpindahan_perubahan;
    const isEksternal = perpindahan.business_area ? false : true;
    if (isEksternal) {
      this.mutasiEksternal(true);
    } else  {
      this.edit('mutasi', true);
    }
  }

  addPotensi() {
    this.router.navigate([`potensi-aset/add`],
    {queryParams: {id: this.asetProperti.id_properti, jenis: 'aset-properti'}});
    return false;
  }

  addPendayagunaan() {
    this.router.navigate([`pendayagunaan-aset/add`],
    {queryParams: {id: this.asetProperti.id_properti, jenis: 'aset-properti'}});
    return false;
  }

  delete() {
    this.asetProperti.detail.is_active = false;
    this.asetProperti.detail.path = `/aset-properti/edit/${this.asetProperti.id_properti}/delete`;
    this.asetPropertiService.saveDetail(this.asetProperti.detail)
    .subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      }
    });
  }

  tolakUsulanDetail() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
      .validasiDetail(this.detailAsetPropertiPerubahan.id_properti_detail, this.memo, false)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetPropertiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  terimaUsulanDetail() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
      .validasiDetail(this.detailAsetPropertiPerubahan.id_properti_detail, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetPropertiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  terimaSemua() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    const terimaDetail = this.asetPropertiService.validasiDetail(
      this.detailAsetPropertiPerubahan.id_properti_detail, this.memo, true);
    const terimaMutasi = this.asetPropertiService.validasiPerpindahan(
        this.perpindahanAsetPropertiPerubahan.id_perpindahan_properti, this.memo, true);
    Observable.combineLatest(terimaDetail, terimaMutasi).subscribe(res => {
      this.modalRef.hide();
      if (res[0].message) {
        alert(res[0].message);
      }
      if (res[1].message) {
        alert(res[1].message);
      }

      if (res[0].success || res[1].success) {
        window.location.reload();
      }
    });
    return false;
  }

  tolakUsulanMutasi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
    .validasiPerpindahan(this.perpindahanAsetPropertiPerubahan.id_perpindahan_properti, this.memo, false)
    .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.perpindahanAsetPropertiPerubahan.action_button.button = false;
        this._init();
      }
    });
    return false;
  }

  terimaUsulanMutasi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
      .validasiPerpindahan(this.perpindahanAsetPropertiPerubahan.id_perpindahan_properti, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.perpindahanAsetPropertiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  tolakUsulanNilai() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
    .validasiNilai(this.nilaiPerubahan.id_nilai_properti, this.memo, false)
    .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.nilaiPerubahan.action_button.button = false;
        this._init();
      }
    });
    return false;
  }

  terimaUsulanNilai() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.asetPropertiService
      .validasiNilai(this.nilaiPerubahan.id_nilai_properti, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.nilaiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    return false;
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  private _mapPerpindahanToDatatable(perpindahans: PerpindahanAsetProperti[]) {
    this.historyPerpindahan.columns = ['KODE ASET PROPERTI', 'LOKASI', 'BUSINESS AREA', 'COMPANY CODE', 'TANGGAL BERLAKU'];
    this.historyPerpindahan.actions = [];
    this.historyPerpindahan.data.next([]);
    const data = [];

    perpindahans.map((r: PerpindahanAsetProperti) => {
      const kode = r.kode_properti ? r.kode_properti : '-';
      const lokasi = r.tanah.perpindahan_aktif[0] && r.tanah.perpindahan_aktif[0].lokasi ?
          this._toUppercase(r.tanah.perpindahan_aktif[0].lokasi.nama) : '-';
      const businessArea = r.business_area ? this._toUppercase(r.business_area.nama_business_area) : '-';
      const companyCode = r.company_code ? this._toUppercase(r.company_code.nama) : '-';
      const tanggalBerlaku = r.start_date_perpindahan ?
        this.datePipe.transform(r.end_date_perpindahan, 'dd MMM yyyy') : '-';

      data.push({
        row: [ kode, lokasi, businessArea, companyCode, tanggalBerlaku ],
        id: r.id_perpindahan_properti
      });
    });

    this.historyPerpindahan.data.next(data);
  }

  private _mapDetailToDatatable(details: AsetPropertiDetail[]) {
    this.history.columns = ['NAMA ASET PROPERTI', 'ALAMAT', 'TANGGAL PERUBAHAN'];
    this.history.actions = [];
    this.history.data.next([]);
    const data = [];

    details.map((r: AsetPropertiDetail) => {
      const nama = r.nama_properti ? this._toUppercase(r.nama_properti) : '-';
      const alamat = r.alamat_properti ? this._toUppercase(r.alamat_properti) : '-';
      const tanggal = r.changed_on ? this.datePipe.transform(r.changed_on, 'dd MMM yyyy')
          : this.datePipe.transform(r.created_on, 'dd MMM yyyy');

      data.push({
        row: [ nama, alamat, tanggal ],
        id: r.id_properti_detail
      });
    });

    this.history.data.next(data);
  }

  private _mapNilaiToDatatable(nilais: NilaiAsetProperti[]) {
    this.historyNilai.columns = ['TAHUN PENILAIAN',	'NILAI', 'KETERANGAN PENILAIAN', 'STATUS'];
    this.historyNilai.actions = [];
    this.historyNilai.data.next([]);
    const data = [];

    nilais.map((r: NilaiAsetProperti) => {
      const tahun = r.tahun_penilaian ? r.tahun_penilaian : '-';
      const nilai = r.nilai_properti ? this.numberPipe.transform(r.nilai_properti, '1.0-2') : '-';
      const keterangan = r.keterangan ? this._toUppercase(r.keterangan) : '-';
      const status = r.is_use ? 'DIPAKAI' : 'TIDAK DIPAKAI';

      data.push({
        row: [ tahun, nilai, keterangan, status ],
        id: r.id_properti
      });
    });

    this.historyNilai.data.next(data);
  }

  private _mapPotensiToDatatable(potensis: PotensiAset[]) {
    this.historyPotensi.columns = ['NILAI POTENSI ', 'DESKRIPSI', 'TANGGAL MULAI', 'TANGGAL SELESAI'];
    this.historyPotensi.actions = [];
    this.historyPotensi.data.next([]);
    const data = [];

    potensis.map((r: PotensiAset) => {
      const nilai = r.nilai_potensi ? this.numberPipe.transform(r.nilai_potensi) : '-';
      const deskripsi = r.deskripsi ? this._toUppercase(r.deskripsi) : '-';
      const tanggalMulai = r.start_date_potensi ? this.datePipe.transform(r.start_date_potensi, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_potensi ? this.datePipe.transform(r.end_date_potensi, 'dd MMM yyyy') : '-';

      data.push({
        row: [ nilai, deskripsi, tanggalMulai, tanggalSelesai ],
        id: r.id_potensi
      });
    });

    this.historyPotensi.data.next(data);
  }

  private _mapPendayagunaanToDatatable(pendayagunaans: PendayagunaanAset[]) {
    this.historyPendayagunaan.columns = ['JENIS PENDAYAGUNAAN',	'NO. KONTRAK',	'NILAI',	'TANGGAL MULAI',	'TANGGAL BERAKHIR'];
    this.historyPendayagunaan.actions = [];
    this.historyPendayagunaan.data.next([]);
    const data = [];

    pendayagunaans.map((r: PendayagunaanAset) => {
      const jenis = r.jenis_dayaguna && r.jenis_dayaguna.nama_jenis_dayaguna ? this._toUppercase(r.jenis_dayaguna.nama_jenis_dayaguna) : '-';
      const kontrak = r.kontrak ? r.kontrak : '-';
      const nilai = r.nilai_pendayagunaan ? this.numberPipe.transform(r.nilai_pendayagunaan) : '-';
      const tanggalMulai = r.start_date_pendayagunaan ? this.datePipe.transform(r.start_date_pendayagunaan, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_pendayagunaan ? this.datePipe.transform(r.end_date_pendayagunaan, 'dd MMM yyyy') : '-';

      data.push({
        row: [ jenis, kontrak, nilai, tanggalMulai, tanggalSelesai ],
        id: r.id_pendayagunaan
      });
    });

    this.historyPendayagunaan.data.next(data);
  }

  private _toUppercase(str: string) {
    return this.uppercasePipe.transform(str || '');
  }

  back() {
    this._location.back();
    return false;
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }
}
