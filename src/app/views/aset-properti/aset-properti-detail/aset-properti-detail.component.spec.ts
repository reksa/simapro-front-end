import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetPropertiDetailComponent } from './aset-properti-detail.component';

describe('AsetPropertiDetailComponent', () => {
  let component: AsetPropertiDetailComponent;
  let fixture: ComponentFixture<AsetPropertiDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetPropertiDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetPropertiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
