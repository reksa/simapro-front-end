import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidasiAsetPropertiComponent } from './validasi-aset-properti.component';

describe('ValidasiAsetPropertiComponent', () => {
  let component: ValidasiAsetPropertiComponent;
  let fixture: ComponentFixture<ValidasiAsetPropertiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidasiAsetPropertiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidasiAsetPropertiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
