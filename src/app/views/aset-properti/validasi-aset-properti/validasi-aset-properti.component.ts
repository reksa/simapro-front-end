import { environment } from './../../../../environments/environment';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AsetProperti, AsetTanah, AsetPropertiDetail, PerpindahanAsetProperti, NilaiAsetProperti } from '../../../models';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { AsetPropertiService } from '../../../services/data-services';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { LatLngBounds, LatLng, Map } from 'leaflet';

@Component({
  selector: 'app-validasi-aset-properti',
  templateUrl: './validasi-aset-properti.component.html',
  styleUrls: ['./validasi-aset-properti.component.scss']
})
export class ValidasiAsetPropertiComponent implements OnInit {
  showMap = false;
  upload = environment.upload_url + '/aset-properti/';
  fotoPerubahan;
  asetProperti = new AsetProperti();
  asetTanah: AsetTanah;
  detailAsetPropertiPerubahan: AsetPropertiDetail;
  perpindahanAsetPropertiPerubahan: PerpindahanAsetProperti;
  nilaiPerubahan: NilaiAsetProperti;

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.asetProperti;
  markerTanah = environment.map.marker.asetTanah;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 18;
  memo = '';
  poly: any;
  polyNonValid: any;

  namaAsetProperti = '';
  namaCompanyCode = '';
  root = 'aset-properti/'
  modalRef: BsModalRef;

  historisDisposisi: any[] = [];
  @ViewChild('map') map: Map;

  constructor(
    private route: ActivatedRoute,
    private asetPropertiService: AsetPropertiService,
    private modalService: BsModalService,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this._init();
  }

  private _init(){
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      this.spinner.hide();
      const id = parseInt(params.get('idAsetProperti'), 10);
      this.asetPropertiService.getById(id).subscribe((asetProperti: AsetProperti) => {
        this.asetProperti = new AsetProperti(asetProperti);
        
        const perpindahanX = this.asetProperti.perpindahan || this.asetProperti.perpindahan_perubahan; 
        this.asetTanah = new AsetTanah(perpindahanX.tanah);

        this.detailAsetPropertiPerubahan = this.asetProperti.detail_perubahan;
        this.perpindahanAsetPropertiPerubahan = this.asetProperti.perpindahan_perubahan;
        this.fotoPerubahan = this.detailAsetPropertiPerubahan && this.detailAsetPropertiPerubahan.foto ? `${this.upload}${this.detailAsetPropertiPerubahan.foto}` : 'assets/img/no-image.jpg';
        this.nilaiPerubahan = this.asetProperti.nilai_perubahan;
      

        this.namaAsetProperti = this.detailAsetPropertiPerubahan.nama_properti;
        this.namaCompanyCode = perpindahanX.company_code.nama;

        if (this.detailAsetPropertiPerubahan && this.detailAsetPropertiPerubahan.polygon && this.detailAsetPropertiPerubahan.polygon[0]) {
          this.polyNonValid = this.detailAsetPropertiPerubahan.polygon[0];
          const center = this._getCenter(this.polyNonValid.area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.polyNonValid.area.coordinates[0]);
        }
      })
    });
  }

  ngAfterContentInit() {
    this.showMap = true;
  }

  log(data: any) {
    // console.log(data);
  }

  tolakUsulan() {
    if(this.modalRef) {
      this.modalRef.hide();
    }
    if (this.detailAsetPropertiPerubahan.action_button.type !== 'validasi' || !this.detailAsetPropertiPerubahan.action_button.button) {
      return false;
    }
    this.spinner.show();
    this.asetPropertiService
      .validasi(this.asetProperti.id_properti, this.memo, false)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetPropertiPerubahan.action_button.button = false;
          this.perpindahanAsetPropertiPerubahan.action_button.button = false;
          this.nilaiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  terimaUsulan() {
    if(this.modalRef){
      this.modalRef.hide();
    }
    if (this.detailAsetPropertiPerubahan.action_button.type !== 'validasi' || !this.detailAsetPropertiPerubahan.action_button.button) {
      return false;
    }
    this.spinner.show();
    this.asetPropertiService
      .validasi(this.asetProperti.id_properti, this.memo, true)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetPropertiPerubahan.action_button.button = false;
          this.perpindahanAsetPropertiPerubahan.action_button.button = false;
          this.nilaiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  back() {
    this._location.back();
    return false;
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }
}
