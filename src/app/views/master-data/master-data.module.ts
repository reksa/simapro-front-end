// import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterDataRoutingModule } from './master-data-routing.module';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { JadwalInputComponent } from './jadwal-input/jadwal-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JadwalInputService } from '../../services/data-services/jadwal-input.service';
// import { PaginationModule } from 'app/components/pagination/pagination.module';

// import { AlertService } from 'app/services/alert.service';
// import { CompanyCodeService } from 'app/services/data-services';

@NgModule({
  imports: [
    CommonModule,
    MasterDataRoutingModule,
    FormsModule,
    ReactiveFormsModule
    // PaginationModule
  ],
  providers: [
    JadwalInputService,
    PermissionGuard
    // AlertService
  ],
  declarations: [JadwalInputComponent],
})
export class MasterDataModule { }
