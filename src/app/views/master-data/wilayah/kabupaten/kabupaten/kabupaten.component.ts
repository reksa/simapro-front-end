import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { URLSearchParams } from '@angular/http';
import { KabupatenService } from 'app/services/data-services/kabupaten.service';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { Kabupaten } from 'app/models/kabupaten.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kabupaten',
  templateUrl: './kabupaten.component.html',
  styleUrls: ['./kabupaten.component.scss']
})
export class KabupatenComponent implements OnInit {
  showDetail = false;
  pageData: Object[] = [];
  kabupaten = new Kabupaten();
  dataSetting = new DataSetting();
  modalTitle: string;
  operation: string;
  isLoading = true;
  root = 'master-data/wilayah/kabupaten/';

  public tableConfig = new AppTableConfig();

  constructor(
    private kabupatenService: KabupatenService,
    private route: ActivatedRoute,
    private router: Router,
    private slugPipe: TextToSlugPipe
  ) {
  }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      
      this.dataSetting.query = params.get('query');

      return this.kabupatenService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;
      
      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(kabupaten: Kabupaten) {
    const nama = this.slugPipe.transform(kabupaten.nama);
    this.router.navigate([`${this.root}show/${kabupaten.id_kabupaten}/${nama}`]);
    return false;
  }

  edit(kabupaten: Kabupaten) {
    const nama = this.slugPipe.transform(kabupaten.nama);
    this.router.navigate([`${this.root}edit/${kabupaten.id_kabupaten}/${nama}`]);
    return false;
  }

  delete(kabupaten: Kabupaten) {
    if (confirm(`Ingin nonaktifkan ${kabupaten.nama} ?`)) {
      kabupaten.is_active = false;
      this.kabupatenService.save(kabupaten).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          kabupaten.is_active = !kabupaten.is_active;
        }
      });
    }
    return false;
  }

  undelete(kabupaten: Kabupaten) {
    if (confirm(`Ingin mengaktifkan ${kabupaten.nama} ?`)) {
      kabupaten.is_active = true;
      this.kabupatenService.save(kabupaten).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          kabupaten.is_active = !kabupaten.is_active;
        }
      });
    }
    return false;
  }
}
