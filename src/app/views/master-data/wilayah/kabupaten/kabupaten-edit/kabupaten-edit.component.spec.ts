import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KabupatenEditComponent } from './kabupaten-edit.component';

describe('KabupatenEditComponent', () => {
  let component: KabupatenEditComponent;
  let fixture: ComponentFixture<KabupatenEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KabupatenEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KabupatenEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
