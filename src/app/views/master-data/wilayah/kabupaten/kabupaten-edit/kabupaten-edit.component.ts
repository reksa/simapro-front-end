import { ViewChild } from '@angular/core';
import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { KabupatenService, ProvinsiService } from 'app/services/data-services';

import { Provinsi, Kabupaten } from 'app/models';
import { SelectComponent } from 'ng2-select';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-kabupaten-edit',
  templateUrl: './kabupaten-edit.component.html',
  styleUrls: ['./kabupaten-edit.component.scss']
})
export class KabupatenEditComponent implements OnInit {
  myForm: FormGroup;
  provinsis: Provinsi[];
  kabupaten = new Kabupaten();
  isSubmitted = false;
  isEdit = true;
  root = 'master-data/wilayah/'
  title = '';
  items = [];
  value: any;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;

  constructor(
    private _fb: FormBuilder,
    private _location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private kabupatenService: KabupatenService,
    private provinsiService: ProvinsiService,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.provinsiService.getAll().subscribe(provinsis => {
      this.spinner.hide();
      this.provinsis = provinsis;
      this.items = provinsis.map(provinsi => {
        return {id: provinsi, text: provinsi.nama};
      });
    });

    this.myForm = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      id_provinsi: ['', [Validators.required]],
    });
    this.route.url.subscribe(url => {
      const act = url[1].path;
      if (act === 'edit') {
        this.title = 'Ubah Kabupaten / Kota';
        this.prepareEdit();
      } else if (act === 'show') {
        this.title = 'Detail Kabupaten / Kota';
        this.prepareEdit();
        this.isEdit = false;
        this.myForm.get('nama').disable();
        this.myForm.get('id_provinsi').disable();
      } else {
        this.prepareAdd();
      }
    });
  }

  prepareAdd() {
    this.title = 'Tambah Kabupaten / Kota';
  }

  prepareEdit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      this.spinner.hide();
      const id = parseInt(params.get('idKabupaten'), 10);
      this.kabupatenService.getById(id).subscribe(kabupaten => {
        this.kabupaten = kabupaten;
        this.items.forEach((item, index) => {
          if(item.id.id_provinsi === kabupaten.id_provinsi) {
            this.selectProvinsi.active = [item];
          }
        });
      });
    });
  }

  edit() {
    const nama = this.slugPipe.transform((this.kabupaten.nama || ''));
    this.router.navigate([`${this.root}kabupaten/edit/${this.kabupaten.id_kabupaten}/${nama}`]);
    return false;
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.kabupatenService.save(this.kabupaten).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

  refreshValue(value: any):void {
    this.myForm.get('id_provinsi').setValue(value.id.id_provinsi);
    this.myForm.get('id_provinsi').updateValueAndValidity();
  }

  typed(e: any) {
    this.myForm.get('id_provinsi').markAsTouched();
    this.myForm.get('id_provinsi').markAsDirty();
  }

  selected(value: any) {
    // console.log(value);
  }

  removed(value: any) {
    // console.log(value);
  }

  log() {
    // console.log(this.myForm)
  }
}
