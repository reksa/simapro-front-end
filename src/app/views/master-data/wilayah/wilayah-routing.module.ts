import { LokasiEditComponent } from './lokasi/lokasi-edit/lokasi-edit.component';
import { ProvinsiEditComponent } from './provinsi/provinsi-edit/provinsi-edit.component';
import { KabupatenEditComponent } from './kabupaten/kabupaten-edit/kabupaten-edit.component';
import { KabupatenComponent } from './kabupaten/kabupaten/kabupaten.component';
import { LokasiComponent } from './lokasi/lokasi/lokasi.component';
import { ProvinsiComponent } from './provinsi/provinsi/provinsi.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';

const routes: Routes = [
  { path: '', redirectTo: 'provinsi', pathMatch: 'full' },
  { path: 'provinsi', component: ProvinsiComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'provinsi/add', component: ProvinsiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'provinsi/show/:idProvinsi/:nama', component: ProvinsiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'provinsi/edit/:idProvinsi/:nama', component: ProvinsiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'kabupaten', component: KabupatenComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'kabupaten/add', component: KabupatenEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'kabupaten/show/:idKabupaten/:nama', component: KabupatenEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'kabupaten/edit/:idKabupaten/:nama', component: KabupatenEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'lokasi', component: LokasiComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'lokasi/add', component: LokasiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'lokasi/show/:idLokasi/:nama', component: LokasiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'lokasi/edit/:idLokasi/:nama', component: LokasiEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WilayahRoutingModule { }
