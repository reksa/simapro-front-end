import { SelectComponent } from 'ng2-select';
import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { LokasiService, ProvinsiService, KabupatenService } from 'app/services/data-services';

import { Provinsi, Lokasi, Kabupaten } from 'app/models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-lokasi-edit',
  templateUrl: './lokasi-edit.component.html',
  styleUrls: ['./lokasi-edit.component.scss']
})
export class LokasiEditComponent implements OnInit {
  myForm: FormGroup;
  provinsis: Provinsi[];
  kabupatens: Kabupaten[];
  lokasi = new Lokasi();
  idProvinsi: number;
  isSubmitted = false;
  isEdit = true;
  root = 'master-data/wilayah/';
  title = '';
  items = [];
  itemsK = [];
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;

  constructor(
    private _fb: FormBuilder,
    private _location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private lokasiService: LokasiService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.provinsiService.getAll().subscribe(provinsis => {
      this.spinner.hide();
      this.provinsis = provinsis;
      this.items = provinsis.map(provinsi => {
        return {id: provinsi, text: provinsi.nama};
      });
    });

    this.myForm = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      id_provinsi: ['', [Validators.required]],
      id_kabupaten: ['', [Validators.required]],
    });

    this.route.url.subscribe(url => {
      const act = url[1].path;
      if (act === 'edit') {
        this.title = 'Ubah Lokasi';
        this.prepareEdit();
      } else if (act === 'show') {
        this.title = 'Detail Lokasi';
        this.prepareEdit();
        this.isEdit = false;
        this.myForm.get('nama').disable();
        this.myForm.get('id_provinsi').disable();
        this.myForm.get('id_kabupaten').disable();
      } else {
        this.prepareAdd();
      }
    });
  }

  getKabupaten() {
    this.kabupatens = [];
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.kabupatens = kabupatens;
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten, text: kabupaten.nama};
      });

      if(this.itemsK[0]) {
        this.selectKabupaten.active = [this.itemsK[0]];
        this.refreshValueK(this.itemsK[0]);
      }

      this.itemsK.forEach((item, index) => {
        if(item.id.id_kabupaten === this.lokasi.kabupaten.id_kabupaten) {
          this.selectKabupaten.active = [item];
        }
      });
    });
  }

  prepareAdd() {
    this.title = 'Tambah Lokasi';
  }

  prepareEdit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idLokasi'), 10);
      this.lokasiService.getById(id).subscribe(lokasi => {
        this.spinner.hide();
        this.lokasi = lokasi;
        this.idProvinsi = lokasi.kabupaten.id_provinsi;
        this.items.forEach((item, index) => {
          if(item.id.id_provinsi === lokasi.kabupaten.id_provinsi) {
            this.selectProvinsi.active = [item];
          }
        });
        this.getKabupaten();
      });
    });
  }

  edit() {
    const nama = this.slugPipe.transform((this.lokasi.nama || ''));
    this.router.navigate([`${this.root}lokasi/edit/${this.lokasi.id_lokasi}/${nama}`]);
    return false;
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.lokasiService.save(this.lokasi).subscribe(res => {
      this.spinner.hide();
      this.isSubmitted = false;
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

  refreshValue(value: any):void {
    this.myForm.get('id_provinsi').setValue(value.id.id_provinsi);
    this.myForm.get('id_provinsi').updateValueAndValidity();
  }

  typed(e: any) {
    this.myForm.get('id_provinsi').markAsTouched();
    this.myForm.get('id_provinsi').markAsDirty();
  }

  selected(value: any) {
    this.getKabupaten();
  }

  removed(value: any) {
  }

  refreshValueK(value: any):void {
    this.myForm.get('id_kabupaten').setValue(value.id.id_kabupaten);
    this.myForm.get('id_kabupaten').updateValueAndValidity();
  }

  typedK(e: any) {
    this.myForm.get('id_kabupaten').markAsTouched();
    this.myForm.get('id_kabupaten').markAsDirty();
  }

  selectedK(value: any) {
  }

  removedK(value: any) {
  }

}
