import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { URLSearchParams } from '@angular/http';
import { LokasiService } from 'app/services/data-services';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { Lokasi } from 'app/models/lokasi.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lokasi',
  templateUrl: './lokasi.component.html',
  styleUrls: ['./lokasi.component.scss']
})
export class LokasiComponent implements OnInit {
  showDetail = false;
  pageData: Object[] = [];
  lokasi = new Lokasi();
  dataSetting = new DataSetting();
  modalTitle: string;
  operation: string;
  isLoading = true;
  root = 'master-data/wilayah/lokasi/';

  private editMode = false;
  private viewMode = false;

  public tableConfig = new AppTableConfig();

  constructor(
    private lokasiService: LokasiService,
    private route: ActivatedRoute,
    private router: Router,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      
      this.dataSetting.query = params.get('query');

      return this.lokasiService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;
      
      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(lokasi: Lokasi) {
    const nama = this.slugPipe.transform(lokasi.nama);
    this.router.navigate([`${this.root}show/${lokasi.id_lokasi}/${nama}`]);
    return false;
  }

  edit(lokasi: Lokasi) {
    const nama = this.slugPipe.transform(lokasi.nama);
    this.router.navigate([`${this.root}edit/${lokasi.id_lokasi}/${nama}`]);
    return false;
  }

  delete(lokasi: Lokasi) {
    if (confirm(`Ingin nonaktifkan ${lokasi.nama} ?`)) {
      lokasi.is_active = false;
      this.lokasiService.save(lokasi).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          lokasi.is_active = !lokasi.is_active;
        }
      });
    }
    return false;
  }

  undelete(lokasi: Lokasi) {
    if (confirm(`Ingin mengaktifkan ${lokasi.nama} ?`)) {
      lokasi.is_active = true;
      this.lokasiService.save(lokasi).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          lokasi.is_active = !lokasi.is_active;
        }
      });
    }
    return false;
  }
}
