import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { Pulau, Provinsi } from 'app/models';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ProvinsiService } from 'app/services/data-services';
import { PulauService } from 'app/services/data-services/pulau.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-provinsi-edit',
  templateUrl: './provinsi-edit.component.html',
  styleUrls: ['./provinsi-edit.component.scss']
})
export class ProvinsiEditComponent implements OnInit {
  myForm: FormGroup;
  provinsi = new Provinsi();
  pulaus: Pulau[] = [];
  isSubmitted = false;
  isEdit = true;
  title = '';
  root = 'master-data/wilayah/';

  constructor(
    private _fb: FormBuilder,
    private _location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private provinsiService: ProvinsiService,
    private pulauService: PulauService,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.pulauService.getAll().subscribe(pulaus => {
      this.spinner.hide();
      this.pulaus = pulaus;
      this.provinsi.id_pulau = pulaus.length ? pulaus[0].id_pulau : null;
    });

    this.myForm = this._fb.group({
      id_pulau: ['', [Validators.required]],
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]]
    });
    this.route.url.subscribe(url => {
      const act = url[1].path;
      if (act === 'edit') {
        this.title = 'Ubah Provinsi';
        this.prepareEdit();
      } else if (act === 'show') {
        this.title = 'Detail Provinsi';
        this.prepareEdit();
        this.isEdit = false;
        this.myForm.get('id_pulau').disable();
        this.myForm.get('nama').disable();
      } else {
        this.prepareAdd();
      }
    });
  }

  prepareAdd() {
    this.title = 'Tambah Provinsi';
  }

  prepareEdit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      this.spinner.hide();
      const id = parseInt(params.get('idProvinsi'), 10);
      this.provinsiService.getById(id).subscribe(provinsi => {
        this.provinsi = provinsi;
      });
    });
  }

  edit() {
    const nama = this.slugPipe.transform((this.provinsi.nama || '') );
    this.router.navigate([`${this.root}provinsi/edit/${this.provinsi.id_provinsi}/${nama}`]);
    return false;
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.provinsiService.save(this.provinsi).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

}
