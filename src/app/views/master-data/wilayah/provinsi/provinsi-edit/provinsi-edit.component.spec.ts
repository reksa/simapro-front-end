import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvinsiEditComponent } from './provinsi-edit.component';

describe('ProvinsiEditComponent', () => {
  let component: ProvinsiEditComponent;
  let fixture: ComponentFixture<ProvinsiEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvinsiEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvinsiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
