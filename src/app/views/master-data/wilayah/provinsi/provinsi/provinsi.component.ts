import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { URLSearchParams } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Provinsi } from 'app/models/provinsi.model';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { ProvinsiService } from 'app/services/data-services';

@Component({
  selector: 'app-provinsi',
  templateUrl: './provinsi.component.html',
  styleUrls: ['./provinsi.component.scss']
})
export class ProvinsiComponent implements OnInit {
  showDetail = false;
  pageData: Object[] = [];
  provinsi = new Provinsi();
  dataSetting = new DataSetting();
  modalTitle: string;
  operation: string;
  isLoading = true;
  root = 'master-data/wilayah/provinsi/';

  public tableConfig = new AppTableConfig();

  constructor(
    private provinsiService: ProvinsiService,
    private route: ActivatedRoute,
    private router: Router,
    private slugPipe: TextToSlugPipe
  ) {}

  ngOnInit() {
    this.dataSetting.page = 1;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      
      this.dataSetting.query = params.get('query');

      return this.provinsiService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.isLoading = true;
    this.pageData = [];
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.isLoading = true;
    this.pageData = [];
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(provinsi: Provinsi) {
    const nama = this.slugPipe.transform(provinsi.nama);
    this.router.navigate([`${this.root}show/${provinsi.id_provinsi}/${nama}`]);
    return false;
  }

  edit(provinsi: Provinsi) {
    const nama = this.slugPipe.transform(provinsi.nama);
    this.router.navigate([`${this.root}edit/${provinsi.id_provinsi}/${nama}`]);
    return false;
  }

  delete(provinsi: Provinsi) {
    if (confirm(`Ingin nonaktifkan ${provinsi.nama} ?`)) {
      provinsi.is_active = false;
      this.provinsiService.save(provinsi).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          provinsi.is_active = !provinsi.is_active;
        }
      });
    }
    return false;
  }

  undelete(provinsi: Provinsi) {
    if (confirm(`Ingin nonaktifkan ${provinsi.nama} ?`)) {
      provinsi.is_active = true;
      this.provinsiService.save(provinsi).subscribe(res => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          provinsi.is_active = !provinsi.is_active;
        }
      });
    }
    return false;
  }
}
