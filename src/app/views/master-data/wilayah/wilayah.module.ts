import { TextToSlugPipe } from './../../../services/text-to-slug.service';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WilayahRoutingModule } from './wilayah-routing.module';
import { ProvinsiComponent } from './provinsi/provinsi/provinsi.component';
import { KabupatenComponent } from './kabupaten/kabupaten/kabupaten.component';
import { LokasiComponent } from './lokasi/lokasi/lokasi.component';
import { YagaModule } from '@yaga/leaflet-ng2';
import { KabupatenEditComponent } from './kabupaten/kabupaten-edit/kabupaten-edit.component';
import { LokasiEditComponent } from './lokasi/lokasi-edit/lokasi-edit.component';
import { ProvinsiEditComponent } from './provinsi/provinsi-edit/provinsi-edit.component';

import { AlertService } from 'app/services/alert.service';
import { ProvinsiService, KabupatenService, LokasiService } from 'app/services/data-services';
import { PermissionGuard } from '../../../services/auth';
import { PulauService } from '../../../services/data-services/pulau.service';
import { SelectModule } from 'ng2-select';

@NgModule({
  imports: [
    CommonModule,
    WilayahRoutingModule,
    YagaModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    SelectModule
  ],
  providers: [
    ProvinsiService,
    KabupatenService,
    LokasiService,
    AlertService,
    PermissionGuard,
    PulauService,
    TextToSlugPipe
  ],
  declarations: [
    ProvinsiComponent,
    KabupatenComponent,
    LokasiComponent,
    KabupatenEditComponent,
    LokasiEditComponent,
    ProvinsiEditComponent
  ]
})
export class WilayahModule {}
