import { JadwalInput } from './../../../models/jadwal-input.model';
import { Component, OnInit } from '@angular/core';
import { JadwalInputService } from '../../../services/data-services/jadwal-input.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-jadwal-input',
  templateUrl: './jadwal-input.component.html',
  styleUrls: ['./jadwal-input.component.scss']
})
export class JadwalInputComponent implements OnInit {
  _isSubmitted = false;
  jadwalInput = new JadwalInput();
  jadwalInputPerubahan = new JadwalInput();
  formJadwal: FormGroup;

  constructor(
    private jadwalInputService: JadwalInputService,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.jadwalInputService.get().subscribe(jadwalInput => {
      this.jadwalInput = jadwalInput[0];
      this.jadwalInputPerubahan = Object.assign({}, this.jadwalInput);
    });
    this.formJadwal = this._fb.group({
      start: ['', [Validators.required]],
      end: ['', [Validators.required]]
    });
  }

  save() {
    this._isSubmitted = true;
    this.jadwalInputService.save(this.jadwalInputPerubahan).subscribe(response => {
      if (response.message) {
        alert(response.message);
      }

      if (response.success) {
        this.jadwalInput = this.jadwalInputPerubahan;
      }
      this._isSubmitted = true;
    })
  }

}
