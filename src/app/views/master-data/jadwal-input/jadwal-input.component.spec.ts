import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JadwalInputComponent } from './jadwal-input.component';

describe('JadwalInputComponent', () => {
  let component: JadwalInputComponent;
  let fixture: ComponentFixture<JadwalInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JadwalInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JadwalInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
