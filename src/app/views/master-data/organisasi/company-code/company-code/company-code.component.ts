import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { CompanyCode } from 'app/models';
import { URLSearchParams } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { CompanyCodeService } from 'app/services/data-services';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-company-code',
  templateUrl: './company-code.component.html',
  styleUrls: ['./company-code.component.scss']
})
export class CompanyCodeComponent implements OnInit {
  tab = 'company-code';
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  operation: string;
  isLoading = true;
  root = 'master-data/organisasi/';

  public tableConfig = new AppTableConfig();

  constructor(
    private companyCodeService: CompanyCodeService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));

      this.dataSetting.query = params.get('query');

      return this.companyCodeService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(data: CompanyCode) {
    const nama = this.slugPipe.transform(data.nama);
    this.router.navigate([`${this.root}company-code/show/${data.id_company_code}/${nama}`]);
    return false;
  }

  edit(data: CompanyCode) {
    const nama = this.slugPipe.transform(data.nama);
    this.router.navigate([`${this.root}company-code/edit/${data.id_company_code}/${nama}`]);
    return false;
  }

  delete(data: CompanyCode) {
    if (confirm(`Ingin nonaktifkan ${data.nama} ?`)) {
      data.is_active = false;
      this.companyCodeService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = true;
        }
      });
    }
    return false;
  }

  undelete(data: CompanyCode) {
    if (confirm(`Ingin mengaktifkan ${data.nama} ?`)) {
      data.is_active = true;
      this.companyCodeService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = false;
        }
      });
    }
    return false;
  }

  chooseTab(tab) {
    this.router.navigate([`${this.root}${tab}`]);
    return false;
  }
}
