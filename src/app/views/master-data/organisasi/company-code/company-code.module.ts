import { CompanyCodeValidator } from 'app/validators';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CompanyCodeRoutingModule } from './company-code-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { TabsModule } from 'ngx-bootstrap';

import { BusinessAreaService, RegionalService, CompanyCodeService, HeadquarterService } from 'app/services/data-services';
import { CompanyCodeComponent } from './company-code/company-code.component';
import { CompanyCodeDetailComponent } from './company-code-detail/company-code-detail.component';
import { CompanyCodeEditComponent } from './company-code-edit/company-code-edit.component';

@NgModule({
  imports: [
    CommonModule,
    CompanyCodeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TabsModule
  ],
  providers: [ HeadquarterService, CompanyCodeService, CompanyCodeValidator, BusinessAreaService, RegionalService ],
  declarations: [CompanyCodeEditComponent, CompanyCodeDetailComponent, CompanyCodeComponent]
})
export class CompanyCodeModule { }
