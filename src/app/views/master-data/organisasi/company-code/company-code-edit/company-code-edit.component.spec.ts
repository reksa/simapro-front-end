import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCodeEditComponent } from './company-code-edit.component';

describe('CompanyCodeEditComponent', () => {
  let component: CompanyCodeEditComponent;
  let fixture: ComponentFixture<CompanyCodeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCodeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCodeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
