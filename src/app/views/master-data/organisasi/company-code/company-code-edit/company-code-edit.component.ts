import { TextToSlugPipe } from "app/services/text-to-slug.service";
import { CompanyCodeValidator } from "app/validators";
import { CompanyCode, Regional, Headquarter } from "app/models";
import {
  CompanyCodeService,
  RegionalService,
  HeadquarterService
} from "app/services/data-services";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-company-code-edit",
  templateUrl: "./company-code-edit.component.html",
  styleUrls: ["./company-code-edit.component.scss"]
})
export class CompanyCodeEditComponent implements OnInit {
  companyCode = new CompanyCode();
  isSubmitted = false;
  title = "";
  companyCodes: CompanyCode[] = [];
  headquarters: Headquarter[] = [];
  headquarter: Headquarter = null;
  regionals: Regional[] = [];
  myForm: FormGroup;
  isEdit = true;
  root = "master-data/organisasi";
  act = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _fb: FormBuilder,
    private _location: Location,
    private companyCodeService: CompanyCodeService,
    private regionalService: RegionalService,
    private headquarterService: HeadquarterService,
    private companyCodeValidator: CompanyCodeValidator,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.headquarterService.getAll().subscribe(headquarters => {
      this.headquarters = headquarters;
      if (headquarters.length) {
        this.headquarter = headquarters[0];
        this.setRegionals();
      }
    });

    this.myForm = this._fb.group({
      id_head_quarters: ["", [Validators.required]],
      id_regional: ["", [Validators.required]],
      kode_company_code: [
        "",
        [Validators.required, Validators.minLength(4), Validators.maxLength(4)],
        [
          this.companyCodeValidator.uniqueKodeCompanyCode.bind(
            this.companyCodeValidator
          )
        ]
      ],
      nama: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(100)
        ],
        [this.companyCodeValidator.uniqueNama.bind(this.companyCodeValidator)]
      ],
      nama_company_code: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20)
        ],
        [
          this.companyCodeValidator.uniqueNamaCompanyCode.bind(
            this.companyCodeValidator
          )
        ]
      ]
    });
    this.spinner.show();
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.spinner.hide();
      this.companyCodes = companyCodes;
      this.route.url.subscribe(url => {
        this.act = url[0].path;
        if (this.act === "edit") {
          this.prepareEdit();
        } else if (this.act === "show") {
          this.prepareEdit();
          this.isEdit = false;
          this.myForm.get("kode_company_code").disable();
          this.myForm.get("nama").disable();
          this.myForm.get("nama_company_code").disable();
          this.myForm.get("id_headquarter").disable();
        } else {
          this.route.queryParamMap.subscribe(queryParams => {
            const id_head_quarters = queryParams.get("headquarter") || null;
            const id_regional = queryParams.get("regional") || null;
            if (id_head_quarters && id_regional) {
              this.headquarters.forEach(h => {
                if (h.id_head_quarters === parseInt(id_head_quarters)) {
                  this.headquarter = h;
                }
              });
              this.companyCode.id_regional = parseInt(id_regional);
            }
            this.prepareAdd();
          });
        }
      });
    });
  }

  prepareAdd() {
    this.title = "Tambah Company Code";
  }

  prepareEdit() {
    this.title = "Ubah Company Code";
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const idCompanyCode = parseInt(params.get("idCompanyCode"), 10);
      this.companyCodeService.getById(idCompanyCode).subscribe(companyCode => {
        this.spinner.hide();
        this.companyCodeValidator.id = companyCode.id_company_code;
        this.companyCode = companyCode;
      });
    });
  }

  cancel() {
    if (confirm("Ingin membatalkan?")) {
      this._location.back();
    }
    return false;
  }

  edit() {
    const nama = this.slugPipe.transform(this.companyCode.nama || "");
    this.router.navigate([
      `${this.root}company-code/edit/${
        this.companyCode.id_company_code
      }/${nama}`
    ]);
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.companyCodeService.save(this.companyCode).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

  setRegionals() {
    this.regionals = this.headquarter.regional || [];
  }
}
