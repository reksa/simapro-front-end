import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CompanyCodeService, BusinessAreaService } from 'app/services/data-services';
import { CompanyCode, BusinessArea } from 'app/models';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-company-code-detail',
  templateUrl: './company-code-detail.component.new.html',
  styleUrls: ['./company-code-detail.component.scss']
})
export class CompanyCodeDetailComponent implements OnInit {
  companyCode = new CompanyCode();
  businessAreas: BusinessArea[] = [];
  showBusinessAreas: BusinessArea[] = [];
  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  root = '/master-data/organisasi/';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private companyCodeService: CompanyCodeService,
    private slugPipe: TextToSlugPipe,
    private businessAreaService: BusinessAreaService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idCompanyCode'), 10);
      this.companyCodeService.getById(id).subscribe(companyCode => {
        this.spinner.hide();
        this.companyCode = companyCode;
        this.businessAreas = companyCode.business_area;
        this.filterData();
      });
    });
  }

  filterData() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.showBusinessAreas = [];
    this.countFiltered = 0;

    this.businessAreas.map((businessArea: BusinessArea, index: number, businessAreas: BusinessArea[]) => {
      if (regexp.test(businessArea.kode_business_area.toLowerCase()) || regexp.test(businessArea.nama_business_area.toLowerCase())) {
        this.showBusinessAreas.push(businessArea);
        this.countFiltered++;
      }
    });

    this.from = ((this.page - 1) * this.dataPerPage) + 1;
    this.to = (this.page * this.dataPerPage) <= this.countFiltered ? (this.page * this.dataPerPage) : this.countFiltered;
    this.showBusinessAreas = this.showBusinessAreas.slice(this.from - 1, this.to);
    this.getPages();
  }

  filterDataP() {
    this.page = 1;
    this.filterData();
  }

  getPages() {
    this.pages = [];
    const length = Math.ceil(this.countFiltered / this.dataPerPage);
    for (let i = 0; i < length; i++) {
      this.pages[i] = i + 1;
    }
  }

  setPage(page) {
    this.page = page;
    this.filterData();
  }

  lastPage() {
    this.page = this.pages[this.pages.length - 1];
    this.filterData();
  }

  edit() {
    const nama = this.slugPipe.transform((this.companyCode.nama || ''));
    this.router.navigate([`${this.root}company-code/edit/${this.companyCode.id_company_code}/${nama}`]);
    return false;
  }

  showBusinessArea(data: any) {
    const nama = this.slugPipe.transform(data.nama_business_area);
    this.router.navigate([`${this.root}business-area/show/${data.id_business_area}/${nama}`]);
    return false;
  }

  deleteBusinessArea(businessArea: BusinessArea) {
    if (confirm(`Ingin menonaktifkan ${businessArea.nama_business_area} ?`)) {
      businessArea.is_active = false;
      this.businessAreaService.save(businessArea)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          businessArea.is_active = false;
        }
      });
    }
    return false;
  }

  undeleteBusinessArea(businessArea: BusinessArea) {
    if (confirm(`Ingin mengaktifkan ${businessArea.nama_business_area} ?`)) {
      businessArea.is_active = true;
      this.businessAreaService.save(businessArea)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          businessArea.is_active = false;
        }
      });
    }
    return false;
  }

  back() {
    this._location.back();
    return false;
  }
}
