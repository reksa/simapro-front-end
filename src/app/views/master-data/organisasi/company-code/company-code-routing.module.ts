import { CompanyCodeComponent } from './company-code/company-code.component';
import { CompanyCodeDetailComponent } from './company-code-detail/company-code-detail.component';
import { CompanyCodeEditComponent } from './company-code-edit/company-code-edit.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CompanyCodeComponent},
  { path: 'add', component: CompanyCodeEditComponent },
  { path: 'show/:idCompanyCode/:namaCompanyCode', component: CompanyCodeDetailComponent },
  { path: 'edit/:idCompanyCode/:namaCompanyCode', component: CompanyCodeEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyCodeRoutingModule { }
