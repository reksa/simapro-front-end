import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HeadquarterRoutingModule } from './headquarter-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { TabsModule } from 'ngx-bootstrap';

import { HeadquarterService, RegionalService } from 'app/services/data-services';
import { HeadquarterComponent } from './headquarter/headquarter.component';
import { HeadquarterDetailComponent } from './headquarter-detail/headquarter-detail.component';
import { HeadquarterEditComponent } from './headquarter-edit/headquarter-edit.component';

@NgModule({
  imports: [
    CommonModule,
    HeadquarterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TabsModule
  ],
  providers: [ HeadquarterService, RegionalService, TextToSlugPipe ],
  declarations: [HeadquarterEditComponent, HeadquarterDetailComponent, HeadquarterComponent]
})
export class HeadquarterModule { }
