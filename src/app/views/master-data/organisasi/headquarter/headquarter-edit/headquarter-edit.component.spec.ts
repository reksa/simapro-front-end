import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadquarterEditComponent } from './headquarter-edit.component';

describe('HeadquarterEditComponent', () => {
  let component: HeadquarterEditComponent;
  let fixture: ComponentFixture<HeadquarterEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadquarterEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadquarterEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
