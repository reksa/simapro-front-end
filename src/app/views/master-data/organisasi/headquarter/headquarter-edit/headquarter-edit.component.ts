import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { Headquarter } from 'app/models';

import { HeadquarterService } from 'app/services/data-services';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-headquarter-edit',
  templateUrl: './headquarter-edit.component.html',
  styleUrls: ['./headquarter-edit.component.scss']
})
export class HeadquarterEditComponent implements OnInit {
  Headquarter = new Headquarter();
  isSubmitted = false;
  isEdit = true;
  title = '';
  Headquarters: Headquarter[] = [];
  myForm: FormGroup;
  root = 'master-data/organisasi/'


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _fb: FormBuilder,
    private _location: Location,
    private HeadquarterService: HeadquarterService,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.HeadquarterService.getAll().subscribe(Headquarters => {
      this.spinner.hide();
      this.Headquarters = Headquarters;
      this.route.url.subscribe(url => {
        const act = url[0].path;
        if (act === 'edit') {
          this.prepareEdit();
        } else if (act === 'show') {
          this.prepareEdit();
          this.isEdit = false;
        } else {
          this.prepareAdd();
        }
      });
    });

    this.myForm = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]]
    });
  }

  prepareAdd() {
    this.title = 'Tambah Headquarter';
  }

  prepareEdit() {
    this.title = 'Ubah Headquarter';
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const idHeadquarter = parseInt(params.get('idHeadquarter'), 10);
      this.HeadquarterService.getById(idHeadquarter).subscribe(Headquarter => {
        this.spinner.hide();
        this.Headquarter = Headquarter;
      });
    });
  }

  edit() {
    const nama = this.slugPipe.transform(this.Headquarter.nama_head_quarters)
    this.router.navigate([`${this.root}headquarter/edit/${this.Headquarter.id_head_quarters}/${nama}`]);
    return false;
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.HeadquarterService.save(this.Headquarter).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

}
