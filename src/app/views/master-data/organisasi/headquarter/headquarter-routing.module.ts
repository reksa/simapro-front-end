import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { HeadquarterComponent } from './headquarter/headquarter.component';
import { HeadquarterDetailComponent } from './headquarter-detail/headquarter-detail.component';
import { HeadquarterEditComponent } from './headquarter-edit/headquarter-edit.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: HeadquarterComponent, canActivate: [AuthGuard, PermissionGuard]},
  { path: 'add', component: HeadquarterEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idHeadquarter/:namaHeadquarter', component: HeadquarterDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idHeadquarter/:namaHeadquarter', component: HeadquarterEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeadquarterRoutingModule { }
