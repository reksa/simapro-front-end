import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HeadquarterService, RegionalService } from 'app/services/data-services';
import { Headquarter, Regional } from 'app/models';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-headquarter-detail',
  templateUrl: './headquarter-detail.component.html',
  styleUrls: ['./headquarter-detail.component.scss']
})
export class HeadquarterDetailComponent implements OnInit {
  headquarter = new Headquarter();
  regionals: Regional[] = [];
  showRegionals: Regional[] = [];
  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  root = 'master-data/organisasi/'

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private headquarterService: HeadquarterService,
    private regionalService: RegionalService,
    private slugPipe: TextToSlugPipe,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idHeadquarter'), 10);
      this.headquarterService.getById(id).subscribe(headquarter => {
        this.spinner.hide();
        this.headquarter = headquarter;
        this.regionals = headquarter.regional;
        this.filterData();
      });
    });
  }

  filterData() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.showRegionals = [];
    this.countFiltered = 0;

    this.regionals.map((r: Regional, index: number, regionals: Regional[]) => {
      if (regexp.test(r.nama_regional.toLowerCase())) {
        this.showRegionals.push(r);
        this.countFiltered++;
      }
    });

    this.from = ((this.page - 1) * this.dataPerPage) + 1;
    this.to = (this.page * this.dataPerPage) <= this.countFiltered ? (this.page * this.dataPerPage) : this.countFiltered;
    this.showRegionals = this.showRegionals.slice(this.from - 1, this.to);
    this.getPages();
  }

  filterDataP() {
    this.page = 1;
    this.filterData();
  }

  getPages() {
    this.pages = [];
    const length = Math.ceil(this.countFiltered / this.dataPerPage);
    for (let i = 0; i < length; i++) {
      this.pages[i] = i + 1;
    }
  }

  setPage(page) {
    this.page = page;
    this.filterData();
  }

  lastPage() {
    this.page = this.pages[this.pages.length - 1];
    this.filterData();
  }

  edit() {
    const nama = this.slugPipe.transform((this.headquarter.nama_head_quarters || ''));
    this.router.navigate([`${this.root}headquarter/edit/${this.headquarter.id_head_quarters}/${nama}`]);
    return false;
  }

  showRegional(data: Regional) {
    const nama = this.slugPipe.transform(data.nama_regional);
    this.router.navigate([`${this.root}regional/show/${data.id_regional}/${nama}`]);
    return false;
  }

  deleteRegional(regional: Regional) {
    if (confirm(`Ingin menonaktifkan ${regional.nama_regional} ?`)) {
      regional.is_active = false;
      this.regionalService.save(regional)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          regional.is_active = false;
        }
      });
    }
    return false;
  }

  undeleteRegional(regional: Regional) {
    if (confirm(`Ingin mengaktifkan ${regional.nama_regional} ?`)) {
      regional.is_active = true;
      this.regionalService.save(regional)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          regional.is_active = false;
        }
      });
    }
    return false;
  }

  back() {
    this._location.back();
    return false;
  }
}
