import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadquarterDetailComponent } from './headquarter-detail.component';

describe('HeadquarterDetailComponent', () => {
  let component: HeadquarterDetailComponent;
  let fixture: ComponentFixture<HeadquarterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadquarterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadquarterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
