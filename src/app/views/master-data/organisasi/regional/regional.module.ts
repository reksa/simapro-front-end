import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { AppDatatableModule } from './../../../../components/app-datatable/app-datatable.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegionalRoutingModule } from './regional-routing.module';
import { RegionalComponent } from './regional/regional.component';
import { RegionalEditComponent } from './regional-edit/regional-edit.component';
import { RegionalDetailComponent } from './regional-detail/regional-detail.component';
import { RegionalService, HeadquarterService, CompanyCodeService } from 'app/services/data-services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RegionalRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppDatatableModule,
    PaginationModule,
    TabsModule
  ],
  declarations: [RegionalComponent, RegionalEditComponent, RegionalDetailComponent],
  providers: [
    RegionalService,
    HeadquarterService,
    CompanyCodeService,
    TextToSlugPipe
  ]
})
export class RegionalModule { }
