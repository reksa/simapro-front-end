import { CompanyCodeService } from 'app/services/data-services/company-code.service';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { RegionalService } from 'app/services/data-services';
import { CompanyCode, Regional } from 'app/models';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-regional-detail',
  templateUrl: './regional-detail.component.html',
  styleUrls: ['./regional-detail.component.scss']
})
export class RegionalDetailComponent implements OnInit {
  regional = new Regional();
  companyCodes: CompanyCode[] = [];
  showCompanyCodes: CompanyCode[] = [];
  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  root = '/master-data/organisasi/';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private regionalService: RegionalService,
    private slugPipe: TextToSlugPipe,
    private companyCodeService: CompanyCodeService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idRegional'), 10);
      this.regionalService.getById(id).subscribe(regional => {
        this.spinner.hide();
        this.regional = regional;
        this.companyCodes = regional.company_code;
        this.filterData();
      });
    });
  }

  filterData() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.showCompanyCodes = [];
    this.countFiltered = 0;

    this.companyCodes.map((companyCode: CompanyCode, index: number, companyCodes: CompanyCode[]) => {
      if (regexp.test(companyCode.kode_company_code.toLowerCase()) || regexp.test(companyCode.nama_company_code.toLowerCase())) {
        this.showCompanyCodes.push(companyCode);
        this.countFiltered++;
      }
    });

    this.from = ((this.page - 1) * this.dataPerPage) + 1;
    this.to = (this.page * this.dataPerPage) <= this.countFiltered ? (this.page * this.dataPerPage) : this.countFiltered;
    this.showCompanyCodes = this.showCompanyCodes.slice(this.from - 1, this.to);
    this.getPages();
  }

  filterDataP() {
    this.page = 1;
    this.filterData();
  }

  getPages() {
    this.pages = [];
    const length = Math.ceil(this.countFiltered / this.dataPerPage);
    for (let i = 0; i < length; i++) {
      this.pages[i] = i + 1;
    }
  }

  setPage(page) {
    this.page = page;
    this.filterData();
  }

  lastPage() {
    this.page = this.pages[this.pages.length - 1];
    this.filterData();
  }

  edit() {
    const nama = this.slugPipe.transform((this.regional.nama_regional || ''));
    this.router.navigate([`${this.root}regional/edit/${this.regional.id_regional}/${nama}`]);
    return false;
  }

  showCompanyCode(data: any) {
    const nama = this.slugPipe.transform(data.nama_company_code);
    this.router.navigate([`${this.root}company-code/show/${data.id_company_code}/${nama}`]);
    return false;
  }

  deleteCompanyCode(companyCode: CompanyCode) {
    if (confirm(`Ingin menonaktifkan ${companyCode.nama_company_code} ?`)) {
      companyCode.is_active = false;
      this.companyCodeService.save(companyCode)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          companyCode.is_active = false;
        }
      });
    }
    return false;
  }

  undeleteCompanyCode(companyCode: CompanyCode) {
    if (confirm(`Ingin mengaktifkan ${companyCode.nama_company_code} ?`)) {
      companyCode.is_active = true;
      this.companyCodeService.save(companyCode)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          companyCode.is_active = false;
        }
      });
    }
    return false;
  }

  back() {
    this._location.back();
    return false;
  }
}

