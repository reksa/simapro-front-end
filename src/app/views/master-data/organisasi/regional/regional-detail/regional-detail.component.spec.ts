import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalDetailComponent } from './regional-detail.component';

describe('RegionalDetailComponent', () => {
  let component: RegionalDetailComponent;
  let fixture: ComponentFixture<RegionalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
