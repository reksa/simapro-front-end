import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { RegionalService, HeadquarterService } from 'app/services/data-services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Regional, Headquarter } from 'app/models';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-regional-edit',
  templateUrl: './regional-edit.component.html',
  styleUrls: ['./regional-edit.component.scss']
})
export class RegionalEditComponent implements OnInit {
  regional = new Regional();
  isSubmitted = false;
  title = '';
  regionals: Regional[] = [];
  headquarters: Headquarter[] = [];
  myForm: FormGroup;
  isEdit = true;
  root = 'master-data/organisasi';
  act = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _fb: FormBuilder,
    private _location: Location,
    private regionalService: RegionalService,
    private headquarterService: HeadquarterService,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.headquarterService.getAll().subscribe(headquarters => {
      this.headquarters = headquarters;
      this.regional.id_head_quarters = headquarters.length ? headquarters[0].id_head_quarters : null;
    });

    this.myForm = this._fb.group({
      nama_regional: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(100)
        ]
      ],
      id_head_quarters: ['', [Validators.required]]
    });
    this.spinner.show();
    this.regionalService.getAll().subscribe(regionals => {
      this.regionals = regionals;
      this.route.url.subscribe(url => {
        this.spinner.show();
        this.act = url[0].path;
        if (this.act === 'edit') {
          this.prepareEdit();
        } else if (this.act === 'show') {
          this.prepareEdit();
          this.isEdit = false;
          this.myForm.get('kode_regional').disable();
          this.myForm.get('nama').disable();
          this.myForm.get('nama_regional').disable();
          this.myForm.get('id_headquarter').disable();
        } else {
          this.route.queryParamMap.subscribe(queryParams => {
            const id_head_quarters = queryParams.get('headquarter') || null;
            if(id_head_quarters) {
              this.regional.id_head_quarters = parseInt(id_head_quarters);
            }
            this.prepareAdd();
          })
        }
      });
    });
  }

  prepareAdd() {
    this.title = 'Tambah Regional';
    this.spinner.hide();
  }

  prepareEdit() {
    this.title = 'Ubah Regional';
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      this.spinner.hide();
      const idRegional = parseInt(params.get('idRegional'), 10);
      this.regionalService.getById(idRegional).subscribe(regional => {
        this.regional = regional;
      });
    });
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  edit(data: Regional) {
    const nama = this.slugPipe.transform(data.nama_regional);
    this.router.navigate([`${this.root}regional/edit/${data.id_regional}/${nama}`]);
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.regionalService.save(this.regional).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

}
