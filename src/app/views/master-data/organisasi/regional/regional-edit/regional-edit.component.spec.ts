import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionalEditComponent } from './regional-edit.component';

describe('RegionalEditComponent', () => {
  let component: RegionalEditComponent;
  let fixture: ComponentFixture<RegionalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
