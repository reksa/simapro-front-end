import { Regional } from 'app/models';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegionalService } from 'app/services/data-services';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { Component, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-regional',
  templateUrl: './regional.component.html',
  styleUrls: ['./regional.component.scss']
})
export class RegionalComponent implements OnInit {
  tab = 'regional';
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  operation: string;
  isLoading = true;
  root = 'master-data/organisasi/';

  public tableConfig = new AppTableConfig();

  constructor(
    private regionalService: RegionalService,
    private route: ActivatedRoute,
    private router: Router,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));

      this.dataSetting.query = params.get('query');

      return this.regionalService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root + this.tab, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(data: Regional) {
    const nama = this.slugPipe.transform(data.nama_regional);
    this.router.navigate([`${this.root}regional/show/${data.id_regional}/${nama}`]);
    return false;
  }

  edit(data: Regional) {
    const nama = this.slugPipe.transform(data.nama_regional);
    this.router.navigate([`${this.root}regional/edit/${data.id_regional}/${nama}`]);
    return false;
  }

  delete(data: Regional) {
    if (confirm(`Ingin nonaktifkan ${data.nama_regional} ?`)) {
      data.is_active = false;
      this.regionalService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = true;
        }
      });
    }
    return false;
  }

  undelete(data: Regional) {
    if (confirm(`Ingin mengaktifkan ${data.nama_regional} ?`)) {
      data.is_active = true;
      this.regionalService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = false;
        }
      });
    }
    return false;
  }

  chooseTab(tab) {
    this.router.navigate([`${this.root}${tab}`]);
    return false;
  }
}
