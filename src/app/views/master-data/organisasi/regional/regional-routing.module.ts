import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { RegionalDetailComponent } from './regional-detail/regional-detail.component';
import { RegionalEditComponent } from './regional-edit/regional-edit.component';
import { RegionalComponent } from './regional/regional.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: RegionalComponent, canActivate: [AuthGuard, PermissionGuard]},
  { path: 'add', component: RegionalEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idRegional/:namaRegional', component: RegionalDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idRegional/:namaRegional', component: RegionalEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionalRoutingModule { }
