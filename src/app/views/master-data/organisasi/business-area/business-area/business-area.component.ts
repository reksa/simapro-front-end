import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { BusinessArea } from 'app/models';
import { URLSearchParams } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { CompanyCodeService, BusinessAreaService } from 'app/services/data-services';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-business-area',
  templateUrl: './business-area.component.html',
  styleUrls: ['./business-area.component.scss']
})
export class BusinessAreaComponent implements OnInit {
  tab = 'business-area';
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  operation: string;
  isLoading = true;
  root = 'master-data/organisasi/';

  public tableConfig = new AppTableConfig();

  constructor(
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));

      this.dataSetting.query = params.get('query');
      
      return this.businessAreaService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root + this.tab, {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(data: any) {
    const nama = this.slugPipe.transform(data.nama_business_area);
    this.router.navigate([`${this.root}business-area/show/${data.id_business_area}/${nama}`]);
    return false;
  }

  edit(data: any) {
    const nama = this.slugPipe.transform(data.nama_business_area);
    this.router.navigate([`${this.root}business-area/edit/${data.id_business_area}/${nama}`]);
    return false;
  }

  delete(businessArea: BusinessArea) {
    if (confirm(`Ingin menonaktifkan ${businessArea.nama_business_area} ?`)) {
      businessArea.is_active = false;
      this.businessAreaService.save(businessArea)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          businessArea.is_active = false;
        }
      });
    }
    return false;
  }

  undelete(businessArea: BusinessArea) {
    if (confirm(`Ingin mengaktifkan ${businessArea.nama_business_area} ?`)) {
      businessArea.is_active = true;
      this.businessAreaService.save(businessArea)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          businessArea.is_active = false;
        }
      });
    }
    return false;
  }

  chooseTab(tab) {
    this.router.navigate([`${this.root}${tab}`]);
    return false;
  }
}
