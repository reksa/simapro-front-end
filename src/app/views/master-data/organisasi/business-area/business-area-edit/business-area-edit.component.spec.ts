import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAreaEditComponent } from './business-area-edit.component';

describe('BusinessAreaEditComponent', () => {
  let component: BusinessAreaEditComponent;
  let fixture: ComponentFixture<BusinessAreaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessAreaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAreaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
