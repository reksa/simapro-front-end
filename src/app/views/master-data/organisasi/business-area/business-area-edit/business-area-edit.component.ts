import { SelectComponent } from 'ng2-select';
import { ViewChild } from '@angular/core';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { BusinessAreaValidator } from 'app/validators/business-area-validator';
import { Observable } from 'rxjs/Rx';
import { CompanyCodeService, BusinessAreaService } from 'app/services/data-services';
import { FormBuilder, FormGroup, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { BusinessArea, CompanyCode } from 'app/models';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-business-area-edit',
  templateUrl: './business-area-edit.component.html',
  styleUrls: ['./business-area-edit.component.scss']
})
export class BusinessAreaEditComponent implements OnInit {
  businessArea = new BusinessArea();
  isSubmitted = false;
  isEdit = true;
  title = '';
  companyCodes: CompanyCode[] = [];
  myForm: FormGroup;
  root = 'master-data/organisasi/';
  items = [];
  @ViewChild('selectCC') selectCC: SelectComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _fb: FormBuilder,
    private _location: Location,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private businessAreaValidator: BusinessAreaValidator,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.spinner.hide();
      this.companyCodes = companyCodes;
      this.items = companyCodes.map(companyCode => {
        return {id: companyCode, text: `${companyCode.kode_company_code} - ${companyCode.nama}`}
      })
      this.route.url.subscribe(url => {
        const act = url[0].path;
        if (act === 'edit') {
          this.title = 'Ubah Business Area';
          this.prepareEdit();
        } else if (act === 'show') {
          this.title = 'Detail Business Area';
          this.prepareEdit();
          this.isEdit = false;
        } else {
          this.route.queryParamMap.subscribe(queryParams => {
            const id_company_code = queryParams.get('companyCode') || '0';
            this.companyCodes.forEach(cc => {
              if(cc.id_company_code === parseInt(id_company_code)) {
                this.businessArea.company_code = cc;
              }
            });
          });
          this.prepareAdd();
        }
      });
    });

    this.myForm = this._fb.group({
      nama_business_area: ['', 
        [Validators.required, Validators.minLength(4), Validators.maxLength(100)]
      ],
      kode_business_area: ['',
        [Validators.required, Validators.minLength(4), Validators.maxLength(4)],
        [this.businessAreaValidator.uniqueKodeBusinessArea.bind(this.businessAreaValidator)]
      ],
      companyCode: ['', [Validators.required]]
    });
  }

  prepareAdd() {
    this.title = 'Tambah Business Area';
    this.route.paramMap.subscribe(params => {
      let idCompanyCode: any = params.get('idCompanyCode');
      if (idCompanyCode !== null) {
        idCompanyCode = parseInt(idCompanyCode, 10);
        this.businessArea.company_code = <CompanyCode> ObjectComparator.findById(this.companyCodes, idCompanyCode, 'id_company_code');
      }
    });
  }

  prepareEdit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const idBusinessArea = parseInt(params.get('idBusinessArea'), 10);
      this.businessAreaService.getById(idBusinessArea).subscribe(businessArea => {
        this.spinner.hide();
        this.businessArea = businessArea;
        this.businessArea.company_code = <CompanyCode> ObjectComparator.findById(this.companyCodes,
                                          businessArea.company_code.id_company_code,
                                          'id_company_code');
        this.businessAreaValidator.id = this.businessArea.id_business_area;
        this.items.forEach((item, index) => {
          if(item.id.id_company_code === businessArea.id_company_code && this.selectCC) {
            this.selectCC.active = [item];
          }
        });
      });
    });
  }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.businessArea.id_company_code = this.businessArea.company_code.id_company_code;
    this.businessAreaService.save(this.businessArea)
      .subscribe(res => {
        this.spinner.hide();
        
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this._location.back();
        } else {
          this.isSubmitted = false;
        }
      });
  }

  edit() {
    const nama = this.slugPipe.transform((this.businessArea.nama_business_area || ''));
    this.router.navigate([`${this.root}business-area/edit/${this.businessArea.id_business_area}/${nama}`]);
    return false;
  }

  log(data: any) {
    console.log(data);
  }

  uniqueKodeBusinessArea(control: FormControl, service: BusinessAreaService): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return service.isUnique({kode_business_area: control.value})
      .map(res => {
        return !res.unique ? {isUsed: true} : null;
      });
  }

  refreshValue(value: any):void {
    this.myForm.get('companyCode').setValue(value.id);
    this.myForm.get('companyCode').updateValueAndValidity();
  }

  typed(e: any) {
    this.myForm.get('companyCode').markAsTouched();
    this.myForm.get('companyCode').markAsDirty();
  }

  selected(value: any) {
    console.log(value);
  }

  removed(value: any) {
    console.log(value);
  }

}
