import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { BusinessAreaComponent } from './business-area/business-area.component';
import { BusinessAreaEditComponent } from './business-area-edit/business-area-edit.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: BusinessAreaComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add', component: BusinessAreaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add/:idCompanyCode', component: BusinessAreaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idBusinessArea/:namaBusinessArea', component: BusinessAreaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idBusinessArea/:namaBusinessArea', component: BusinessAreaEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessAreaRoutingModule { }
