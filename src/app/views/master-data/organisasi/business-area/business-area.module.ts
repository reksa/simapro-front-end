import { SelectModule } from 'ng2-select';
import { BusinessAreaValidator } from 'app/validators';
import { BusinessAreaEditComponent } from './business-area-edit/business-area-edit.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessAreaRoutingModule } from './business-area-routing.module';
import { BusinessAreaService, CompanyCodeService } from 'app/services/data-services';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { BusinessAreaComponent } from './business-area/business-area.component';
import { PaginationModule } from 'app/components/pagination/pagination.module';

@NgModule({
  imports: [
    CommonModule,
    BusinessAreaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    SelectModule
  ],
  providers: [
    BusinessAreaService, CompanyCodeService, ObjectComparator,
    BusinessAreaValidator
  ],
  declarations: [
    BusinessAreaEditComponent,
    BusinessAreaComponent
  ]
})
export class BusinessAreaModule { }
