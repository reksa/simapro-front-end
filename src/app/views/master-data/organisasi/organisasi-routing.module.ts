import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'company-code', pathMatch: 'full'},
    { path: 'headquarter', loadChildren: './headquarter/headquarter.module#HeadquarterModule' },
    { path: 'regional', loadChildren: './regional/regional.module#RegionalModule' },
    { path: 'company-code', loadChildren: './company-code/company-code.module#CompanyCodeModule' },
    { path: 'business-area', loadChildren: './business-area/business-area.module#BusinessAreaModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisasiRoutingModule { }
