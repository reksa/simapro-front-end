import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OrganisasiRoutingModule } from './organisasi-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';

@NgModule({
  imports: [
    CommonModule,
    OrganisasiRoutingModule,
    FormsModule,
    PaginationModule,
  ],
  declarations: [],
  providers: []
})
export class OrganisasiModule { }
