import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManajemenPenggunaRoutingModule } from './manajemen-pengguna-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ManajemenPenggunaRoutingModule
  ],
  declarations: []
})
export class ManajemenPenggunaModule { }
