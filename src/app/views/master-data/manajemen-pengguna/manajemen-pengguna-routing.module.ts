import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'pengguna', pathMatch: 'full' },
  { path: 'pengguna', loadChildren: './pengguna/pengguna.module#PenggunaModule' },
  { path: 'role', loadChildren: './role/role.module#RoleModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManajemenPenggunaRoutingModule { }
