import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenggunaDetailComponent } from './pengguna-detail.component';

describe('PenggunaDetailComponent', () => {
  let component: PenggunaDetailComponent;
  let fixture: ComponentFixture<PenggunaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenggunaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenggunaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
