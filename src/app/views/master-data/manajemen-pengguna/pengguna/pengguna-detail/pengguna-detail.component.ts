import { PendayagunaanAset } from './../../../../../models/pendayagunaan-aset.model';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Pengguna, Role, AsetTanah, AsetProperti, PotensiAset, Anggaran, Realisasi } from 'app/models';
import { Component, OnInit } from '@angular/core';
import { PenggunaService } from 'app/services/data-services';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { DataSetting, AppTableConfig } from '../../../../../components/pagination/page/page.component';

@Component({
  selector: 'app-pengguna-detail',
  templateUrl: './pengguna-detail.component.html',
  styleUrls: ['./pengguna-detail.component.scss']
})
export class PenggunaDetailComponent implements OnInit {
  pengguna: Pengguna = new Pengguna();
  title = ' ';
  uploadURL = environment.upload_url;
  fotoDir = `${this.uploadURL}/pengguna/`;
  root = 'master-data/manajemen-pengguna/pengguna/';
  self: Pengguna;
  activities = {
    data: []
  };
  fragment: string;
  pageData: Array<Object> = [];
  users: Array<Pengguna> = [];
  user: Pengguna = new Pengguna();
  dataSetting = new DataSetting();
  modalTitle: string;
  operation: string;
  isLoading = true;
  action = 'showAll';

  isUserProfile = false;
  pageHit: number;

  public tableConfig = new AppTableConfig();


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private penggunaService: PenggunaService,
    private slugPipe: TextToSlugPipe
  ) {
    this.self = this.penggunaService.getCurrentUser();
  }

  ngOnInit() {
    this.pageHit = 0;
    this.route.url.subscribe(url => {
      this.isUserProfile = url.length && url[0].path ==='profile' ? true : false;
      if(this.isUserProfile) {
        this.route.fragment.subscribe(fragment => {
          this.pageHit++;
          this.fragment = fragment;
          const me = this.penggunaService.getCurrentUser();
          this._getLogActivity(me.id_pengguna);
          this.penggunaService.getById(me.id_pengguna).subscribe( pengguna => {
            this.pengguna = pengguna;
            this.title = 'Profil';
            this.root = `user/profile`;
          });
        })
      } else {
        Observable.combineLatest(this.route.paramMap, this.route.fragment)
        .subscribe( params => {
          this.pageHit++;
          const id = parseInt(params[0].get('idPengguna'), 10);
          this.fragment = params[1];
          this._getLogActivity(id);
          this.penggunaService.getById(id).subscribe( pengguna => {
            this.pengguna = pengguna;
            this.title = pengguna.nama;
            this._setRoot();
          });
        });
      }
    });
  }

  edit() {
    const nama = this.slugPipe.transform(this.pengguna.nama || '')
    this.router.navigate([`master-data/manajemen-pengguna/pengguna/edit/${this.pengguna.id_pengguna}/${nama}`]);
    return false;
  }

  showRole(role: Role) {
    const nama = this.slugPipe.transform(role.nama || '')
    this.router.navigate([`master-data/manajemen-pengguna/role/show/${role.id_role}/${nama}`]);
    return false;
  }

  showActivity(activity) {
    return false;
  }

  back() {
    if (this.fragment === 'activity') {
      this.router.navigate([this.root]);
    } else {
      history.go((-1 * this.pageHit));
    }
  }

  private _getLogActivity(id: number) {
    const params = new URLSearchParams();
    params.set('dataPerPage', '10');
    if(this.fragment === 'activity') {
      this.route.queryParamMap.switchMap(params => {
        const parameters = new URLSearchParams();
        parameters.append('query', params.get('query'));
        parameters.append('page', params.get('page'));
        parameters.append('dataPerPage', params.get('dataPerPage'));
        parameters.append('orderBy', params.get('orderBy'));
        parameters.append('orderDirection', params.get('orderDirection'));
        return this.penggunaService.getLogActivity(id, parameters);
      }).subscribe(res => {
        if(res.current_page == 1) {
          this.activities = res;
        } new Activity
        this.pageData = res.data;
        this.tableConfig.current = res.current_page;
        this.tableConfig.limit = res.per_page;
        this.tableConfig.from = res.from;
        this.tableConfig.to = res.to;
        this.tableConfig.total = res.total;
        this.tableConfig.totalPages = res.last_page;
        this.tableConfig.generatePages();
        
        this.dataSetting.dataPerPage = res.per_page;

        this.isLoading = false;
      });
    } else {
      this.penggunaService.getLogActivity(id, params).subscribe(activities => {
        this.activities = activities;
      });
    }
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.pageHit++;
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting,
      fragment: this.fragment
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  getNama(obj: Activity) {
    let nama = '';
    if (obj.logable_type.toLocaleLowerCase().indexOf('tanah')) {
      const o = (obj.logable as AsetTanah);
      nama = o.detail_aktif && o.detail_aktif.length ? o.detail_aktif[0].nama_tanah : (o.detail_non_valid && o.detail_non_valid[0] ? o.detail_non_valid[0].nama_tanah : '-');
    } else if(obj.logable_type.toLocaleLowerCase().indexOf('properti')) {
      const o = (obj.logable as AsetProperti);
      nama = o.detail_aktif && o.detail_aktif.length ? o.detail_aktif[0].nama_properti : (o.detail_non_valid && o.detail_non_valid[0] ? o.detail_non_valid[0].nama_properti : '-');
    } else if(obj.logable_type.toLocaleLowerCase().indexOf('potensi')) {
      const o = (obj.logable as PotensiAset);
      nama = o.potensiable ? (o.potensiable.detail_aktif[0].nama_properti || o.potensiable.detail_aktif[0].nama_tanah) : '-';
    } else if(obj.logable_type.toLocaleLowerCase().indexOf('pendayagunaan')) {
      const o = (obj.logable as PendayagunaanAset);
      nama = o.pendayagunaanable ? (o.pendayagunaanable.detail_aktif[0].nama_properti || o.pendayagunaanable.detail_aktif[0].nama_tanah) : '-';
    } else if(obj.logable_type.toLocaleLowerCase().indexOf('anggaran')) {
      const o = (obj.logable as Anggaran);
      nama = o.nama_anggaran;
    } else if(obj.logable_type.toLocaleLowerCase().indexOf('realisasi')) { 
      const o = (obj.logable as Realisasi);
      nama = o.anggaran ? o.anggaran.nama_anggaran : '-';
    } else {
      nama = 'Tidak Terdefinisi'; 
    }
    return nama;
  }

  private _setRoot() {
    const nama = this.slugPipe.transform(this.pengguna.nama);
    this.root = `master-data/manajemen-pengguna/pengguna/show/${this.pengguna.id_pengguna}/${nama}`;
    return false;
  }

}

class Activity{
  record_on: Date;
  keterangan: string;
  api_access: string;
  geo_ip : GeoIP;
  id_log: number;
  id_pengguna: number;
  ip_address: string;
  logable: {};
  logable_id: number;
  logable_type: string;
  pengguna: Pengguna;
}

class GeoIP {
  ip: string;
  iso_code: string;
  country: string;
  city: string;
  state: string;
  state_name: string;
  postal_code: string;
  lat: number;
  lon: number;
  timezone: string;
  continent: string;
  currency: string;
  default: boolean;
  cached: boolean;
}
