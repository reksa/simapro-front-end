import { PenggunaEditComponent } from './pengguna-edit/pengguna-edit.component';
import { PenggunaDetailComponent } from './pengguna-detail/pengguna-detail.component';
import { PenggunaComponent } from './pengguna/pengguna.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

const routes: Routes = [
  { path: '', component: PenggunaComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idPengguna/:nama', component: PenggunaDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idPengguna/:nama', component: PenggunaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add/ldap', component: PenggunaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add/non-ldap', component: PenggunaEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'profile', component: PenggunaDetailComponent, canActivate: [AuthGuard] },
  { path: 'profile/edit', component: EditProfileComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PenggunaRoutingModule { }
