import { AbstractControl, ValidationErrors, ValidatorFn, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, TabsetComponent } from 'ngx-bootstrap';
import { SelectComponent } from 'ng2-select';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Location } from '@angular/common';

import { PenggunaService, RoleService, CompanyCodeService, BusinessAreaService } from 'app/services/data-services';

import { Pengguna, Role, CompanyCode, BusinessArea } from 'app/models';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-pengguna-edit',
  templateUrl: './pengguna-edit.component.html',
  styleUrls: ['./pengguna-edit.component.scss']
})
export class PenggunaEditComponent implements OnInit {
  files: FileList;
  foto: File;
  myForm: FormGroup;
  pengguna: Pengguna = new Pengguna();
  companyCodes: CompanyCode[] = [];
  businessAreas: BusinessArea[] = [];
  roles: Role[] = [];
  rolesToShow: Role[] = [];
  filter = '';
  title = '';
  isSubmitted = false;
  isEdit = true;
  @ViewChild('fileInput') fileInput;
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent; 
  @ViewChild('template') template: TemplateRef<any>;
  @ViewChild('tabset') tabset: TabsetComponent;
  itemsCC = [];
  itemsBA = [];
  modalRef: BsModalRef;
  penggunaLDAP: Pengguna;
  qUsername: string;
  cancelable = false;
  konfirmasi = '';
  isSearching = false;
  act;
  preview = '';
  uploadURL = environment.upload_url;
  fotoDir = `${this.uploadURL}/pengguna/`;
  old = '';
  isLDAP = false;
  formLDAP: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private penggunaService: PenggunaService,
    private roleService: RoleService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private _fb: FormBuilder,
    private _location: Location,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getRole();
    this.getCompanyCode();

    this.myForm = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]+')]],
      email: ['', [Validators.required, Validators.maxLength(100), Validators.email]],
      foto: ['', []],
      companyCode: ['', [Validators.required]],
      businessArea: ['', [Validators.required]],
      filter: ['', []],
      username: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]], // 50
      password: ['', [
          Validators.minLength(6),
          Validators.maxLength(20),
        ]
      ], // 50
      konfirmasi: ['', [matchPassword(this.pengguna)]], // 50
      company: ['', [Validators.maxLength(50)]], // 50
      department: ['', [Validators.maxLength(50)]], // 50
      title: ['', [Validators.maxLength(50)]], // 50
      employee_number: ['', [Validators.maxLength(50)]], // 50
    });
    this.route.url.subscribe(uri => {
      this.act = uri[0].path;
      const tipe = uri[1] ? uri[1].path : null;
      if (this.act === 'add' && tipe === 'ldap') {
        this.formLDAP = this._fb.group({
          nama: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9 -]+\\\\[a-zA-Z0-9.]+')]]
        });
        this.isLDAP = true;
        this.title = 'Tambah Pengguna LDAP';  
        this.showModal(this.template, false);      
        this.penggunaLDAP = new Pengguna();
        this.myForm.get('username').disable();
        this.myForm.get('password').disable();
        this.myForm.get('konfirmasi').disable();
        this.pengguna.active_directory = true;
      } else if (this.act === 'add' && tipe === 'non-ldap') {
        this.title = 'Tambah Pengguna Non-LDAP';
        this.myForm.get('password').enable();
        this.myForm.get('password').setValidators(Validators.required);
        this.myForm.get('konfirmasi').enable();
      } else if (this.act === 'edit') {
        this.title = 'Ubah Pengguna';
        this.myForm.get('username').disable();
        this.myForm.get('password').disable();
        this.myForm.get('konfirmasi').disable();
        this.prepareEdit();
      }
    });
  }

  prepareAdd() {
    this.title = 'Tambah';
  }

  prepareEdit() {
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idPengguna'), 10);
      this.penggunaService.getById(id).subscribe(pengguna => {
        this.pengguna = pengguna;
        this.penggunaLDAP = pengguna;
        this.selectCompanyCode();
        this.filterRoles();
        this.myForm.get('konfirmasi').clearValidators();
        this.myForm.get('konfirmasi').setValidators(matchPassword(this.pengguna));
        this.old = this.pengguna.foto;
        this.pengguna.foto = null;
      });
    });
  }

  addFile(): void {
    const fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
        const fileToUpload = fi.files[0];
        this.penggunaService
            .uploadFoto(fileToUpload)
            .subscribe(res => {
                // console.log(res);
            });
        }
    }

  cancel(hideModal = false) {
    if(hideModal && this.cancelable){
      this.closeModal();
    }else if (confirm('Ingin membatalkan?')) {
      if (this.modalRef) {
        this.closeModal();
      }
      this._location.back();
    }
    return false;
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file: File = event.target.files[0];
      if(file.size > environment.max_file_upload_size) {
        alert(`Ukuran foto maksimal ${environment.max_file_upload_size/1000000} MB`);
        this.myForm.get('foto').setValue(this.pengguna.foto);
        return false;
      }
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.pengguna.foto = {
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        };
        this.myForm.get('foto').setValue(this.pengguna.foto);
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    this.pengguna.foto = null;
    this.myForm.get('foto').setValue(null);
    this.preview = null;
    return false;
  }

  save() {
    if(this.act !== 'edit') {
      this.pengguna.active_directory = this.penggunaLDAP ? true : false;
    }
    this.pengguna.foto = this.pengguna.foto || this.old;
    this.closeModal();
    this.isSubmitted = true;
    this.penggunaService.save(this.pengguna).subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
      this._location.back();
    });
  }

  isGranted(role: Role) {
    return ObjectComparator.indexOf(this.pengguna.role, role, 'id_role') !== -1;
  }

  toggleRole(role: Role) {
    const index = ObjectComparator.indexOf(this.pengguna.role, role, 'id_role');
    if (index === -1) {
      this.pengguna.role.push(role);
    } else {
      this.pengguna.role.splice(index, 1);
    }
  }

  filterRoles() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.rolesToShow = [];

    this.roles.map((role: Role, index: number, roles: Role[]) => {
      if (regexp.test(role.nama.toLowerCase()) || 
          (role.organisasi_level && regexp.test(role.organisasi_level.deskripsi.toLowerCase()))) {
        this.rolesToShow.push(role);
      }
    });
  }

  getFiles(event) {
    this.files = event.target.files;
    this.logForm();
  }

  getRole() {
    this.roleService.getAll().subscribe(roles => {
      this.roles = roles;
      this.filterRoles();
    });
  }

  getCompanyCode() {
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.companyCodes = companyCodes;
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode, text: `${companyCode.kode_company_code} - ${companyCode.nama}`};
      });
      this.selectCompanyCode();
    });
  }

  getBusinessArea() {
    const id = this.pengguna.id_company_code;
    this.businessAreaService.getByIdCompanyCode(id).subscribe(businessAreas => {
      this.businessAreas = businessAreas;
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${businessArea.nama_business_area}`};
      });
      this.selectBusinessArea();
    });
  }

  selectCompanyCode() {
    for(let i = 0; i < this.itemsCC.length; i++) {
      if (this.pengguna.business_area && this.itemsCC[i].id.id_company_code === this.pengguna.business_area.id_company_code) {
        this.selectCC.active = [this.itemsCC[i]];
        this.refreshValue(this.itemsCC[i]);
        this.selected('');
      }
    }
  }

  selectBusinessArea() {
    for(let i = 0; i < this.itemsBA.length; i++) {
      if (this.pengguna.business_area && this.itemsBA[i].id === this.pengguna.business_area.id_business_area) {
        this.selectBA.active = [this.itemsBA[i]]
        this.refreshValueBA(this.itemsBA[i]);
      }
    }
  }

  logForm(event?) {
    // console.log(this.files);
  }

  log(obj){
    // console.log(obj);
  }

  refreshValue(value: any):void {
    this.myForm.get('companyCode').setValue(value.id);
    this.pengguna.id_company_code = value.id.id_company_code;
    this.pengguna.id_regional = value.id.id_regional;
    this.myForm.get('company').setValue(value.id.nama);
    this.myForm.get('companyCode').updateValueAndValidity();
  }

  typed(e: any) {
    this.myForm.get('companyCode').markAsTouched();
    this.myForm.get('companyCode').markAsDirty();
  }

  selected(value: any) {
    // console.log(value);
    this.getBusinessArea();
  }

  removed(value: any) {
    // console.log(value);
  }

  refreshValueBA(value: any):void {
    this.myForm.get('businessArea').setValue(value.id);
    this.myForm.get('businessArea').updateValueAndValidity();
  }

  typedBA(e: any) {
    this.myForm.get('businessArea').markAsTouched();
    this.myForm.get('businessArea').markAsDirty();
  }

  selectedBA(value: any) {
    // console.log(value);
  }

  removedBA(value: any) {
    // console.log(value);
  }

  showModal(template: TemplateRef<any>, cancelable?: boolean) {
    this.modalRef = this.modalService.show(template, {
      ignoreBackdropClick: true,
      keyboard: false
    });
    this.cancelable = cancelable || false;
    return false;
  }

  closeModal() {
    if(this.modalRef) {
      this.modalRef.hide();
    }
    return false;
  }

  search() {
    this.isSearching = true;
    this.penggunaService.getFromLDAP(this.qUsername).subscribe(res => {
      if(res.message) {
        alert(res.message)
      }
      if(!res.success && res.success != false) {
        this.penggunaLDAP = res;
        this.penggunaLDAP.role = [];
      }
      this.isSearching = false;
    });
  }

  setPengguna() {
    const role = this.pengguna.role || [];
    this.pengguna = this.penggunaLDAP;
    this.pengguna.role = role;
    this.closeModal();
  }

  chooseTab(index: number) {
    this.tabset.tabs[index].active = true;
  }

}

function matchPassword(pengguna: Pengguna): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if(pengguna.password && control.value !== pengguna.password) {
      return {unmatch: true};
    } else {
      return null;
    }
  }
}
