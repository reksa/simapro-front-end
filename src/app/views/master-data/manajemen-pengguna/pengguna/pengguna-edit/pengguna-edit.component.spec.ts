import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenggunaEditComponent } from './pengguna-edit.component';

describe('PenggunaEditComponent', () => {
  let component: PenggunaEditComponent;
  let fixture: ComponentFixture<PenggunaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenggunaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenggunaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
