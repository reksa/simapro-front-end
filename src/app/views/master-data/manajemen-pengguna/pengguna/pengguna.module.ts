import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PenggunaRoutingModule } from './pengguna-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';

import { PenggunaDetailComponent } from './pengguna-detail/pengguna-detail.component';
import { PenggunaEditComponent } from './pengguna-edit/pengguna-edit.component';
import { PenggunaComponent } from './pengguna/pengguna.component';

import { PenggunaService, RoleService, CompanyCodeService, BusinessAreaService } from 'app/services/data-services';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { SelectModule } from 'ng2-select';
import { ModalModule, TabsModule } from 'ngx-bootstrap';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

@NgModule({
  imports: [
    CommonModule,
    PenggunaRoutingModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    ModalModule,
    TabsModule
  ],
  providers: [
    PenggunaService, RoleService, ObjectComparator, CompanyCodeService, BusinessAreaService, TextToSlugPipe
  ],
  declarations: [PenggunaComponent, PenggunaDetailComponent, PenggunaEditComponent, EditProfileComponent]
})
export class PenggunaModule { }
