import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { Pengguna } from 'app/models';
import { PenggunaService } from 'app/services/data-services';

@Component({
  selector: 'app-user',
  templateUrl: './pengguna.component.html',
  styleUrls: ['./pengguna.component.scss']
})
export class PenggunaComponent implements OnInit {
  pageData: Array<Object> = [];
  users: Array<Pengguna> = [];
  user: Pengguna = new Pengguna();
  dataSetting = new DataSetting();
  modalTitle: string;
  operation: string;
  isLoading = true;
  action = 'showAll';
  root = 'master-data/manajemen-pengguna/pengguna';
  self: Pengguna;

  public modalRef: BsModalRef;

  public tableConfig = new AppTableConfig();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: PenggunaService,
    private slugPipe: TextToSlugPipe,
    private modalService: BsModalService
  ) {
  }

  ngOnInit() {
    this.self = this.userService.getCurrentUser();
    this.pageData = [];
    this.tableConfig.current = 1;
    this.tableConfig.limit = 5;
    this.tableConfig.from = 0;
    this.tableConfig.to = 0;
    this.tableConfig.total = 0;
    this.tableConfig.totalPages = 0;
    this._showAll();
  }

  private _showAll() {
    this.action = 'showAll';
    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      return this.userService.getPaginate(parameters);
    }).subscribe(res => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();
      
      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  private _show(id: number) {
    this.action = 'show';
    this._getUserById(id).subscribe(user => {
      this.user = user;
    });
  }

  private _add() {
    // this.action = 'add';
    // this.roleService.getAll().subscribe(users => {
    //   this.users = users;
    // });
  }

  private _edit(id: number) {
    this.action = 'edit';
    this._getUserById(id).subscribe(user => {
      this.user = user;
    });
  }

  private _delete(id: number) {
    this.action = 'showAll';
    this._getUserById(id).subscribe(user => {
      this.user = user;
    });
  }

  private _getUserById(id: number) {
    return this.userService.getById(id);
  }

  public show(pengguna: Pengguna) {
    const nama = this.slugPipe.transform(pengguna.nama)
    this.router.navigate([`${this.root}/show/${pengguna.id_pengguna}/${nama}`]);
    return false;
  }

  public add(useLDAP: boolean) {
    this.closeModal();
    if (useLDAP) {
      this.router.navigate([`${this.root}/add/ldap`]);
    } else {
      this.router.navigate([`${this.root}/add/non-ldap`]);
    }
    return false;
  }

  private edit(pengguna: Pengguna) {
    const nama = this.slugPipe.transform(pengguna.nama)
    this.router.navigate([`${this.root}/edit/${pengguna.id_pengguna}/${nama}`]);
    return false;
  }
  
  delete(data: Pengguna) {
    if (confirm(`Ingin nonaktifkan ${data.nama} ?`)) {
      data.is_active = false;
      this.userService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = true;
        }
      });
    }
    return false;
  }

  undelete(data: Pengguna) {
    if (confirm(`Ingin mengaktifkan ${data.nama} ?`)) {
      data.is_active = true;
      this.userService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = false;
        }
      });
    }
    return false;
  }

  showModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-sm'
    });
    return false;
  }

  closeModal() {
    this.modalRef.hide();
  }

}
