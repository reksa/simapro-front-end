import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';
import { ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Pengguna } from 'app/models';
import { TabsetComponent } from 'ngx-bootstrap';
import { PenggunaService } from 'app/services/data-services';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  files: FileList;
  foto: File;
  myForm: FormGroup;
  pengguna: Pengguna = new Pengguna();
  title = '';
  isSubmitted = false;
  isEdit = true;
  @ViewChild('fileInput') fileInput;
  @ViewChild('tabset') tabset: TabsetComponent;
  act;
  uploadURL = environment.upload_url;
  fotoDir = `${this.uploadURL}/pengguna/`;
  old = '';
  isLDAP = false;
  preview;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private penggunaService: PenggunaService,
    private _fb: FormBuilder,
    private _location: Location
  ) {}

  ngOnInit() {
    this.myForm = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('[a-zA-Z ]+')]],
      email: ['', [Validators.required, Validators.maxLength(100), Validators.email]],
      foto: ['', []],
      password: ['', [
          Validators.minLength(6),
          Validators.maxLength(20)
        ]
      ], // 50
      konfirmasi: ['', [matchPassword(this.pengguna)]], // 50
    });
    this.title = 'Ubah Profil';
    this.prepareEdit();
  }

  prepareEdit() {
    this.route.paramMap.subscribe(params => {
      const me = this.penggunaService.getCurrentUser();
      this.penggunaService.getById(me.id_pengguna).subscribe(pengguna => {
        this.pengguna = pengguna;
        if(this.pengguna.active_directory) {
          this.isLDAP = true;
          this.myForm.get('nama').disable()
          this.myForm.get('email').disable();
          this.myForm.get('password').disable();
          this.myForm.get('konfirmasi').disable();
        }
        this.old = this.pengguna.foto;
        this.pengguna.foto = null;
      });
    });
  }

  addFile(): void {
    const fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
        const fileToUpload = fi.files[0];
        this.penggunaService
            .uploadFoto(fileToUpload)
            .subscribe(res => {
                // console.log(res);
            });
        }
    }

  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file: File = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.pengguna.foto = {
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        };
        this.myForm.get('foto').setValue(this.pengguna.foto);
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    this.pengguna.foto = null;
    this.myForm.get('foto').setValue(null);
    this.preview = null;
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.penggunaService.save(this.pengguna).subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        alert('Silahkan Login Kembali');
        this.router.navigate(['auth/logout']);
      } else {
        this.isSubmitted = false;
      }
      this._location.back();
    });
  }

  getFiles(event) {
    this.files = event.target.files;
  }

  log(obj){
    // console.log(obj);
  }

  chooseTab(index: number) {
    this.tabset.tabs[index].active = true;
  }

}

function matchPassword(pengguna: Pengguna): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if(pengguna.password && control.value !== pengguna.password) {
      return {unmatch: true};
    } else {
      return null;
    }
  }
}
