import { TextToSlugPipe } from './../../../../../services/text-to-slug.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Role, Pengguna, Permission } from 'app/models';
import { RoleService, PermissionService, PenggunaService } from 'app/services/data-services';
import { Location } from '@angular/common';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.scss']
})
export class RoleDetailComponent implements OnInit {
  role: Role = new Role();
  users: Array<Pengguna> = [];
  showUsers: Array<Pengguna> = [];
  permissions: Array<Permission>;
  showPermissions: Array<Permission>;
  show = 'all';
  filter = '';
  filterP = '';
  root = 'master-data/manajemen-pengguna/role/'

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private roleService: RoleService,
    private userService: PenggunaService,
    private permissionService: PermissionService,
    private slugPipe: TextToSlugPipe,
    private _location: Location
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((param) => {
      const idRole = parseInt(param.get('idRole'), 10);
      const act = param.get('act');
      this.show = 'all';
      this.roleService.getById(idRole).subscribe( role => {
        this.role = role;
      });

      if (act === 'members') {
        this.show = act;
        this.userService.getAll().subscribe(users => {
          this.users = users;
          this.filterUsers();
        });
      } else if (act === 'permissions') {
        this.show = act;
        this.permissionService.get().subscribe(permissions => {
          this.permissions = permissions;
          this.filterPermissions();
        });
      }
    });
  }

  filterUsers() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.showUsers = []; // this.users.slice();

    this.users.map((user, index, users) => {
      if (ObjectComparator.indexOf(this.role.members, user) === -1 && regexp.test(user.nama.toLowerCase())) {
        this.showUsers.push(user); // users.splice(index, 1);
      }
    });
  }

  filterPermissions() {
    const filter = this.filterP.toLowerCase();
    const regexp = new RegExp(filter);
    this.showPermissions = [];

    this.permissions.map((permission: Permission, index: number, permissions: Permission[]) => {
      if (ObjectComparator.indexOf(this.role.permission, permission) === -1 && regexp.test(permission.nama.toLowerCase())) {
        this.showPermissions.push(permission);
      }
    });
  }

  edit() {
    const nama = this.slugPipe.transform(this.role.nama || '');
    this.router.navigate([`${this.root}edit/${this.role.id_role}/${nama}`]);
    return false;
  }

  showPengguna(pengguna: Pengguna) {
    const nama = this.slugPipe.transform(pengguna.nama || '');
    this.router.navigate([`master-data/manajemen-pengguna/pengguna/show/${pengguna.id_pengguna}/${nama}`]);
    return false;
  }

  public save() {
    this.roleService.save(this.role)
      .subscribe((res) => {
        if (res.success) {
          alert(res.message);
          this.router.navigate(['../'], {
            relativeTo: this.route
          });
        } else {
          alert(res.message);
        }
      });
  }

  public saveMembers() {
    this.userService.saveAll(this.role.members, {id_role: this.role.id_role})
      .subscribe((res) => {
        if (res.success) {
          alert(res.message);
          this.router.navigate(['../'], {
            relativeTo: this.route
          });
        } else if (!res.message) {
          alert(res.message);
        }
      });
  }

  back() {
    this._location.back();
  }

}

class ObjectComparator {
  static compare(obj1: object, obj2: object, id: string = 'id'): boolean {
    return obj1[id] === obj2[id];
  }

  static indexOf(array: object[], obj2: object, id: string = 'id'): number {
    for (let i = 0; i < array.length; i++) {
      if (array[i][id] === obj2[id]) {
        return i;
      }
    }
    return -1;
  }

  static findById(array: Object[], id: string, idName = 'id' ) {
    for (let i = 0; i < array.length; i++) {
      if (array[i][idName] === id) {
        return array[i];
      }
    }
    return null;
  }
}
