import { RoleDetailComponent } from './role-detail/role-detail.component';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { RoleComponent } from './role/role.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';

const routes: Routes = [
  { path: '', component: RoleComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idRole/:namaRole', component: RoleDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  // { path: 'show/:idRole/:namaRole/:act', component: RoleDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  // { path: ':idRole/:act', component: RoleDetailComponent },
  { path: 'add', component: RoleEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idRole/:namaRole', component: RoleEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
