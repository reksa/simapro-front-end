import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { AlertService } from 'app/services/alert.service';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Role, Permission } from 'app/models';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { RoleService, PermissionService } from 'app/services/data-services';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  pageData: Object[] = [];
  role = new Role();
  permissions: Permission[] = [];
  dataSetting = new DataSetting();
  operation: string;
  isLoading = true;
  root = 'master-data/manajemen-pengguna/role/';

  public tableConfig = new AppTableConfig();

  constructor(
    private roleService: RoleService,
    private permissionService: PermissionService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private slugPipe: TextToSlugPipe
  ) {}

  ngOnInit() {
    this.permissionService.get().subscribe(permissions => {
      this.permissions = permissions;

      this.route.url.subscribe(data => {
        this.operation = data.length > 0 ? data[0].path : '';
        const id = data.length > 1 ? parseInt(data[1].path, 10) : 0;
        if (this.operation !== '') {
          // this.modalService.onHide.subscribe((reason: string) => {
          //   this._navigate();
          // });
        }
        // if (this.operation === 'add') {
        //   this.append(this.modalTemplate);
        // } else if (this.operation === 'edit') {
        //   this.roleService.getById(id).subscribe((role) => {
        //     this.role = role;
        //     this.update(role, this.modalTemplate);
        //   })
        // } else if (this.operation === 'delete') {
        //   this.roleService.getById(id).subscribe((role) => {
        //     this.role = role;
        //     this.delete(role);
        //   })
        // }
      });
    }, (response) => {
      this.alertService.alert({
        type: 'info',
        msg: response.error.message
      })
    });

    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));

      return this.roleService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      this.dataSetting.dataPerPage = res.per_page;

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(role: Role) {
    const nama = this.slugPipe.transform(role.nama || '');
    this.router.navigate([`${this.root}show/${role.id_role}/${nama}`]);
    return false;
  }

  edit(role: Role) {
    const nama = this.slugPipe.transform(role.nama || '');
    this.router.navigate([`${this.root}edit/${role.id_role}/${nama}`]);
    return false;
  }

  delete(data: Role) {
    if (confirm(`Ingin nonaktifkan ${data.nama} ?`)) {
      data.is_active = false;
      this.roleService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = true;
        }
      });
    }
    return false;
  }

  undelete(data: Role) {
    if (confirm(`Ingin mengaktifkan ${data.nama} ?`)) {
      data.is_active = true;
      this.roleService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = false;
        }
      });
    }
    return false;
  }

}
