import { forEach } from '@angular/router/src/utils/collection';
import { ViewChild, AfterViewChecked } from "@angular/core";
import { ObjectComparator } from "app/services/object-comparator.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import {
  RoleService,
  PermissionService,
  OrganisasiLevelService
} from "app/services/data-services";
import { Role, Permission, OrganisasiLevel } from "app/models";
import { Location } from "@angular/common";
import { TreeModel, Ng2TreeSettings, TreeComponent, TreeController } from "ng2-tree";

@Component({
  selector: "app-role-edit",
  templateUrl: "./role-edit.component.html",
  styleUrls: ["./role-edit.component.scss"]
})
export class RoleEditComponent implements OnInit, AfterViewChecked {
  myForm: FormGroup;
  isSubmitted = false;
  isEdit = true;
  role = new Role();
  permissions: Permission[] = [];
  permissionsToShow: Permission[] = [];
  organisasiLevels: OrganisasiLevel[] = [];
  filter = "";
  title = "";
  tree: TreeModel = {
    value: "Permission",
    id: 'all',
    settings: {
      cssClasses: {
        expanded: "fa fa-caret-down",
        collapsed: "fa fa-caret-right",
        empty: "fa fa-caret-right disabled",
        leaf: "fa"
      },
      templates: {
        node: '<i class="fa fa-bars"></i>',
        leaf: '<i class="fa fa-hashtag"></i>'
      },
      isCollapsedOnInit: true,
      leftMenu: false,
      rightMenu: false,
      static: true,
      selectionAllowed: false
    },
    loadChildren: callback => {
      this.loadModuls(callback);
    }
  };
  public settings: Ng2TreeSettings = {
    rootIsVisible: true,
    showCheckboxes: true,
    enableCheckboxes: this.isEdit
  };

  @ViewChild('permissionTree') permissionTree: TreeComponent;

  constructor(
    private route: ActivatedRoute,
    private roleService: RoleService,
    private permissionService: PermissionService,
    private _fb: FormBuilder,
    private _location: Location,
    private organisasiLevelService: OrganisasiLevelService
  ) {}

  ngOnInit() {
    this.organisasiLevelService.getAll().subscribe(organisasiLevels => {
      this.organisasiLevels = organisasiLevels;
    });

    this.myForm = this._fb.group({
      nama: [
        "",
        [Validators.required, Validators.minLength(4), Validators.maxLength(50)]
      ],
      id_organisasi_level: ["", [Validators.required]],
      deskripsi: [
        "",
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(255)
        ]
      ],
      permissions: ["", []],
      filter: ["", []],
      is_active: ["", []]
    });

    this.route.url.subscribe(url => {
      const path = url[0].path;
      if (path === "show") {
        this.prepareEdit();
        this.title = "Detail Role";
        this.myForm.get('nama').disable();
        this.myForm.get('id_organisasi_level').disable();
        this.myForm.get('deskripsi').disable();
        this.isEdit = false;
      } else if (path === "edit") {
        this.prepareEdit();
        this.title = "Ubah Role";
      } else {
        this.prepareAdd();
      }
    });
  }

  ngAfterViewChecked() {
    setTimeout(() => {
      const oopNodeController = this.permissionTree.getControllerByNodeId('all');
      if(oopNodeController)
        oopNodeController.expand();
    }, 100);
  }

  loadModuls(callback: Function) {
    this.permissionService.getModul().subscribe(moduls => {
      callback(moduls.map(modul => {
        let expand = 0;
        return {
          value: modul.nama_modul,
          id: `modul-${modul.id_modul}`,
          children: modul.permission.map(p => {
            if(ObjectComparator.indexOf(this.role.permission, p, 'id_permission')!==-1)
              expand++
            return {
              value: p.nama,
              id: p,
              settings: {
                checked: (ObjectComparator.indexOf(this.role.permission, p, 'id_permission')!==-1)
              }
            };
          }),
          settings: {
            isCollapsedOnInit: (expand === 0)
          }
        };
      }));
    });
  }

  prepareAdd() {
    this.title = "Tambah Role";
  }

  prepareEdit() {
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get("idRole"), 10);
      this.roleService.getById(id).subscribe(role => {
        this.role = role;
      });
    });
  }

  isGranted(permission: Permission) {
    return (
      ObjectComparator.indexOf(
        this.role.permission,
        permission,
        "id_permission"
      ) !== -1
    );
  }

  togglePermission(permission: Permission) {
    const index = ObjectComparator.indexOf(
      this.role.permission,
      permission,
      "id_permission"
    );
    if (index === -1) {
      this.role.permission.push(permission);
    } else {
      this.role.permission.splice(index, 1);
    }
  }

  selectPermission(e) {
    const oopNodeController = this.permissionTree.getControllerByNodeId(e.node.id);
    const model = oopNodeController.toTreeModel();
    const isNode = (model.children && model.children.length) || e.node.id === 'all';
    if(isNode && oopNodeController.isCollapsed()) {
      oopNodeController.expand();
      oopNodeController.allowSelection();
    } else if (isNode) {
      oopNodeController.collapse();
      oopNodeController.forbidSelection();
    } else if (!isNode && oopNodeController.isChecked()) {
      oopNodeController.uncheck();
    } else {
      oopNodeController.check();
    }
    oopNodeController.unselect();
  }

  checkPermission(e) {
    const oopNodeController = this.permissionTree.getControllerByNodeId(e.node.id);
    const model = oopNodeController.toTreeModel();
    const isNode = (model.children && model.children.length) || e.node.id === 'all';
    if(isNode) {
      model.children.forEach(leaf => {
        // @ts-ignore
        if(ObjectComparator.indexOf(this.role.permission, leaf.id, 'id_permission') === -1 && leaf.id.id_permission)
        // @ts-ignore
          this.role.permission.push(leaf.id);
      });
    } else {
      // @ts-ignore
      if(ObjectComparator.indexOf(this.role.permission, model.id, 'id_permission') === -1)
      // @ts-ignore
        this.role.permission.push(model.id);
    }
  }

  uncheckPermission(e) {
    const oopNodeController = this.permissionTree.getControllerByNodeId(e.node.id);
    const model = oopNodeController.toTreeModel();
    const isNode = (model.children && model.children.length) || e.node.id === 'all';
    if(isNode) {
      model.children.forEach(leaf => {
        // @ts-ignore
        const index = ObjectComparator.indexOf(this.role.permission, leaf.id, 'id_permission');
        if(index !== -1)
          this.role.permission.splice(index, 1);
      });
    } else {
      // @ts-ignore
      const index = ObjectComparator.indexOf(this.role.permission, model.id, 'id_permission');
      if(index !== -1)
        this.role.permission.splice(index, 1);
    }
  }

  onExpand(e) {
    const oopNodeController = this.permissionTree.getControllerByNodeId(e.node.id);
    const model = oopNodeController.toTreeModel();
    setTimeout(() => {
      model.children.forEach(leaf => {
        // @ts-ignore
        if(ObjectComparator.indexOf(this.role.permission, leaf.id, 'id_permission') !== -1) {
          const leafController = this.permissionTree.getControllerByNodeId(leaf.id);
          leafController.check();
        }
      });
    }, 100);
  }

  cancel() {
    if (confirm("Ingin membatalkan?")) {
      this._location.back();
    }
    return false;
  }

  save() {
    this.isSubmitted = true;
    const oopNodeController = this.permissionTree.getControllerByNodeId('all');
    const model = oopNodeController.toTreeModel();
    this.roleService.save(this.role).subscribe(res => {
      this.isSubmitted = false;
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

  filterPermissions() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.permissionsToShow = [];

    this.permissions.map(
      (permission: Permission, index: number, permissions: Permission[]) => {
        if (regexp.test(permission.nama.toLowerCase())) {
          this.permissionsToShow.push(permission);
        }
      }
    );
  }
}
