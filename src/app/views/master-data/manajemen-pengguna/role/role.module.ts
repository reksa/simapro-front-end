import { TextToSlugPipe } from './../../../../services/text-to-slug.service';
import { TreeModule } from 'ng2-tree';
import { ObjectComparator } from 'app/services/object-comparator.service';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { RoleRoutingModule } from './role-routing.module';
import { RoleComponent } from './role/role.component';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { RoleService, PermissionService, PenggunaService, OrganisasiLevelService } from 'app/services/data-services';
import { ErrorHandler } from '@angular/core';
import { AppErrorHandler } from 'app/error-handler/app-error-handler';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { TabsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    SortableModule.forRoot(),
    TreeModule,
    TabsModule
  ],
  providers: [
    RoleService,
    PermissionService,
    PenggunaService,
    ObjectComparator,
    OrganisasiLevelService,
    TextToSlugPipe
    // AppErrorHandlerService,
    // ApiService,
    // AuthGuard,
    // NoAuthGuard,
    // PermissionGuard,
    // JwtService,
    // AuthService,
    // {
    //   provide: ErrorHandler,
    //   useClass: AppErrorHandler
    // }
  ],
  declarations: [RoleComponent, RoleDetailComponent, RoleEditComponent]
})
export class RoleModule { }
