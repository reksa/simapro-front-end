import { JadwalInputComponent } from './jadwal-input/jadwal-input.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';


const routes: Routes = [
  { path: '', redirectTo: 'manajemen-pengguna', pathMatch: 'full'},
  { path: 'jadwal-input', component: JadwalInputComponent, canActivate: [AuthGuard, PermissionGuard]},
  { path: 'manajemen-pengguna', loadChildren: './manajemen-pengguna/manajemen-pengguna.module#ManajemenPenggunaModule' },
  { path: 'wilayah', loadChildren: './wilayah/wilayah.module#WilayahModule' },
  { path: 'organisasi',
    loadChildren: './organisasi/organisasi.module#OrganisasiModule' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterDataRoutingModule { }
