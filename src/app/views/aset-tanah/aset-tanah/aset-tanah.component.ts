import { PaginationData } from './../../../components/pagination/page/page.component';
import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';

import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import {
  AsetTanahService, PenggunaService
} from 'app/services/data-services';

import { environment } from 'environments/environment';
import { AsetTanah, Pengguna, Koordinat } from 'app/models';
import { DataMapService } from '../../../services/data-services/data-map.service';
import { TextToSlugPipe } from '../../../services/text-to-slug.service';
import { ModalFilterContentComponent } from '../../modal-filter-content/modal-filter-content.component';
import { Map } from 'leaflet';

@Component({
  selector: 'app-aset-tanah',
  templateUrl: './aset-tanah.component.html',
  styleUrls: ['./aset-tanah.component.scss']
})
export class AsetTanahComponent implements OnInit {
  // page setting
  tab = 'map';
  root = 'aset-tanah/';
  showFilter = {
    wilayah: 'provinsi',
    unit: 'company-code'
  };
  mapSubscription: Subscription;
  dataSubscription: Subscription;

  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;

  fotoDir = `${environment.upload_url}/aset-tanah/`;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  public lat = -2.964984369333955;
  public lng = 114.08203125000001;
  zoom = 5;
  markersTanah: {clustered: number, nama: string, koordinat: Koordinat}[] = [];
  @ViewChild('map') map: Map;
  lastRequestParams = {
    zoom: null,
    bounds: null
  };
  public modalRef: BsModalRef;

  public tableConfig = new AppTableConfig();
  public pengguna = new Pengguna();

  constructor(
    private asetTanahService: AsetTanahService,
    private route: ActivatedRoute,
    private router: Router,
    private penggunaService: PenggunaService,
    private mapService: DataMapService,
    private modalService: BsModalService,
    private slugPipe: TextToSlugPipe
  ) {}

  ngOnInit() {
    this.penggunaService.getRoleForModule('tanah').subscribe(pengguna => this.pengguna = pengguna);
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      if (this.tab === 'map') {
        if(this.dataSubscription) {
          this.dataSubscription.unsubscribe();
        }
        this._map();
      } else {
        if(this.mapSubscription) {
          this.mapSubscription.unsubscribe();
        }
        this._data();
      }
    });
  }

  ngOnDestroy() {
    if(this.mapSubscription) {
      this.mapSubscription.unsubscribe();
    }
    if(this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  private _map() {
    this._getMapData();
  }

  _getMapData() {
    if(this.tab !== 'map') {
      return false;
    }

    if(this.mapSubscription) {
      this.mapSubscription.unsubscribe();
    }
    if (this.lastRequestParams.zoom !== this.zoom || this.lastRequestParams.bounds !== this.map.getBounds()) {
      const params = {};
      if (this.dataSetting.idKabupaten && this.dataSetting.idKabupaten != 0) {
        params['id_kabupaten'] = this.dataSetting.idKabupaten;
      }
      if (this.dataSetting.idProvinsi && this.dataSetting.idProvinsi != 0) {
        params['id_provinsi'] = this.dataSetting.idProvinsi;
      }
      if (this.dataSetting.idBusinessArea && this.dataSetting.idBusinessArea != 0){
        params['id_business_area'] = this.dataSetting.idBusinessArea;
      }
      if (this.dataSetting.idCompanyCode && this.dataSetting.idCompanyCode != 0){
        params['id_company_code'] = this.dataSetting.idCompanyCode;
      }
      if (this.dataSetting.idLokasiAset && this.dataSetting.idLokasiAset != 0){
        params['id_lokasi'] = this.dataSetting.idLokasiAset;
      }
      if (this.dataSetting.nama) {
        params['nama'] = this.dataSetting.nama;
      }
      if (this.dataSetting.progress_sertifikat && this.dataSetting.progress_sertifikat !== 'all') {
        params['progress_sertifikat'] = this.dataSetting.progress_sertifikat;
      }
      if (this.dataSetting.progress_sertifikat === 'selesai' && this.dataSetting.nomor_sertifikat && this.dataSetting.nomor_sertifikat !== '') {
        params['nomor_sertifikat'] = this.dataSetting.nomor_sertifikat;
      }
      if (this.dataSetting.start_range_luas) {
        params['start_range_luas'] = this.dataSetting.start_range_luas;
      }
      if (this.dataSetting.end_range_luas) {
        params['end_range_luas'] = this.dataSetting.end_range_luas;
      }
      if(this.lastRequestParams.zoom !== this.zoom) {
        this.map.closePopup();
      }
      this.lastRequestParams = {zoom: this.zoom, bounds: this.map.getBounds()};
      this.mapSubscription = this.mapService.get(this.zoom, this.map.getBounds(), ['tanah'], params)
        .subscribe(markers => {
          this.markersTanah = markers.tanah || [];
        });
      }
  }

  private _data() {
    if(this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    this.pageData = [];
    this.dataSubscription = this.route.queryParamMap.switchMap((params) => {
      this.pageData = [];
      this.isLoading = true;
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      if (params.get('idKabupaten')) {
        parameters.append('id_kabupaten', params.get('idKabupaten'));
        this.dataSetting.idKabupaten = parseInt(params.get('idKabupaten'));
      }
      if (params.get('idProvinsi')) {
        parameters.append('id_provinsi', params.get('idProvinsi'));
        this.dataSetting.idProvinsi = parseInt(params.get('idProvinsi'));
      }
      if (params.get('idBusinessArea')) {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.dataSetting.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      if (params.get('idCompanyCode')) {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.dataSetting.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('idLokasiAset')) {
        parameters.append('id_lokasi', params.get('idLokasiAset'));
        this.dataSetting.idLokasiAset = parseInt(params.get('idLokasiAset'));
      }
      if (params.get('nama') !== '') {
        parameters.append('nama', params.get('nama'));
        this.dataSetting.nama = params.get('nama');
      }
      if (params.get('progress_sertifikat') && params.get('progress_sertifikat') !== 'all') {
        parameters.append('progress_sertifikat', params.get('progress_sertifikat'));
        this.dataSetting.progress_sertifikat = params.get('progress_sertifikat');
      }
      if (this.dataSetting.progress_sertifikat === 'selesai' && params.get('nomor_sertifikat') && params.get('nomor_sertifikat') !== '') {
        parameters.append('nomor_sertifikat', params.get('nomor_sertifikat'));
        this.dataSetting.nomor_sertifikat = params.get('nomor_sertifikat');
      }
      if (params.get('start_range_luas') && params.get('start_range_luas') !== '') {
        parameters.append('start_range_luas', params.get('start_range_luas'));
        this.dataSetting.start_range_luas = parseFloat(params.get('start_range_luas'));
      }
      if (params.get('end_range_luas') && params.get('end_range_luas') !== '') {
        parameters.append('end_range_luas', params.get('end_range_luas'));
        this.dataSetting.end_range_luas = parseFloat(params.get('end_range_luas'));
      }

      let data: Observable<PaginationData>;
      switch(this.tab) {
        case 'data-perubahan': data = this.asetTanahService.getPaginateNonValid(parameters);
          break;
        case 'data-penerimaan': data = this.asetTanahService.getPaginatePenerimaan(parameters);
          break;
        default: data = this.asetTanahService.getPaginateValid(parameters);
      }
                                  
      return data;
    }).subscribe((res) => {
      this.dataSetting.dataPerPage = <number> res.per_page;
      this.pageData = res.data.map(d => {
        return new AsetTanah(d);
      });
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      if (this.modalRef) {
        this.modalRef.hide();
      }

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      fragment: this.tab,
      queryParams: this.dataSetting      
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  openModal() {
    const initialState = {
      dataSetting: Object.assign({}, this.dataSetting),
      isAsetTanah: true
    }
    this.modalRef = this.modalService.show(ModalFilterContentComponent, { initialState, class: 'modal-lg', ignoreBackdropClick: true });
    const content = <ModalFilterContentComponent> this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
      if (content.filter) {
        this.dataSetting = content.dataSetting;
        if(this.tab !== 'map') {
          this.search();
        } else {
          this._getMapData();
        }
      }
      subscribeModal.unsubscribe();
    })
  }

  show(asetTanah: AsetTanah) {
    const detail = asetTanah.detail || asetTanah.detail_perubahan;
    let nama =  detail ? detail.nama_tanah : 'detail';
    nama = this.slugPipe.transform(nama);
    this.router.navigate([`${this.root}show/${asetTanah.id_tanah}/${nama}`]);
    return false;
  }

  validasiPengajuan(asetTanah: AsetTanah) {
    const detail = asetTanah.detail || asetTanah.detail_perubahan;
    let nama =  detail ? detail.nama_tanah : 'detail';
    nama = this.slugPipe.transform(nama);
    this.router.navigate([`${this.root}validasi/${asetTanah.id_tanah}/${nama}`]);
    return false;
  }

  showFromMap(id: number, nama: string) {
    nama = this.slugPipe.transform(nama);
    const url = `/${this.root}show/${id}/${nama}`;
    return url;
  }

  mutasiPenerimaan(asetTanah: AsetTanah) {
    const detail = asetTanah.detail || asetTanah.detail_perubahan;
    let nama =  detail ? detail.nama_tanah : 'detail';
    nama = this.slugPipe.transform(nama);
    this.router.navigate([`${this.root}edit/${asetTanah.id_tanah}/${nama}/mutasi/penerimaan`]);
    return false;
  }

}
