import { environment } from './../../../../environments/environment';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AsetTanah, AsetTanahDetail, PerpindahanAsetTanah, DokumenLegal, NilaiAsetTanah } from '../../../models';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { AsetTanahService } from '../../../services/data-services';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { LatLng, LatLngBounds, Map } from 'leaflet';

@Component({
  selector: 'app-validasi-aset-tanah',
  templateUrl: './validasi-aset-tanah.component.html',
  styleUrls: ['./validasi-aset-tanah.component.scss']
})
export class ValidasiAsetTanahComponent implements OnInit {
  showMap = false;
  tab = 'detail';
  upload = environment.upload_url + '/aset-tanah/';
  foto;
  fotoPerubahan;
  asetTanah = new AsetTanah();
  detailAsetTanahPerubahan: AsetTanahDetail;
  perpindahanAsetTanahPerubahan: PerpindahanAsetTanah;
  dokumenPerubahan: DokumenLegal;
  nilaiPerubahan: NilaiAsetTanah;

  editable = true;
  name = 'Passau';

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.asetTanah;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 17;
  memo = '';
  poly: any;
  polyNonValid: any;

  namaAsetTanah = '';
  namaCompanyCode = '';
  root = 'aset-tanah/'
  modalRef: BsModalRef;

  historisDisposisi = [];

  @ViewChild('map') map: Map;

  constructor(
    private route: ActivatedRoute,
    private asetTanahService: AsetTanahService,
    private modalService: BsModalService,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this._init();
  }

  private _init() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idAsetTanah'), 10);
      this.asetTanahService.getById(id).subscribe((asetTanah: AsetTanah) => {
        this.spinner.hide();
        this.asetTanah = new AsetTanah(asetTanah);

        this.detailAsetTanahPerubahan = this.asetTanah.detail_perubahan;
        this.fotoPerubahan = this.detailAsetTanahPerubahan && this.detailAsetTanahPerubahan.foto ? `${this.upload}${this.detailAsetTanahPerubahan.foto}` : 'assets/img/no-image.jpg';
        this.perpindahanAsetTanahPerubahan = this.asetTanah.perpindahan_perubahan;
        this.dokumenPerubahan = this.asetTanah.dokumen_perubahan;
        this.nilaiPerubahan = this.asetTanah.nilai_perubahan;
        
        // template's need
        this.namaAsetTanah = this.detailAsetTanahPerubahan.nama_tanah;
        this.namaCompanyCode = this.perpindahanAsetTanahPerubahan.company_code.nama;

        if (this.detailAsetTanahPerubahan && this.detailAsetTanahPerubahan.polygon && this.detailAsetTanahPerubahan.polygon[0]) {
          this.polyNonValid = this.detailAsetTanahPerubahan.polygon[0];
          const center = this._getCenter(this.polyNonValid.area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.polyNonValid.area.coordinates[0]);
        }
      })
    });
  }

  ngAfterContentInit() {
    this.showMap = true;
  }

  log(data: any) {
    // console.log(data);
  }

  chooseTab(tab: string) {
    this.tab = tab;
    return false;
  }

  tolakUsulan() {
    if(this.modalRef){
      this.modalRef.hide();
    }
    if (this.detailAsetTanahPerubahan.action_button.type !== 'validasi' || !this.detailAsetTanahPerubahan.action_button.button) {
      return false;
    }
    this.spinner.show();
    this.asetTanahService
      .validasi(this.asetTanah.id_tanah, this.memo, false)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetTanahPerubahan.action_button.button = false;
          this.perpindahanAsetTanahPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  terimaUsulan() {
    if(this.modalRef){
      this.modalRef.hide();
    }
    if (this.detailAsetTanahPerubahan.action_button.type !== 'validasi' || !this.detailAsetTanahPerubahan.action_button.button) {
      return false;
    }
    this.spinner.show();
    this.asetTanahService
      .validasi(this.asetTanah.id_tanah, this.memo, true)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetTanahPerubahan.action_button.button = false;
          this.perpindahanAsetTanahPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  openModal(template: TemplateRef<any>) {
    this.memo = '';
    this.modalRef = this.modalService.show(template);
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  delete() {
    this.asetTanah.detail.is_active = false;
    this.asetTanah.detail.path = `/aset-tanah/edit/${this.asetTanah.id_tanah}/tanah`;
    this.asetTanahService.saveDetail(this.asetTanah.detail)
    .subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.back();
      }
    });
  }

  back() {
    this._location.back();
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}

