import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidasiAsetTanahComponent } from './validasi-aset-tanah.component';

describe('ValidasiAsetTanahComponent', () => {
  let component: ValidasiAsetTanahComponent;
  let fixture: ComponentFixture<ValidasiAsetTanahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidasiAsetTanahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidasiAsetTanahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
