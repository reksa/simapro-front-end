import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetTanahDetailComponent } from './aset-tanah-detail.component';

describe('AsetTanahDetailComponent', () => {
  let component: AsetTanahDetailComponent;
  let fixture: ComponentFixture<AsetTanahDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetTanahDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetTanahDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
