import { Subscription } from 'rxjs/Rx';
import { AsetTanahService, PenggunaService } from 'app/services/data-services';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { environment } from 'environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  PerpindahanAsetTanah, AsetTanahDetail, AsetTanah, DokumenLegal,
  NilaiAsetTanah, PotensiAset, PendayagunaanAset, Pengguna, TahapDokumenLegal
} from 'app/models';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { TemplateRef } from '@angular/core/src/linker/template_ref';
import { DatePipe, DecimalPipe, Location, UpperCasePipe } from '@angular/common';
import { DatatablesContent } from 'app/components/app-datatable/datatables-content.model';
import { TextToSlugPipe } from '../../../services/text-to-slug.service';
import { Disposisi } from '../../../models/disposisi.model';
import { Map, LatLng, LatLngBounds } from 'leaflet';
import { TabsetComponent } from 'ngx-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-tanah-detail',
  templateUrl: './aset-tanah-detail.component.html',
  styleUrls: ['./aset-tanah-detail.component.scss']
})
export class AsetTanahDetailComponent implements OnInit, AfterContentInit {
  showMap = false;
  tab = 'detail';
  upload = environment.upload_url + '/aset-tanah/';
  foto;
  fotoPerubahan;
  asetTanah = new AsetTanah();
  detailAsetTanah: AsetTanahDetail;
  detailAsetTanahPerubahan: AsetTanahDetail;
  perpindahanAsetTanah: PerpindahanAsetTanah;
  perpindahanAsetTanahPerubahan: PerpindahanAsetTanah;
  dokumen: DokumenLegal;
  dokumenPerubahan: DokumenLegal;
  dokumenOnProgress: DokumenLegal;
  nilai: NilaiAsetTanah;
  potensi: PotensiAset;
  nilaiPerubahan: NilaiAsetTanah;

  history = new DatatablesContent();
  historyDokumen = new DatatablesContent();
  historyProgressDokumen = new DatatablesContent();
  historyPerpindahan = new DatatablesContent();
  historyNilai = new DatatablesContent();
  historyPotensi = new DatatablesContent();
  historyPendayagunaan = new DatatablesContent();

  is_validator = false;
  editable = true;
  name = 'Passau';

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.asetTanah;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 17;
  memo = '';
  poly: any;
  polyNonValid: any;

  namaAsetTanah = '';
  namaCompanyCode = '';
  root = 'aset-tanah/'
  modalRef: BsModalRef;

  pengguna = new Pengguna();

  subsRole: Subscription;

  historisDisposisi = [];

  @ViewChild('map') map: Map;
  @ViewChild('tabset') tabset: TabsetComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private asetTanahService: AsetTanahService,
    private modalService: BsModalService,
    private datePipe: DatePipe,
    private numberPipe: DecimalPipe,
    private penggunaService: PenggunaService,
    private slugPipe: TextToSlugPipe,
    private uppercasePipe: UpperCasePipe,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this._init();
  }

  private _init() {
    this.spinner.show();
    if(this.subsRole) {
      this.subsRole.unsubscribe();
    }
    this.route.fragment.subscribe(fragment => {
      this.chooseTab(fragment);
    });
    this.subsRole = this.penggunaService.getRoleForModule('tanah').subscribe(pengguna => {
      this.pengguna = pengguna
      this.route.paramMap.subscribe(params => {
        const id = parseInt(params.get('idAsetTanah'), 10);
        const canViewChange = this.pengguna.is_user_input || this.pengguna.is_validator_pusat || this.pengguna.is_validator_unit;
        this.asetTanahService.getById(id).subscribe((asetTanah: AsetTanah) => {
          this.asetTanah = new AsetTanah(asetTanah);
          this.detailAsetTanah = this.asetTanah.detail;
          this.foto = this.detailAsetTanah && this.detailAsetTanah.foto ? `${this.upload}${this.detailAsetTanah.foto}` : 'assets/img/no-image.jpg';
          this.perpindahanAsetTanah = this.asetTanah.perpindahan;
          this.dokumen = this.asetTanah.dokumen;
          this.nilai = this.asetTanah.nilai;
          this.potensi = this.asetTanah.potensi;

          // perubahan data
          if (canViewChange) {
            this.detailAsetTanahPerubahan = this.asetTanah.detail_perubahan;
            this.fotoPerubahan = this.detailAsetTanahPerubahan && this.detailAsetTanahPerubahan.foto ? `${this.upload}${this.detailAsetTanahPerubahan.foto}` : 'assets/img/no-image.jpg';
            this.perpindahanAsetTanahPerubahan = this.asetTanah.perpindahan_perubahan;
            this.dokumenPerubahan = this.asetTanah.dokumen_perubahan;
            this.nilaiPerubahan = this.asetTanah.nilai_perubahan;
          }

          // map data ke datatable
          this._mapDetailToDatatable(this.asetTanah.histori_detail || []);
          this._mapPerpindahanToDatatable(this.asetTanah.histori_perpindahan || []);
          this._mapDokumenToDatatable(this.asetTanah.histori_sertifikat || []);
          this._mapNilaiToDatatable(this.asetTanah.histori_nilai || []);
          this._mapPotensiToDatatable(this.asetTanah.histori_potensi || []);
          this._mapPendayagunaanToDatatable(this.asetTanah.pendayagunaan_aktif || []);
// @ts-ignore
          const progress = this.asetTanah.dokumen_on_progress ? this.asetTanah.dokumen_on_progress.tahap_sertifikat : [];
          this.dokumenOnProgress = this.asetTanah.dokumen_on_progress;
          this._mapProgressDokumenToDatatable(progress);

          // template's need
          this.namaAsetTanah = this.detailAsetTanah ? this.detailAsetTanah.nama_tanah : this.detailAsetTanahPerubahan.nama_tanah;
          this.namaCompanyCode = this.perpindahanAsetTanah ?
              this.perpindahanAsetTanah.company_code.nama :
              this.perpindahanAsetTanahPerubahan.company_code.nama;

          // set map center & polygon
          if (this.detailAsetTanah && this.detailAsetTanah.polygon && this.detailAsetTanah.polygon[0]) {
            this.poly = this.detailAsetTanah.polygon[0];
            const center = this._getCenter(this.poly.area.coordinates[0]);
            this.map.panTo(center);
            this.fitMap(this.poly.area.coordinates[0]);
          }
          if (this.detailAsetTanahPerubahan && this.detailAsetTanahPerubahan.polygon && this.detailAsetTanahPerubahan.polygon[0]) {
            this.polyNonValid = this.detailAsetTanahPerubahan.polygon[0];
            const center = this._getCenter(this.polyNonValid.area.coordinates[0]);
            this.map.panTo(center);
            this.fitMap(this.polyNonValid.area.coordinates[0]);
          }
          this.spinner.hide();
        })
      });
    });
  }

  ngAfterContentInit() {
    this.showMap = true;
  }

  log(data: any) {
    console.log(data);
  }

  chooseTab(tab: string) {
    const tabs = ['detail', 'legal', 'nilai', 'mutasi'];
    this.tab = tab;
    const index = tabs.indexOf(tab);
    if(index > -1) {
      this.tabset.tabs[index].active = true;
    }
    return false;
  }

  edit(fragment?: string, isRevisi = false) {
    const nama = this.slugPipe.transform(this.namaAsetTanah);
    fragment = fragment || 'detail';
    const tipe = isRevisi ? 'revisi' : 'edit';
    if(!this.asetTanah.is_use) {
      this.router
        .navigate([
          `${this.root}revisi-pengajuan/${this.asetTanah.id_tanah}/${nama}`]);
    } else {
      this.router
        .navigate([
          `${this.root}${tipe}/${this.asetTanah.id_tanah}/${nama}/${fragment}`]);
    }
    return false;
  }

  updateProgress() {
    const nama = this.slugPipe.transform(this.namaAsetTanah);
    this.router
      .navigate([
        `${this.root}edit/${this.asetTanah.id_tanah}/${nama}/legal`
      ], {queryParams: {progress: 'true'}});
    return false;
  }

  mutasiEksternal(isRevisi = false) {
    const nama = this.slugPipe.transform(this.namaAsetTanah);
    const tipe = isRevisi ? 'revisi' : 'edit';
    this.router
      .navigate([
        `${this.root}${tipe}/${this.asetTanah.id_tanah}/${nama}/mutasi/eksternal`]);
    return false;
  }

  revisi(fragment: string) {
    this.edit(fragment, true);
    return false;
  }

  revisiMutasi() {
    const perpindahan = this.asetTanah.perpindahan_perubahan;
    const isEksternal = perpindahan.business_area ? false : true;
    if (isEksternal) {
      this.mutasiEksternal(true);
    } else  {
      this.edit('mutasi', true);
    }
  }

  addPotensi() {
    this.router.navigate([`potensi-aset/add`],
    {queryParams: {id: this.asetTanah.id_tanah, jenis: 'aset-tanah'}});
    return false;
  }

  addPendayagunaan() {
    this.router.navigate([`pendayagunaan-aset/add`],
    {queryParams: {id: this.asetTanah.id_tanah, jenis: 'aset-tanah'}});
  }

  tolakUsulanDetail() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiDetail(this.detailAsetTanahPerubahan.id_tanah_detail, this.memo, false)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetTanahPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  terimaUsulanDetail() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiDetail(this.detailAsetTanahPerubahan.id_tanah_detail, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.detailAsetTanahPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  tolakUsulanMutasi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiPerpindahan(this.perpindahanAsetTanahPerubahan.id_perpindahan_tanah, this.memo, false)
      .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.perpindahanAsetTanahPerubahan.action_button.button = false;
        this._init();
      }
    });
    return false;
  }

  terimaUsulanMutasi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiPerpindahan(this.perpindahanAsetTanahPerubahan.id_perpindahan_tanah, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.perpindahanAsetTanahPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  tolakUsulanNilai() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiNilai(this.nilaiPerubahan.id_nilai_tanah, this.memo, false)
      .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.nilaiPerubahan.action_button.button = false;
        this._init();
      }
    });
    return false;
  }

  terimaUsulanNilai() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiNilai(this.nilaiPerubahan.id_nilai_tanah, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.nilaiPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  tolakUsulanLegal() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiSertifikat(this.dokumenPerubahan.id_sertifikat_tanah, this.memo, false)
      .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.dokumenPerubahan.action_button.button = false;
        this._init();
      }
    });
    return false;
  }

  terimaUsulanLegal() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    this.asetTanahService
      .validasiSertifikat(this.dokumenPerubahan.id_sertifikat_tanah, this.memo, true)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.dokumenPerubahan.action_button.button = false;
          this._init();
        }
    });
    return false;
  }

  openModal(template: TemplateRef<any>) {
    this.memo = '';
    this.modalRef = this.modalService.show(template);
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  delete() {
    this.asetTanah.detail.is_active = false;
    this.asetTanah.detail.path = `/aset-tanah/edit/${this.asetTanah.id_tanah}/tanah`;
    this.asetTanahService.saveDetail(this.asetTanah.detail)
    .subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this._location.back();
      }
    });
  }

  private _mapPerpindahanToDatatable(dokumens: PerpindahanAsetTanah[]) {
    this.historyPerpindahan.columns = ['KODE ASET TANAH', 'LOKASI ASET', 'BUSINESS AREA', 'COMPANY CODE', 'TANGGAL BERLAKU'];
    this.historyPerpindahan.actions = [];
    this.historyPerpindahan.data.next([]);
    const data = [];

    dokumens.map((r: PerpindahanAsetTanah) => {
      const kode = r.kode_tanah ? r.kode_tanah : '-';
      const lokasi = r.lokasi ? this._toUppercase(r.lokasi.nama) : '-';
      const businessArea = r.business_area ? this._toUppercase(r.business_area.nama_business_area) : '-';
      const companyCode = r.company_code ? this._toUppercase(r.company_code.nama) : '-';
      const tanggalBerlaku = r.start_date_perpindahan ?
        this.datePipe.transform(r.start_date_perpindahan, 'dd MMM yyyy') : '-';

      data.push({
        row: [ kode, lokasi, businessArea, companyCode, tanggalBerlaku ],
        id: r.id_perpindahan_tanah
      });
    });

    this.historyPerpindahan.data.next(data);
  }

  private _mapDetailToDatatable(details: AsetTanahDetail[]) {
    this.history.columns = ['NAMA ASET TANAH', 'LUAS', 'NO. BPHTB', 'ALAMAT', 'TANGGAL PERUBAHAN'];
    this.history.actions = [];
    this.history.data.next([]);
    const data = [];

    details.map((r: AsetTanahDetail) => {
      const nama = r.nama_tanah ? this._toUppercase(r.nama_tanah) : '-';
      const bphtb = r.no_bphtb || '-';
      const alamat = r.alamat_tanah ? this._toUppercase(r.alamat_tanah) : '-';
      const luas = r.luas_tanah ? this.numberPipe.transform(r.luas_tanah, '1.0') : '-';
      const tanggal = r.changed_on ? this.datePipe.transform(r.changed_on, 'dd MMM yyyy')
          : this.datePipe.transform(r.created_on, 'dd MMM yyyy');

      data.push({
        row: [ nama, luas, bphtb, alamat, tanggal ],
        id: r.id_tanah_detail
      });
    });

    this.history.data.next(data);
  }

  private _mapDokumenToDatatable(dokumens: DokumenLegal[]) {
    this.historyDokumen.columns = ['NAMA DOKUMEN', 'NO. SERTIFIKAT', 'NO. OBJEK PAJAK', 'TANGGAL BERLAKU', 'TANGGAL BERAKHIR'];
    this.historyDokumen.actions = [];
    this.historyDokumen.data.next([]);
    const data = [];

    dokumens.map((r: DokumenLegal) => {
      const nama = r.ijin ? r.ijin.nama_ijin : '-';
      const no = r.no_sertifikat ? r.no_sertifikat : '-';
      const noPbb = r.no_sertifikat ? r.no_pbb : '-';
      const tanggalBerlaku = r.start_date_sertifikat ? this.datePipe.transform(r.start_date_sertifikat, 'dd MMM yyyy') : '-';
      const tanggalBerakhir = r.start_date_sertifikat ? this.datePipe.transform(r.end_date_sertifikat, 'dd MMM yyyy') : '-';

      data.push({
        row: [ nama, no, noPbb, tanggalBerlaku, tanggalBerakhir ],
        id: r.id_sertifikat_tanah
      });
    });

    this.historyDokumen.data.next(data);
  }

  private _mapProgressDokumenToDatatable(dokumens: TahapDokumenLegal[]) {
    this.historyProgressDokumen.columns = ['NAMA PROSES',	'TANGGAL PROSES',	'KETERANGAN', 'STATUS PROSES'];
    this.historyProgressDokumen.actions = [];
    this.historyProgressDokumen.data.next([]);
    const data = [];

    dokumens.map((r: TahapDokumenLegal) => {
      const namaDokumen = r.master_progress ? this._toUppercase(r.master_progress.nama_master_progress) : '-';
      const tanggal = r.tanggal_progress ? this.datePipe.transform(r.tanggal_progress, 'dd MMM yyyy') : '-';
      const keterangan = r.keterangan ? r.keterangan : '-';
      const status = r.is_finish ? 'SELESAI' : 'BELUM SELESAI';

      data.push({
        row: [ namaDokumen, tanggal, keterangan, status ],
        id: r.id_sertifikat_tanah
      });
    });

    this.historyProgressDokumen.data.next(data);
  }

  private _mapNilaiToDatatable(nilais: NilaiAsetTanah[]) {
    this.historyNilai.columns = ['NILAI ASET', 'TAHUN PENILAIAN', 'KETERANGAN'];
    this.historyNilai.actions = [];
    this.historyNilai.data.next([]);
    const data = [];

    nilais.map((r: NilaiAsetTanah) => {
      const nilai = r.nilai_tanah ? this.numberPipe.transform(r.nilai_tanah) : '-';
      const tahun = r.tahun_penilaian ? r.tahun_penilaian : '-';
      const keterangan = r.keterangan ? this._toUppercase(r.keterangan) : '-';

      data.push({
        row: [ nilai, tahun, keterangan ],
        id: r.id_tanah
      });
    });

    this.historyNilai.data.next(data);
  }

  private _mapPotensiToDatatable(potensis: PotensiAset[]) {
    this.historyPotensi.columns = ['NILAI POTENSI', 'DESKRIPSI', 'TANGGAL MULAI', 'TANGGAL SELESAI'];
    this.historyPotensi.actions = [];
    this.historyPotensi.data.next([]);
    const data = [];

    potensis.map((r: PotensiAset) => {
      const nilai = r.nilai_potensi ? this.numberPipe.transform(r.nilai_potensi) : '-';
      const deskripsi = r.deskripsi ? this._toUppercase(r.deskripsi) : '-';
      const tanggalMulai = r.start_date_potensi ? this.datePipe.transform(r.start_date_potensi, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_potensi ? this.datePipe.transform(r.end_date_potensi, 'dd MMM yyyy') : '-';

      data.push({
        row: [ nilai, deskripsi, tanggalMulai, tanggalSelesai ],
        id: r.id_potensi
      });
    });

    this.historyPotensi.data.next(data);
  }

  private _mapPendayagunaanToDatatable(pendayagunaans: PendayagunaanAset[]) {
    this.historyPendayagunaan.columns = ['JENIS PENDAYAGUNAAN',	'NO. KONTRAK',	'NILAI',	'TANGGAL MULAI',	'TANGGAL SELESAI'];
    this.historyPendayagunaan.actions = [];
    this.historyPendayagunaan.data.next([]);
    const data = [];

    pendayagunaans.map((r: PendayagunaanAset) => {
      const jenis = r.jenis_dayaguna && r.jenis_dayaguna.nama_jenis_dayaguna ? this._toUppercase(r.jenis_dayaguna.nama_jenis_dayaguna) : '-';
      const kontrak = r.kontrak ? r.kontrak : '-';
      const nilai = r.nilai_pendayagunaan ? this.numberPipe.transform(r.nilai_pendayagunaan) : '-';
      const tanggalMulai = r.start_date_pendayagunaan ? this.datePipe.transform(r.start_date_pendayagunaan, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_pendayagunaan ? this.datePipe.transform(r.end_date_pendayagunaan, 'dd MMM yyyy') : '-';

      data.push({
        row: [ jenis, kontrak, nilai, tanggalMulai, tanggalSelesai ],
        id: r.id_pendayagunaan
      });
    });

    this.historyPendayagunaan.data.next(data);
  }

  private _toUppercase(str: string) {
    return this.uppercasePipe.transform(str || '');
  }

  back() {
    this._location.back();
    return false;
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}

