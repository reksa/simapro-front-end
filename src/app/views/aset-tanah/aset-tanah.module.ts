import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { AppGeometryModule } from './../../components/app-geometry/app-geometry.module';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe, UpperCasePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AsetTanahRoutingModule } from './aset-tanah-routing.module';
import { PaginationModule } from 'app/components/pagination/pagination.module';

import { AsetTanahEditComponent } from './aset-tanah-edit/aset-tanah-edit.component';
import { AsetTanahComponent } from './aset-tanah/aset-tanah.component';
import { AsetTanahDetailComponent } from './aset-tanah-detail/aset-tanah-detail.component';

import { AlertService } from 'app/services/alert.service';
import {
  AsetTanahService, ProvinsiService, KabupatenService, LokasiService,
  CompanyCodeService, BusinessAreaService, JenisDayaGunaService, JenisPenghapusanService,
  JenisPerizinanService, OpiniLegalService, StatusKelayakanService, SumberPerolehanService,
  DataMapService, MasterProgressService
} from 'app/services/data-services';
import { YagaModule } from '@yaga/leaflet-ng2';
import { TabsModule } from 'ngx-bootstrap';
import { AsetTanahRevisiComponent } from './aset-tanah-revisi/aset-tanah-revisi.component';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { SelectModule } from 'ng2-select';
import { TimelineModule } from '../timeline/timeline.module';
import { ValidasiAsetTanahComponent } from './validasi-aset-tanah/validasi-aset-tanah.component';
import { InputNumberModule } from '../input-number/input-number.module';
import { MapLegendComponent } from '../map-legend/map-legend.component';
import { MapLegendModule } from '../map-legend/map-legend.module';

@NgModule({
  declarations: [
    AsetTanahEditComponent, AsetTanahComponent, AsetTanahDetailComponent,
    AsetTanahRevisiComponent, ValidasiAsetTanahComponent
  ],
  imports: [
    CommonModule,
    AsetTanahRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    HttpModule,
    YagaModule,
    TabsModule,
    AppGeometryModule,
    AppDatatableModule,
    SelectModule,
    TimelineModule,
    InputNumberModule,
    MapLegendModule
  ],
  providers: [
    AsetTanahService, AlertService, ProvinsiService,
    KabupatenService, LokasiService, CompanyCodeService,
    BusinessAreaService, JenisDayaGunaService, JenisPenghapusanService,
    JenisPerizinanService, OpiniLegalService, StatusKelayakanService,
    SumberPerolehanService, LokasiService, DatePipe, DecimalPipe,
    PermissionGuard, DataMapService, TextToSlugPipe, UpperCasePipe,
    MasterProgressService
  ]
})
export class AsetTanahModule { }
