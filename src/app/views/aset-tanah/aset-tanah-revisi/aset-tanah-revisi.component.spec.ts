import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetTanahRevisiComponent } from './aset-tanah-revisi.component';

describe('AsetTanahRevisiComponent', () => {
  let component: AsetTanahRevisiComponent;
  let fixture: ComponentFixture<AsetTanahRevisiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetTanahRevisiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetTanahRevisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
