import { MasterProgressService } from './../../../services/data-services/master-progress.service';
import { SumberPerolehanService } from './../../../services/data-services/sumber-perolehan.service';
import { Observable } from 'rxjs/Rx';
import {
  AsetTanahService, ProvinsiService, KabupatenService, BusinessAreaService,
  LokasiService, JenisPerizinanService, CompanyCodeService
} from 'app/services/data-services';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { environment } from 'environments/environment';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  PerpindahanAsetTanah, AsetTanahDetail, AsetTanah, DokumenLegal, NilaiAsetTanah,
  TahapDokumenLegal, JenisPerizinan, Pengguna, PotensiAset, SumberPerolehan, MasterProgress
} from 'app/models';
import { Location, UpperCasePipe } from '@angular/common';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { LatLng, LatLngBounds, Map } from 'leaflet';
import { TabsetComponent, BsModalService } from 'ngx-bootstrap';
import { SelectItem, SelectComponent } from 'ng2-select';
import { URLSearchParams } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-tanah-revisi',
  templateUrl: './aset-tanah-revisi.component.html',
  styleUrls: ['./aset-tanah-revisi.component.scss']
})
export class AsetTanahRevisiComponent implements OnInit {
  upload = environment.upload_url + '/aset-tanah/';
  documentReady = false;
  isSubmitted = false;
  isRevisi = false;
  jenisMutasi;
  isOnProgress = false; // dokumen legal
  lastProgress: TahapDokumenLegal;
  fragment = '';
  formDetail: FormGroup;
  foto;
  oldFoto;
  preview = '';

  formLegal: FormGroup;
  formTahapDokumen: FormGroup;
  tahapDokumenLegal: TahapDokumenLegal;
  jenisPerizinans: JenisPerizinan[];
  masterProgresses: MasterProgress[];
  selectedProgress: MasterProgress = new MasterProgress();

  formMutasi: FormGroup;
  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;
  pengguna: Pengguna;

  formAsetTanah: FormGroup;
  asetTanahPerubahan: AsetTanah;
  sumberPerolehans: SumberPerolehan[] = [];

  formNilai: FormGroup;
  private min = 1965;
  private max = new Date().getFullYear();

  formPotensi: FormGroup;
  potensiAset: PotensiAset;

  tab = 'detail';
  asetTanah = new AsetTanah();
  detailAsetTanah: AsetTanahDetail;
  detailAsetTanahPerubahan: AsetTanahDetail;
  perpindahanAsetTanah: PerpindahanAsetTanah;
  perpindahanAsetTanahPerubahan: PerpindahanAsetTanah;
  dokumen: DokumenLegal;
  dokumenPerubahan: DokumenLegal;
  nilai: NilaiAsetTanah;
  nilaiPerubahan: NilaiAsetTanah;

  is_validator = false;
  editable = true;
  name = 'Passau';

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.asetTanah;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 17;
  memo = '';
  poly: LatLng[][];
  polyOld: any;

  namaAsetTanah = '';
  namaCompanyCode = '';
  private root = 'aset-tanah/'
  path = '';
  modalRef: BsModalRef;
  
  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];

  @ViewChild('tabRevisi') tabRevisi: TabsetComponent;
  @ViewChild('templatePoint') templatePoint: TemplateRef<any>;
  @ViewChild('templateKonfirmasi') templateKonfirmasi: TemplateRef<any>;
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('map') map: Map;

  endDateStatus = false;
  hasPoint = false;

  is_terima;

  constructor(
    private route: ActivatedRoute,
    private asetTanahService: AsetTanahService,
    private _location: Location,
    private _fb: FormBuilder,
    private jenisPerizinanService: JenisPerizinanService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private businessAreaService: BusinessAreaService,
    private lokasiService: LokasiService,
    private sumberPerolehanService: SumberPerolehanService,
    private masterProgressService: MasterProgressService,
    private companyCodeService: CompanyCodeService,
    private modalService: BsModalService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.documentReady = false;
    this.isSubmitted = false;
    this.isRevisi = false;
    this.jenisMutasi;
    this.isOnProgress = false; // dokumen legal
    this.is_validator = false;
    this.editable = true;
    this._init();
  }

  private _init() {
    this.poly = [[]];
    Observable.combineLatest(this.route.url, this.route.paramMap, this.route.fragment)
    .subscribe(obs => {
      this.isRevisi = obs[0][0].path === 'revisi';
      const params = obs[1];
      this.jenisMutasi = params.get('jenisMutasi') || 'internal'; // edit or revisi
      const fragment = params.get('fragment');
      this.fragment = fragment;

      const id = parseInt(params.get('idAsetTanah'), 10);

      this.path = `/${this.root}${obs[0][0].path}/${id}/aset/${fragment}`;

      if(this.jenisMutasi === 'eksternal' || this.jenisMutasi === 'penerimaan') {
        this.path += `/${this.jenisMutasi}`;
      }

      this.spinner.show();

      this.asetTanahService.getById(id).subscribe((asetTanah: AsetTanah) => {
        this.spinner.hide();
        asetTanah = new AsetTanah(asetTanah);
        this.oldFoto = asetTanah.detail ? `${this.upload}${asetTanah.detail.foto}` : null;
        if (['detail', 'legal', 'nilai', 'mutasi'].indexOf(fragment) === -1) {
          this.cancel();
        }
        if ( ! (
          (
            fragment === 'detail' && (
              ( ! this.isRevisi && asetTanah.detail && asetTanah.detail.is_editable_detail) || 
              ( this.isRevisi && asetTanah.detail_perubahan &&
                ( asetTanah.detail_perubahan.action_button.type === 'revisi' && asetTanah.detail_perubahan.action_button.button)
              )
            )
          ) ||
          (
            fragment === 'legal' && (
              ( ! asetTanah.dokumen && ! asetTanah.dokumen_on_progress && ! asetTanah.dokumen_perubahan) || 
              ( ! this.isRevisi && asetTanah.dokumen && asetTanah.dokumen.is_editable_sertifikat) || 
              ( this.isRevisi && asetTanah.dokumen_perubahan && 
                ( asetTanah.dokumen_perubahan.action_button.type === 'revisi' && asetTanah.dokumen_perubahan.action_button.button)
              ) ||
              ( asetTanah.dokumen_on_progress )
            )
          ) || 
          (
            fragment === 'nilai' && (
              ( ! this.isRevisi && asetTanah.nilai && asetTanah.nilai.is_editable_nilai) || 
              ( this.isRevisi && asetTanah.nilai_perubahan && ( 
                  asetTanah.nilai_perubahan.action_button.type === 'revisi' && asetTanah.nilai_perubahan.action_button.button
                )
              ) || 
              ( ! asetTanah.nilai && ! asetTanah.nilai_perubahan )
            )
          ) ||
          (
            fragment === 'mutasi' && (
              ( ! this.isRevisi && asetTanah.perpindahan && asetTanah.perpindahan.is_editable_perpindahan) || 
              ( this.isRevisi && asetTanah.perpindahan_perubahan && (
                  asetTanah.perpindahan_perubahan.action_button.type === 'revisi' && asetTanah.perpindahan_perubahan.action_button.button
                )
              )
            )
          )
        )) {
          alert('Anda tidak memiliki izin atau Masih ada pengajuan yang sedang diproses!');
          this._location.back();
        }
        this.asetTanah = asetTanah;
        this.detailAsetTanah = asetTanah.detail;
        this.perpindahanAsetTanah = asetTanah.perpindahan;
        this.dokumen = asetTanah.dokumen;

        this.nilai = asetTanah.nilai;
        this._initFormDataStatis();
        
        switch (fragment) {
          case 'mutasi': this._initFormMutasi();
            break;
          case 'legal': this._initFormLegal();
            break;
          case 'nilai': this._initFormNilai();
            break;
          default: this._initFormDetail();
        }


        const perpindahanX = asetTanah.perpindahan || asetTanah.perpindahan_perubahan;
        this.namaAsetTanah = this.detailAsetTanah ? this.detailAsetTanah.nama_tanah : this.asetTanah.detail_perubahan.nama_tanah;
        this.namaCompanyCode = perpindahanX.company_code.nama; 
        
        if (this.detailAsetTanah && this.detailAsetTanah.polygon && this.detailAsetTanah.polygon[0]) {
          this.polyOld = this.detailAsetTanah.polygon[0].area;
          const center = this._getCenter(this.detailAsetTanah.polygon[0].area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.polyOld.coordinates[0]);
        }
        if (this.asetTanah.detail_perubahan && this.asetTanah.detail_perubahan.polygon && this.asetTanah.detail_perubahan.polygon[0]) {
          const center = this._getCenter(this.asetTanah.detail_perubahan.polygon[0].area.coordinates[0]);
          this.lat = center.lat;
          this.lng = center.lng;
          this.fitMap(this.asetTanah.detail_perubahan.polygon[0].area.coordinates[0]);
        }
        if (this.tabRevisi.tabs[1]) {
          this.tabRevisi.tabs[1].active = true;
        }
        this.documentReady = true;
      });
    });
  }

  changeEndDateStatus() {
    if(!this.endDateStatus) {
      if(this.formLegal) this.formLegal.get('end_date_sertifikat').disable();
      if(this.formTahapDokumen) this.formTahapDokumen.get('end_date_sertifikat').disable();
    } else {
      if(this.formLegal) this.formLegal.get('end_date_sertifikat').enable();
      if(this.formTahapDokumen) this.formTahapDokumen.get('end_date_sertifikat').enable();
    }
    this.endDateStatus = !this.endDateStatus;
  }

  log(data: any) {
    // console.log(data);
  }

  chooseTab(tab: string) {
    this.tab = tab;
    return false;
  }

  private _initFormDataStatis() {
    this.sumberPerolehanService.getAll().subscribe(sumberPerolehans => {
      this.sumberPerolehans = sumberPerolehans;
    })
    this.asetTanahPerubahan = Object.assign({}, this.asetTanah);
    this.formAsetTanah = this._fb.group({
      id_sumber_perolehan: ['', Validators.required],
      tahun_perolehan: ['', [Validators.required, Validators.min(1800), Validators.max(this.max)]],
      nilai_perolehan: ['', Validators.required],
    });
  }

  private _initFormDetail() {
    if (this.isRevisi) {
      this.detailAsetTanahPerubahan = this.asetTanah.detail_perubahan || Object.assign({}, this.detailAsetTanah);
    } else {
      this.detailAsetTanahPerubahan = Object.assign({}, this.detailAsetTanah);
      this.detailAsetTanahPerubahan.id_tanah_detail = null;

      this.detailAsetTanahPerubahan.created_on = null;
      this.detailAsetTanahPerubahan.changed_on = null;
      this.detailAsetTanahPerubahan.created_by = null;
      this.detailAsetTanahPerubahan.changed_by = null;
      this.detailAsetTanahPerubahan.is_use = null;
      this.detailAsetTanahPerubahan.start_date_detail = null;
      this.detailAsetTanahPerubahan.end_date_detail = null;
    }
    this.foto = this.detailAsetTanahPerubahan.foto ? `${this.upload}${this.detailAsetTanahPerubahan.foto}` : null;
    
    this.formDetail = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]],
      alamat: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255)]],
      polygon: this._fb.group({
        points: this._fb.array([], Validators.minLength(4))
      }),
      foto: ['', ''],
      luas_tanah: ['', [Validators.required]],
      no_bphtb: ['', [Validators.required]],
      no_sap: ['', []],
    });

    const defaultFoto = this.detailAsetTanahPerubahan ? this.detailAsetTanahPerubahan.foto : null;
    this.formDetail.get('foto').setValue(defaultFoto);

    const p = this.detailAsetTanahPerubahan.polygon && this.detailAsetTanahPerubahan.polygon[0] ? this.detailAsetTanahPerubahan.polygon[0].area.coordinates[0] : [];
    for (let i = 0; i < (p.length - 1); i++) {
      this.addPoint(true, new LatLng(p[i][0], p[i][1]), true);
    }
  }

  private _initPoint(withModel = true, latLng: LatLng) {
    if (withModel) {
      const newData = latLng;
      if(!this.poly) {
        this.poly = [[]];
      }
      this.poly[0].splice(this.poly[0].length, 0, newData);
    }
    return this._fb.group({
      lat: ['', [Validators.required]],
      lng: ['', [Validators.required]]
    });
  }

  public addPoint(withModel = true, latLng: LatLng, isInit = false) {
    if((this.poly[0].length || this.modalRef || isInit)) {
      const polygon = <FormGroup> this.formDetail.get('polygon');
      const points = <FormArray> polygon.get('points');
      latLng = latLng || new LatLng(this.lat, this.lng);
      points.push(this._initPoint(withModel, latLng));
    } else {
      this.modalRef = this.modalService.show(this.templatePoint, {
        class: 'modal-sm',
        keyboard: false,
        ignoreBackdropClick: true
      });
      this.modalService.onHide.subscribe(e => {
        this.hasPoint = true;
      });
    }
    return false;
  }

  addInitialPoint() {
    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng - 0.0005), true);
    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng - 0.0005));
    this.poly[0].push(new LatLng(this.lat - 0.0005, this.lng - 0.0005));
    this.modalRef.hide();
    this.modalRef = null;
  }

  public removePoint(index: number) {
    const polygon = <FormGroup> this.formDetail.get('polygon');
    const points = <FormArray> polygon.get('points');
    points.removeAt(index);
    this.poly[0].splice(index, 1);
    return false;
  }

  private _initFormLegal() {
    this.route.queryParamMap.subscribe(params => {
      this.isOnProgress = params.get('progress') && params.get('progress') === 'true';
      const lastIndex = this.asetTanah.dokumen_on_progress ? this.asetTanah.dokumen_on_progress.tahap_sertifikat.length - 1 : -1;
      const lastProgress = this.asetTanah.dokumen_on_progress && this.asetTanah.dokumen_on_progress.tahap_sertifikat.length ? this.asetTanah.dokumen_on_progress.tahap_sertifikat[lastIndex] : null;
      const p = new URLSearchParams();
      if(this.isOnProgress && lastProgress) {
        p.set('latest_progress', lastProgress.id_master_progress.toString());
      } else if(this.isOnProgress) {
        p.set('show_all', '1');
      }
      this.masterProgressService.getAll(p).subscribe(masterProgresses => {
        this.masterProgresses = masterProgresses;
      });
    });
    if (this.isRevisi) {
      this.dokumenPerubahan = this.asetTanah.dokumen_perubahan;
    } else {
      this.dokumenPerubahan = Object.assign({}, this.dokumen);
      this.dokumenPerubahan.id_sertifikat_tanah = null;

      this.dokumenPerubahan.created_on = null;
      this.dokumenPerubahan.changed_on = null;
      this.dokumenPerubahan.created_by = null;
      this.dokumenPerubahan.changed_by = null;
      this.dokumenPerubahan.is_use = null;
      this.dokumenPerubahan.status_progress = false;
    }

    if(this.dokumenPerubahan.start_date_sertifikat && !this.dokumenPerubahan.end_date_sertifikat) {
      this.changeEndDateStatus();
    }

    this.jenisPerizinanService.getAll().subscribe(jenisPerizinans => {
      this.jenisPerizinans = jenisPerizinans;
    });
    
    this.tahapDokumenLegal = new TahapDokumenLegal();
    if(this.isOnProgress) {
      this.formTahapDokumen = this._fb.group({
        id_master_progress: ['', [Validators.required]],
        tanggal_progress: ['', [Validators.required]],
        no_sertifikat: ['', [Validators.required]],
        no_pbb: ['', [Validators.required]],
        start_date_sertifikat: ['', [Validators.required]],
        end_date_sertifikat: ['', [Validators.required]],
        keterangan: ['', []]
      });
      this.formTahapDokumen.get('no_sertifikat').disable();
      this.formTahapDokumen.get('no_pbb').disable();
      this.formTahapDokumen.get('start_date_sertifikat').disable();
      this.formTahapDokumen.get('end_date_sertifikat').disable();

      const lastIndex = this.asetTanah.dokumen_on_progress.tahap_sertifikat.length - 1;
      this.lastProgress = this.asetTanah.dokumen_on_progress && this.asetTanah.dokumen_on_progress.tahap_sertifikat.length ? this.asetTanah.dokumen_on_progress.tahap_sertifikat[lastIndex] : null;
    } else {
      this.formLegal = this._fb.group({
        id_ijin: ['', [Validators.required]],
        no_sertifikat: ['', [Validators.required]],
        no_pbb: ['', [Validators.required]],
        start_date_sertifikat: ['', [Validators.required]],
        end_date_sertifikat: ['', [Validators.required]],
        status_progress: ['', []],
        formTahapDokumen: this._fb.group({
          id_master_progress: ['', [Validators.required]],
          tanggal_progress: ['', [Validators.required]],
          keterangan: ['', []]
        })
      });
      if(!this.dokumenPerubahan.status_progress) {
        this.formLegal.controls['formTahapDokumen'].disable();
      }
      this.setStatusProgress(false, true);
    }
  }

  onSelectMasterProgress(fromModal: boolean, success: boolean) {
    this.masterProgresses.forEach(masterProgress => {
      if(masterProgress.id_master_progress == this.tahapDokumenLegal.id_master_progress) {
        this.selectedProgress = masterProgress;
      }
    });
    let form: FormGroup;
    if(this.formTahapDokumen) {
      form = this.formTahapDokumen;
    } else {
      form = this.formLegal;
    }
    if(this.modalRef) {
      this.modalRef.hide();
    }
    if(!fromModal && this.selectedProgress && this.selectedProgress.is_finish) {
      this.modalRef = this.modalService.show(this.templateKonfirmasi, {ignoreBackdropClick: true, keyboard: false});
    } else if(fromModal && success) {
      this.is_terima = true;
      form.get('no_sertifikat').enable();
      form.get('no_pbb').enable();
      form.get('start_date_sertifikat').enable();
      form.get('end_date_sertifikat').enable();
    } else {
      this.is_terima = false;
      form.get('no_sertifikat').disable();
      form.get('no_pbb').disable();
      form.get('start_date_sertifikat').disable();
      form.get('end_date_sertifikat').disable();
    }
  }

  private _initFormMutasi() {
    if (this.isRevisi) {
      this.perpindahanAsetTanahPerubahan = this.asetTanah.perpindahan_non_valid && this.asetTanah.perpindahan_non_valid.length ?
        this.asetTanah.perpindahan_non_valid[0] : new PerpindahanAsetTanah();
    } else {
      this.perpindahanAsetTanahPerubahan = Object.assign({}, this.perpindahanAsetTanah);

      this.perpindahanAsetTanahPerubahan.id_perpindahan_tanah = null;
      this.perpindahanAsetTanahPerubahan.created_on = null;
      this.perpindahanAsetTanahPerubahan.changed_on = null;
      this.perpindahanAsetTanahPerubahan.created_by = null;
      this.perpindahanAsetTanahPerubahan.changed_by = null;
      this.perpindahanAsetTanahPerubahan.is_use = null;
      this.perpindahanAsetTanahPerubahan.start_date_perpindahan = null;
      this.perpindahanAsetTanahPerubahan.end_date_perpindahan = null;
    }

    this.idCompanyCode = this.perpindahanAsetTanahPerubahan.business_area ? this.perpindahanAsetTanahPerubahan.id_company_code : this.perpindahanAsetTanahPerubahan.id_company_code;
    this.idBusinessArea = this.perpindahanAsetTanahPerubahan.id_business_area;
    this.idProvinsi = this.perpindahanAsetTanahPerubahan.lokasi.kabupaten.id_provinsi;
    this.idKabupaten = this.perpindahanAsetTanahPerubahan.lokasi.id_kabupaten;
    this.idLokasi = this.perpindahanAsetTanahPerubahan.id_lokasi;
    this.formMutasi = this._fb.group({
      idProvinsi: ['', [Validators.required]],
      idKabupaten: ['', [Validators.required]],
      idLokasi: ['', [Validators.required]],
      idCompanyCode: ['', [Validators.required]],
      idBusinessArea: ['', [Validators.required]],
      no_bast: ['', []],
      keterangan: ['', [Validators.required, Validators.maxLength(255)]]
    });
    if (this.jenisMutasi === 'eksternal') {
      const all = true;
      this.getCompanyCode(all);
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').disable();
      this.formMutasi.get('idProvinsi').disable();
      this.formMutasi.get('idKabupaten').disable();
      this.formMutasi.get('idLokasi').disable();
    } else if(this.jenisMutasi === 'penerimaan') {
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').setValue(this.idBusinessArea);
      this.formMutasi.get('idProvinsi').disable();
      this.formMutasi.get('idKabupaten').disable();
      this.formMutasi.get('idLokasi').disable();
      this.getBusinessArea();
    } else {
      this.getProvinsi();
      this.getKabupaten();
      this.getLokasi();
      this.formMutasi.get('idCompanyCode').setValue(this.idCompanyCode);
      this.formMutasi.get('idBusinessArea').setValue(this.idBusinessArea);
      this.getBusinessArea();
    }
  }

  private setStatusProgress(selesai, init?: boolean) {
    if(this.isRevisi && !init) {
      this.formLegal.controls['status_progress'].disable();
      return false;
    }
    this.dokumenPerubahan.status_progress = selesai;
    if (selesai) {
      this.formLegal.controls['formTahapDokumen'].enable();
      this.formLegal.get('no_sertifikat').disable();
      this.formLegal.get('no_pbb').disable();
      this.formLegal.get('start_date_sertifikat').disable();
      this.formLegal.get('end_date_sertifikat').disable();
    } else {
      this.formLegal.controls['formTahapDokumen'].disable();
      this.formLegal.get('no_sertifikat').enable();
      this.formLegal.get('no_pbb').enable();
      this.formLegal.get('start_date_sertifikat').enable();
      this.formLegal.get('end_date_sertifikat').enable();
    }
  }

  private _initFormNilai() {
    if (this.isRevisi) {
      this.nilaiPerubahan = this.asetTanah.nilai_perubahan || new NilaiAsetTanah();
    } else {
      this.nilaiPerubahan = this.nilai ? Object.assign({}, this.nilai) : new NilaiAsetTanah();

      this.nilaiPerubahan.id_nilai_tanah = null;
      this.nilaiPerubahan.created_on = null;
      this.nilaiPerubahan.changed_on = null;
      this.nilaiPerubahan.created_by = null;
      this.nilaiPerubahan.changed_by = null;
      this.nilaiPerubahan.is_use = null;
    }

    if (!this.nilaiPerubahan) {
      this.nilaiPerubahan = new NilaiAsetTanah();
    }
    this.min = this.nilai ? this.nilai.tahun_penilaian : this.asetTanah.tahun_perolehan;
    this.formNilai = this._fb.group({
      nilai: ['', [Validators.required]],
      keterangan: ['', [Validators.required, Validators.maxLength(254)]],
      tahun_penilaian: ['', [Validators.required, Validators.min(this.min), Validators.max(this.max)]],
    });
  }

  private _getCenter(array: number[]): {lat: number, lng: number} {
    const x = [];
    const y = [];
    for (let i = 0; i < array.length; i++) {
      x.push(array[i][0]);
      y.push(array[i][1]);
    }
    return {lat: this._mid(x), lng: this._mid(y)};
  }

  private _mid(array: number[]) {
    const max = Math.max.apply(null, array);
    const min = Math.min.apply(null, array);
    return (max + min) / 2;
  }

  cancel() {
    this._backToDetail();
    return false;
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      if(file.size > environment.max_file_upload_size) {
        alert(`Ukuran foto maksimal ${environment.max_file_upload_size/1000000} MB`);
        return false;
      }
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.formDetail.get('foto').setValue({
          filename: file.name,
          filetype: file.type,
          // @ts-ignore
          value: reader.result.split(',')[1]
        });
        // @ts-ignore
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    const defaultFoto = this.detailAsetTanahPerubahan ? this.detailAsetTanahPerubahan.foto : null;
    this.preview = null;
    this.formDetail.get('foto').setValue(defaultFoto);
    return false;
  }

  saveDataStatis() {
    this.spinner.show();
    this.asetTanahPerubahan.path = '/aset-tanah/add';
    this.isSubmitted = true;
    this.asetTanahService.saveDataStatis(this.asetTanahPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  saveDetail() {
    this.spinner.show();
    this.detailAsetTanahPerubahan.path = this.path;
    this.isSubmitted = true;
    this.detailAsetTanahPerubahan.polygon = this.poly;
    if (this.preview) {
      this.detailAsetTanahPerubahan.foto = this.formDetail.get('foto').value;
    }
    if(this.isRevisi) {
      this.revisiDetail();
    } else {
      this.addDetail();
    }
  }

  private addDetail() {
    this.asetTanahService.saveDetail(this.detailAsetTanahPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  private revisiDetail() {
    this.asetTanahService.revisiDetail(this.detailAsetTanahPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  saveDokumenLegal() {
    this.spinner.show();
    this.dokumenPerubahan.path = this.path;
    this.isSubmitted = true;
    this.dokumenPerubahan.id_tanah = this.asetTanah.id_tanah;

    if(this.dokumenPerubahan.status_progress) {
      this.dokumenPerubahan.no_sertifikat = null;
      this.dokumenPerubahan.no_pbb = null;
      this.dokumenPerubahan.start_date_sertifikat = null;
      this.dokumenPerubahan.end_date_sertifikat = null;
      this.dokumenPerubahan.id_master_progress = this.tahapDokumenLegal.id_master_progress;
      this.dokumenPerubahan.keterangan = this.tahapDokumenLegal.keterangan;
      this.dokumenPerubahan.tanggal_progress = this.tahapDokumenLegal.tanggal_progress;
    }
    if(this.isRevisi) {
      this.revisiDokumenLegal();
    } else {
      this.addDokumenLegal();
    }
    return false;
  }

  private addDokumenLegal() {
    this.asetTanahService.saveDokumenLegal(this.dokumenPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiDokumenLegal() {
    this.asetTanahService.revisiDokumenLegal(this.dokumenPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  saveTahapDokumen() {
    this.spinner.show();
    this.tahapDokumenLegal.path = this.path;
    this.tahapDokumenLegal.is_terima = this.is_terima;

    this.isSubmitted = true;
    this.asetTanahService.saveProgressDokumen(this.tahapDokumenLegal, this.asetTanah.id_tanah)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          if(this.is_terima === false) {
            this._init();
          } else {
            this._location.back();
          }
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  saveNilaiAset() {
    this.spinner.show();
    this.nilaiPerubahan.path = this.path;
    this.isSubmitted = true;
    this.nilaiPerubahan.id_tanah = this.asetTanah.id_tanah;
    if(this.isRevisi) {
      this.revisiNilaiAset();
    } else {
      this.addNilaiAset();
    }
    return false;
  }

  private addNilaiAset() {
    this.asetTanahService.saveNilaiAset(this.nilaiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiNilaiAset() {
    this.asetTanahService.revisiNilaiAset(this.nilaiPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  saveMutasi() {
    this.spinner.show();
    if (this.jenisMutasi === 'eksternal' ) {
      this.perpindahanAsetTanahPerubahan.id_business_area = null;
    } else {
      this.perpindahanAsetTanahPerubahan.id_business_area = this.idBusinessArea;
    }
    this.perpindahanAsetTanahPerubahan.path = this.path;
    this.perpindahanAsetTanahPerubahan.id_company_code = this.idCompanyCode;
    this.perpindahanAsetTanahPerubahan.path = this.path;
    this.perpindahanAsetTanahPerubahan.id_lokasi = this.idLokasi;
    this.isSubmitted = true;
    if(this.isRevisi) {
      this.revisiMutasi();
    } else {
      this.addMutasi();
    }
    return false;
  }

  private addMutasi() {
    this.asetTanahService.saveMutasi(this.perpindahanAsetTanahPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private revisiMutasi() {
    this.asetTanahService.revisiMutasi(this.perpindahanAsetTanahPerubahan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();;
        } else {
          this.isSubmitted = false;
        }
    });
  }

  private _backToDetail() {
    if (confirm('Kembali ke halaman detail aset?')) {
      this._location.back();
    }
    return false;
  }

  dblClick(e) {
    if(this.fragment === 'detail' && e.latlng && this.poly[0].length) {
      this.addPoint(true, e.latlng);
    }
  }

  private getCompanyCode(all?: boolean) {
    const params = new URLSearchParams();
    all = all || false;
    this.companyCodeService.getAll(params, all).subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
        }
      });
    });
  }

  private getBusinessArea() {
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id === this.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  private getProvinsi() {
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
        }
      });
    });
  }

  private getKabupaten() {
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
        }
      });
    });
  }

  private getLokasi() {
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id === this.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formMutasi.get('idCompanyCode').setValue(e.id);
    this.formMutasi.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formMutasi.get('idCompanyCode').setValue(e.id);
    this.formMutasi.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.formMutasi.get('idBusinessArea').setValue(e.id);
    this.formMutasi.get('idBusinessArea').updateValueAndValidity();
  }

  selectedBA(e: SelectItem) {
    this.formMutasi.get('idBusinessArea').setValue(e.id);
    this.formMutasi.get('idBusinessArea').updateValueAndValidity();
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formMutasi.get('idProvinsi').setValue(e.id);
    this.formMutasi.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formMutasi.get('idProvinsi').setValue(e.id);
    this.formMutasi.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formMutasi.get('idKabupaten').setValue(e.id);
    this.formMutasi.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formMutasi.get('idKabupaten').setValue(e.id);
    this.formMutasi.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    this.formMutasi.get('idLokasi').setValue(e.id);
    this.formMutasi.get('idLokasi').updateValueAndValidity();
  }

  selectedL(e: SelectItem) {
    this.formMutasi.get('idLokasi').setValue(e.id);
    this.formMutasi.get('idLokasi').updateValueAndValidity();
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}

