import { AsetTanahEditComponent } from './aset-tanah-edit/aset-tanah-edit.component';
import { AsetTanahDetailComponent } from './aset-tanah-detail/aset-tanah-detail.component';
import { AsetTanahComponent } from './aset-tanah/aset-tanah.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsetTanahRevisiComponent } from './aset-tanah-revisi/aset-tanah-revisi.component';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { ValidasiAsetTanahComponent } from './validasi-aset-tanah/validasi-aset-tanah.component';

const routes: Routes = [
  { path: '', component: AsetTanahComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idAsetTanah/:nama', component: AsetTanahDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'validasi/:idAsetTanah/:nama', component: ValidasiAsetTanahComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add', component: AsetTanahEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi-pengajuan/:idAsetTanah/:nama', component: AsetTanahEditComponent, canActivate: [] },
  { path: 'edit/:idAsetTanah/:nama/:fragment', component: AsetTanahRevisiComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idAsetTanah/:nama/:fragment/:jenisMutasi', component: AsetTanahRevisiComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idAsetTanah/:nama/:fragment', component: AsetTanahRevisiComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idAsetTanah/:nama/:fragment/:jenisMutasi', component: AsetTanahRevisiComponent, canActivate: [AuthGuard, PermissionGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsetTanahRoutingModule { }
