import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsetTanahEditComponent } from './aset-tanah-edit.component';

describe('AsetTanahEditComponent', () => {
  let component: AsetTanahEditComponent;
  let fixture: ComponentFixture<AsetTanahEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsetTanahEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsetTanahEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
