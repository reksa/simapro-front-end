import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Location, UpperCasePipe } from '@angular/common';

import { TabsetComponent } from 'ngx-bootstrap/tabs/tabset.component';

import {
  JenisPerizinanService,  StatusKelayakanService, SumberPerolehanService, ProvinsiService, PenggunaService,
  BusinessAreaService, KabupatenService, LokasiService, AsetTanahService, CompanyCodeService, MasterProgressService,
} from 'app/services/data-services';

import { environment } from 'environments/environment';

import {
  JenisDayaGuna, JenisPerizinan, OpiniLegal,
  StatusKelayakan, SumberPerolehan, AsetTanah, AsetTanahDetail,
  PerpindahanAsetTanah, DokumenLegal, TahapDokumenLegal, Pengguna, MasterProgress } from 'app/models';
import { LatLng, LatLngBounds, Map } from 'leaflet';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SelectItem, SelectComponent } from 'ng2-select';
import { URLSearchParams } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-aset-tanah-edit',
  templateUrl: './aset-tanah-edit.component.html',
  styleUrls: ['./aset-tanah-edit.component.scss']
})
export class AsetTanahEditComponent implements OnInit {
  // page setting
  action = '';
  title = '';
  ready = false;
  hasPoint = false;
  maxYear = new Date().getFullYear();
  root = '/aset-tanah/';
  oldFoto: string;
  uploadPath = environment.upload_url + '/aset-tanah/';

  // form
  isSubmitted: boolean;
  formAsetTanah: FormGroup;
  formUmum: FormGroup;
  formLegal: FormGroup;
  jenisDayaGunas: JenisDayaGuna[] = [];
  jenisPerizinans: JenisPerizinan[] = [];
  statusKelayakans: StatusKelayakan[] = [];
  sumberPerolehans: SumberPerolehan[] = [];
  masterProgresses: MasterProgress[] = [];
  statusProgress = true;
  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;

  asetTanah: AsetTanah;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  lat = -6.2357278;
  lng = 106.818673;
  zoom = 17;

  modalRef: BsModalRef;

  poly: LatLng[][] = [[]];
  
  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];

  preview = '';

  @ViewChild('tabset') tabset: TabsetComponent;
  @ViewChild('templatePoint') templatePoint: TemplateRef<any>;
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('map') map: Map;

  endDateStatus = false;
  isRevisi = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private asetTanahService: AsetTanahService,
    private jenisPerizinanService: JenisPerizinanService,
    private statusKelayakanService: StatusKelayakanService,
    private sumberPerolehanService: SumberPerolehanService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private lokasiService: LokasiService,
    private masterProgressService: MasterProgressService,
    private _location: Location,
    private _fb: FormBuilder,
    private modalService: BsModalService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.jenisPerizinanService.getAll().subscribe(jenisPerizinans => {
      this.jenisPerizinans = jenisPerizinans;
    });
    this.statusKelayakanService.getAll().subscribe(statusKelayakans => {
      this.statusKelayakans = statusKelayakans;
    });
    this.sumberPerolehanService.getAll().subscribe(sumberPerolehans => {
      this.sumberPerolehans = sumberPerolehans;
    });
    this.masterProgressService.getAll().subscribe(masterProgresses =>{
      this.masterProgresses = masterProgresses;
    });

    this.formAsetTanah = this._fb.group({
      nama: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]],
      alamat: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(255)]],
      idProvinsi: ['', [Validators.required]],
      idKabupaten: ['', [Validators.required]],
      idLokasi: ['', [Validators.required]],
      idCompanyCode: ['', [Validators.required]],
      idBusinessArea: ['', [Validators.required]],
      polygon: this._fb.group({
        points: this._fb.array([], Validators.minLength(4))
      }),
      foto: ['', []],
      luas_tanah: ['', Validators.required],
      id_sumber_perolehan: ['', Validators.required],
      tahun_perolehan: ['', [Validators.required, Validators.min(1800), Validators.max(this.maxYear)]],
      nilai_perolehan: ['', Validators.required],
      no_bphtb: ['', [Validators.required]],
      no_sap: ['', []],
      id_ijin: ['', [Validators.required]],
      status_progress: ['', [Validators.required]],
      no_pbb: ['', [Validators.required]],
      no_sertifikat: ['', [Validators.required]],
      start_date_sertifikat: ['', [Validators.required]],
      end_date_sertifikat: ['', [Validators.required]],
      id_master_progress: ['', [Validators.required]],
      tanggal_progress: ['', [Validators.required]],
      keterangan_progress: ['', [Validators.required, Validators.maxLength(255)]]
    });
    // get list Kabupaten by Selected Id Provinsi
    this.route.url.subscribe(url => {
      this.asetTanah = new AsetTanah();
      this.asetTanah.detail = new AsetTanahDetail();
      this.asetTanah.perpindahan = new PerpindahanAsetTanah();
      this.asetTanah.dokumen = new DokumenLegal();
      this.asetTanah.dokumen.ijin = new JenisPerizinan();
      this.asetTanah.dokumen.opini_legal = new OpiniLegal();
      this.action = url[0].path;
      if (this.action === 'revisi-pengajuan') {
        this.title = 'Revisi Aset Tanah';
        this.isRevisi = true;
        this._revisi();
      } else {
        this.title = 'Tambah Aset Tanah';
        this._add();
      }
    });
  }

  private _add() {
    this.getProvinsi();
    this.getCompanyCode();
    this.asetTanah.dokumen.tahap_sertifikat.push(new TahapDokumenLegal());
    this.setStatusProgress(true);
    this.ready = true;
  }

  private _revisi() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idAsetTanah'));
      this.asetTanahService.getById(id).subscribe(asetTanah => {
        this.asetTanah = new AsetTanah(asetTanah);
        this.asetTanah.detail = this.asetTanah.detail_perubahan;
        this.asetTanah.perpindahan = this.asetTanah.perpindahan_perubahan;
        this.asetTanah.dokumen = this.asetTanah.dokumen_perubahan || this.asetTanah.dokumen_on_progress; 
        this.idCompanyCode = this.asetTanah.perpindahan.id_company_code;
        this.idBusinessArea = this.asetTanah.perpindahan.id_business_area;
        this.idProvinsi = this.asetTanah.perpindahan.lokasi ? this.asetTanah.perpindahan.lokasi.kabupaten.id_provinsi : null;
        this.idKabupaten = this.asetTanah.perpindahan.lokasi ? this.asetTanah.perpindahan.lokasi.id_kabupaten : null;
        this.idLokasi = this.asetTanah.perpindahan.id_lokasi;
        this.setStatusProgress(true);
        if(!this.asetTanah.dokumen_on_progress && !this.asetTanah.dokumen.end_date_sertifikat) {
          this.endDateStatus = false;
          this.changeEndDateStatus();
        } else if(this.asetTanah.dokumen_on_progress) {
          this.setStatusProgress(false);
        }
        if(this.asetTanah.detail && this.asetTanah.detail.polygon && this.asetTanah.detail.polygon[0] && this.asetTanah.detail.polygon[0].area) {
          const latlngs: number[] = this.asetTanah.detail.polygon[0].area.coordinates[0];
          for(let i = 0; i < (latlngs.length -1); i++) {
            this.addPoint(true, new LatLng(latlngs[i][0], latlngs[i][1]), true);
          }
          this.fitMap(this.poly[0]);
          this.hasPoint = true;
          this.lat = latlngs[0][0] ? latlngs[0][0] : this.lat;
          this.lng = latlngs[0][1] ? latlngs[0][1] : this.lng;
        }
        if(this.asetTanah.detail.foto) {
          this.oldFoto = this.uploadPath + this.asetTanah.detail.foto;
          this.formAsetTanah.get('foto').setValue(this.oldFoto);
        }
        this.getProvinsi();
        this.getCompanyCode();
        this.getKabupaten();
        this.getBusinessArea();
        this.getLokasi();
        this.ready = true;
        this.spinner.hide();
      })
    });
  }

  changeEndDateStatus() {
    if(!this.endDateStatus) {
      this.formAsetTanah.get('end_date_sertifikat').disable();
    } else {
      this.formAsetTanah.get('end_date_sertifikat').enable();
    }
    this.endDateStatus = !this.endDateStatus;
  }

  private _initPoint(withModel = true, latLng: LatLng) {
    if (withModel) {
      const newData = latLng;
      this.poly[0].splice(this.poly[0].length, 0, newData);
    }
    return this._fb.group({
      lat: ['', Validators.required],
      lng: ['', Validators.required]
    });
  }

  private setStatusProgress(selesai) {
    this.statusProgress = selesai;
    this.formAsetTanah.get('status_progress').setValue(selesai);
    this.asetTanah.dokumen.status_progress = selesai;
    if (selesai) {
      this.formAsetTanah.controls['no_pbb'].enable();
      this.formAsetTanah.controls['no_sertifikat'].enable();
      this.formAsetTanah.controls['start_date_sertifikat'].enable();
      this.formAsetTanah.controls['end_date_sertifikat'].enable();
      this.formAsetTanah.controls['id_master_progress'].disable();
      this.formAsetTanah.controls['tanggal_progress'].disable();
      this.formAsetTanah.controls['keterangan_progress'].disable();
    } else {
      this.formAsetTanah.controls['no_pbb'].disable();
      this.formAsetTanah.controls['no_sertifikat'].disable();
      this.formAsetTanah.controls['start_date_sertifikat'].disable();
      this.formAsetTanah.controls['end_date_sertifikat'].disable();
      this.formAsetTanah.controls['id_master_progress'].enable();
      this.formAsetTanah.controls['tanggal_progress'].enable();
      this.formAsetTanah.controls['keterangan_progress'].enable();
    }
  }

  public addPoint(withModel = true, latLng?: LatLng, isRevisi = false) {
    if((this.poly[0].length || this.modalRef) || isRevisi) {
      const formUmum = <FormGroup> this.formAsetTanah;//this.formAsetTanah.get('formUmum');
      const polygon = <FormGroup> formUmum.get('polygon');
      const points = <FormArray> polygon.get('points');
      latLng = latLng || new LatLng(this.lat, this.lng);
      points.push(this._initPoint(withModel, latLng));
    } else {
      this.modalRef = this.modalService.show(this.templatePoint, {
        class: 'modal-sm',
        keyboard: false,
        ignoreBackdropClick: true
      });
      this.modalService.onHide.subscribe(e => {
        this.hasPoint = true;
      });
    }
    return false;
  }

  addInitialPoint() {
    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng - 0.0005));
    this.addPoint(true, new LatLng(this.lat - 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng + 0.0005));
    this.addPoint(true, new LatLng(this.lat + 0.0005, this.lng - 0.0005));
    this.poly[0].push(new LatLng(this.lat - 0.0005, this.lng - 0.0005));
    this.modalRef.hide();
    this.modalRef = null;
  }

  public removePoint(index: number) {
    const formUmum = <FormGroup> this.formAsetTanah;//.get('formUmum');
    const polygon = <FormGroup> formUmum.get('polygon');
    const points = <FormArray> polygon.get('points');
    points.removeAt(index);
    this.poly[0].splice(index, 1);
    return false;
  }

  setLokasi() {
    this.asetTanah.perpindahan.id_lokasi = this.idLokasi; //this.lokasi.id_lokasi;
    this.lat = 0;// this.lokasi.lokasi_aset_detail.koordinat.coordinates[0];
    this.lng = 0;// this.lokasi.lokasi_aset_detail.koordinat.coordinates[1];
    this.poly[0] = [];
    const formUmum = <FormGroup> this.formAsetTanah;//.get('formUmum');
    const polygon = <FormGroup> formUmum.get('polygon');
    const points = <FormArray> polygon.get('points');
    const length = points.controls.length;
    for (let i = 0; i < length; i++) {
      points.removeAt(0);
    }
    return false;
  }

  next() {
    this.tabset.tabs[1].active = true;
    return false;
  }

  back() {
    this.tabset.tabs[0].active = true;
    return false;
  }

  cancel() {
    if (confirm('Batal menambah Aset Tanah?')) {
      this._location.back();
    }
    return false;
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      if(file.size > environment.max_file_upload_size) {
        alert(`Ukuran foto maksimal ${environment.max_file_upload_size/1000000} MB`);
        this.formUmum.get('foto').setValue(this.asetTanah.detail.foto);
        return false;
      }
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.oldFoto = null;
        const formUmum = this.formAsetTanah; // (this.formAsetTanah.controls.formUmum as FormGroup);
        formUmum.get('foto').setValue({
          filename: file.name,
          filetype: file.type,
          // @ts-ignore
          value: reader.result.split(',')[1]
        });
        this.asetTanah.detail.foto = formUmum.get('foto').value;
        // @ts-ignore
        this.preview = reader.result;
      };
    }
  }

  removeFoto() {
    this.asetTanah.detail.foto = null;
    this.preview = null;
    this.formAsetTanah.get('foto').setValue(null);
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.asetTanah.path = this.root + this.action;
    this.asetTanah.id_business_area = this.idBusinessArea;
    this.asetTanah.id_company_code = this.idCompanyCode;
    this.asetTanah.id_lokasi = this.idLokasi;
    this.asetTanah.status_progress = this.statusProgress;
    this.asetTanah.id_ijin = this.asetTanah.dokumen.ijin.id_ijin;
    this.asetTanah.nama_tanah = this.asetTanah.detail.nama_tanah;
    this.asetTanah.alamat_tanah = this.asetTanah.detail.alamat_tanah;
    this.asetTanah.luas_tanah = this.asetTanah.detail.luas_tanah;
    this.asetTanah.no_bphtb = this.asetTanah.detail.biaya_bphtb;
    this.asetTanah.no_sap = this.asetTanah.detail.no_sap;
    this.asetTanah.has_sertifikat = this.statusProgress;

    if(this.statusProgress) {
      this.asetTanah.no_sertifikat = this.asetTanah.dokumen.no_sertifikat;
      this.asetTanah.no_pbb = this.asetTanah.dokumen.no_pbb;
      this.asetTanah.start_date_sertifikat = this.asetTanah.dokumen.start_date_sertifikat;
      this.asetTanah.end_date_sertifikat = this.asetTanah.dokumen.end_date_sertifikat;
    } else {
      this.asetTanah.id_master_progress = this.asetTanah.dokumen.tahap_sertifikat[0].id_master_progress;
      this.asetTanah.keterangan = this.asetTanah.dokumen.tahap_sertifikat[0].keterangan;
      this.asetTanah.tanggal_progress = this.asetTanah.dokumen.tahap_sertifikat[0].tanggal_progress;
    }
    this.asetTanah.polygon = this.poly;
    this.asetTanahService.save(this.asetTanah)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this.router.navigate(['/aset-tanah']);
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  log(obj) {
    // console.log(obj);
    return false;
  }

  dblClick(e) {
    if(e.latlng && this.poly[0].length) {
      this.addPoint(true, e.latlng);
    }
  }

  private getCompanyCode() {
    const params = new URLSearchParams();
    this.companyCodeService.getAll(params).subscribe(companyCodes => {
      let selected = -1;
      this.itemsCC = companyCodes.map((companyCode, index) => {
        if(this.idCompanyCode == companyCode.id_company_code) {
          selected = index;
        }
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      if(selected > -1) {
        this.selectCC.active = [this.itemsCC[selected]];
      }
    });
  }

  private getBusinessArea() {
    const params = new URLSearchParams();
    params.set('level_access', 'true');
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode, params).subscribe(businessAreas => {
      let selected = -1;
      this.itemsBA = businessAreas.map((businessArea, index) => {
        if(this.idBusinessArea == businessArea.id_business_area) {
          selected = index;
        }
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      if(selected > -1) {
        this.selectBA.active = [this.itemsBA[selected]];
      }
    });
  }

  private getProvinsi() {
    this.provinsiService.getAll().subscribe(provinsis => {
      let selected = -1;
      this.itemsP = provinsis.map((provinsi, index) => {
        if(this.idProvinsi == provinsi.id_provinsi) {
          selected = index;
        }
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      if(selected > -1) {
        this.selectProvinsi.active = [this.itemsP[selected]];
      }
    });
  }

  private getKabupaten() {
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      let selected = -1;
      this.itemsK = kabupatens.map((kabupaten, index) => {
        if(this.idKabupaten == kabupaten.id_kabupaten) {
          selected = index;
        }
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      if(selected > -1) {
        this.selectKabupaten.active = [this.itemsK[selected]];
      }
    });
  }

  private getLokasi() {
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      let selected = -1;
      this.itemsL = lokasis.map((lokasi, index) => {
        if(this.idLokasi == lokasi.id_lokasi) {
          selected = index;
        }
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      if(selected > -1) {
        this.selectLokasi.active = [this.itemsL[selected]];
      }
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAsetTanah.get('idCompanyCode').setValue(e.id);
    this.formAsetTanah.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAsetTanah.get('idCompanyCode').setValue(e.id);
    this.formAsetTanah.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.formAsetTanah.get('idBusinessArea').setValue(e.id);
    this.formAsetTanah.get('idBusinessArea').updateValueAndValidity();
  }

  selectedBA(e: SelectItem) {
    this.formAsetTanah.get('idBusinessArea').setValue(e.id);
    this.formAsetTanah.get('idBusinessArea').updateValueAndValidity();
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formAsetTanah.get('idProvinsi').setValue(e.id);
    this.formAsetTanah.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.formAsetTanah.get('idProvinsi').setValue(e.id);
    this.formAsetTanah.get('idProvinsi').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formAsetTanah.get('idKabupaten').setValue(e.id);
    this.formAsetTanah.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.formAsetTanah.get('idKabupaten').setValue(e.id);
    this.formAsetTanah.get('idKabupaten').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    this.formAsetTanah.get('idLokasi').setValue(e.id);
    this.formAsetTanah.get('idLokasi').updateValueAndValidity();
  }

  selectedL(e: SelectItem) {
    this.formAsetTanah.get('idLokasi').setValue(e.id);
    this.formAsetTanah.get('idLokasi').updateValueAndValidity();
  }

  fitMap(coordinates: LatLng[]) {
    const bounds = this._getBounds(coordinates);
    this.map.fitBounds(bounds);
  }

  _getBounds(coordinates: LatLng[]): LatLngBounds {
    const lats: number[] = [];
    const lngs: number[] = [];
    
    coordinates.forEach(latlng => {
      lats.push(latlng[0] || latlng.lat);
      lngs.push(latlng[1] || latlng.lng);
    });
    
    const sw = new LatLng(Math.min(...lats), Math.min(...lngs));
    const ne = new LatLng(Math.max(...lats), Math.max(...lngs));

    return new LatLngBounds(sw, ne);
  }

}
