import { TimelineModule } from './../timeline/timeline.module';
import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe, DatePipe, UpperCasePipe, CurrencyPipe } from '@angular/common';

import { PotensiAsetRoutingModule } from './potensi-aset-routing.module';
import { PotensiAsetComponent } from './potensi-aset/potensi-aset.component';
import { PotensiAsetDetailComponent } from './potensi-aset-detail/potensi-aset-detail.component';
import { PotensiAsetEditComponent } from './potensi-aset-edit/potensi-aset-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { AlertService } from 'app/services/alert.service';
import {
  CompanyCodeService, KabupatenService, BusinessAreaService,
  PotensiAsetService, ProvinsiService, LokasiService, AsetTanahService, AsetPropertiService, JenisPropertiService, DataMapService
} from 'app/services/data-services';
import { YagaModule } from '@yaga/leaflet-ng2';
import { AppGeometryModule } from 'app/components/app-geometry/app-geometry.module';
import { TabsModule } from 'ngx-bootstrap';
import { AppDatatableModule } from '../../components/app-datatable/app-datatable.module';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { SelectModule } from 'ng2-select';
import { InputNumberModule } from '../input-number/input-number.module';
import { MapLegendModule } from '../map-legend/map-legend.module';

@NgModule({
  imports: [
    CommonModule,
    PotensiAsetRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    YagaModule,
    TabsModule,
    AppGeometryModule,
    AppDatatableModule,
    SelectModule,
    TabsModule,
    TimelineModule,
    InputNumberModule,
    MapLegendModule
  ],
  providers: [
    PotensiAsetService, AlertService, ProvinsiService,
    KabupatenService, CompanyCodeService, BusinessAreaService,
    AsetPropertiService, AsetTanahService, LokasiService,
    JenisPropertiService, DecimalPipe, DatePipe, TextToSlugPipe,
    UpperCasePipe, CurrencyPipe, DataMapService
  ],
  declarations: [PotensiAsetComponent, PotensiAsetDetailComponent, PotensiAsetEditComponent]
})
export class PotensiAsetModule { }
