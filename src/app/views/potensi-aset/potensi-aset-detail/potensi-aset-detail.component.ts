import { TextToSlugPipe } from './../../../services/text-to-slug.service';
import { AsetTanah } from './../../../models/aset-tanah.model';
import { AsetProperti } from './../../../models/aset-properti.model';
import { PendayagunaanAset } from 'app/models/pendayagunaan-aset.model';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { PotensiAsetService, PenggunaService } from 'app/services/data-services';
import { environment } from 'environments/environment';

import { PotensiAset } from 'app/models/potensi-aset.model';
import { DatatablesContent } from '../../../components/app-datatable/datatables-content.model';
import { DecimalPipe, DatePipe, UpperCasePipe, Location } from '@angular/common';
import { Pengguna } from '../../../models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-potensi-aset-detail',
  templateUrl: './potensi-aset-detail.component.html',
  styleUrls: ['./potensi-aset-detail.component.scss']
})
export class PotensiAsetDetailComponent implements OnInit {
  memo = '';
  tab = 'detail';
  potensiAset: PotensiAset;
  potensi: PotensiAset;
  potensiPerubahan: PotensiAset;
  potensiHistory: PotensiAset;
  pendayagunaanAset: PendayagunaanAset[] = [];
  aset: AsetProperti | AsetTanah;
  idAset: number;

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.lokasi;

  name = 'Passau';
  lat = -6.2357278;
  lng = 106.818673;
  potensiLat = -6.2357278;
  potensiLng = 106.818673;
  zoom = 15;
  namaPotensi = '';
  namaCompanyCode = '';
  root = 'potensi-aset/'
  modalRef: BsModalRef;

  historyPendayagunaan = new DatatablesContent();
  history = new DatatablesContent();
  isAsetTanah = false;
  documentReady = false;
  pengguna =  new Pengguna();

  historisDisposisi = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private potensiService: PotensiAsetService,
    private modalService: BsModalService,
    private numberPipe: DecimalPipe,
    private datePipe: DatePipe,
    private slugPipe: TextToSlugPipe,
    private uppercasePipe: UpperCasePipe,
    private penggunaService: PenggunaService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.penggunaService.getRoleForModule('potensi').subscribe(pengguna => {
      this.pengguna = pengguna;
      this.route.paramMap.subscribe(params => {
        const id = parseInt(params.get('idPotensiAset'), 10);
        this.potensiService.getById(id).subscribe((potensiAset: PotensiAset) => {
          this.potensiAset = potensiAset;
          this.aset = potensiAset.potensiable_type === 'App\\Tanah' ? new AsetTanah(potensiAset.potensiable) : new AsetProperti(potensiAset.potensiable);
          this.potensi = this.aset.potensi;
          this.potensiHistory = this.potensiAset.is_history ? this.potensiAset : null;
          if (this.pengguna.is_user_input || this.pengguna.is_validator_unit || this.pengguna.is_validator_pusat) {
            this.potensiPerubahan = ! this.potensiAset.is_use && ! this.potensiAset.is_history ? this.potensiAset : null;
          }
          this.isAsetTanah = potensiAset.potensiable_type === 'App\\Tanah';
          this.idAset = potensiAset.potensiable_id;
          this._mapPendayagunaanToDatatable(this.potensiAset.pendayagunaan_aktif || []);
          this._mapHistoryToDatatable(this.potensiAset.potensiable.histori_potensi || []);
          this.documentReady = true;
          this.spinner.hide();
        });
      });
    });
  }

  edit(potensiAset, isRevisi = false) {
    const tipe = isRevisi ? 'revisi' : 'edit';
    const nama = this.namaPotensi ? this.slugPipe.transform(this.namaPotensi) : 'potensi';
    this.router
      .navigate([`${this.root}${tipe}/${potensiAset.id_potensi}/${nama}`]);
    return false;
  }

  revisi(potensiAset) {
    this.edit(potensiAset, true);
    return false;
  }

  delete() {
    this.potensiAset.path = '/potensi-aset/non-aktif/1/delete';
    this.potensiService.destroy(this.potensiAset)
    .subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.potensiAset.is_active = false;
      }
    });
    return false;
  }

  tolakUsulanPotensi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.potensiService
      .tolakUsulanPotensi(this.potensiPerubahan.id_potensi, this.memo, validatorType)
      .subscribe(res => {
      this.modalRef.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.potensiPerubahan = null;
      }
    });
    return false;
  }

  terimaUsulanPotensi() {
    if (!this.pengguna.is_validator_unit && !this.pengguna.is_validator_pusat) {
      return false;
    }
    const validatorType = this.pengguna.is_validator_pusat ? 'pusat' : (this.pengguna.is_validator_unit ? 'unit' : 'none');
    this.potensiService
      .terimaUsulanPotensi(this.potensiPerubahan.id_potensi, this.memo, validatorType)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.potensiAset = this.potensiPerubahan;
          this.potensiPerubahan = null;
        }
    });
    return false;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  addPendayagunaan() {
    const jenisAset = this.isAsetTanah ? 'aset-tanah' : 'aset-properti';
    this.router.navigate(['pendayagunaan-aset/add'], {queryParams: {id: this.idAset, jenis: jenisAset}});
    return false;
  }

  showPendayagunaan(id: number) {
    this.router.navigate(['pendayagunaan-aset/show/' + id + '/pendayagunaan']);
    return false;
  }

  private _mapPendayagunaanToDatatable(pendayagunaans: PendayagunaanAset[]) {
    this.historyPendayagunaan.columns = ['JENIS PENDAYAGUNAAN',	'NO. KONTRAK',	'NILAI',	'TANGGAL MULAI',	'TANGGAL BERAKHIR'];
    this.historyPendayagunaan.actions = ['show', 'nonaktif'];
    this.historyPendayagunaan.data.next([]);
    const data = [];

    pendayagunaans.map((r: PendayagunaanAset) => {
      const jenis = r.jenis_dayaguna && r.jenis_dayaguna.nama_jenis_dayaguna ? r.jenis_dayaguna.nama_jenis_dayaguna : '-';
      const kontrak = r.kontrak ? r.kontrak : '-';
      const nilai = r.nilai_pendayagunaan ? this.numberPipe.transform(r.nilai_pendayagunaan) : '-';
      const tanggalMulai = r.start_date_pendayagunaan ? this.datePipe.transform(r.start_date_pendayagunaan, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_pendayagunaan ? this.datePipe.transform(r.end_date_pendayagunaan, 'dd MMM yyyy') : '-';

      data.push({
        row: [ jenis, kontrak, nilai, tanggalMulai, tanggalSelesai ],
        id: r.id_pendayagunaan
      });
    });

    this.historyPendayagunaan.data.next(data);
  }

  private _mapHistoryToDatatable(potensis: PotensiAset[]) {
    this.history.columns = ['DESKRIPSI', 'NILAI (Rp)', 'TANGGAL MULAI', 'TANGGAL SELESAI'];
    this.history.actions = [];
    this.history.data.next([]);
    const data = [];

    potensis.map((r: PotensiAset) => {
      const deskripsi = r.deskripsi ? this._toUppercase(r.deskripsi) : '-';
      const nilai = r.nilai_potensi ? this.numberPipe.transform(r.nilai_potensi, '1.0-2') : '-';
      const tanggalMulai = r.start_date_potensi ? this.datePipe.transform(r.start_date_potensi, 'dd MMM yyyy') : '-';
      const tanggalSelesai = r.end_date_potensi ? this.datePipe.transform(r.end_date_potensi, 'dd MMM yyyy') : '-';

      data.push({
        row: [ deskripsi, nilai, tanggalMulai, tanggalSelesai ],
        id: r.id_potensi
      });
    });

    this.history.data.next(data);
  }

  private _toUppercase(str: string) {
    return this.uppercasePipe.transform(str || '');
  }

  back() {
    this._location.back();
    return false;
  }

}
