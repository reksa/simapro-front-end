import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotensiAsetDetailComponent } from './potensi-aset-detail.component';

describe('PotensiAsetDetailComponent', () => {
  let component: PotensiAsetDetailComponent;
  let fixture: ComponentFixture<PotensiAsetDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotensiAsetDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotensiAsetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
