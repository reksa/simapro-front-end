import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotensiAsetEditComponent } from './potensi-aset-edit.component';

describe('PotensiAsetEditComponent', () => {
  let component: PotensiAsetEditComponent;
  let fixture: ComponentFixture<PotensiAsetEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotensiAsetEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotensiAsetEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
