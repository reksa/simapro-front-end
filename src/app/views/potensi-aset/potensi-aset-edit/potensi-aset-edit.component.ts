import {
  PotensiAsetService, BusinessAreaService, ProvinsiService,
  KabupatenService, AsetTanahService, AsetPropertiService,
  LokasiService,
  JenisPropertiService,
  CompanyCodeService
} from 'app/services/data-services';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Location, UpperCasePipe } from '@angular/common';
import { 
  Provinsi, Kabupaten, BusinessArea, Lokasi, AsetTanah, AsetProperti, Pengguna,
  PotensiAset, JenisAsetProperti 
} from 'app/models';
import { URLSearchParams } from '@angular/http';
import { SelectItem, SelectComponent } from 'ng2-select';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-potensi-aset-edit',
  templateUrl: './potensi-aset-edit.component.html',
  styleUrls: ['./potensi-aset-edit.component.scss']
})
export class PotensiAsetEditComponent implements OnInit {
  // page setting
  action = '';
  title = '';
  ready = false;
  asetSelected = false;
  formTitle = '';
  path;
  isRevisi = false;

  isSubmitted: boolean;
  formPotensi: FormGroup;
  provinsis: Provinsi[] = [];
  kabupatens: Kabupaten[] = [];
  businessAreas: BusinessArea[] = [];
  lokasis: Lokasi[] = [];
  jenisAsetPropertis: JenisAsetProperti[] = [];
  asets: AsetTanah[] | AsetProperti[] = [];
  aset: AsetTanah | AsetProperti;

  idJenisAset: number;

  jenisAset = 'App\\Tanah';

  potensiAset: PotensiAset;
  pengguna: Pengguna;
  
  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;

  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];
  itemsT = [];

  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('selectAset') selectAset: SelectComponent;

  addByAset: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private _location: Location,
    private _fb: FormBuilder,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private potensiAsetService: PotensiAsetService,
    private asetTanahService: AsetTanahService,
    private asetPropertiService: AsetPropertiService,
    private lokasiService: LokasiService,
    private jenisPropertiService: JenisPropertiService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.formPotensi = this._fb.group({
      aset: ['', [Validators.required]],
      nilai_potensi: ['', [Validators.required]],
      deskripsi: ['', [Validators.required]],
      start_date_potensi: ['', [Validators.required]],
      end_date_potensi: ['', [Validators.required]]
    });
    this.route.url.subscribe(url => {
      this.action = url[0].path;

      switch (this.action) {
        case 'revisi': this._revisi();
          break;
        case 'edit': this._edit();
          break;
        case 'add':
        default: this._add();
      }
    });
  }

  private _add() {
    this.path = '/potensi-aset/add';
    this.route.queryParamMap.subscribe(queryParams => {
      const idAset = parseInt(queryParams.get('id'), 10);
      const jenisAset = queryParams.get('jenis');
      this.jenisAset = jenisAset === 'aset-properti' ? 'App\\Properti' : 'App\\Tanah';
      if (idAset && this.jenisAset) {
        this.addByAset = true;
        if (this.jenisAset === 'App\\Tanah') {
          this.asetTanahService.getById(idAset).subscribe(asetTanah => {
            this.aset = new AsetTanah(asetTanah);
            this.potensiAset = this.aset.potensi ? Object.assign({}, this.aset.potensi) : new PotensiAset();
            this.potensiAset.id_potensi = null;
            this.asetSelected = true;

            this.idCompanyCode = this.aset.perpindahan.business_area.id_company_code;
            this.idBusinessArea = this.aset.perpindahan.business_area.id_business_area;
            this.idProvinsi = this.aset.perpindahan.lokasi.kabupaten.id_provinsi;
            this.idKabupaten = this.aset.perpindahan.lokasi.id_kabupaten;
            this.idLokasi = this.aset.perpindahan.id_lokasi;
            this.getProvinsi();
            this.getCompanyCode();
            this.getKabupaten()
            this.getBusinessArea();
            this.getLokasi();
            this._getAset();
            this.formPotensi.get('aset').setValue(this.aset);
          });
        } else if (this.jenisAset === 'App\\Properti') {
          this.asetPropertiService.getById(idAset).subscribe(asetProperti => {
            this.aset = new AsetProperti(asetProperti);
            this.potensiAset = this.aset.potensi ? Object.assign({}, this.aset.potensi) : new PotensiAset();
            this.potensiAset.id_potensi = null;
            this.asetSelected = true;

            const tanah = new AsetTanah(this.aset.perpindahan.tanah);
            this.idCompanyCode = this.aset.perpindahan.business_area.id_company_code;
            this.idBusinessArea = this.aset.perpindahan.business_area.id_business_area;
            this.idProvinsi = tanah.perpindahan.lokasi.kabupaten.id_provinsi;
            this.idKabupaten = tanah.perpindahan.lokasi.id_kabupaten;
            this.idLokasi = tanah.perpindahan.id_lokasi;
            this.getProvinsi();
            this.getCompanyCode();
            this.getKabupaten()
            this.getBusinessArea();
            this.getLokasi();
            this._getAset();
            this.formPotensi.get('aset').setValue(this.aset);
          });
        }
      } else {
        this.potensiAset = new PotensiAset();
        this.getCompanyCode();
        this.getProvinsi();
      }
    });
    this.jenisPropertiService.getAll().subscribe(jenisPropertis => {
      this.jenisAsetPropertis = jenisPropertis;
    });
    this.formTitle = 'Tambah Potensi Aset';
  }

  private _edit() {
    this.path = '/potensi-aset/edit/1/nama';
    this.formTitle = 'Ubah Potensi Aset';
    this.aset = new AsetTanah();
    this._getSelectedAset();
  }

  private _revisi() {
    this.isRevisi = true;
    this.path = '/potensi-aset/revisi/1/nama';
    this.formTitle = 'Revisi Potensi Aset';
    this.aset = new AsetTanah();
    this._getSelectedAset(true);
  }

  private _getSelectedAset(isRevisi = true) {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idPotensiAset'), 10);
      this.potensiAsetService.getById(id).subscribe(potensiAset => {
        this.spinner.hide();
        if (!(potensiAset.is_editable_potensi || (potensiAset.action_button && potensiAset.action_button.type === 'revisi' && potensiAset.action_button.button))) {
          alert('Masih Ada Pengajuan Yang Sedang Diproses');
          this.cancel();
        }
        this.potensiAset = potensiAset;
        this.potensiAset.id_potensi = this.isRevisi ? this.potensiAset.id_potensi : null;

        this.jenisAset = this.potensiAset.potensiable_type;
        if (this.jenisAset === 'App\\Tanah') {
          this.aset = new AsetTanah(potensiAset.potensiable);
          this.asetSelected = true;

          this.idCompanyCode = this.aset.perpindahan.business_area.id_company_code;
          this.idBusinessArea = this.aset.perpindahan.business_area.id_business_area;
          this.idProvinsi = this.aset.perpindahan.lokasi.kabupaten.id_provinsi;
          this.idKabupaten = this.aset.perpindahan.lokasi.id_kabupaten;
          this.idLokasi = this.aset.perpindahan.id_lokasi;
        } else if (this.jenisAset === 'App\\Properti') {
          this.aset = new AsetProperti(potensiAset.potensiable);
          this.asetSelected = true;

          const tanah = new AsetTanah(this.aset.perpindahan.tanah);
          this.idCompanyCode = this.aset.perpindahan.business_area.id_company_code;
          this.idBusinessArea = this.aset.perpindahan.business_area.id_business_area;
          this.idProvinsi = tanah.perpindahan.lokasi.kabupaten.id_provinsi;
          this.idKabupaten = tanah.perpindahan.lokasi.id_kabupaten;
          this.idLokasi = tanah.perpindahan.id_lokasi;
        }
        this.formPotensi.get('aset').setValue(this.aset);
        this.getCompanyCode();
        this.getProvinsi();
        this.getKabupaten();
        this.getBusinessArea();
        this.getLokasi();
        this._getAset();
        this.jenisAset = potensiAset.potensiable_type;
        this.asetSelected = true;
        if (!isRevisi) {
          this.potensiAset.id_potensi = null;
        } 
        if (this.aset.detail) {
          this.asetSelected = true;
        } else {
          alert('aset belum di validasi');
        }
      });
    });
  }

  private _getKabupaten() {
    this.kabupatenService.getByIdProvinsi(this.idProvinsi)
      .subscribe(kabupatens => {
        this.kabupatens = kabupatens;
      });
  }

  private _getLokasi() {
    this.lokasiService.getByIdKabupaten(this.idKabupaten)
      .subscribe(lokasis => {
        this.lokasis = lokasis;
    });
  }

  _getAset() {
    if (! this.idLokasi || ! this.idBusinessArea) {
      return false;
    }
    if (this.jenisAset === 'App\\Tanah') {
      this.getTanah();
    } else if (this.jenisAset === 'App\\Properti') {
      this.getProperti();
    }
  }

  changeAset() {
    this.asetSelected = false;
  }

  chooseAset(aset: AsetTanah | AsetProperti) {
    if (aset.detail) {
      return true;
    }else return false;
  }

  cancel() {
    this._location.back();
    return false;
  }

  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.potensiAset.jenis_potensi = this.jenisAset;
    this.potensiAset.path = this.path;
    if (this.jenisAset === 'App\\Tanah') {
      const aset = <AsetTanah> this.aset
      this.potensiAset.potensiable_id = aset.id_tanah;
    } else if (this.jenisAset === 'App\\Properti') {
      const aset = <AsetProperti> this.aset
      this.potensiAset.potensiable_id = aset.id_properti;
    }
    this.potensiAsetService.save(this.potensiAset, this.isRevisi)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();
        } else {
          this.isSubmitted = false;
        }
    });
  }

  log(obj) {
    // console.log(obj);
    return false;
  }

  private getCompanyCode() {
    this.itemsCC = [];
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
        }
      });
    });
  }

  private getBusinessArea() {
    this.itemsBA = [];
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id == this.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  private getProvinsi() {
    this.itemsP = [];
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
        }
      });
    });
  }

  private getKabupaten() {
    this.itemsK = [];
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
        }
      });
    });
  }

  private getLokasi() {
    this.itemsL = [];
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id == this.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
        }
      });
    });
  }

  private getTanah() {
    this.itemsT = [];
    const params = new URLSearchParams();
    params.set('id_lokasi', this.idLokasi.toString());
    params.set('id_business_area', this.idBusinessArea.toString());
    this.asetTanahService.getAll(params).subscribe(tanahs => {
        this.itemsT = tanahs.map(tanah => {
        tanah = new AsetTanah(tanah);
        return {id: tanah, text: this.uppercasePipe.transform(tanah.detail.nama_tanah)};
      });
      this.itemsT.forEach((item,index) => {
        if (this.aset && item.id.id_tanah == this.aset.id) {
          this.selectAset.active = [this.itemsT[index]];
        }
      });
    });
  }

  private getProperti() {
    this.itemsT = [];
    const params = new URLSearchParams();
    params.set('id_lokasi', this.idLokasi.toString());
    params.set('id_business_area', this.idBusinessArea.toString());
    this.asetPropertiService.getAll(params).subscribe(propertis => {
        this.itemsT = propertis.map(properti => {
        properti = new AsetProperti(properti);
        return {id: properti, text: this.uppercasePipe.transform(properti.detail.nama_properti)};
      });
      this.itemsT.forEach((item,index) => {
        if (this.aset && item.id.id_properti == this.aset.id) {
          this.selectAset.active = [this.itemsT[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    const old = this.idBusinessArea;
    this.idBusinessArea = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  selectedBA(e: SelectItem) {
    const old = this.idBusinessArea;
    this.idBusinessArea = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    const old = this.idLokasi;
    this.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  selectedL(e: SelectItem) {
    const old = this.idLokasi;
    this.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  refreshValueT(e) {
    if(this.chooseAset(e.id)) {
      this.aset = e.id;
      this.asetSelected = true;
      this.formPotensi.get('aset').setValue(this.aset);
    }
  }

  selectedT(e) {
    if(this.chooseAset(e.id)) {
      this.aset = e.id;
      this.asetSelected = true;
      this.formPotensi.get('aset').setValue(this.aset);
    }
  }

}
