import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotensiAsetComponent } from './potensi-aset.component';

describe('PotensiAsetComponent', () => {
  let component: PotensiAsetComponent;
  let fixture: ComponentFixture<PotensiAsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotensiAsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotensiAsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
