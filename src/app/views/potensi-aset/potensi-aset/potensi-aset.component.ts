import { TextToSlugPipe } from './../../../services/text-to-slug.service';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'environments/environment';

import {
  CompanyCodeService, ProvinsiService, PotensiAsetService, PenggunaService, DataMapService
 } from 'app/services/data-services';

import { PotensiAset, Koordinat, Provinsi, CompanyCode, Pengguna, AppGeoPolygon } from 'app/models';
import { LatLngBoundsExpression } from 'leaflet';
import { ModalFilterContentComponent } from '../../modal-filter-content/modal-filter-content.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-potensi-aset',
  templateUrl: './potensi-aset.component.html',
  styleUrls: ['./potensi-aset.component.scss']
})
export class PotensiAsetComponent implements OnInit {
  tab = 'map';
  root = 'potensi-aset/';
  showFilter = {
    wilayah: 'provinsi',
    unit: 'company-code'
  };
  subscription: Subscription;

  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;

  uploadDir = `${environment.upload_url}/`;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  public lat = -2.964984369333955;
  public lng = 114.08203125000001;
  zoom = 5;
  markers: Koordinat[] = [];

  idProvinsi = null;
  idKabupaten = null;
  provinsis: Provinsi[] = [];
  provinsi: Provinsi;
  companyCodes: CompanyCode[] = [];
  companyCode: CompanyCode;
  idBusinessArea = null;
  markersPotensiTanah: {
    id_tanah: number,
    id_potensi?: number,
    nama_tanah: string,
    alamat_tanah: string,
    luas_tanah: number,
    foto:string,
    no_bphtb: string,
    clustered: number,
    cluster_name: string,
    lokasi: string,
    koordinat?: Koordinat,
    area?: AppGeoPolygon,
    deskripsi_potensi: string
  }[] = [];
  markersPotensiProperti: {
    id_properti: number,
    id_potensi?: number,
    nama_properti: string,
    alamat_properti: string,
    luas_properti: number,
    foto:string,
    clustered: number,
    cluster_name: string,
    lokasi: string,
    koordinat: Koordinat,
    area?: AppGeoPolygon,
    deskripsi_potensi: string
  }[] = [];


  bounds: LatLngBoundsExpression;
  @ViewChild('map') map;
  array: Array<number> = [];
  markersTanah: {clustered: number, nama: string, koordinat: Koordinat}[] = [];
  lastRequestParams = {
    zoom: null,
    bounds: null
  };

  public tableConfig = new AppTableConfig();
  pengguna = new Pengguna();
  modalRef: BsModalRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private potensiAsetService: PotensiAsetService,
    private companyCodeService: CompanyCodeService,
    private provinsiService: ProvinsiService,
    private penggunaService: PenggunaService,
    private modalService: BsModalService,
    private mapService: DataMapService,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.penggunaService.getRoleForModule('potensi').subscribe(pengguna => this.pengguna = pengguna);

    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.companyCodes = companyCodes;
    });

    this.provinsiService.getAll().subscribe(provinsis => {
      this.provinsis = provinsis;
    });

    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      if (this.tab === 'map') {
        this._map();
      } else {
        this._data();
      }
    });
  }
  
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private _map() {
    this._getMapData();
  }

  _getMapData() {
    if (this.lastRequestParams.zoom !== this.zoom || this.lastRequestParams.bounds !== this.map.getBounds()) {
      if (this.lastRequestParams.zoom !== this.zoom){
        this.map.closePopup();
      }
      const params = {};
      if (this.dataSetting.idKabupaten && this.dataSetting.idKabupaten != 0) {
        params['id_kabupaten'] = this.dataSetting.idKabupaten;
      }
      if (this.dataSetting.idProvinsi && this.dataSetting.idProvinsi != 0) {
        params['id_provinsi'] = this.dataSetting.idProvinsi;
      }
      if (this.dataSetting.idBusinessArea && this.dataSetting.idBusinessArea != 0){
        params['id_business_area'] = this.dataSetting.idBusinessArea;
      }
      if (this.dataSetting.idCompanyCode && this.dataSetting.idCompanyCode != 0){
        params['id_company_code'] = this.dataSetting.idCompanyCode;
      }
      if (this.dataSetting.idLokasiAset && this.dataSetting.idLokasiAset != 0){
        params['id_lokasi'] = this.dataSetting.idLokasiAset;
      }
      if (this.dataSetting.nama) {
        params['nama'] = this.dataSetting.nama;
      }
      this.lastRequestParams = {zoom: this.zoom, bounds: this.map.getBounds()};
      this.subscription = this.mapService.get(this.zoom, this.map.getBounds(), ['potensi_tanah', 'potensi_properti'], params)
        .subscribe(markers => {
          this.markersPotensiTanah = markers.potensi_tanah || [];
          this.markersPotensiProperti = markers.potensi_properti || [];
        });
      }
  }

  private _data() {
    let data: Subscription;
    if(data) {
      data.unsubscribe();
    }
    this.pageData = [];
    data = this.route.queryParamMap.switchMap(params => {
      this.pageData = [];
      this.isLoading = true;
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      if (params.get('idKabupaten')) {
        parameters.append('id_kabupaten', params.get('idKabupaten'));
        this.dataSetting.idKabupaten = parseInt(params.get('idKabupaten'));
      }
      if (params.get('idProvinsi')) {
        parameters.append('id_provinsi', params.get('idProvinsi'));
        this.dataSetting.idProvinsi = parseInt(params.get('idProvinsi'));
      }
      if (params.get('idBusinessArea')) {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.dataSetting.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      if (params.get('idCompanyCode')) {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.dataSetting.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('idLokasiAset')) {
        parameters.append('id_lokasi', params.get('idLokasiAset'));
        this.dataSetting.idLokasiAset = parseInt(params.get('idLokasiAset'));
      }
      if (params.get('nama') !== '') {
        parameters.append('nama', params.get('nama'));
      }

      const data = this.tab === 'data-perubahan' ?
                                  this.potensiAsetService.getPaginateNonValid(parameters) :
                                  this.potensiAsetService.getPaginateValid(parameters);
      return data;
    }).subscribe((res) => {
      this.dataSetting.dataPerPage = <number> res.per_page;
      this.pageData = res.data.map(d => {
        return new PotensiAset(d);
      });
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      if (this.modalRef) {
        this.modalRef.hide();
      }

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  chooseTab(tab: string) {
    this.tab = tab;
    return false;
  }

  openModal() {
    const initialState = {
      dataSetting: Object.assign({}, this.dataSetting)
    }
    this.modalRef = this.modalService.show(ModalFilterContentComponent, { initialState, class: 'modal-lg', ignoreBackdropClick: true });
    const content = <ModalFilterContentComponent> this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
      if (content.filter) {
        this.dataSetting = content.dataSetting;
        if(this.tab !== 'map') {
          this.search();
        } else {
          this._getMapData();
        }
      }
      subscribeModal.unsubscribe();
    })
  }

  show(potensiAset: PotensiAset) {
    this.router.navigate([`${this.root}show/${potensiAset.id_potensi}/detail`]);
    return false;
  }

  showFromMap(id: number) {
    return `show/${id}/detail`;
  }
}
