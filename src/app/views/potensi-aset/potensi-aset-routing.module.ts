import { PotensiAsetEditComponent } from './potensi-aset-edit/potensi-aset-edit.component';
import { PotensiAsetDetailComponent } from './potensi-aset-detail/potensi-aset-detail.component';
import { PotensiAsetComponent } from './potensi-aset/potensi-aset.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';

const routes: Routes = [
  { path: '', component: PotensiAsetComponent },
  { path: 'add', component: PotensiAsetEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idPotensiAset/:nama', component: PotensiAsetDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idPotensiAset/:nama', component: PotensiAsetEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idPotensiAset/:nama', component: PotensiAsetEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PotensiAsetRoutingModule { }
