import { PendayagunaanEditComponent } from './pendayagunaan-edit/pendayagunaan-edit.component';
import { PendayagunaanDetailComponent } from './pendayagunaan-detail/pendayagunaan-detail.component';
import { PendayagunaanComponent } from './pendayagunaan/pendayagunaan.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';

const routes: Routes = [
  { path: '', component: PendayagunaanComponent },
  { path: 'show/:idPendayagunaanAset/:nama', component: PendayagunaanDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'add', component: PendayagunaanEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idPendayagunaanAset/:nama', component: PendayagunaanEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendayagunaanAsetRoutingModule { }
