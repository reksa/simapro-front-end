import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendayagunaanDetailComponent } from './pendayagunaan-detail.component';

describe('PendayagunaanDetailComponent', () => {
  let component: PendayagunaanDetailComponent;
  let fixture: ComponentFixture<PendayagunaanDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendayagunaanDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendayagunaanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
