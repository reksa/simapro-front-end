import { AsetTanah } from './../../../models/aset-tanah.model';
import { AsetProperti } from './../../../models/aset-properti.model';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { PendayagunaanAsetService, PenggunaService } from 'app/services/data-services';
import { environment } from 'environments/environment';

import { PendayagunaanAset } from 'app/models/pendayagunaan-aset.model';
import { DatatablesContent } from '../../../components/app-datatable/datatables-content.model';
import { DecimalPipe, DatePipe, Location } from '@angular/common';
import { PotensiAset, Pengguna } from '../../../models';
import { TextToSlugPipe } from '../../../services/text-to-slug.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pendayagunaan-detail',
  templateUrl: './pendayagunaan-detail.component.html',
  styleUrls: ['./pendayagunaan-detail.component.scss']
})
export class PendayagunaanDetailComponent implements OnInit {
  memo = '';
  tab = 'detail';
  pendayagunaanAset: PendayagunaanAset;
  pendayagunaanAsetPerubahan: PendayagunaanAset;
  aset: AsetProperti | AsetTanah;
  potensiAset: PotensiAset;
  idAset: number;

  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker.lokasi;
  is_validator = false;
  editable = true;
  name = 'Passau';
  lat = -6.2357278;
  lng = 106.818673;
  pendayagunaanLat = -6.2357278;
  pendayagunaanLng = 106.818673;
  zoom = 15;
  namaPendayagunaan = '';
  namaCompanyCode = '';
  root = 'pendayagunaan-aset/'
  modalRef: BsModalRef;

  historyPendayagunaan = new DatatablesContent();
  isAsetTanah = false;
  documentReady = false;
  pengguna = new Pengguna();

  historisDisposisi = [];
  id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pendayagunaanService: PendayagunaanAsetService,
    private modalService: BsModalService,
    private numberPipe: DecimalPipe,
    private datePipe: DatePipe,
    private slugPipe: TextToSlugPipe,
    private penggunaService: PenggunaService,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.penggunaService.getRoleForModule('pendayagunaan').subscribe(pengguna => {
      this.pengguna = pengguna;
      this.route.paramMap.subscribe(params => {
        this.id = parseInt(params.get('idPendayagunaanAset'), 10);
        this._init();
      });
    });
  }

  private _init() {
    this.spinner.show();
    this.pendayagunaanService.getById(this.id).subscribe((pendayagunaanAset: PendayagunaanAset) => {
      this.pendayagunaanAset = pendayagunaanAset;
      this.pendayagunaanAsetPerubahan = null;
      this.isAsetTanah = pendayagunaanAset.pendayagunaanable_type === 'App\\Tanah';
      this.aset = this.isAsetTanah ? new AsetTanah(pendayagunaanAset.pendayagunaanable) : new AsetProperti(pendayagunaanAset.pendayagunaanable);
      this.potensiAset = pendayagunaanAset.potensi;
      this.idAset = pendayagunaanAset.pendayagunaanable_id;
      this.documentReady = true;
      this.spinner.hide();
    });
  }

  revisi() {
    // @ts-ignore
    const nama = this.slugPipe.transform(this.aset.detail.nama_tanah || this.aset.detail.nama_properti || '' );
    this.router
      .navigate([`${this.root}revisi/${this.pendayagunaanAset.id_pendayagunaan}/${nama}`]);
    return false;
  }

  delete() {
    this.pendayagunaanAset.path = '/pendayagunaan-aset/non-aktif/1/delete';
    this.pendayagunaanService.destroy(this.pendayagunaanAset)
      .subscribe(response => {
        if (response.message) {
          alert(response.message);
        }

        if (response.success) {
          this.pendayagunaanAset.is_active = false;
        }
      });
  }

  tolakUsulanPendayagunaan() {
    if(this.modalRef) {
      this.modalRef.hide();
    }
    if (!(this.pendayagunaanAset.action_button && this.pendayagunaanAset.action_button.type === 'validasi' && this.pendayagunaanAset.action_button.button)) {
      return false;
    }
    this.spinner.show();
    this.pendayagunaanService
      .validasi(this.pendayagunaanAset.id_pendayagunaan, false, this.memo)
      .subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.pendayagunaanAset.action_button.button = false;
      }
    });
  }

  terimaUsulanPendayagunaan() {
    if(this.modalRef) {
      this.modalRef.hide();
    }
    if (!(this.pendayagunaanAset.action_button && this.pendayagunaanAset.action_button.type === 'validasi' && this.pendayagunaanAset.action_button.button)) {
      return false;
    }
    this.spinner.show();
    this.pendayagunaanService
      .validasi(this.pendayagunaanAset.id_pendayagunaan, true, this.memo)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.pendayagunaanAset.action_button.button = false;
          this.pendayagunaanAset.is_use = true;
        }
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  addPendayagunaan() {
    const jenisAset = this.isAsetTanah ? 'aset-tanah' : 'aset-properti';
    this.router.navigate(['pendayagunaan-aset/add'], {queryParams: {id: this.idAset, jenis: jenisAset}});
  }

  back() {
    this._location.back();
    return false;
  }
}
