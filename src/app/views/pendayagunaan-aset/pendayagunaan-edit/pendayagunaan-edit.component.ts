import {
  PendayagunaanAsetService, BusinessAreaService, ProvinsiService,
  KabupatenService, AsetTanahService, AsetPropertiService,
  LokasiService,
  PenggunaService,
  JenisPropertiService,
  JenisDayaGunaService,
  CompanyCodeService
} from 'app/services/data-services';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Location, UpperCasePipe } from '@angular/common';
import {
  Provinsi, Kabupaten, BusinessArea, Lokasi, AsetTanah, AsetProperti,
  Pengguna, PendayagunaanAset, JenisAsetProperti, JenisDayaGuna,
  PotensiAset
} from 'app/models';
import { URLSearchParams } from '@angular/http';
import { SelectComponent, SelectItem } from 'ng2-select';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pendayagunaan-edit',
  templateUrl: './pendayagunaan-edit.component.html',
  styleUrls: ['./pendayagunaan-edit.component.scss']
})
export class PendayagunaanEditComponent implements OnInit {
  // page setting
  action = '';
  title = '';
  ready = false;
  asetSelected = false;
  formTitle = '';

  isSubmitted: boolean;
  formPendayagunaan: FormGroup;
  formRevisiPendayagunaan: FormGroup;
  provinsis: Provinsi[] = [];
  kabupatens: Kabupaten[] = [];
  businessAreas: BusinessArea[] = [];
  lokasis: Lokasi[] = [];
  jenisAsetPropertis: JenisAsetProperti[] = [];
  asets: AsetTanah[] | AsetProperti[] = [];
  jenisPendayagunaans: JenisDayaGuna[] = [];
  aset: AsetTanah | AsetProperti = new AsetProperti();
  potensiAset: PotensiAset;

  idJenisAset: number;

  jenisAset = 'App\\Tanah';

  pendayagunaans: PendayagunaanAset[] = [];
  pendayagunaan: PendayagunaanAset;
  pendayagunaanAset: PendayagunaanAset;
  pengguna: Pengguna;

  idCompanyCode: number;
  idBusinessArea: number;
  idProvinsi: number;
  idKabupaten: number;
  idLokasi: number;

  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];
  itemsT = [];

  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('selectAset') selectAset: SelectComponent;

  addByAset = false;

  constructor(
    private route: ActivatedRoute,
    private _location: Location,
    private _fb: FormBuilder,
    private pendayagunaanService: PendayagunaanAsetService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private asetTanahService: AsetTanahService,
    private asetPropertiService: AsetPropertiService,
    private lokasiService: LokasiService,
    private penggunaService: PenggunaService,
    private jenisPropertiService: JenisPropertiService,
    private jenisPendayagunaanService: JenisDayaGunaService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.jenisPendayagunaanService.getAll().subscribe(jenisPendayagunaans => {
      this.jenisPendayagunaans = jenisPendayagunaans;
    });
    this.route.url.subscribe(url => {
      this.action = url[0].path;

      switch (this.action) {
        case 'revisi': this._revisi();
          break;
        case 'add':
        default: this._add();
      }
    });
    this.formPendayagunaan = this._fb.group({
      potensi: ['', [Validators.required]],
      listPendayagunaan: this._fb.array([], Validators.required)
    });
    this.addRow();
  }

  public addRow(pendayagunaan?: PendayagunaanAset) {
    if (pendayagunaan) {
      this.pendayagunaans.push(pendayagunaan);
    } else {
      this.pendayagunaans.push(new PendayagunaanAset());
    }
    const lists = <FormArray> this.formPendayagunaan.get('listPendayagunaan');
    lists.push(
      this._initFormGroup());
    return false;
  }

  private _initFormGroup(): FormGroup {
    return this._fb.group({
        id_jenis_dayaguna: ['', [Validators.required]],
        // potensi: ['', [Validators.required]],
        kontrak: ['', [Validators.required]],
        nilai_pendayagunaan: ['', [Validators.required]],
        // pihak_terkait: ['', [Validators.required]],
        start_date_pendayagunaan: ['', [Validators.required]],
        end_date_pendayagunaan: ['', [Validators.required]],
        keterangan: ['', [Validators.required]]
      })
  }

  _removePendayagunaan(index: number) {
    const lists = <FormArray> this.formPendayagunaan.get('listPendayagunaan');
    lists.removeAt(index);
    this.pendayagunaans.splice(index, 1);
    return false;
  }

  private _add() {
    this.route.queryParamMap.subscribe(queryParams => {
      const idAset = parseInt(queryParams.get('id'), 10);
      const jenisAset = queryParams.get('jenis');
      this.jenisAset = jenisAset === 'aset-properti' ? 'App\\Properti' : 'App\\Tanah';
      if (idAset && this.jenisAset) {
        this.addByAset = true;
        if (this.jenisAset === 'App\\Tanah') {
          this.asetTanahService.getById(idAset).subscribe(asetTanah => {
            this.aset = new AsetTanah(asetTanah);
            if (this.aset.potensi) {
              this.potensiAset = this.aset.potensi;
              this.formPendayagunaan.get('potensi').setValue(this.potensiAset);
              this.asetSelected = true;

              this.idCompanyCode = this.aset.perpindahan.id_company_code;
              this.idBusinessArea = this.aset.perpindahan.id_business_area;
              this.idProvinsi = this.aset.perpindahan.lokasi.kabupaten.id_provinsi;
              this.idKabupaten = this.aset.perpindahan.lokasi.id_kabupaten;
              this.idLokasi = this.aset.perpindahan.id_lokasi;
              this.getProvinsi();
              this.getCompanyCode();
              this.getKabupaten()
              this.getBusinessArea();
              this.getLokasi();
              this._getAset();
            } else {
              alert('aset tidak memiliki potensi');
              this.aset = null;
            }
          });
        } else if (this.jenisAset === 'App\\Properti') {
          this.asetPropertiService.getById(idAset).subscribe(asetProperti => {
            this.aset = new AsetProperti(asetProperti);
            if (this.aset.potensi) {
              this.potensiAset = this.aset.potensi;
              this.formPendayagunaan.get('potensi').setValue(this.potensiAset);
              this.asetSelected = true;

              const tanah = new AsetTanah(this.aset.perpindahan.tanah);
              this.idCompanyCode = this.aset.perpindahan.id_company_code;
              this.idBusinessArea = this.aset.perpindahan.id_business_area;
              this.idProvinsi = tanah.perpindahan.lokasi.kabupaten.id_provinsi;
              this.idKabupaten = tanah.perpindahan.lokasi.id_kabupaten;
              this.idLokasi = tanah.perpindahan.id_lokasi;
              this.getProvinsi();
              this.getCompanyCode();
              this.getKabupaten()
              this.getBusinessArea();
              this.getLokasi();
              this._getAset();
            } else {
              alert('aset tidak memiliki potensi');
              this.aset = null;
            }
          });
        }
      } else {
        this.getCompanyCode();
        this.getProvinsi();
      }
    });
    this.pendayagunaan = new PendayagunaanAset();
    this.pendayagunaan.path = this._location.path();
    this.pengguna = this.penggunaService.getCurrentUser();
    this.jenisPropertiService.getAll().subscribe(jenisPropertis => {
      this.jenisAsetPropertis = jenisPropertis;
    });
    this.formTitle = 'Tambah Pendayagunaan Aset';
  }

  private _revisi() {
    this.formTitle = 'Revisi Pendayagunaan Aset';
    this.aset = new AsetProperti();
    this.pendayagunaan = new PendayagunaanAset();
    this.pendayagunaan.path = this._location.path();
    this.formRevisiPendayagunaan = this._initFormGroup();
    this._getSelectedAset(true);
  }

  private _getSelectedAset(isRevisi = true) {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idPendayagunaanAset'), 10);

      this.pendayagunaanService.getById(id).subscribe(pendayagunaan => {
        this.pendayagunaan = pendayagunaan;
        if (pendayagunaan.pendayagunaanable_type === 'App\\Tanah') {
          this.aset =  new AsetTanah(pendayagunaan.pendayagunaanable);

          this.idCompanyCode = this.aset.perpindahan.id_company_code;
          this.idBusinessArea = this.aset.perpindahan.id_business_area;
          this.idProvinsi = this.aset.perpindahan.lokasi.kabupaten.id_provinsi;
          this.idKabupaten = this.aset.perpindahan.lokasi.id_kabupaten;
          this.idLokasi = this.aset.perpindahan.id_lokasi;
        } else {
          this.aset = new AsetProperti(pendayagunaan.pendayagunaanable);

          const tanah = new AsetTanah(this.aset.perpindahan.tanah);
          this.idCompanyCode = this.aset.perpindahan.id_company_code;
          this.idBusinessArea = this.aset.perpindahan.id_business_area;
          this.idProvinsi = tanah.perpindahan.lokasi.kabupaten.id_provinsi;
          this.idKabupaten = tanah.perpindahan.lokasi.id_kabupaten;
          this.idLokasi = tanah.perpindahan.id_lokasi;
        }
        this.potensiAset = pendayagunaan.potensi;
        this.jenisAset = pendayagunaan.pendayagunaanable_type;
        this.getProvinsi();
        this.getCompanyCode();
        this.getKabupaten()
        this.getBusinessArea();
        this.getLokasi();
        this._getAset();
        this.pendayagunaanAset = Object.assign({}, this.pendayagunaan);
        this.asetSelected = true;
        this.spinner.hide();
      });
    });
  }

  log(obj) {
    return false;
  }

  changeAset() {
    this.asetSelected = false;
  }

  chooseAset(aset: AsetTanah | AsetProperti) {
    if (!aset.potensi) {
      alert('aset tidak memiliki potensi');
    } else if (this.aset) {
      this.asetSelected = true;
      this.potensiAset = this.aset.potensi;
    }
  }

  cancel() {
    this._location.back();
    return false;
  }

  save() {
    if (this.action !== 'add') {
      this.update();
    }
    this.isSubmitted = true;
    this.spinner.show();
    this.aset.path = '/pendayagunaan-aset/add';
    this.aset.jenis_aset = this.jenisAset;
    this.aset.dayaguna = this.pendayagunaans;
    this.pendayagunaanService.save(this.aset)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  update() {
    this.isSubmitted = true;
    this.spinner.show();
    this.pendayagunaan.pendayagunaanable_type = this.jenisAset;
    this.pendayagunaanService.update(this.pendayagunaan)
      .subscribe(result => {
        this.spinner.hide();
        if (result.message) {
          alert(result.message);
        }

        if (result.success) {
          this._location.back();
        } else {
          this.isSubmitted = false;
        }
    });
    return false;
  }

  _getAset() {
    if (! this.idLokasi || ! this.idBusinessArea) {
      return false;
    }
    if (this.jenisAset === 'App\\Tanah') {
      this.getTanah();
    } else if (this.jenisAset === 'App\\Properti') {
      this.getProperti();
    }
  }

  private getCompanyCode() {
    this.itemsCC = [];
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
        }
      });
    });
  }

  private getBusinessArea() {
    this.itemsBA = [];
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id == this.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  private getProvinsi() {
    this.itemsP = [];
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
        }
      });
    });
  }

  private getKabupaten() {
    this.itemsK = [];
    this.kabupatenService.getByIdProvinsi(this.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
        }
      });
    });
  }

  private getLokasi() {
    this.itemsL = [];
    this.lokasiService.getByIdKabupaten(this.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id == this.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
        }
      });
    });
  }

  private getTanah() {
    this.itemsT = [];
    const params = new URLSearchParams();
    params.set('id_lokasi', this.idLokasi.toString());
    params.set('id_business_area', this.idBusinessArea.toString());
    this.asetTanahService.getAll(params).subscribe(tanahs => {
      tanahs.forEach(tanah => {
        tanah = new AsetTanah(tanah);
        if(tanah.potensi) {
          this.itemsT.push({id: tanah, text: this.uppercasePipe.transform(tanah.detail.nama_tanah)});
        }
      });
      this.itemsT.forEach((item,index) => {
        if (item.id.id_tanah == this.aset.id) {
          this.selectAset.active = [this.itemsT[index]];
        }
      });
    });
  }

  private getProperti() {
    this.itemsT = [];
    const params = new URLSearchParams();
    params.set('id_lokasi', this.idLokasi.toString());
    params.set('id_business_area', this.idBusinessArea.toString());
    this.asetPropertiService.getAll(params).subscribe(propertis => {
      propertis.forEach(properti => {
        properti = new AsetProperti(properti);
        if(properti.potensi) {
          this.itemsT.push({id: properti, text: this.uppercasePipe.transform(properti.detail.nama_properti)});
        }
      });
      this.itemsT.forEach((item,index) => {
        if (item.id.id_properti == this.aset.id) {
          this.selectAset.active = [this.itemsT[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    const old = this.idBusinessArea;
    this.idBusinessArea = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  selectedBA(e: SelectItem) {
    const old = this.idBusinessArea;
    this.idBusinessArea = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  refreshValueP(e: SelectItem) {
    const old = this.idProvinsi;
    this.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.idProvinsi;
    this.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.idKabupaten;
    this.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.idKabupaten;
    this.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    const old = this.idLokasi;
    this.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  selectedL(e: SelectItem) {
    const old = this.idLokasi;
    this.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this._getAset();
    }
  }

  refreshValueT(e) {
    if(this.chooseAset(e.id)) {
      this.aset = e.id;
      this.formPendayagunaan.get('potensi').setValue(this.aset.potensi);
    } else {
      this.selectAset.active = [];
    }
  }

  selectedT(e) {
    if(this.chooseAset(e.id)) {
      this.aset = e.id;
      this.formPendayagunaan.get('potensi').setValue(this.aset.potensi);
    } else {
      this.selectAset.active = [];
    }
  }

}
