import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendayagunaanEditComponent } from './pendayagunaan-edit.component';

describe('PendayagunaanEditComponent', () => {
  let component: PendayagunaanEditComponent;
  let fixture: ComponentFixture<PendayagunaanEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendayagunaanEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendayagunaanEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
