import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendayagunaanComponent } from './pendayagunaan.component';

describe('PendayagunaanComponent', () => {
  let component: PendayagunaanComponent;
  let fixture: ComponentFixture<PendayagunaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendayagunaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendayagunaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
