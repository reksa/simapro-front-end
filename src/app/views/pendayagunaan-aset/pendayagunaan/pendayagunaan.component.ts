import { JenisDayaGunaService } from './../../../services/data-services/jenis-daya-guna.service';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';

import { AppTableConfig, DataSetting } from 'app/components/pagination/page/page.component';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { environment } from 'environments/environment';

import {
  CompanyCodeService, ProvinsiService,
  PendayagunaanAsetService, PenggunaService
} from 'app/services/data-services';

import { PendayagunaanAset, Koordinat, Provinsi, CompanyCode, Pengguna, JenisDayaGuna } from 'app/models';
import { LatLngBoundsExpression } from 'leaflet';
import { ModalFilterContentComponent } from '../../modal-filter-content/modal-filter-content.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pendayagunaan',
  templateUrl: './pendayagunaan.component.html',
  styleUrls: ['./pendayagunaan.component.scss']
})
export class PendayagunaanComponent implements OnInit {
  tab = 'data';
  root = 'pendayagunaan-aset/';
  showFilter = {
    wilayah: 'provinsi',
    unit: 'company-code'
  };

  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;

  // map
  tileServer = environment.map.tile_server_url;
  marker = environment.map.marker;
  public lat = -2.964984369333955;
  public lng = 114.08203125000001;
  zoom = 5;
  markers: Koordinat[] = [];

  idProvinsi = null;
  idKabupaten = null;
  provinsis: Provinsi[] = [];
  provinsi: Provinsi;
  companyCodes: CompanyCode[] = [];
  companyCode: CompanyCode;
  idBusinessArea = null;
  jenisPendayagunaans: JenisDayaGuna[] = [];

  bounds: LatLngBoundsExpression;
  @ViewChild('map') map;
  array: Array<number> = [];

  public tableConfig = new AppTableConfig();
  pengguna = new Pengguna();
  modalRef: BsModalRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pendayagunaanAsetService: PendayagunaanAsetService,
    private companyCodeService: CompanyCodeService,
    private provinsiService: ProvinsiService,
    private penggunaService: PenggunaService,
    private jenisDayaGunaService: JenisDayaGunaService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.penggunaService.getRoleForModule('pendayagunaan').subscribe(pengguna => this.pengguna = pengguna);

    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.companyCodes = companyCodes;
    });

    this.provinsiService.getAll().subscribe(provinsis => {
      this.provinsis = provinsis;
    });

    this.jenisDayaGunaService.getAll().subscribe(jenisPendayagunaans => {
      this.jenisPendayagunaans = jenisPendayagunaans;
    });

    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      this._data();
    });
  }

  private _data() {
    let data: Subscription;
    if(data) {
      data.unsubscribe();
    }
    this.pageData = [];
    data = this.route.queryParamMap.switchMap(params => {
      this.pageData = [];
      this.isLoading = true;
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      if (params.get('idKabupaten')) {
        parameters.append('id_kabupaten', params.get('idKabupaten'));
        this.dataSetting.idKabupaten = parseInt(params.get('idKabupaten'));
      }
      if (params.get('idProvinsi')) {
        parameters.append('id_provinsi', params.get('idProvinsi'));
        this.dataSetting.idProvinsi = parseInt(params.get('idProvinsi'));
      }
      if (params.get('idBusinessArea')) {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.dataSetting.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      if (params.get('idCompanyCode')) {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.dataSetting.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('idLokasiAset')) {
        parameters.append('id_lokasi', params.get('idLokasiAset'));
        this.dataSetting.idLokasiAset = parseInt(params.get('idLokasiAset'));
      }
      if (params.get('nama') !== '') {
        parameters.append('nama', params.get('nama'));
        this.dataSetting.nama = params.get('nama');
      }
      if (params.get('idJenisPendayagunaan')) {
        parameters.append('id_jenis_dayaguna', params.get('idJenisPendayagunaan'));
        this.dataSetting.idJenisPendayagunaan = parseInt(params.get('idJenisPendayagunaan'));
      }

      const data = this.tab === 'data-perubahan' ?
                                  this.pendayagunaanAsetService.getPaginateNonValid(parameters) :
                                  this.pendayagunaanAsetService.getPaginateValid(parameters);
      return data;
    }).subscribe((res) => {
      this.dataSetting.dataPerPage = <number> res.per_page;
      this.pageData = res.data.map(d => {
        return new PendayagunaanAset(d);
      });
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();
      this.dataSetting.dataPerPage = res.per_page;

      if (this.modalRef) {
        this.modalRef.hide();
      }

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  openModal() {
    const initialState = {
      isPendayagunaan: true,
      dataSetting: Object.assign({}, this.dataSetting)
    }
    this.modalRef = this.modalService.show(ModalFilterContentComponent, { initialState, class: 'modal-lg', ignoreBackdropClick: true });
    const content = <ModalFilterContentComponent> this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
      if (content.filter) {
        this.dataSetting = content.dataSetting;
        this.search();
      }
      subscribeModal.unsubscribe();
    })
  }

  show(pendayagunaanAset: PendayagunaanAset) {
    this.router.navigate([`${this.root}show/${pendayagunaanAset.id_pendayagunaan}/detail`]);
    return false;
  }
}
