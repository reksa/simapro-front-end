import { TimelineModule } from './../timeline/timeline.module';
import { TabsModule } from 'ngx-bootstrap';
import { PendayagunaanAsetService } from './../../services/data-services/pendayagunaan-aset.service';
import { PendayagunaanEditComponent } from './pendayagunaan-edit/pendayagunaan-edit.component';
import { PendayagunaanDetailComponent } from './pendayagunaan-detail/pendayagunaan-detail.component';
import { PendayagunaanComponent } from './pendayagunaan/pendayagunaan.component';
import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe, DatePipe, UpperCasePipe } from '@angular/common';

import { PendayagunaanAsetRoutingModule } from './pendayagunaan-aset-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { AlertService } from 'app/services/alert.service';
import { YagaModule } from '@yaga/leaflet-ng2';
import {
  CompanyCodeService, BusinessAreaService, ProvinsiService, KabupatenService,
  AsetTanahService, LokasiService, JenisPropertiService, AsetPropertiService, JenisDayaGunaService
} from 'app/services/data-services';
import { ItemPendayagunaanComponent } from './item-pendayagunaan/item-pendayagunaan.component';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { SelectModule } from 'ng2-select';
import { InputNumberModule } from '../input-number/input-number.module';

@NgModule({
  imports: [
    CommonModule,
    PendayagunaanAsetRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    YagaModule,
    TabsModule,
    SelectModule,
    TimelineModule,
    InputNumberModule
  ],
  providers: [
    CompanyCodeService, BusinessAreaService, ProvinsiService,
    KabupatenService, PendayagunaanAsetService, AlertService,
    KabupatenService, CompanyCodeService, BusinessAreaService,
    AsetPropertiService, AsetTanahService, LokasiService,
    JenisPropertiService, JenisDayaGunaService, DecimalPipe,
    DatePipe, TextToSlugPipe, UpperCasePipe
  ],
  declarations: [PendayagunaanComponent, PendayagunaanDetailComponent, PendayagunaanEditComponent, ItemPendayagunaanComponent]
})
export class PendayagunaanAsetModule { }
