import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPendayagunaanComponent } from './item-pendayagunaan.component';

describe('ItemPendayagunaanComponent', () => {
  let component: ItemPendayagunaanComponent;
  let fixture: ComponentFixture<ItemPendayagunaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPendayagunaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPendayagunaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
