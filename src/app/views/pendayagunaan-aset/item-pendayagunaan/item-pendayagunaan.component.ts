import { PendayagunaanAset, JenisDayaGuna } from 'app/models';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-item-pendayagunaan]',
  templateUrl: './item-pendayagunaan.component.html',
  styleUrls: ['./item-pendayagunaan.component.scss']
})
export class ItemPendayagunaanComponent {
  @Input('index') index: number;
  @Input('deletable') deletable: boolean;
  @Input('pendayagunaan')
  set pendayagunaan(pendayagunaan) {
    this._pendayagunaan = pendayagunaan;
  }
  @Input('group') set group(group) {
    this._group = group;
  }
  @Input('jenisPendayagunaans') jenisPendayagunaans: JenisDayaGuna[];
  @Output('remove') remove = new EventEmitter();
  // tslint:disable-next-line:no-output-rename
  @Output('pendayagunaan') getPendayagunaan = this.pendayagunaan;
  // tslint:disable-next-line:no-output-rename
  @Output('group') getGroup = this.group;

  _pendayagunaan: PendayagunaanAset;
  _group: FormGroup;

  onRemove($event) {
    this.remove.emit($event);
  }

  get pendayagunaan() {
    return this._pendayagunaan;
  }

  get group() {
    return this._group;
  }
}
