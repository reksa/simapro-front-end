import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LaporanRoutingModule } from './laporan-routing.module';
import { LaporanAsetComponent } from './laporan-aset/laporan-aset.component';
import { LaporanPendapatanComponent } from './laporan-pendapatan/laporan-pendapatan.component';
import { LaporanPengeluaranComponent } from './laporan-pengeluaran/laporan-pengeluaran.component';
import { TabsModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from '../../components/pagination/pagination.module';
import { LaporanService, MasterProgressService, JenisPerizinanService, JenisDayaGunaService, JenisPropertiService } from '../../services/data-services';
import { SelectModule } from 'ng2-select';
import { InputNumberModule } from '../input-number/input-number.module';

@NgModule({
  imports: [
    CommonModule,
    LaporanRoutingModule,
    AppDatatableModule,
    PaginationModule,
    TabsModule,
    FormsModule,
    SelectModule,
    InputNumberModule
  ],
  providers: [
    LaporanService, MasterProgressService, JenisPerizinanService, JenisDayaGunaService, JenisPropertiService
  ],
  declarations: [
    LaporanAsetComponent, LaporanPendapatanComponent, LaporanPengeluaranComponent
  ]
})
export class LaporanModule { }
