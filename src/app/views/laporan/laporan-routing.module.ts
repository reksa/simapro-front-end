import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LaporanAsetComponent } from './laporan-aset/laporan-aset.component';
import { LaporanPendapatanComponent } from './laporan-pendapatan/laporan-pendapatan.component';
import { LaporanPengeluaranComponent } from './laporan-pengeluaran/laporan-pengeluaran.component';

const routes: Routes = [
  { path: '', redirectTo: 'aset', pathMatch: 'full' },
  { path: 'aset', component: LaporanAsetComponent, canActivate:[AuthGuard, PermissionGuard] },
  { path: 'pendapatan', component: LaporanPendapatanComponent, canActivate:[AuthGuard, PermissionGuard] },
  { path: 'realisasi', component: LaporanPengeluaranComponent, canActivate:[AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LaporanRoutingModule { 

  constructor() {
  }
}
