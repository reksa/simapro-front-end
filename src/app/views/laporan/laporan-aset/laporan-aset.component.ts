import {
  JenisAsetProperti,
  AsetProperti,
  AsetTanah,
  MasterProgress,
  JenisPerizinan,
  JenisDayaGuna
} from "app/models";
import { AppTableConfig } from "./../../../components/pagination/page/page.component";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, ViewChild } from "@angular/core";
import { DataSetting } from "app/components/pagination/page/page.component";
import {
  LaporanService,
  CompanyCodeService,
  BusinessAreaService,
  JenisPerizinanService,
  JenisDayaGunaService,
  MasterProgressService,
  JenisPropertiService
} from "app/services/data-services";
import { URLSearchParams } from "@angular/http";
import { TabsetComponent } from "ngx-bootstrap";
import { SelectItem, SelectComponent } from "ng2-select";
import { UpperCasePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from "rxjs";

@Component({
  selector: "app-laporan-aset",
  templateUrl: "./laporan-aset.component.html",
  styleUrls: ["./laporan-aset.component.scss"]
})
export class LaporanAsetComponent implements OnInit {
  // page setting
  tab = "tanah";
  root = "laporan/aset/";

  pageDataT: Object[] = [];
  pageDataP: Object[] = [];
  jenisAset = "Aset Tanah";
  dataSetting = new DataSetting();
  public tableConfig = new AppTableConfig();
  isLoading = true;

  @ViewChild("tabsetX")
  tabsetX: TabsetComponent;
  @ViewChild("selectCC")
  selectCC: SelectComponent;
  @ViewChild("selectBA")
  selectBA: SelectComponent;

  // filter
  idCompanyCode: number;
  idBusinessArea: number;
  jenisProperti: JenisAsetProperti = new JenisAsetProperti();
  itemsCC = [];
  itemsBA = [];
  progresses: MasterProgress[] = [];
  jenisIzins: JenisPerizinan[] = [];
  jenisDayagunas: JenisDayaGuna[] = [];
  jenisPropertis: JenisAsetProperti[] = [];
  dataSub: Subscription;

  optProperti = {
    idCompanyCode: 0,
    idBusinessArea: 0,
    jenis: 0,
    subJenis: 0,
    potensi: "all",
    pendayagunaan: 0,
    start_range_luas: null,
    end_range_luas: null
  };

  optTanah = {
    idCompanyCode: 0,
    idBusinessArea: 0,
    sertifikat: "all",
    progress: 0,
    kepemilikan: 0,
    potensi: "all",
    pendayagunaan: 0,
    start_range_luas: null,
    end_range_luas: null
  };
  params: URLSearchParams = new URLSearchParams();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private laporanService: LaporanService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private masterProgressService: MasterProgressService,
    private jenisPerizinanService: JenisPerizinanService,
    private jenisPendayagunaanService: JenisDayaGunaService,
    private jenisPropertiService: JenisPropertiService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.masterProgressService.getAll().subscribe(progresses => {
      this.progresses = progresses;
    });
    this.jenisPerizinanService.getAll().subscribe(jenisIzins => {
      this.jenisIzins = jenisIzins;
    });
    this.jenisPendayagunaanService.getAll().subscribe(jenisDayagunas => {
      this.jenisDayagunas = jenisDayagunas;
    });
    this.jenisPropertiService.getAll().subscribe(jenisPropertis => {
      this.jenisPropertis = jenisPropertis;
    });
    this.route.fragment.subscribe(tab => {
      if(tab != this.tab) {
        this.dataSetting.page = 1;
      }
      this.getCompanyCode();
      this.tab = tab || this.tab;
      this.itemsBA = [];
      const tabs = { tanah: 0, properti: 1 };
      const index = tabs[tab] || 0;
      this.tabsetX.tabs[index].active = true;
      this.jenisAset = tab === "tanah" ? "Aset Tanah" : "Aset Properti";
      if (this.tab === "properti") {
        this._initProperti();
      } else if (this.tab === "tanah") {
        this._initTanah();
      }
    });
  }

  private _initTanah() {
    if(this.dataSub) {
      this.dataSub.unsubscribe();
    }
    this.dataSub = this.route.queryParamMap
      .switchMap(params => {
        this.pageDataT = [];
        this.isLoading = true;
        const parameters = new URLSearchParams();
        parameters.append("page", params.get("page"));
        parameters.append("dataPerPage", params.get("dataPerPage"));
        parameters.append("orderBy", params.get("orderBy"));
        parameters.append("orderDirection", params.get("orderDirection"));
        this._setParamsTanah(parameters, params);
        this.params = parameters;

        return this.laporanService.tanah(parameters);
      })
      .subscribe(res => {
        this.dataSetting.dataPerPage = <number>res.per_page;
        this.pageDataT = res.data.map(d => {
          return new AsetTanah(d);
        });
        this.tableConfig.current = res.current_page;
        this.tableConfig.limit = res.per_page;
        this.tableConfig.from = res.from;
        this.tableConfig.to = res.to;
        this.tableConfig.total = res.total;
        this.tableConfig.totalPages = res.last_page;
        this.tableConfig.generatePages();

        this.isLoading = false;
      });
  }

  private _setParamsTanah(parameters: URLSearchParams, params) {
    if (params.get("idCompanyCode") && params.get("idCompanyCode") != "0") {
      parameters.append("id_company_code", params.get("idCompanyCode"));
      this.optTanah.idCompanyCode = parseInt(params.get("idCompanyCode"));
    }
    if (params.get("idBusinessArea") && params.get("idBusinessArea") != "0") {
      parameters.append("id_business_area", params.get("idBusinessArea"));
      this.optTanah.idBusinessArea = parseInt(params.get("idBusinessArea"));
    }
    if (params.get("sertifikat") && params.get("sertifikat") != "all") {
      parameters.append("progress_sertifikat", params.get("sertifikat"));
      this.optTanah.sertifikat = params.get("sertifikat");
      if (
        this.optTanah.sertifikat === "progress" &&
        params.get("progress") &&
        params.get("progress") != "0"
      ) {
        parameters.append("id_master_progress", params.get("progress"));
        this.optTanah.progress = parseInt(params.get("progress"));
      }
    }
    if (params.get("potensi") && params.get("potensi") != "all") {
      parameters.append("operasi", params.get("potensi"));
      this.optTanah.potensi = params.get("potensi");
    }
    if (params.get("kepemilikan") && params.get("kepemilikan") != "0") {
      parameters.append("id_izin", params.get("kepemilikan"));
      this.optTanah.progress = parseInt(params.get("kepemilikan"));
    }
    if (params.get("pendayagunaan") && params.get("pendayagunaan") != "0") {
      parameters.append("id_jenis_pendayagunaan", params.get("pendayagunaan"));
      this.optTanah.pendayagunaan = parseInt(params.get("pendayagunaan"));
    }
    if (params.get("start_range_luas")) {
      parameters.append("start_range_luas", params.get("start_range_luas"));
      this.optTanah.start_range_luas = parseInt(params.get("start_range_luas"));
    }
    if (params.get("end_range_luas")) {
      parameters.append("end_range_luas", params.get("end_range_luas"));
      this.optTanah.end_range_luas = parseInt(params.get("end_range_luas"));
    }
  }

  private _initProperti() {
    if(this.dataSub) {
      this.dataSub.unsubscribe();
    }
    this.dataSub = this.route.queryParamMap
      .switchMap(params => {
        this.pageDataP = [];
        this.isLoading = true;
        const parameters = new URLSearchParams();
        parameters.append("query", params.get("query"));
        parameters.append("page", params.get("page"));
        parameters.append("dataPerPage", params.get("dataPerPage"));
        parameters.append("orderBy", params.get("orderBy"));
        parameters.append("orderDirection", params.get("orderDirection"));
        this._setParamsProperti(parameters, params);
        this.params = parameters;

        return this.laporanService.properti(parameters);
      })
      .subscribe(res => {
        this.dataSetting.dataPerPage = <number>res.per_page;
        this.pageDataP = res.data.map(d => {
          return new AsetProperti(d);
        });
        this.tableConfig.current = res.current_page;
        this.tableConfig.limit = res.per_page;
        this.tableConfig.from = res.from;
        this.tableConfig.to = res.to;
        this.tableConfig.total = res.total;
        this.tableConfig.totalPages = res.last_page;
        this.tableConfig.generatePages();

        this.isLoading = false;
      });
  }

  private _setParamsProperti(parameters: URLSearchParams, params) {
    if (params.get("idCompanyCode") && params.get("idCompanyCode") != "0") {
      parameters.append("id_company_code", params.get("idCompanyCode"));
      this.optProperti.idCompanyCode = parseInt(params.get("idCompanyCode"));
    }
    if (params.get("idBusinessArea") && params.get("idBusinessArea") != "0") {
      parameters.append("id_business_area", params.get("idBusinessArea"));
      this.optProperti.idBusinessArea = parseInt(params.get("idBusinessArea"));
    }
    if (params.get("jenis") && params.get("jenis") != "0") {
      parameters.append("id_jenis_aset", params.get("jenis"));
      this.optProperti.jenis = parseInt(params.get("jenis"));
      if (
        params.get("subJenis") &&
        this.jenisProperti &&
        this.jenisProperti.sub_jenis_aset.length &&
        params.get("subJenis") != "0"
      ) {
        parameters.append("id_sub_jenis_aset", params.get("subJenis"));
        this.optProperti.subJenis = parseInt(params.get("subJenis"));
      }
    }
    if (params.get("potensi") && params.get("potensi") != "all") {
      parameters.append("operasi", params.get("potensi"));
      this.optProperti.potensi = params.get("potensi");
    }
    if (params.get("pendayagunaan") && params.get("pendayagunaan") != "0") {
      parameters.append("id_jenis_pendayagunaan", params.get("pendayagunaan"));
      this.optProperti.pendayagunaan = parseInt(params.get("pendayagunaan"));
    }
    if (params.get("start_range_luas")) {
      parameters.append("start_range_luas", params.get("start_range_luas"));
      this.optProperti.start_range_luas = parseInt(
        params.get("start_range_luas")
      );
    }
    if (params.get("end_range_luas")) {
      parameters.append("end_range_luas", params.get("end_range_luas"));
      this.optProperti.end_range_luas = parseInt(params.get("end_range_luas"));
    }
  }

  changeJenis() {
    this.optProperti.jenis = this.jenisProperti.id_jenis_aset;
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    // this._navigate();
    let filterOptions = this.tab == 'tanah' ? this.optTanah : this.optProperti;
    this.filter(filterOptions);
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    // this._navigate();
    let filterOptions = this.tab == 'tanah' ? this.optTanah : this.optProperti;
    this.filter(filterOptions);
  }

  search() {
    this.dataSetting.page = 1;
    // this._navigate();
    let filterOptions = this.tab == 'tanah' ? this.optTanah : this.optProperti;
    this.filter(filterOptions);
  }

  filter(filterOptions, page = null) {
    if(page) {
      this.dataSetting.page = page;
    }
    const queryParams = {};
    for (let prop in filterOptions) {
      if (
        filterOptions[prop] !== "0" &&
        filterOptions[prop] !== "all" &&
        filterOptions[prop] != false
      ) {
        queryParams[prop] = filterOptions[prop];
      }
    }
    for (let prop in this.dataSetting) {
      if (
        this.dataSetting[prop] !== "0" &&
        this.dataSetting[prop] !== "all" &&
        this.dataSetting[prop] != false
      ) {
        queryParams[prop] = this.dataSetting[prop];
      }
    }
    this.router.navigate([this.root, {}], {
      queryParams: queryParams,
      fragment: this.tab
    });
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting,
      fragment: this.tab
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = "asc";
    } else if (this.dataSetting.orderDirection === "asc") {
      this.dataSetting.orderDirection = "desc";
    } else {
      this.dataSetting.orderDirection = "asc";
    }
    // this._navigate();
    let filterOptions = this.tab == 'tanah' ? this.optTanah : this.optProperti;
    this.filter(filterOptions);
  }

  chooseTab(fragment) {
    // this.fragment.next(fragment);
    this.router.navigate([this.root], { fragment: fragment });
    return false;
  }

  private getCompanyCode() {
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {
          id: companyCode.id_company_code,
          text: `${
            companyCode.kode_company_code
          } - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`
        };
      });
      this.itemsCC.splice(0, 0, { id: "0", text: "SEMUA" });

      const id =
        this.tab === "properti"
          ? this.optProperti.idCompanyCode
          : this.optTanah.idCompanyCode;
      if (id && id != 0) {
        this.itemsCC.forEach((item, index) => {
          if (item.id === id) {
            this.selectCC.active = [this.itemsCC[index]];
            this.getBusinessArea();
          }
        });
      }
    });
  }

  private getBusinessArea() {
    const id =
      this.tab === "properti"
        ? this.optProperti.idCompanyCode
        : this.optTanah.idCompanyCode;
    this.businessAreaService.getByIdCompanyCode(id).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {
          id: businessArea.id_business_area,
          text: `${
            businessArea.kode_business_area
          } - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`
        };
      });
      this.itemsBA.splice(0, 0, { id: "0", text: "SEMUA" });

      const id =
        this.tab === "properti"
          ? this.optProperti.idCompanyCode
          : this.optTanah.idBusinessArea;
      if (id && id != 0) {
        this.itemsBA.forEach((item, index) => {
          if (item.id === id) {
            this.selectBA.active = [this.itemsBA[index]];
          }
        });
      }
    });
  }

  refreshValueCC(e: SelectItem) {
    const id =
      this.tab === "properti"
        ? this.optProperti.idCompanyCode
        : this.optTanah.idCompanyCode;
    const old = id;
    if (old !== parseInt(e.id)) {
      if (this.tab === "properti") {
        this.optProperti.idCompanyCode = parseInt(e.id);
        if (this.optProperti.idCompanyCode == 0) {
          this.itemsBA = [];
        } else {
          this.getBusinessArea();
        }
      } else {
        this.optTanah.idCompanyCode = parseInt(e.id);
        if (this.optTanah.idCompanyCode == 0) {
          this.itemsBA = [];
        } else {
          this.getBusinessArea();
        }
      }
    }
  }

  selectedCC(e: SelectItem) {
    const id =
      this.tab === "properti"
        ? this.optProperti.idCompanyCode
        : this.optTanah.idCompanyCode;
    const old = id;
    if (old !== parseInt(e.id)) {
      if (this.tab === "properti") {
        this.optProperti.idCompanyCode = parseInt(e.id);
        if (this.optProperti.idCompanyCode == 0) {
          this.itemsBA = [];
        } else {
          this.getBusinessArea();
        }
      } else {
        this.optTanah.idCompanyCode = parseInt(e.id);
        if (this.optTanah.idCompanyCode == 0) {
          this.itemsBA = [];
        } else {
          this.getBusinessArea();
        }
      }
    }
  }

  refreshValueBA(e: SelectItem) {
    if (this.tab === "properti") {
      this.optProperti.idBusinessArea = parseInt(e.id);
    } else {
      this.optTanah.idBusinessArea = parseInt(e.id);
    }
  }

  selectedBA(e: SelectItem) {
    if (this.tab === "properti") {
      this.optProperti.idBusinessArea = parseInt(e.id);
    } else {
      this.optTanah.idBusinessArea = parseInt(e.id);
    }
  }

  download() {
    this.spinner.show();
    const d = new Date();
    const m = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember"
    ];
    const filename = `laporan-aset-${this.tab}-${d.getDate()}-${
      m[d.getMonth()]
    }-${d.getFullYear()}.xlsx`;
    this.laporanService
      .downloadExcel(this.tab, this.params, filename)
      .subscribe(
        res => {
          this.spinner.hide();

          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement("a");
          document.body.appendChild(a);
          a.setAttribute("style", "display: none");
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        },
        error => {
          console.log("download error:", JSON.stringify(error));
        },
        () => {}
      );
  }
}
