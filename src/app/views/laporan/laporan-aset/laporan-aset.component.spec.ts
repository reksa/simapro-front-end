import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanAsetComponent } from './laporan-aset.component';

describe('LaporanAsetComponent', () => {
  let component: LaporanAsetComponent;
  let fixture: ComponentFixture<LaporanAsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanAsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanAsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
