import { Component, OnInit, ViewChild } from "@angular/core";
import { ReplaySubject } from "rxjs";
import {
  DataSetting,
  AppTableConfig
} from "../../../components/pagination/page/page.component";
import { TabsetComponent } from "ngx-bootstrap";
import { Router, ActivatedRoute } from "@angular/router";
import {
  LaporanService,
  CompanyCodeService,
  BusinessAreaService
} from "app/services/data-services";
import { URLSearchParams } from "@angular/http";
import { Realisasi } from "../../../models";
import { SelectComponent, SelectItem } from "ng2-select";
import { UpperCasePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-laporan-pengeluaran",
  templateUrl: "./laporan-pengeluaran.component.html",
  styleUrls: ["./laporan-pengeluaran.component.scss"]
})
export class LaporanPengeluaranComponent implements OnInit {
  // page setting
  fragment = new ReplaySubject<string>(1);
  root = "laporan/realisasi/";

  pageData: Object[] = [];
  dataSetting = new DataSetting();
  public tableConfig = new AppTableConfig();
  isLoading = true;

  @ViewChild("tabsetX")
  tabsetX: TabsetComponent;
  @ViewChild("selectCC")
  selectCC: SelectComponent;
  @ViewChild("selectBA")
  selectBA: SelectComponent;

  // filter
  itemsCC = [];
  itemsBA = [];

  optRealisasi = {
    idCompanyCode: 0,
    idBusinessArea: 0,
    type: "all",
    startDate: "",
    endDate: ""
  };
  params: URLSearchParams = new URLSearchParams();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private laporanService: LaporanService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.fragment.subscribe(tab => {
      this.getCompanyCode();
      const tabs = { tanah: 0, properti: 1 };
      const index = tabs[tab] || 0;
      this.tabsetX.tabs[index].active;

      this.route.queryParamMap
        .switchMap(params => {
          this.pageData = [];
          this.isLoading = true;
          const parameters = new URLSearchParams();
          parameters.append("page", params.get("page"));
          parameters.append("dataPerPage", params.get("dataPerPage"));
          parameters.append("orderBy", params.get("orderBy"));
          parameters.append("orderDirection", params.get("orderDirection"));
          if (
            params.get("idCompanyCode") &&
            params.get("idCompanyCode") != "0"
          ) {
            parameters.append("id_company_code", params.get("idCompanyCode"));
            this.optRealisasi.idCompanyCode = parseInt(
              params.get("idCompanyCode")
            );
          }
          if (
            params.get("idBusinessArea") &&
            params.get("idBusinessArea") != "0"
          ) {
            parameters.append("id_business_area", params.get("idBusinessArea"));
            this.optRealisasi.idBusinessArea = parseInt(
              params.get("idBusinessArea")
            );
          }
          if (params.get("type") && params.get("type") != "all") {
            parameters.append("type", params.get("type"));
            this.optRealisasi.type = params.get("type");
          }
          if (params.get("startDate")) {
            parameters.append("start_date_realisasi", params.get("startDate"));
            this.optRealisasi.type = params.get("startDate");
          }
          if (params.get("endDate")) {
            parameters.append("end_date_realisasi", params.get("endDate"));
            this.optRealisasi.type = params.get("endDate");
          }
          this.params = parameters;

          return this.laporanService.realisasi(parameters);
        })
        .subscribe(res => {
          this.dataSetting.dataPerPage = <number>res.per_page;
          this.pageData = res.data.map(d => {
            return new Realisasi(d);
          });
          this.tableConfig.current = res.current_page;
          this.tableConfig.limit = res.per_page;
          this.tableConfig.from = res.from;
          this.tableConfig.to = res.to;
          this.tableConfig.total = res.total;
          this.tableConfig.totalPages = res.last_page;
          this.tableConfig.generatePages();

          this.isLoading = false;
        });
    });
    this.fragment.next("realisasi");
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  filter(filterOptions) {
    const queryParams = {};
    for (let prop in filterOptions) {
      if (
        filterOptions[prop] !== "0" &&
        filterOptions[prop] !== "all" &&
        filterOptions[prop] != false
      ) {
        queryParams[prop] = filterOptions[prop];
      }
    }
    this.router.navigate([this.root, {}], {
      queryParams: queryParams
    });
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = "asc";
    } else if (this.dataSetting.orderDirection === "asc") {
      this.dataSetting.orderDirection = "desc";
    } else {
      this.dataSetting.orderDirection = "asc";
    }
    this._navigate();
  }

  chooseTab(fragment) {
    this.fragment.next(fragment);
    return false;
  }

  private getCompanyCode() {
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {
          id: companyCode.id_company_code,
          text: `${
            companyCode.kode_company_code
          } - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`
        };
      });
      this.itemsCC.forEach((item, index) => {
        if (item.id === this.optRealisasi.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
          this.getBusinessArea();
        }
      });
    });
  }

  private getBusinessArea() {
    this.businessAreaService
      .getByIdCompanyCode(this.optRealisasi.idCompanyCode)
      .subscribe(businessAreas => {
        this.itemsBA = businessAreas.map(businessArea => {
          return {
            id: businessArea.id_business_area,
            text: `${
              businessArea.kode_business_area
            } - ${this.uppercasePipe.transform(
              businessArea.nama_business_area
            )}`
          };
        });
        this.itemsBA.forEach((item, index) => {
          if (item.id === this.optRealisasi.idBusinessArea) {
            this.selectBA.active = [this.itemsBA[index]];
          }
        });
      });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.optRealisasi.idCompanyCode;
    if (old !== parseInt(e.id)) {
      this.optRealisasi.idCompanyCode = parseInt(e.id);
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.optRealisasi.idCompanyCode;
    if (old !== parseInt(e.id)) {
      this.optRealisasi.idCompanyCode = parseInt(e.id);
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.optRealisasi.idBusinessArea = parseInt(e.id);
  }

  selectedBA(e: SelectItem) {
    this.optRealisasi.idBusinessArea = parseInt(e.id);
  }

  download() {
    this.spinner.show();
    const d = new Date();
    const m = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember"
    ];
    const filename = `laporan-realisasi-pekerjaan-${d.getDate()}-${
      m[d.getMonth()]
    }-${d.getFullYear()}.xlsx`;
    this.laporanService
      .downloadExcel("realisasi", this.params, filename)
      .subscribe(
        res => {
          this.spinner.hide();

          var url = window.URL.createObjectURL(res.data);
          var a = document.createElement("a");
          document.body.appendChild(a);
          a.setAttribute("style", "display: none");
          a.href = url;
          a.download = res.filename;
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove(); // remove the element
        },
        error => {
          console.log("download error:", JSON.stringify(error));
        },
        () => {}
      );
  }
}
