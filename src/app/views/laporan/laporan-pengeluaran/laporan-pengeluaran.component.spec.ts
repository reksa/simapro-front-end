import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPengeluaranComponent } from './laporan-pengeluaran.component';

describe('LaporanPengeluaranComponent', () => {
  let component: LaporanPengeluaranComponent;
  let fixture: ComponentFixture<LaporanPengeluaranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPengeluaranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPengeluaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
