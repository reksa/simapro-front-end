import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPendapatanComponent } from './laporan-pendapatan.component';

describe('LaporanPendapatanComponent', () => {
  let component: LaporanPendapatanComponent;
  let fixture: ComponentFixture<LaporanPendapatanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPendapatanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPendapatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
