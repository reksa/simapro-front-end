import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAsetComponent } from './select-aset.component';

describe('SelectAsetComponent', () => {
  let component: SelectAsetComponent;
  let fixture: ComponentFixture<SelectAsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
