import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AsetTanah, AsetProperti, Lokasi } from '../../../models';
import { ModalContentSelectAsetComponent } from '../modal-content-select-aset/modal-content-select-aset.component';

@Component({
  selector: 'select-aset',
  templateUrl: './select-aset.component.html',
  styleUrls: ['./select-aset.component.scss']
})
export class SelectAsetComponent implements OnInit {

  modalRef: BsModalRef;
  public modalData = {
    idProvinsi: 0,
    idKabupaten: 0,
    idLokasi: 0
  }

  @Input('idCompanyCode') idCompanyCode: number;
  @Input('idBusinessArea') idBusinessArea: number;
  @Input('aset') aset: AsetTanah | AsetProperti;
  @Input('editable') editable: AsetTanah | AsetProperti;
  @Output('changeAset') changeAset = new EventEmitter();

  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    if(this.aset && this.aset.id) {
      // @ts-ignore
      const lokasi: Lokasi = this.aset.perpindahan.lokasi || this.aset.perpindahan.tanah.perpindahan_aktif[0].lokasi;

      this.modalData.idLokasi = lokasi.id_lokasi;
      this.modalData.idKabupaten = lokasi.id_kabupaten;
      this.modalData.idProvinsi = lokasi.kabupaten.id_provinsi;
      this.changed();
    }
  }

  showModal() {
    if(this.editable) {
      const initialState = {
        aset: Object.assign({}, this.aset),
        modalData: Object.assign({}, this.modalData),
        idCompanyCode: this.idCompanyCode,
        idBusinessArea: this.idBusinessArea
      }
      this.modalRef = this.modalService.show(ModalContentSelectAsetComponent, { initialState });
      const content = <ModalContentSelectAsetComponent> this.modalRef.content;
      const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
        if (content.selected) {
          this.aset = content.aset;
          this.modalData = content.modalData;
          this.changed();
        }
        subscribeModal.unsubscribe();
      });
    }
  }

  changed() {
    this.changeAset.emit(this.aset);
  }

}
