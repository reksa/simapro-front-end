import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SelectItem, SelectComponent } from 'ng2-select';
import { AsetTanah, AsetProperti } from '../../../models';
import { BsModalRef } from 'ngx-bootstrap';
import { CompanyCodeService, BusinessAreaService, ProvinsiService, KabupatenService, LokasiService, AsetPropertiService, AsetTanahService } from '../../../services/data-services';
import { UpperCasePipe } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-modal-content-select-aset',
  templateUrl: './modal-content-select-aset.component.html',
  styleUrls: ['./modal-content-select-aset.component.scss']
})
export class ModalContentSelectAsetComponent implements OnInit {

  aset: AsetTanah | AsetProperti;
  itemsP = [];
  itemsK = [];
  itemsL = [];
  itemsA = [];

  public idCompanyCode: number;
  public idBusinessArea: number;
  public modalData = {
    idProvinsi: 0,
    idKabupaten: 0,
    idLokasi: 0,
  }

  public selected = false;

  @ViewChild('selectProvinsi') selectProvinsi: SelectComponent;
  @ViewChild('selectKabupaten') selectKabupaten: SelectComponent;
  @ViewChild('selectLokasi') selectLokasi: SelectComponent;
  @ViewChild('selectAset') selectAset: SelectComponent;


  constructor(
    public modalRef: BsModalRef,
    private provinsiService: ProvinsiService,
    private kabupatenService: KabupatenService,
    private lokasiService: LokasiService,
    private asetTanahService: AsetTanahService,
    private asetPropertiService: AsetPropertiService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) {}
  public upperCasePipe: UpperCasePipe

  ngOnInit() {
    this.getProvinsi();
  }

  save() {
    if (this.aset && this.aset.id) {
      this.selected = true;
    }
    this.modalRef.hide();
  }

  private getAset() {
    this.itemsA = [];
    const params = new URLSearchParams();
    params.set('id_lokasi', this.modalData.idLokasi.toString());
    if(this.idBusinessArea) {
      params.set('id_business_area', this.idBusinessArea.toString());
    } else {
      params.set('id_company_code', this.idCompanyCode.toString());
    }
    this.spinner.show();
    Observable.combineLatest(this.asetTanahService.getAll(params), this.asetPropertiService.getAll(params))
      .subscribe(asets => {
        const itemsT = asets[0].map(tanah => {
          tanah = new AsetTanah(tanah);
          return {id: tanah, text: this.uppercasePipe.transform(tanah.detail.nama_tanah)};
        });
        const itemsP = asets[1].map(properti => {
          properti = new AsetProperti(properti);
          return {id: properti, text: this.uppercasePipe.transform(properti.detail.nama_properti)};
        });
        // @ts-ignore
        this.itemsA = itemsT.concat(itemsP);
        this.itemsA.forEach((item,index) => {
          // @ts-ignore
          if (this.aset && (item.id.id_tanah == this.aset.id_tanah || item.id.id_properti == this.aset.id_properti)) {
            this.selectAset.active = [this.itemsA[index]];
          }
        });
        this.spinner.hide();
    });
  }

  private getProvinsi() {
    this.itemsP = [];
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.forEach((item,index) => {
        if (item.id === this.modalData.idProvinsi) {
          this.selectProvinsi.active = [this.itemsP[index]];
          this.getKabupaten();
        }
      });
    });
  }

  private getKabupaten() {
    this.itemsK = [];
    this.kabupatenService.getByIdProvinsi(this.modalData.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)}
      });
      this.itemsK.forEach((item,index) => {
        if (item.id === this.modalData.idKabupaten) {
          this.selectKabupaten.active = [this.itemsK[index]];
          this.getLokasi();
        }
      });
    });
  }

  private getLokasi() {
    this.itemsL = [];
    this.lokasiService.getByIdKabupaten(this.modalData.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.forEach((item,index) => {
        if (item.id == this.modalData.idLokasi) {
          this.selectLokasi.active = [this.itemsL[index]];
          this.getAset();
        }
      });
    });
  }

  refreshValueP(e: SelectItem) {
    const old = this.modalData.idProvinsi;
    this.modalData.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.modalData.idProvinsi;
    this.modalData.idProvinsi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getKabupaten();
    }
  }

  refreshValueK(e: SelectItem) {
    const old = this.modalData.idKabupaten;
    this.modalData.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.modalData.idKabupaten;
    this.modalData.idKabupaten = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getLokasi();
    }
  }

  refreshValueL(e: SelectItem) {
    const old = this.modalData.idLokasi;
    this.modalData.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getAset();
    }
  }

  selectedL(e: SelectItem) {
    const old = this.modalData.idLokasi;
    this.modalData.idLokasi = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getAset();
    }
  }

  refreshValueA(e) {
    this.aset = e.id;
  }

  selectedA(e) {
    this.aset = e.id;
  }
}