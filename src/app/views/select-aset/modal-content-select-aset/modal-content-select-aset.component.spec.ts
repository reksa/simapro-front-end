import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalContentSelectAsetComponent } from './modal-content-select-aset.component';

describe('ModalContentSelectAsetComponent', () => {
  let component: ModalContentSelectAsetComponent;
  let fixture: ComponentFixture<ModalContentSelectAsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalContentSelectAsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContentSelectAsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
