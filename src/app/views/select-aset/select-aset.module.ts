import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectAsetComponent } from './select-aset/select-aset.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SelectAsetComponent],
  providers: [BsModalService],
  exports: [SelectAsetComponent]
})
export class SelectAsetModule { }
