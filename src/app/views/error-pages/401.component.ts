import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './401.component.html'
})
export class P401Component implements OnInit {

  constructor() { }

  ngOnInit() {
    // remove JWT & localstorage data lainnya untuk memaksa user login
  }

}
