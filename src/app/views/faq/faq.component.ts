import { JsonService } from "./../../services/data-services/json.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-faq",
  templateUrl: "./faq.component.html",
  styleUrls: ["./faq.component.scss"]
})
export class FaqComponent implements OnInit {
  query = "";

  questionAndAnswers: QuestionAndAndswer[] = [];

  questionAndAnswersFiltered: QuestionAndAndswer[] = [];

  constructor(private jsonService: JsonService) {}

  ngOnInit() {
    this.jsonService
      .getAll("./assets/faq.json")
      .subscribe(
        (questionAndAnswers: { question: string; answer: string }[]) => {
          console.log(questionAndAnswers);
          this.questionAndAnswers = questionAndAnswers.map(
            qna => new QuestionAndAndswer(qna.question, qna.answer)
          );
          this.search();
        }
      );
  }

  toggle(qna: QuestionAndAndswer) {
    qna.isCollapsed = !qna.isCollapsed;
    return false;
  }

  search(): void {
    if (!this.query) {
      this.questionAndAnswersFiltered = this.questionAndAnswers;
    } else {
      this.questionAndAnswersFiltered = this.questionAndAnswers.filter(qna => {
        return (
          qna.question.toLowerCase().indexOf(this.query.toLowerCase()) > -1 ||
          qna.question.toLowerCase().indexOf(this.query.toLowerCase()) > -1
        );
      });
    }
  }
}

class QuestionAndAndswer {
  question: string;
  answer: string;
  isCollapsed: boolean;

  constructor(question, answer, isCollapsed = true) {
    this.question = question;
    this.answer = answer;
    this.isCollapsed = isCollapsed === false ? false : true;
  }
}
