import { Observable, Subscription } from "rxjs/Rx";
import { URLSearchParams } from "@angular/http";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import {
  DataSetting,
  AppTableConfig,
  PaginationData
} from "app/components/pagination/page/page.component";
import { AlertService } from "app/services/alert.service";
import { Pengguna, Anggaran } from "app/models";
import { PenggunaService, AnggaranService } from "../../../services/data-services";
import { TextToSlugPipe } from "../../../services/text-to-slug.service";
import { ModalFilterContentComponent } from "../../modal-filter-content/modal-filter-content.component";

@Component({
  selector: "app-anggaran",
  templateUrl: "./anggaran.component.html",
  styleUrls: ["./anggaran.component.scss"]
})
export class AnggaranComponent implements OnInit {
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;
  root = "anggaran/";
  tab = "data";
  public tableConfig = new AppTableConfig();
  public pengguna = new Pengguna();
  public modalRef: BsModalRef;

  constructor(
    private anggaranService: AnggaranService,
    private penggunaService: PenggunaService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private modalService: BsModalService,
    private slugPipe: TextToSlugPipe
  ) {}

  ngOnInit() {
    this.penggunaService
      .getRoleForModule("anggaran")
      .subscribe(pengguna => (this.pengguna = pengguna));
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      this._data();
    });
  }

  private _data() {
    let data: Subscription;
    if (data) {
      data.unsubscribe();
    }
    this.pageData = [];
    data = this.route.queryParamMap
      .switchMap(params => {
        this.pageData = [];
        this.isLoading = true;
        const parameters = new URLSearchParams();
        parameters.append("query", params.get("query"));
        parameters.append("page", params.get("page"));
        parameters.append("dataPerPage", params.get("dataPerPage"));
        parameters.append("orderBy", params.get("orderBy"));
        parameters.append("orderDirection", params.get("orderDirection"));
        if (params.get("idBusinessArea")) {
          parameters.append("id_business_area", params.get("idBusinessArea"));
          this.dataSetting.idBusinessArea = parseInt(
            params.get("idBusinessArea")
          );
        }
        if (params.get("idCompanyCode")) {
          parameters.append("id_company_code", params.get("idCompanyCode"));
          this.dataSetting.idCompanyCode = parseInt(
            params.get("idCompanyCode")
          );
        }
        if (params.get("nama")) {
          parameters.append("nama", params.get("nama"));
          this.dataSetting.nama = params.get("nama");
        }
        if (params.get("nomor_anggaran")) {
          parameters.append("nomor_anggaran", params.get("nomor_anggaran"));
          this.dataSetting.nomor_anggaran = params.get("nomor_anggaran");
        }
        if (params.get("tahun_anggaran")) {
          parameters.append("tahun_anggaran", params.get("tahun_anggaran"));
          this.dataSetting.tahun_anggaran = parseInt(
            params.get("tahun_anggaran")
          );
        }

        let data: Observable<PaginationData>;
        switch (this.tab) {
          case "data-perubahan":
            data = this.anggaranService.getPaginateNonValid(parameters);
            break;
          default:
            data = this.anggaranService.getPaginateValid(parameters);
        }

        return data;
      })
      .subscribe(res => {
        this.dataSetting.dataPerPage = <number>res.per_page;
        this.pageData = res.data.map(d => {
          return new Anggaran(d);
        });
        this.tableConfig.current = res.current_page;
        this.tableConfig.limit = res.per_page;
        this.tableConfig.from = res.from;
        this.tableConfig.to = res.to;
        this.tableConfig.total = res.total;
        this.tableConfig.totalPages = res.last_page;
        this.tableConfig.generatePages();

        if (this.modalRef) {
          this.modalRef.hide();
        }

        this.isLoading = false;
      });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      fragment: this.tab,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = "asc";
    } else if (this.dataSetting.orderDirection === "asc") {
      this.dataSetting.orderDirection = "desc";
    } else {
      this.dataSetting.orderDirection = "asc";
    }
    this._navigate();
  }

  openModal() {
    const initialState = {
      isAnggaran: true,
      dataSetting: Object.assign({}, this.dataSetting)
    };
    this.modalRef = this.modalService.show(ModalFilterContentComponent, {
      initialState,
      class: "modal-lg",
      ignoreBackdropClick: true
    });
    const content = <ModalFilterContentComponent>this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe(
      (reason: string) => {
        if (content.filter) {
          this.dataSetting = content.dataSetting;
          this.search();
        }
        subscribeModal.unsubscribe();
      }
    );
  }

  show(anggaran: Anggaran) {
    const p = this.slugPipe.transform(
      anggaran.nomor_anggaran || "detail-anggaran"
    );
    this.router.navigate([`${this.root}show/${anggaran.id_anggaran}/${p}`]);
    return false;
  }
}
