import { AnggaranEditComponent } from './anggaran-edit/anggaran-edit.component';
import { AnggaranDetailComponent } from './anggaran-detail/anggaran-detail.component';
import { AnggaranComponent } from './anggaran/anggaran.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AnggaranApprovalComponent } from './anggaran-approval/anggaran-approval.component';
import { PermissionGuard, AuthGuard } from 'app/services/auth';

const routes: Routes = [
  { path: '', component: AnggaranComponent },
  { path: 'add', component: AnggaranEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idAnggaran/:nama', component: AnggaranEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'approve/:idAnggaran/:nama', component: AnggaranApprovalComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idAnggaran/:nama', component: AnggaranDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
  // { path: 'show/:idAnggaran/:nama/progress/add', component: AnggaranDetailComponent },
  // { path: 'show/:idAnggaran/:nama/progress/edit/:idProgressPekerjaan', component: AnggaranDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnggaranRoutingModule {}
