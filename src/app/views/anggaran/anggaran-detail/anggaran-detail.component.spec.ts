import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnggaranDetailComponent } from './anggaran-detail.component';

describe('AnggaranDetailComponent', () => {
  let component: AnggaranDetailComponent;
  let fixture: ComponentFixture<AnggaranDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnggaranDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnggaranDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
