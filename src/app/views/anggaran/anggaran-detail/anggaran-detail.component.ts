import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Anggaran, AnggaranDetail, AnggaranPekerjaan, Pengguna, WorkflowProgress } from 'app/models';
import { Location, UpperCasePipe, DecimalPipe } from '@angular/common';
import { PenggunaService, AnggaranService } from 'app/services/data-services';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { DatatablesContent } from '../../../components/app-datatable/datatables-content.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-anggaran-detail',
  templateUrl: './anggaran-detail.component.html',
  styleUrls: ['./anggaran-detail.component.scss']
})
export class AnggaranDetailComponent implements OnInit {

  tab = 'detail';
  id: number;
  anggaran: Anggaran;
  anggaranDetails: AnggaranDetail[];
  anggaranPekerjaans: AnggaranPekerjaan[];
  anggaranDetailX: AnggaranDetail;

  pengguna = new Pengguna();
  historyApproval = new DatatablesContent();
  workflowProgress: WorkflowProgress[];

  memo: string = '';
  modalRef: BsModalRef;
  subs: Subscription;

  @ViewChild('templateAnggaranDetail') templateAnggaranDetail: TemplateRef<any>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private penggunaService: PenggunaService,
    private anggaranService: AnggaranService,
    private modalService: BsModalService,
    private uppercasePipe: UpperCasePipe,
    private numberPipe: DecimalPipe,
    private slugPipe: TextToSlugPipe,
    private _location: Location,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.penggunaService.getRoleForModule('anggaran').subscribe(pengguna => {
      this.pengguna = pengguna;
      this.route.paramMap.subscribe(params => {
        this.id = parseInt(params.get('idAnggaran'), 10);
        this._init();
      });
    });
  }

  private _init() {
    this.spinner.show();
    if(this.subs) {
      this.subs.unsubscribe();
    }
    this.subs = this.anggaranService.getById(this.id).subscribe((anggaran: Anggaran) => {
      this.anggaran = new Anggaran(anggaran);
      this._mapApprovalToDatatable(this.anggaran.detail);
      this.workflowProgress = anggaran && anggaran.detail && anggaran.detail[0] && anggaran.detail[0].workflow_progress ? anggaran.detail[0].workflow_progress : null;
      this.spinner.hide();
    });
  }

  showPemeliharaan() {
    this.router.navigate(['/pemeliharaan/show/1/detail']);
  }

  private _mapApprovalToDatatable(details: AnggaranDetail[]) {
    this.historyApproval.columns = ['TANGGAL APPROVAL/VALIDASI', 'NILAI ANGGARAN (Rp)', 'ITEM ALOKASI', 'APPROVAL LEVEL'];
    this.historyApproval.actions = ['show'];
    this.historyApproval.data.next([]);
    const data = [];

    details.map((r: AnggaranDetail) => {
      const tanggal = r.tanggal_approve;
      const nilai = this.numberPipe.transform(r.nilai_anggaran, '1.0-2');
      const item = r.alokasi.length;
      const level = this.uppercasePipe.transform(r.approval_level);


      data.push({
        row: [ tanggal, nilai, item, level ],
        id: r
      });
    });

    this.historyApproval.data.next(data);
  }

  showModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    return false;
  }

  showDetail(anggaranDetail: AnggaranDetail) {
    this.spinner.show();
    this.anggaranService.getAlokasi(anggaranDetail.id_anggaran_detail).subscribe(alokasi => {
      this.spinner.hide();
      anggaranDetail.alokasi = alokasi;
      this.anggaranDetailX = anggaranDetail;
      this.modalRef = this.modalService.show(this.templateAnggaranDetail, {class:'modal-lg', ignoreBackdropClick: true});
    })
    return false;
  }

  approve() {
    const nama = this.slugPipe.transform(this.anggaran.nama_anggaran || '-');
    this.router.navigate([`/anggaran/approve/${this.anggaran.id_anggaran}/${nama}`]);
    return false;
  }

  validasi(status: boolean) {
    if(this.modalRef) {
      this.modalRef.hide();
    }
    this.spinner.show();
    this.anggaranService.validasi(this.anggaran.detail_perubahan.id_anggaran_detail, this.memo, status)
      .subscribe(res => {
        this.spinner.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.anggaran.detail_perubahan.action_button.button = false;
          this._init();
        }
      });
  }

  revisi() {
    const nama = this.slugPipe.transform(this.anggaran.nama_anggaran || '-');
    this.router.navigate([`/anggaran/revisi/${this.anggaran.id_anggaran}/${nama}`]);
    return false;
  }

  back() {
    this._location.back();
    return false;
  }

}
