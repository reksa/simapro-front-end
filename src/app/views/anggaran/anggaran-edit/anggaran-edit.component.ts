import { ViewChild } from '@angular/core';
import { Anggaran, AnggaranDetail, AnggaranPekerjaan } from './../../../models/anggaran.model';
import { Location, UpperCasePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CompanyCodeService, BusinessAreaService, AnggaranService } from '../../../services/data-services';
import { SelectItem, SelectComponent } from 'ng2-select';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-anggaran-edit',
  templateUrl: './anggaran-edit.component.html',
  styleUrls: ['./anggaran-edit.component.scss']
})
export class AnggaranEditComponent implements OnInit {

  title: string = '';
  subtitle: string = '';
  action: string;
  submitted = false;
  anggaran = new Anggaran();
  anggaranDetail = new AnggaranDetail();
  modalRef: BsModalRef;
  formAnggaran: FormGroup;

  itemsCC = [];
  itemsBA = [];
  idCompanyCode: number;
  idBusinessArea: number = 0;

  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;

  constructor(
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private _location: Location,
    private _fb: FormBuilder,
    private anggaranService: AnggaranService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    const minYear = 1968;
    const maxYear = new Date().getFullYear();
    this.formAnggaran = this._fb.group({
      idCompanyCode: ['', [Validators.required]],
      nama_anggaran: ['', [Validators.required, Validators.maxLength(60)]],
      tahun_anggaran: ['', [Validators.required, Validators.min(minYear), Validators.max(maxYear)]],
      deskripsi: ['', [Validators.maxLength(255)]],
      detail: this._fb.array([])
    });
    this.route.url.subscribe(url => {
      this.action = url[0].path;

      switch(this.action) {
        case 'revisi': this._revisi();
          break;
        case 'add':
        default : this._add();
      }
    });
  }

  private _add() {
    this.anggaranDetail.alokasi = [];
    this.addPekerjaan();
    this.getCompanyCode();
    this.title = 'Tambah Anggaran';
    this.subtitle = 'Tambah anggaran pekerjaan aset';
  }

  private _revisi() {
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idAnggaran'), 10);
      this.anggaranService.getById(id).subscribe((anggaran: Anggaran) => {
        this.anggaran = new Anggaran(anggaran);

        if (!(this.anggaran.detail_perubahan && (
          this.anggaran.detail_perubahan.action_button && this.anggaran.detail_perubahan.action_button.type === 'revisi' && this.anggaran.detail_perubahan.action_button.button))) {
          alert('Data tidak dapat direvisi');
          this._location.back();
        }
        
        this.idCompanyCode = this.anggaran.id_company_code;

        this.anggaranDetail = this.anggaran.detail_perubahan;
        if(!this.anggaranDetail.alokasi || this.anggaranDetail.alokasi.length < 1) {
          this._location.back();
        }
        for(let i = 0; i < this.anggaranDetail.item_alokasi; i++) {
          this.addPekerjaan(true);
        }
        this.getCompanyCode();
        this.getBusinessArea();
      });
    });
    this.title = 'Revisi Anggaran';
    this.subtitle = 'Revisi anggaran pekerjaan aset';
  }

  addPekerjaan(withoutModel?: boolean) {
    if(!withoutModel) {
      this.anggaranDetail.alokasi.push(new AnggaranPekerjaan());
    }
    (this.formAnggaran.get('detail') as FormArray).push(this._initFormDetail());
    return false;
  }

  private _initFormDetail() {
    return this._fb.group({
      aset: ['', [Validators.required]],
      nama_pekerjaan: ['', [Validators.required]],
      keterangan_pekerjaan: ['', [Validators.maxLength(255)]],
      nilai_pekerjaan: ['', [Validators.required, Validators.min(1000000)]]
    });
  }

  removePekerjaan(index: number) {
    this.anggaranDetail.alokasi.splice(index, 1);
    (this.formAnggaran.get('detail') as FormArray).removeAt(index);
  }

  get totalAnggaran() {
    let total = 0;
    if(this.anggaranDetail && this.anggaranDetail.alokasi) {
      this.anggaranDetail.alokasi.map((p) => {
        const nilai = p.nilai_pekerjaan ? parseInt(p.nilai_pekerjaan.toString()) : 0;
        total += nilai;
      });
    }
    return total;
  }

  log(obj: any) {
    // console.log(obj);
    return false;
  }

  cancel() {
    this._location.back();
    return false;
  }

  save() {
    this.submitted = true;
    this.spinner.show();
    this.anggaran.path = this._location.path();
    this.anggaran.id_company_code = this.idCompanyCode;
    this.anggaran.nilai_anggaran = this.totalAnggaran;
    this.anggaran.alokasi = this.anggaranDetail.alokasi;
    if(this.action === 'revisi') {
      this._revisiAnggaran()
    } else {
      this._addAnggaran();
    }
  }

  private _addAnggaran() {
    this.anggaranService.save(this.anggaran).subscribe(res => {
      this.spinner.hide();
      if(res.message) {
        alert(res.message);
      }

      if(res.success) {
        this._location.back();
      } else {
        this.submitted = false;
      }
    });
  }

  private _revisiAnggaran() {
    this.anggaranService.revisi(this.anggaran).subscribe(res => {
      this.spinner.hide();
      if(res.message) {
        alert(res.message);
      }

      if(res.success) {
        this._location.back();
      } else {
        this.submitted = false;
      }
    });
  }

  showModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  private getCompanyCode() {
    const params = new URLSearchParams();
    params.set('level_access', 'true');
    // params.set('path', this._location.path());
    this.companyCodeService.getAll(params).subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      if (this.idCompanyCode) {
        this.itemsCC.forEach((item,index) => {
          if (item.id === this.idCompanyCode) {
            this.selectCC.active = [this.itemsCC[index]];
            // this.getBusinessArea();
          }
        });
      }
      // this.setSelected();
    });
  }

  private getBusinessArea() {
    const params = new URLSearchParams();
    params.set('level_access', 'true');
    this.businessAreaService.getByIdCompanyCode(this.idCompanyCode, params).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      if (this.idBusinessArea) {
        this.itemsBA.forEach((item,index) => {
          if (item.id == this.idBusinessArea) {
            this.selectBA.active = [this.itemsBA[index]];
          }
        });
      }
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAnggaran.get('idCompanyCode').setValue(e.id);
    this.formAnggaran.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.formAnggaran.get('idCompanyCode').setValue(e.id);
    this.formAnggaran.get('idCompanyCode').updateValueAndValidity();
    if (old !== parseInt(e.id)) {
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.formAnggaran.get('idBusinessArea').setValue(e.id);
    this.formAnggaran.get('idBusinessArea').updateValueAndValidity();
  }

  selectedBA(e: SelectItem) {
    this.formAnggaran.get('idBusinessArea').setValue(e.id);
    this.formAnggaran.get('idBusinessArea').updateValueAndValidity();
  }

}
