import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnggaranEditComponent } from './anggaran-edit.component';

describe('AnggaranEditComponent', () => {
  let component: AnggaranEditComponent;
  let fixture: ComponentFixture<AnggaranEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnggaranEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnggaranEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
