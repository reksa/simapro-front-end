import { TimelineModule } from './../timeline/timeline.module';
import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { TextToSlugPipe } from './../../services/text-to-slug.service';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe, UpperCasePipe, CurrencyPipe } from '@angular/common';

import { AnggaranRoutingModule } from './anggaran-routing.module';
import { AnggaranComponent } from './anggaran/anggaran.component';
import { AnggaranDetailComponent } from './anggaran-detail/anggaran-detail.component';
import { AnggaranEditComponent } from './anggaran-edit/anggaran-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'app/components/pagination/pagination.module';
import { AnggaranService } from 'app/services/data-services/anggaran.service';
import { AlertService } from 'app/services/alert.service';
import { RowPekerjaanComponent } from './row-pekerjaan/row-pekerjaan.component';
import { PenggunaService, AsetPropertiService, AsetTanahService, ProvinsiService, KabupatenService, LokasiService } from '../../services/data-services';
import { ModalModule, TabsModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { SelectModule } from 'ng2-select';
import { ShowAnggaranDetailComponent } from './show-anggaran-detail/show-anggaran-detail.component';
import { AnggaranApprovalComponent } from './anggaran-approval/anggaran-approval.component';
import { SelectAsetModule } from '../select-aset/select-aset.module';
import { InputNumberModule } from '../input-number/input-number.module';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    AnggaranRoutingModule,
    AppDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    ModalModule,
    TabsModule,
    SelectModule,
    TimelineModule,
    SelectAsetModule,
    InputNumberModule
  ],
  providers: [
    AnggaranService, AsetPropertiService, AsetTanahService, ProvinsiService, KabupatenService,
    LokasiService, AlertService, PenggunaService, TextToSlugPipe, DatePipe,
    DecimalPipe, UpperCasePipe, CurrencyPipe
  ],
  declarations: [
    AnggaranComponent, AnggaranDetailComponent, AnggaranEditComponent,
    RowPekerjaanComponent, ShowAnggaranDetailComponent, AnggaranApprovalComponent
  ]
})
export class AnggaranModule { }
