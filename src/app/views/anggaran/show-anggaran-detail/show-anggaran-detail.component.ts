import { Component, OnInit, Input } from '@angular/core';
import { AnggaranDetail, AnggaranPekerjaan } from '../../../models';
import { DatatablesContent } from '../../../components/app-datatable/datatables-content.model';
import { DecimalPipe, UpperCasePipe } from '@angular/common';

@Component({
  selector: 'show-anggaran-detail',
  templateUrl: './show-anggaran-detail.component.html',
  styleUrls: ['./show-anggaran-detail.component.scss']
})
export class ShowAnggaranDetailComponent implements OnInit {

  @Input('anggaranDetail') anggaranDetail: AnggaranDetail;
  items = new DatatablesContent();

  constructor(
    private numberPipe: DecimalPipe,
    private uppercasePipe: UpperCasePipe
  ) { }

  ngOnInit() {
    this._mapItemsToDatatable(this.anggaranDetail.alokasi);
  }

  private _mapItemsToDatatable(items: AnggaranPekerjaan[]) {
    this.items.columns = ['NO.', 'NAMA ASET', 'NAMA PEKERJAAN', 'NO. DRAFT PENGAJUAN PEKERJAAN', 'NILAI PEKERJAAN (Rp)', 'KETERANGAN'];
    this.items.actions = [];
    this.items.data.next([]);
    const data = [];
    let i = 1;

    items.forEach((r: AnggaranPekerjaan) => {
      const no = i;
      // @ts-ignore
      const aset = this.uppercasePipe.transform(r.nama_aset || '-');
      const pekerjaan = r.nama_pekerjaan
      const noPengajuan = r.nomor_pengajuan_anggaran_pekerjaan;
      const nilai = this.numberPipe.transform(r.nilai_pekerjaan, '1.0-2');
      const keterangan = this.uppercasePipe.transform(r.keterangan || '');

      i++;

      data.push({
        row: [ no, aset, pekerjaan, noPengajuan, nilai, keterangan ],
        id: r.id_anggaran_pekerjaan
      });
    });

    this.items.data.next(data);
  }

}
