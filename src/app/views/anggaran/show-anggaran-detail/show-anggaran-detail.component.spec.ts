import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAnggaranDetailComponent } from './show-anggaran-detail.component';

describe('ShowAnggaranDetailComponent', () => {
  let component: ShowAnggaranDetailComponent;
  let fixture: ComponentFixture<ShowAnggaranDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAnggaranDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAnggaranDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
