import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnggaranApprovalComponent } from './anggaran-approval.component';

describe('AnggaranApprovalComponent', () => {
  let component: AnggaranApprovalComponent;
  let fixture: ComponentFixture<AnggaranApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnggaranApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnggaranApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
