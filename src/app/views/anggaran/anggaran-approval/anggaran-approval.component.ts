import { ActivatedRoute } from "@angular/router";
import { AnggaranService } from "app/services/data-services";
import { Anggaran, AnggaranDetail } from "app/models";
import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-anggaran-approval",
  templateUrl: "./anggaran-approval.component.html",
  styleUrls: ["./anggaran-approval.component.scss"]
})
export class AnggaranApprovalComponent implements OnInit {
  anggaran: Anggaran;
  anggaranDetail: AnggaranDetail;
  formAnggaran: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private anggaranService: AnggaranService,
    private _location: Location,
    private _fb: FormBuilder,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.formAnggaran = this._fb.group({
      no_anggaran: ["", [Validators.required]],
      detail: this._fb.array([])
    });
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get("idAnggaran"), 10);
      this.anggaranService.getById(id).subscribe((anggaran: Anggaran) => {
        this.anggaran = new Anggaran(anggaran);

        this.anggaranDetail = this.anggaran.detail_perubahan;
        if (
          !this.anggaranDetail.alokasi ||
          this.anggaranDetail.alokasi.length < 1
        ) {
          this._location.back();
        }
        for (let i = 0; i < this.anggaranDetail.alokasi.length; i++) {
          this.addPekerjaan();
        }

        if(!this.anggaranDetail.action_button.is_last_approvement) {
          this.formAnggaran.get('no_anggaran').disable();
        }
      });
    });
  }

  addPekerjaan() {
    (this.formAnggaran.get("detail") as FormArray).push(this._initFormDetail());
  }

  private _initFormDetail() {
    return this._fb.group({
      aset: ["", [Validators.required]],
      nama_pekerjaan: ["", [Validators.required]],
      keterangan_pekerjaan: ["", [Validators.maxLength(255)]],
      nilai_pekerjaan: ["", [Validators.required, Validators.min(1000000)]]
    });
  }

  removePekerjaan(index: number) {
    this.anggaranDetail.alokasi.splice(index, 1);
    (this.formAnggaran.get("detail") as FormArray).removeAt(index);
  }

  get totalAnggaran() {
    let total = 0;
    this.anggaranDetail.alokasi.map(p => {
      const nilai = p.nilai_pekerjaan
        ? parseInt(p.nilai_pekerjaan.toString())
        : 0;
      total += nilai;
    });
    return total;
  }

  log(obj: any) {
    // console.log(obj);
  }

  save() {
    this.spinner.show();
    this.anggaranDetail.path = "/anggaran/approve/1/approval";
    this.anggaranDetail.nilai_anggaran = this.totalAnggaran;
    this.anggaranService.approve(this.anggaranDetail).subscribe(res => {
      this.spinner.hide();
      if (res.message) {
        alert(res.message);
      }

      if (res.success) {
        this.anggaranDetail.action_button.button = false;
        this._location.back();
      }
    });
  }

  cancel() {
    if (confirm("Batalkan approval?")) {
      this._location.back();
    }
    return false;
  }
}
