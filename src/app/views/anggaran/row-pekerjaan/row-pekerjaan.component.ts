import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnggaranPekerjaan, AsetTanah, AsetProperti } from '../../../models';
import { FormGroup } from '@angular/forms';

@Component({
  selector: '[app-row-pekerjaan]',
  templateUrl: './row-pekerjaan.component.html',
  styleUrls: ['./row-pekerjaan.component.scss']
})
export class RowPekerjaanComponent implements OnInit {
  @Input('model') model: AnggaranPekerjaan;
  @Input('group') fGroup: FormGroup;
  @Input('isApproval') isApproval: boolean = false;
  @Input('disableNilai') disableNilai: boolean = false;
  @Input('index') index: number;
  @Input('idBusinessArea') idBusinessArea: number;
  @Input('idCompanyCode') idCompanyCode: number;
  @Input('removable') removable: boolean;
  @Output('onRemove') onRemove = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if(this.model.anggaranable && !this.model.anggaranable.detail) {
      const aset = this.model.anggaranable;
      this.model.anggaranable = this.model.anggaranable_type === 'App\\Tanah' ? new AsetTanah(aset) : new AsetProperti(aset);
    }
    if(this.isApproval) {
      this.fGroup.get('aset').disable();
      this.fGroup.get('nama_pekerjaan').disable();
      this.fGroup.get('keterangan_pekerjaan').disable();
    }
    if(this.disableNilai) {
      this.fGroup.get('nilai_pekerjaan').disable();
    }
  }

  remove(index) {
    this.onRemove.emit(index);
  }

  changeAset(e) {
    if(e && (e.id_tanah || e.id_properti)) {
      this.fGroup.get('aset').setValue(e);
      this.model.anggaranable_id = e.id_tanah || e.id_properti;
      this.model.anggaranable_type = e.id_tanah ? 'App\\Tanah' : 'App\\Properti';
    }
  }

}
