import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowPekerjaanComponent } from './row-pekerjaan.component';

describe('RowPekerjaanComponent', () => {
  let component: RowPekerjaanComponent;
  let fixture: ComponentFixture<RowPekerjaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowPekerjaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowPekerjaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
