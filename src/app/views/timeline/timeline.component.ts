import { ViewChild, ElementRef, Input, ViewChildren, QueryList, Output, AfterViewInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Workflow } from '../../models';
import { TimelineItem } from './timeline-item.directive';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit, AfterViewInit {
  @Input('progress') progress: Workflow[] = [];
  @ViewChild('timeline') timeline: ElementRef;
  @ViewChild('lastItem') lastItem: ElementRef;
  @ViewChildren(TimelineItem) timelineItems: QueryList<TimelineItem>;

  currentPosition = 0;
  movePerClick = 280;
  limit = 0;

  constructor() {
  }

  ngOnInit() {
    const timelineRect = this.timeline.nativeElement.getBoundingClientRect();
    this.limit = timelineRect.right - this.movePerClick;
  }

  ngAfterViewInit() {
    const item = this.currentItem();
    if(item && item.elementRef) {
      item.elementRef.nativeElement
    }

  }

  currentItem(): TimelineItem {
    let current;
    this.timelineItems.forEach(item => {
      if (item.itsCurrent) {
        current = item;
      }
    });
    return current;
  }

  prev() {
    const x = this.currentPosition+this.movePerClick;
    this.currentPosition = true || (x >= 0 && x <= this.limit) ? x : this.currentPosition;
    this.translateX();
  }

  next() {
    const x = this.currentPosition-this.movePerClick;
    this.currentPosition = true || (x >= 0 && x <= this.limit) ? x : this.currentPosition;
    this.translateX();
  }

  translateX() {
    this.timeline.nativeElement.style.transform = `translateX(${this.currentPosition}px)`;
  }

  isElementInViewport(el: Element) {
    const rect = el.getBoundingClientRect();
    if (!rect) return false;
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= this.limit
    );
  }
  

}
