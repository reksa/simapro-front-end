import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[timelineItem]'
})
export class TimelineItem {
  @Input() itsCurrent: boolean;
  @Input('index') index: number;

  constructor(public elementRef: ElementRef) {
  }

}