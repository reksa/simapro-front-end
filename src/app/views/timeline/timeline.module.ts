import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineComponent } from './timeline.component';
import { TimelineItem } from './timeline-item.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TimelineComponent, TimelineItem],
  exports: [TimelineComponent]
})
export class TimelineModule { }
