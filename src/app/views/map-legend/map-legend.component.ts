import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'map-legend',
  templateUrl: './map-legend.component.html',
  styleUrls: ['./map-legend.component.scss']
})
export class MapLegendComponent implements OnInit {
  @Input('showTanah') showTanah: boolean = false;
  @Input('showProperti') showProperti: boolean = false;
  @Input('zoom') zoom: number = 20;

  constructor() { }

  ngOnInit() {
  }

}
