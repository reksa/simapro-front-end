import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapLegendComponent } from './map-legend.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MapLegendComponent],
  exports: [MapLegendComponent]
})
export class MapLegendModule { }
