import { LaporanService, CompanyCodeService, BusinessAreaService, AsetTanahService, AsetPropertiService, PotensiAsetService, PendayagunaanAsetService, AnggaranService, RealisasiPekerjaanService } from 'app/services/data-services';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem, SelectComponent } from 'ng2-select';
import { UpperCasePipe, DatePipe } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { DatatablesContent } from '../../components/app-datatable/datatables-content.model';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subscription, Observable } from 'rxjs';
import { AppHttpResponse } from '../../models/app-http-respose.model';
import { DokumenLegal } from '../../models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  dataDashboard: DataDashboard;
  chartAsetTanah =  {
    chartType: 'PieChart',
    dataTable: [
      ['Tipe', 'Jumlah'],
      ['Tanah', 100],
    ],
    options: {
      legend: 'left',
      height: 300,
      title: '',
      chartArea: {left: 10, top: 20, width: '100%', height: '100%'},
      colors: ['#FFA1B5', '#86C7F3', '#FFE199', '#e0440e', '#f6c7b6']
    }
  };
  chartAsetProperti =  {
    chartType: 'PieChart',
    dataTable: [
      ['Tipe', 'Jumlah'],
      ['Properti', 100],
    ],
    options: {
      legend: 'left',
      height: 300,
      title: '',
      chartArea: {left: 10, top: 20, width: '100%', height: '100%'},
      colors: ['#FFA1B5', '#86C7F3', '#FFE199', '#e0440e', '#f6c7b6']
    }
  };
  chartPotensi =  {
    chartType: 'PieChart',
    dataTable: [
      ['Tipe', 'Jumlah'],
      ['Tanah', 100],
    ],
    options: {
      legend: 'left',
      height: 300,
      title: '',
      chartArea: {left: 10, top: 20, width: '100%', height: '100%'},
      colors: ['#FFA1B5', '#86C7F3', '#FFE199', '#e0440e', '#f6c7b6']
    }
  };
  chartPendayagunaan =  {
    chartType: 'PieChart',
    dataTable: [
      ['Tipe', 'Jumlah'],
      ['Tanah', 100],
    ],
    options: {
      legend: 'left',
      height: 300,
      title: '',
      chartArea: {left: 10, top: 20, width: '100%', height: '100%'},
      colors: ['#FFA1B5', '#86C7F3', '#FFE199', '#e0440e', '#f6c7b6']
    }
  };
  
  chartAnggaranRealisasi = {
    options: {
      scaleShowVerticalLines: false,
      responsive: true,
      events: false,
      animation: {
        onComplete: function () {
          var ctx = this.chart.ctx;
          ctx.textAlign = "center";
          ctx.textBaseline = "top";
          var chart = this;
          var datasets = this.config.data.datasets;
  
          datasets.forEach(function (dataset: any, i: number) {
            ctx.font = "12px Arial";  
  
            ctx.fillStyle = "Black";
            chart.getDatasetMeta(i).data.forEach(function (p: any, j: any) {
              ctx.fillText('Rp. ' + dataset.data[j].toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), p._model.x, p._model.y - 20);
            });
  
          });
        }
      },
    },
    labels: ['Angggaran dan Realisasi'],
    type: 'bar',
    legend: true,
   
    datasets: [
      {data: [], label: 'Anggaran'},
      {data: [], label: 'Realisasi'}
    ],
  }

  itemsCC = [];
  itemsBA = [];
  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('templateValidasi') templateValidasi: SelectComponent;
  params = {
    idCompanyCode: 0,
    idBusinessArea: 0
  }
  inbox = new DatatablesContent();
  sertifikat = new DatatablesContent();
  isInboxExist = false;

  subsInbox: Subscription;
  subsSertifikatExpired: Subscription;
  // validasi
  _item: Inbox;
  memo: string = '';
  modalRef: BsModalRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private laporanService: LaporanService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private asetTanahService: AsetTanahService,
    private asetPropertiService: AsetPropertiService,
    private potensiAsetService: PotensiAsetService,
    private pendayagunaanService: PendayagunaanAsetService,
    private anggaranService: AnggaranService,
    private realisasiService: RealisasiPekerjaanService,
    private modalService: BsModalService,
    private uppercasePipe: UpperCasePipe,
    private datePipe: DatePipe,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.spinner.show();
    this.getCompanyCode();
    this._getInbox();
    // this._getSertifikatWillBeExpired();
    this.route.queryParamMap.subscribe(params => {
      const parameters = new URLSearchParams();
      if (params.get('idCompanyCode') && params.get('idCompanyCode') != '0') {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.params.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('idBusinessArea') && params.get('idBusinessArea') != '0') {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.params.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      this.laporanService.dashboard(parameters).subscribe((dashboard: DataDashboard) =>{
        this._mapSertifikatToDatatable(dashboard.sertifikat_akan_habis || [])
        this.dataDashboard = dashboard;

        this.chartAsetTanah = Object.create(this.chartAsetTanah);
        this.chartAsetProperti = Object.create(this.chartAsetProperti);
        this.chartPotensi = Object.create(this.chartPotensi);
        this.chartPendayagunaan = Object.create(this.chartPendayagunaan);
        
        this.chartAsetTanah.dataTable = dashboard.tanah.chart.map(c => {
          return [c.jenis, c.total]
        });
        this.chartAsetTanah.dataTable.splice(0,0, ['Tipe', 'Jumlah']);
        
        this.chartAsetProperti.dataTable = dashboard.properti.chart.map(c => {
          return [c.jenis, c.total]
        });
        this.chartAsetProperti.dataTable.splice(0,0, ['Tipe', 'Jumlah']);
        
        this.chartPotensi.dataTable = dashboard.potensi.chart.map(c => {
          return [c.jenis, c.total]
        });
        this.chartPotensi.dataTable.splice(0,0, ['Tipe', 'Jumlah']);
        
        this.chartPendayagunaan.dataTable = dashboard.pendayagunaan.chart.map(c => {
          return [c.jenis, c.total]
        });
        this.chartPendayagunaan.dataTable.splice(0,0, ['Tipe', 'Jumlah']);

        this.chartAnggaranRealisasi.datasets = dashboard.anggaran.chart.map(c => {
          return {data: [c.total], label: c.jenis}
        });
        this.spinner.hide();
      });
    });
  }

  private _getInbox() {
    if (this.subsInbox) {
      this.subsInbox.unsubscribe;
      this.isInboxExist = false;
    }
    this.subsInbox = this.laporanService.getInbox().subscribe((inbox: any[]) => {
      this._mapInboxToDatatable(inbox || []);
      this.isInboxExist = true;
    });
  }

  // private _getSertifikatWillBeExpired() {
  //   if (this.subsSertifikatExpired) {
  //     this.subsSertifikatExpired.unsubscribe;
  //   }
  //   this.subsSertifikatExpired = this.laporanService.getSertifikatWillBeExpired().subscribe((inbox: any[]) => {
  //     this._mapSertifikatToDatatable(inbox || []);
  //   });
  // }

  filter(filterOptions) {
    const queryParams = {};
    for(let prop in filterOptions) {
      if(filterOptions[prop] !== '0' && filterOptions[prop] !== 'all' && filterOptions[prop] != false) {
        queryParams[prop] = filterOptions[prop];
      }
    }
    this.router.navigate(['dashboard', {}], {
      queryParams: queryParams
    });
  }

  private getCompanyCode() {
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.params.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
          this.getBusinessArea();
        }
      });
    });
  }

  private getBusinessArea() {
    this.businessAreaService.getByIdCompanyCode(this.params.idCompanyCode).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
      });
      this.itemsBA.forEach((item,index) => {
        if (item.id === this.params.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.params.idCompanyCode;
    if (old !== parseInt(e.id)) {
      this.params.idCompanyCode = parseInt(e.id);
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.params.idCompanyCode;
    if (old !== parseInt(e.id)) {
      this.params.idCompanyCode = parseInt(e.id);
      this.getBusinessArea();
    }
  }

  refreshValueBA(e: SelectItem) {
    this.params.idBusinessArea = parseInt(e.id);
  }

  selectedBA(e: SelectItem) {
    this.params.idBusinessArea = parseInt(e.id);
  }

  private _mapInboxToDatatable(inbox: Inbox[]) {
    this.inbox.columns = ['KATEGORI', 'FROM', 'NAMA ITEM YANG DIAJUKAN', 'JENIS PENGAJUAN', 'MEMO', 'TANGGAL'];
    this.inbox.actions = ['show', 'validate', 'approve'];
    this.inbox.data.next([]);
    const data = [];

    inbox.map((r) => {
      const category = r.category ? this._toUppercase(r.category) : '-';
      const subyek = r.subject ? this._toUppercase(r.subject) : '-';
      const type = r.jenis ? this._toUppercase(r.jenis) : '-';
      const from = r.from ? this._toUppercase(r.from) : '-';
      const memo = r.memo ? this._toUppercase(r.memo) : '-';
      const tanggal = r.date ?
      this.datePipe.transform(r.date, 'dd MMM yyyy') : '-';

      data.push({
        row: [ category, from, subyek, type, memo, tanggal ],
        id: r
      });
    });

    this.inbox.data.next(data);
  }

  private _mapSertifikatToDatatable(sertifikat: SertifikatExpired[]) {
    this.sertifikat.columns = ['NAMA ASET TANAH', 'KEPEMILIKAN', 'NO. SERTIFIKAT', 'NO. PBB', 'TANGGAL AKHIR MASA BERLAKU'];
    this.sertifikat.actions = ['show'];
    this.sertifikat.data.next([]);
    const data = [];

    sertifikat.map((r) => {
      const nama = r.nama_tanah ? this._toUppercase(r.nama_tanah) : '-';
      const kepemilikan = r.ijin ? this._toUppercase(r.ijin) : '-';
      const noSertifikat = r.no_sertifikat || '-';
      const endDate = r.end_date_sertifikat ? this.datePipe.transform(r.end_date_sertifikat, 'dd MMM yyyy') : '-';
      const pbb = r.no_pbb || '-';
      

      data.push({
        row: [ nama, kepemilikan, noSertifikat, pbb, endDate ],
        id: r
      });
    });

    this.sertifikat.data.next(data);
  }

  show(item: Inbox) {
    const path = this._getPath(item);
    const fragment = item.category !== item.data_type ? item.data_type.toLowerCase() : null;
    const opt = fragment ? {fragment: fragment} : {};
    this.router.navigate([path], opt);
    return false
  }

  showSertifikat(sertifikat: SertifikatExpired) {
    const nama = this.slugPipe.transform(sertifikat.nama_tanah || '-');
    const path = `/aset-tanah/show/${sertifikat.id_tanah}/${nama}`;
    const fragment = 'legal';
    this.router.navigate([path], {fragment: fragment} );
    return false
  }

  showModalValidasi(item: Inbox) {
    this._item = item;
    this.modalRef = this.modalService.show(this.templateValidasi);
    return false;
  }

  showApprovePage(item: Inbox) {
    const name = this.slugPipe.transform(item.data_name || 'approve')
    this.router.navigate([`/anggaran/approve/${item.data_id}/${name}`]);
    return false
  }

  validate(selectedOption: boolean) {
    this.spinner.show();

    if(this.modalRef) {
      this.modalRef.hide();
    }
    let subs: Observable<any>;
    if (this._item.category === 'Tanah') {
      if (this._item.data_type === 'Tanah') {
        subs = this.asetTanahService.validasi(this._item.data_id, this.memo, selectedOption);
      } else if (this._item.data_type === 'Detail') {
        subs = this.asetTanahService.validasiDetail(this._item.id_tanah_detail, this.memo, selectedOption);
      } else if (this._item.data_type === 'Mutasi') {
        subs = this.asetTanahService.validasiPerpindahan(this._item.id_perpindahan_tanah, this.memo, selectedOption);
      } else if (this._item.data_type === 'Legal') {
        subs = this.asetTanahService.validasiSertifikat(this._item.id_sertifikat_tanah, this.memo, selectedOption);
      } else if (this._item.data_type === 'Nilai') {
        subs = this.asetTanahService.validasiNilai(this._item.id_nilai_tanah, this.memo, selectedOption);
      }
    } else if (this._item.category === 'Properti') {
      if (this._item.data_type === 'Properti') {
        subs = this.asetPropertiService.validasi(this._item.data_id, this.memo, selectedOption);
      } else if (this._item.data_type === 'Detail') {
        subs = this.asetPropertiService.validasiDetail(this._item.id_properti_detail, this.memo, selectedOption);
      } else if (this._item.data_type === 'Mutasi') {
        subs = this.asetPropertiService.validasiPerpindahan(this._item.id_perpindahan_properti, this.memo, selectedOption);
      } else if (this._item.data_type === 'Nilai') {
        subs = this.asetPropertiService.validasiNilai(this._item.id_nilai_properti, this.memo, selectedOption);
      }
    } else if (this._item.category === 'Potensi') {
      if (selectedOption) {
        subs = this.potensiAsetService.terimaUsulanPotensi(this._item.data_id, this.memo);
      } else {
        subs = this.potensiAsetService.tolakUsulanPotensi(this._item.data_id, this.memo);
      }
    } else if (this._item.category === 'Pendayagunaan') {
      subs = this.pendayagunaanService.validasi(this._item.data_id, selectedOption, this.memo);
    } else if (this._item.category === 'Anggaran') {
      const id = this._item.id_anggaran_detail || this._item.data_id;
      subs = this.anggaranService.validasi(id, this.memo, selectedOption);
    } else if (this._item.category === 'Realisasi') {
      subs = this.realisasiService.validasi(this._item.data_id, this.memo, selectedOption);
    }
    if (subs) {
      subs.subscribe((res: AppHttpResponse) => {
        this.spinner.hide();
        if(res.message) {
          alert(res.message)
        }
        
        this._getInbox();
      })
    }
  }

  private _toUppercase(str: string): string {
    return this.uppercasePipe.transform(str || '');
  }

  private _getPath(item: Inbox): string {
    let path = '';
    switch(item.category) {
      case 'Tanah': 
          if (item.data_type === 'Tanah') {
            path = '/aset-tanah/validasi';
          } else {
            path = '/aset-tanah/show';
          }
        break;
      case 'Properti': 
          if (item.data_type === 'Tanah') {
            path = '/aset-properti/validasi';
          } else {
            path = '/aset-properti/show';
          }
        break;
      case 'Potensi': path = '/potensi-aset/show';
        break;
      case 'Pendayagunaan': path = '/pendayagunaan-aset/show';
        break;
      case 'Anggaran': path = '/anggaran/show';
        break;
      case 'Realisasi': path = '/realisasi/show';
        break;
      default: path = '/error/404'
    }
    const nama = this.slugPipe.transform(item.data_name);
    return `${path}/${item.data_id}/${nama}`;
  }
}

class DataDashboard {
  pendayagunaan: {
    chart: ChartData[];
    jumlah: number;
    nilai: number;
  };
  potensi: {
    chart: ChartData[];
    didayagunakan: number;
    nilai: number;
    potensi: number;
  };
  properti: {
    chart: ChartData[];
    jumlah: number;
    luas: number;
    nilai: number;
  };
  tanah: {
    chart: ChartData[];
    jumlah: number;
    kepemilikan_tidak_jelas: number;
    luas: number;
    nilai: number;
  };
  anggaran: {
    chart: ChartData[];
    item_anggaran: number;
    item_realisasi: number;
  };
  sertifikat_akan_habis: SertifikatExpired[];
}

class ChartData {
  jenis: string;
  total: number;
}

class SertifikatExpired {
  id_tanah: number;
  end_date_sertifikat: Date;
  ijin: string;
  kode_tanah: string;
  nama_tanah: string;
  no_pbb: string;
  no_sertifikat: string;
}

class Inbox {
  category: string;
  data_id: number;
  id_anggaran_detail: number;
  id_tanah_detail: number;
  id_properti_detail: number;
  id_nilai_tanah: number;
  id_nilai_properti: number;
  id_perpindahan_tanah: number;
  id_perpindahan_properti: number;
  id_sertifikat_tanah: number;
  data_name: string;
  data_type: string;
  date: Date;
  foto: string;
  from: string;
  jenis: string;
  memo: string;
  pengguna: string;
  subject: string;
  to: string;
}