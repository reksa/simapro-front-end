import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { CommonModule } from '@angular/common';
import { AuthGuard } from 'app/services/auth';
import { SelectModule } from 'ng2-select';
import { LaporanService, PotensiAsetService, PendayagunaanAsetService, RealisasiPekerjaanService, AnggaranService } from '../../services/data-services';
import { PaginationModule } from '../../components/pagination/pagination.module';
import { FormsModule } from '@angular/forms';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    SelectModule,
    AppDatatableModule,
    PaginationModule,
    FormsModule,
    Ng2GoogleChartsModule
  ],
  providers: [
    AuthGuard,
    LaporanService,
    PotensiAsetService,
    PendayagunaanAsetService,
    AnggaranService,
    RealisasiPekerjaanService
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule { }
