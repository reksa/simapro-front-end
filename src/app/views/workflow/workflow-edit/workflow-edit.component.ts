import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { RoleService, WorkflowService } from 'app/services/data-services';
import { Workflow, RoleWorkflow, TypeWorkflow, Role, Permission } from 'app/models';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-workflow-edit',
  templateUrl: './workflow-edit.component.html',
  styleUrls: ['./workflow-edit.component.css']
})
export class WorkflowEditComponent implements OnInit {
  workflow = new Workflow();
  roles: Role[] = [];
  rolesToShow: Role[] = [];
  myForm: FormGroup;
  title = '';
  isSubmitted = false;
  root = 'workflow/';
  act = 'add';
  isEdit = true;
  filter = '';
  detail: RoleWorkflow[] = [];
  usedRole: Role[] = [];
  hasStart: boolean = false;
  hasFinish: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private workflowService: WorkflowService,
    private roleService: RoleService,
    private _fb: FormBuilder,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.roleService.getAll().subscribe(roles => {
      this.spinner.hide();
      this.roles = roles;
      this.filterRoles();
    });

    this.myForm = this._fb.group({
      nama: ['', [
          Validators.required,
          Validators.maxLength(50)
        ]
      ],
      role_workflow: this._fb.array([], Validators.minLength(1))
    });
    this.route.url.subscribe(url => {
      this.act = url[0].path;
      if (this.act === 'edit') {
        this.prepareEdit();
        this.title = 'Ubah Workflow';
      } else if (this.act === 'show') {
        this.prepareEdit();
        this.isEdit = false;
        this.myForm.get('nama').disable();
        this.title = 'Detail Workflow';
      } else {
        this.prepareAdd();
      }
    });
  }

  prepareAdd() {
    this.title = 'Tambah Workflow';
  }
  
  prepareEdit() {
    this.spinner.show();
    this.route.paramMap.subscribe(params => {
      const idWorkflow = parseInt(params.get('idWorkflow'), 10);
      this.workflowService.getById(idWorkflow).subscribe(workflow => {
        this.spinner.hide();
        this.workflow = workflow;
        this.workflow.role_workflow.forEach(r =>{
          const formRoleWorkflow = this.myForm.get('role_workflow') as FormArray;
          formRoleWorkflow.push(this.createFormRoleWorkflow());
        });
        this.hasStart = true;
        this.hasFinish = true;
        // this.filterRoles();
      });
    });
  }
  
  cancel() {
    if (confirm('Ingin membatalkan?')) {
      this._location.back();
    }
    return false;
  }
  
  edit(data: Workflow) {
    const nama = this.slugPipe.transform(data.nama_workflow);
    this.router.navigate([`${this.root}company-code/edit/${data.id_workflow}/${nama}`]);
    return false;
  }
  
  save() {
    this.isSubmitted = true;
    this.spinner.show();
    this.workflowService.save(this.workflow).subscribe(res => {
      this.spinner.hide();
      this.isSubmitted = false;
      if (res.message) {
        alert(res.message);
      }
  
      if (res.success) {
        this._location.back();
      } else {
        this.isSubmitted = false;
      }
    });
  }

  filterRoles() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.rolesToShow = [];

    this.roles.map((role: Role, index: number, roles: Role[]) => {
      if (regexp.test(role.nama.toLowerCase()) && (this.usedRole.indexOf(role) === -1)) {
        this.rolesToShow.push(role);
      }
    });
  }

  isInvolved(role: Role) {
    this.workflow.role_workflow.forEach(d => {
      if (d.role.id_role === role.id_role) {
        return true;
      }
    });
  }
  
  log() {
    // console.log(this.myForm);
  }

  addToWorkflow(role: Role) {
    const d = new RoleWorkflow();
    d.role = role;
    d.id_role = role.id_role;
    this.workflow.role_workflow.push(d);
    const formRoleWorkflow = this.myForm.get('role_workflow') as FormArray;
    formRoleWorkflow.push(this.createFormRoleWorkflow());
  }

  createFormRoleWorkflow() {
    return this._fb.group({
      id_permission: ['', [Validators.required]],
      prev: [null, []],
      next: [null, []]
    });
  }

  removeFromWorkflow(index){
    const roleWorkflow = this.workflow.role_workflow[index];
    const j =  this.usedRole.indexOf(roleWorkflow.role);
    this.workflow.role_workflow.splice(index, 1);
    const formRoleWorkflow = this.myForm.get('role_workflow') as FormArray;
    formRoleWorkflow.removeAt(index);
    // this.filterRoles();
    return false;
  }

  toggleStart(index: number) {
    this.hasStart = !this.hasStart;
    this.workflow.role_workflow[index].is_start = this.hasStart;
  }

  toggleFinish(index: number) {
    this.hasFinish = !this.hasFinish;
    this.workflow.role_workflow[index].is_finish = this.hasFinish;
  }
}
