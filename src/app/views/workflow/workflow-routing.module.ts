import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { WorkflowComponent } from './workflow/workflow.component';
import { WorkflowEditComponent } from './workflow-edit/workflow-edit.component';


const routes: Routes = [
  { path: '', component: WorkflowComponent, canActivate: [AuthGuard, PermissionGuard]},
  { path: 'add', component: WorkflowEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idWorkflow/:nama', component: WorkflowEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'edit/:idWorkflow/:nama', component: WorkflowEditComponent, canActivate: [AuthGuard, PermissionGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowRoutingModule { }
