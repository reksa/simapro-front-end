import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { Workflow } from 'app/models';
import { URLSearchParams } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { DataSetting, AppTableConfig } from 'app/components/pagination/page/page.component';
import { WorkflowService } from 'app/services/data-services';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'app/services/alert.service';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  tab = 'workflow';
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  operation: string;
  isLoading = true;
  root = ''; // 'master-data/organisasi/';

  public tableConfig = new AppTableConfig();

  constructor(
    private workflowService: WorkflowService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private slugPipe: TextToSlugPipe
  ) { }

  ngOnInit() {
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;

    this.route.queryParamMap.switchMap(params => {
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));

      this.dataSetting.query = params.get('query');

      return this.workflowService.getPaginate(parameters);
    }).subscribe((res) => {
      this.pageData = res.data;
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();


      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root + 'workflow', {}], {
      // relativeTo: this.route,
      queryParams: this.dataSetting
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  show(data: Workflow) {
    const nama = this.slugPipe.transform(data.nama_workflow);
    this.router.navigate([`${this.root}workflow/show/${data.id_workflow}/${nama}`]);
    return false;
  }

  edit(data: Workflow) {
    const nama = this.slugPipe.transform(data.nama_workflow);
    this.router.navigate([`${this.root}workflow/edit/${data.id_workflow}/${nama}`]);
    return false;
  }

  delete(data: Workflow) {
    if (confirm(`Ingin nonaktifkan ${data.nama_workflow} ?`)) {
      data.is_active = false;
      this.workflowService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = true;
        }
      });
    }
    return false;
  }

  undelete(data: Workflow) {
    if (confirm(`Ingin mengaktifkan ${data.nama_workflow} ?`)) {
      data.is_active = true;
      this.workflowService.save(data)
      .subscribe((res) => {
        if (res.message) {
          alert(res.message);
        }

        if (!res.success) {
          data.is_active = false;
        }
      });
    }
    return false;
  }

  private chooseTab(tab) {
    this.router.navigate([`${this.root}${tab}`]);
    return false;
  }
}
