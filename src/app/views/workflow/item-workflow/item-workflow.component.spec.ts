import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemWorkflowComponent } from './item-workflow.component';

describe('ItemWorkflowComponent', () => {
  let component: ItemWorkflowComponent;
  let fixture: ComponentFixture<ItemWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
