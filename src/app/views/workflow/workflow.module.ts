import { SelectModule } from 'ng2-select';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { PaginationModule } from './../../components/pagination/pagination.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { WorkflowService, RoleService } from 'app/services/data-services';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowEditComponent } from './workflow-edit/workflow-edit.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { WorkflowRoutingModule } from './workflow-routing.module';
import { ItemWorkflowComponent } from './item-workflow/item-workflow.component';
import { RoleWorkflowComponent } from './role-workflow/role-workflow.component';

@NgModule({
  imports: [
    CommonModule,
    WorkflowRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    SortableModule.forRoot(),
    SelectModule
  ],
  declarations: [
    WorkflowComponent,
    WorkflowEditComponent,
    ItemWorkflowComponent,
    RoleWorkflowComponent,
  ],
  providers: [
    WorkflowService,
    TextToSlugPipe,
    RoleService
  ]
})
export class WorkflowModule { }
