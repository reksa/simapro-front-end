import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleWorkflowComponent } from './role-workflow.component';

describe('RoleWorkflowComponent', () => {
  let component: RoleWorkflowComponent;
  let fixture: ComponentFixture<RoleWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
