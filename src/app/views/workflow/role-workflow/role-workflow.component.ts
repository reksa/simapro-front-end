import { SelectComponent } from 'ng2-select';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { RoleWorkflow } from 'app/models';

@Component({
  selector: 'role-workflow',
  templateUrl: './role-workflow.component.html',
  styleUrls: ['./role-workflow.component.scss']
})
export class RoleWorkflowComponent implements OnInit {
  @Input('roleWorkflow') item: RoleWorkflow;
  @Input('index') i: number;
  @Input('roleWorkflows') allRoleWorkflow: RoleWorkflow[] = [];
  @Input('group') group: FormGroup;
  @Input('hasStart') hasStart: boolean;
  @Input('hasFinish') hasFinish: boolean;
  @Output('onRemove') close = new EventEmitter();
  @Output('toggleStart') toggleStart = new EventEmitter();
  @Output('toggleFinish') toggleFinish = new EventEmitter();
  
  @ViewChild('selectPermission') selectPermission: SelectComponent;
  items = [];

  constructor() { }

  ngOnInit() {
    this.items = this.item.role.permission.map(permission => {
      return {id: permission.id_permission, text: permission.nama};
    });

    this.items.forEach((item, index) => {
      if(item.id === this.item.id_permission) {
        this.selectPermission.active = [item];
        this.refreshValue(item);
      }
    });
  }

  onClose() {
    this.close.emit(this.i);
  }

  clickStart() {
    this.toggleStart.emit(this.i);
  }

  clickFinish() {
    this.toggleFinish.emit(this.i);
  }

  refreshValue(value: any):void {
    this.group.get('id_permission').setValue(value.id);
    this.group.get('id_permission').updateValueAndValidity();
  }

  typed(e: any) {
    this.group.get('id_permission').markAsTouched();
    this.group.get('id_permission').markAsDirty();
  }
}
