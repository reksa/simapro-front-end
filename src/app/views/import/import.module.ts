import { ProgressbarModule, TabsModule } from 'ngx-bootstrap';
import { AsetTanahService, AsetPropertiService, PendayagunaanAsetService, LaporanService, CompanyCodeService, ProvinsiService, PotensiAsetService } from 'app/services/data-services';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImportRoutingModule } from './import-routing.module';
import { ImportTanahComponent } from './import-tanah/import-tanah.component';
import { FormsModule } from '@angular/forms';
import { AppDatatableModule } from '../../components/app-datatable/app-datatable.module';
import { ImportPropertiComponent } from './import-properti/import-properti.component';
import { ImportPendayagunaanComponent } from './import-pendayagunaan/import-pendayagunaan.component';
import { SelectModule } from 'ng2-select';

@NgModule({
  imports: [
    CommonModule,
    ImportRoutingModule,
    FormsModule,
    ProgressbarModule.forRoot(),
    AppDatatableModule,
    SelectModule,
    TabsModule
  ],
  providers: [
    AsetTanahService,
    AsetPropertiService,
    PendayagunaanAsetService,
    LaporanService,
    CompanyCodeService,
    ProvinsiService,
    PotensiAsetService
  ],
  declarations: [ImportTanahComponent, ImportPropertiComponent, ImportPendayagunaanComponent]
})
export class ImportModule { }
