import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { ImportPropertiComponent } from './import-properti/import-properti.component';
import { OnProgressGuardService } from './../../services/auth/on-progress-guard.service';
import { ImportTanahComponent } from './import-tanah/import-tanah.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImportPendayagunaanComponent } from './import-pendayagunaan/import-pendayagunaan.component';

const routes: Routes = [
  { path: '', redirectTo: 'tanah', pathMatch: 'full' },
  { path: 'tanah', component: ImportTanahComponent, canActivate:[AuthGuard, PermissionGuard], canDeactivate:[OnProgressGuardService] },
  { path: 'properti', component: ImportPropertiComponent, canActivate:[AuthGuard, PermissionGuard], canDeactivate:[OnProgressGuardService] },
  { path: 'pendayagunaan', component: ImportPendayagunaanComponent, canActivate:[AuthGuard, PermissionGuard], canDeactivate:[OnProgressGuardService] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImportRoutingModule { }
