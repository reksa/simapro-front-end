import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportPropertiComponent } from './import-properti.component';

describe('ImportPropertiComponent', () => {
  let component: ImportPropertiComponent;
  let fixture: ComponentFixture<ImportPropertiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportPropertiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportPropertiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
