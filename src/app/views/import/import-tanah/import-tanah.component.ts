import { ProvinsiService } from "./../../../services/data-services/provinsi.service";
import { RowData } from "./../../../components/app-datatable/datatables-content.model";
import { _Tanah } from "./../../../models/_tanah.model";
import { DatatablesContent } from "app/components/app-datatable/datatables-content.model";
import { OnProgressService } from "./../../../services/on-progress.service";
import {
  AsetTanahService,
  LaporanService,
  CompanyCodeService
} from "app/services/data-services";
import { Component, OnInit, ViewChild } from "@angular/core";
import * as XLSX from "xlsx";
import { URLSearchParams } from "@angular/http";
import { SelectComponent, SelectItem } from "ng2-select";
import { UpperCasePipe } from "@angular/common";
import { CompanyCode, Provinsi } from "../../../models";
import { TextToSlugPipe } from "../../../services/text-to-slug.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-import-tanah",
  templateUrl: "./import-tanah.component.html",
  styleUrls: ["./import-tanah.component.scss"]
})
export class ImportTanahComponent implements OnInit {
  private setting = {
    element: {
      dynamicDownload: null as HTMLElement
    }
  }
  file: any;
  dataObject: any;
  counter: number = 0;
  succeed: number = 0;
  failed: number = 0;
  result: DatatablesContent = new DatatablesContent();
  res: any[] = [];
  timerElapsed: number;
  isSubmitted = false;
  onProgress = false;

  i = 0;
  x;

  itemsCC = [];
  itemsP = [];

  companyCode: CompanyCode;
  provinsi: Provinsi;

  @ViewChild("selectCC")
  selectCC: SelectComponent;
  @ViewChild("selectP")
  selectP: SelectComponent;

  constructor(
    private asetTanahService: AsetTanahService,
    private onProgressService: OnProgressService,
    private laporanService: LaporanService,
    private companyCodeService: CompanyCodeService,
    private provinsiService: ProvinsiService,
    private uppercasePipe: UpperCasePipe,
    private slugPipe: TextToSlugPipe,
    private spinner: NgxSpinnerService
  ) {
    this.result.columns = ["Row No.", "Nama", "Result", "Message"];
    this.onProgressService.onProgress.subscribe(onProgress => {
      this.onProgress = onProgress;
    });
  }

  ngOnInit() {
    this.getCompanyCode();
    this.getProvinsi();
  }

  onFileChange(event) {
    this.spinner.show();
    // Get The File From The Input
    var file = event.target.files[0];
    // var sFilename = file.name;
    // Create A File Reader HTML5
    var reader = new FileReader();

    // Ready The Event For When A File Gets Selected
    reader.onload = e => {
      // @ts-ignore
      var data = e.target.result;
      var cfb = XLSX.CFB.read(data, { type: "binary" });
      // @ts-ignore
      var wb = XLSX.parse_xlscfb(cfb);
      // Loop Over Each Sheet
      var obj = [];
      wb.SheetNames.forEach(function(sheetName) {
        // Obtain The Current Row As CSV
        // @ts-ignore
        // var sCSV = XLSX.utils.make_csv(wb.Sheets[sheetName]);
        // @ts-ignore
        var oJS = XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName], {
          raw: true
        });

        obj[sheetName] = oJS;
      });
      const t = obj["TEMPLATE ASET TANAH"];
      this.dataObject = t.map(tanah => {
        if (parseInt(tanah["0"]) > 0) {
          const _t = new _Tanah(tanah);
          _t.is_active = true;
          return _t;
        }
      });

      for (let i = 0; i < this.dataObject.length; i++) {
        if (!this.dataObject[i]) {
          this.dataObject.splice(i, 1);
          i--;
        }
      }
      this.spinner.hide();
    };

    // Tell JS To Start Reading The File.. You could delay this if desired
    reader.readAsBinaryString(file);
  }

  submit() {
    this.spinner.show();
    this.res = [];
    this.result.data.next([]);
    this.isSubmitted = true;
    this.onProgressService.onProgress.next(true);

    this.asetTanahService.import(this.dataObject).subscribe(res => {
      if (res.message) {
        alert(res.message);
      }

      if(res.logs) {
        this.dyanmicDownloadByHtmlTag({
          fileName: 'Log',
          text: res.logs
        });
      }

      this.onProgressService.onProgress.next(false);
      this.isSubmitted = false;
      this.spinner.hide();
    });
  }

  private dyanmicDownloadByHtmlTag(arg: {fileName: string, text: string}) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType = 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    var event = new MouseEvent("click");
    element.dispatchEvent(event);
  }

  public _simpan(tanah) {
    this.asetTanahService.save(tanah).subscribe(res => {
      // this.result.data.next(res);
      const rowResult = new RowData();
      rowResult.id = tanah.no_urut;
      rowResult.row = [
        tanah.no_urut,
        tanah.nama_tanah,
        res.success
          ? '<span class="bg-success" style="padding: 3px 5px;">Berhasil</span>'
          : '<span class="bg-danger">Gagal</span>',
        res.message
      ];
      this.res.push(rowResult);
      if (res.success) {
        this.succeed++;
      } else {
        this.failed++;
      }
    });
  }

  exportBusinessArea() {
    if (!this.companyCode) {
      return false;
    }
    this.spinner.show();
    const namaFile =
      "business-area-" +
      this.slugPipe.transform(this.companyCode.nama) +
      ".xlsx";
    const params = new URLSearchParams();
    params.set("id_company_code", this.companyCode.id_company_code.toString());
    this.laporanService.downloadExcel("business_area", params).subscribe(
      res => {
        this.spinner.hide();

        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = namaFile;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      },
      error => {
        console.log("download error:", JSON.stringify(error));
      },
      () => {}
    );
  }

  exportLokasi() {
    if (!this.provinsi) {
      return false;
    }
    this.spinner.show();
    const namaFile =
      "lokasi-" + this.slugPipe.transform(this.provinsi.nama) + ".xlsx";
    const params = new URLSearchParams();
    params.set("id_provinsi", this.provinsi.id_provinsi.toString());
    this.laporanService.downloadExcel("lokasi", params).subscribe(
      res => {
        this.spinner.hide();

        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = url;
        a.download = namaFile;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element
      },
      error => {
        console.log("download error:", JSON.stringify(error));
      },
      () => {}
    );
  }

  private getCompanyCode(all?: boolean) {
    const params = new URLSearchParams();
    all = all || false;
    this.companyCodeService.getAll(params, all).subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {
          id: companyCode,
          text: `${
            companyCode.kode_company_code
          } - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`
        };
      });
      if (this.companyCode) {
        this.itemsCC.forEach((item, index) => {
          if (item.id.id_company_code === this.companyCode.id_company_code) {
            this.selectCC.active = [this.itemsCC[index]];
          }
        });
      }
    });
  }

  private getProvinsi() {
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {
          id: provinsi,
          text: this.uppercasePipe.transform(provinsi.nama)
        };
      });
      if (this.provinsi) {
        this.itemsP.forEach((item, index) => {
          if (item.id.id_provinsi === this.provinsi.id_provinsi) {
            this.selectP.active = [this.itemsP[index]];
          }
        });
      }
    });
  }

  refreshValueCC(e) {
    this.companyCode = e.id;
  }

  selectedCC(e) {
    this.companyCode = e.id;
  }

  refreshValueP(e) {
    this.provinsi = e.id;
  }

  selectedP(e) {
    this.provinsi = e.id;
  }
}
