import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportTanahComponent } from './import-tanah.component';

describe('ImportTanahComponent', () => {
  let component: ImportTanahComponent;
  let fixture: ComponentFixture<ImportTanahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportTanahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportTanahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
