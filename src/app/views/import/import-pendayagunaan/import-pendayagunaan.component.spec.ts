import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportPendayagunaanComponent } from './import-pendayagunaan.component';

describe('ImportPendayagunaanComponent', () => {
  let component: ImportPendayagunaanComponent;
  let fixture: ComponentFixture<ImportPendayagunaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportPendayagunaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportPendayagunaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
