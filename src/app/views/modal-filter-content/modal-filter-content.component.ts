import { URLSearchParams } from '@angular/http';
import { Component, OnInit, ViewChildren, ViewChild } from '@angular/core';
import { DataSetting } from '../../components/pagination/page/page.component';
import { SelectComponent, SelectItem } from 'ng2-select';
import { BsModalRef } from 'ngx-bootstrap';
import { CompanyCodeService, BusinessAreaService, ProvinsiService, KabupatenService, LokasiService, JenisDayaGunaService } from '../../services/data-services';
import { UpperCasePipe } from '@angular/common';
import { JenisDayaGuna } from '../../models';

@Component({
  selector: 'modal-content',
  templateUrl: './modal-filter-content.component.html',
  styleUrls: ['./modal-filter-content.component.scss']
})
export class ModalFilterContentComponent implements OnInit {
  dataSetting = new DataSetting();
  filter = false;
  levelAccess = true;
  isAsetTanah = false;
  isAsetProperti = false;
  isPendayagunaan = false;
  isAnggaran = false;
  isRealisasi = false;

  itemsCC = [];
  itemsBA = [];
  itemsP = [];
  itemsK = [];
  itemsL = [];
  jenisPendayagunaans: JenisDayaGuna[] = [];

  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectProvinsi') selectP: SelectComponent;
  @ViewChild('selectKabupaten') selectK: SelectComponent;
  @ViewChild('selectLokasi') selectL: SelectComponent;


  constructor(
    public modalRef: BsModalRef,
    public companyCodeService: CompanyCodeService,
    public businessAreaService: BusinessAreaService,
    public provinsiService: ProvinsiService,
    public kabupatenService: KabupatenService,
    public lokasiService: LokasiService,
    public jenisPendayagunaanService: JenisDayaGunaService,
    public uppercasePipe: UpperCasePipe
  ) {}

  ngOnInit() {
    this.getCompanyCode();
    this.getProvinsi();
    if(this.isPendayagunaan) {
      this.getJenisPendayagunaan();
    }
  }

  search() {
    this.filter = true;
    this.modalRef.hide();
  }

  private getJenisPendayagunaan() {
    this.jenisPendayagunaanService.getAll().subscribe(jenisPendayagunaans => {
      this.jenisPendayagunaans = jenisPendayagunaans;
    });
  }

  private getCompanyCode() {
    const params = new URLSearchParams();
    this.companyCodeService.getAll(params).subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      this.itemsCC.splice(0, 0, {id: 'all', text: 'SEMUA'});
      if(this.dataSetting.idCompanyCode) {
        this.itemsCC.forEach((item,index) => {
          if (item.id === this.dataSetting.idCompanyCode) {
            this.selectCC.active = [this.itemsCC[index]];
            this.getBusinessArea();
          }
        });
      }
    });
  }

  private getBusinessArea() {
    const params = new URLSearchParams();
    this.businessAreaService.getByIdCompanyCode(this.dataSetting.idCompanyCode, params).subscribe(businessAreas => {
      this.itemsBA = businessAreas.map(businessArea => {
        return {id: businessArea.id_business_area, text: this.uppercasePipe.transform(businessArea.nama_business_area)}
      });
      this.itemsBA.splice(0, 0, {id: 'all', text: 'SEMUA'});
      if(this.dataSetting.idBusinessArea) {
        this.itemsBA.forEach((item,index) => {
          if (item.id === this.dataSetting.idBusinessArea) {
            this.selectBA.active = [this.itemsBA[index]];
          }
        });
      }
    });
  }

  private getProvinsi() {
    this.provinsiService.getAll().subscribe(provinsis => {
      this.itemsP = provinsis.map(provinsi => {
        return {id: provinsi.id_provinsi, text: this.uppercasePipe.transform(provinsi.nama)};
      });
      this.itemsP.splice(0, 0, {id: 'all', text: 'SEMUA'});
      if(this.dataSetting.idProvinsi) {
        this.itemsP.forEach((item,index) => {
          if (item.id === this.dataSetting.idProvinsi) {
            this.selectP.active = [this.itemsP[index]];
            this.getKabupaten();
          }
        });
      }
      // this.setSelected();
    });
  }

  private getKabupaten() {
    this.kabupatenService.getByIdProvinsi(this.dataSetting.idProvinsi).subscribe(kabupatens => {
      this.itemsK = kabupatens.map(kabupaten => {
        return {id: kabupaten.id_kabupaten, text: this.uppercasePipe.transform(kabupaten.nama)};
      });
      this.itemsK.splice(0, 0, {id: 'all', text: 'SEMUA'});
      if(this.dataSetting.idKabupaten) {
        this.itemsK.forEach((item,index) => {
          if (item.id === this.dataSetting.idKabupaten) {
            this.selectK.active = [this.itemsK[index]];
            this.getLokasi();
          }
        });
      }
      // this.setSelected();
    });
  }

  private getLokasi() {
    this.lokasiService.getByIdKabupaten(this.dataSetting.idKabupaten).subscribe(lokasis => {
      this.itemsL = lokasis.map(lokasi => {
        return {id: lokasi.id_lokasi, text: this.uppercasePipe.transform(lokasi.nama)};
      });
      this.itemsL.splice(0, 0, {id: 'all', text: 'SEMUA'});
      if(this.dataSetting.idLokasiAset) {
        this.itemsL.forEach((item,index) => {
          if (item.id === this.dataSetting.idLokasiAset) {
            this.selectL.active = [this.itemsL[index]];
          }
        });
      }
    });
  }

  setSelected() {
    if (this.dataSetting.idCompanyCode && ! this.selectCC.active.length) {
      this.itemsCC.forEach((item,index) => {
        if (item.id === this.dataSetting.idCompanyCode) {
          this.selectCC.active = [this.itemsCC[index]];
          this.getBusinessArea();
        }
      });
    }
    if (this.dataSetting.idBusinessArea && ! this.selectBA.active.length) {
      this.itemsBA.forEach((item,index) => {
        if (item.id === this.dataSetting.idBusinessArea) {
          this.selectBA.active = [this.itemsBA[index]];
        }
      });
    }
    if (this.dataSetting.idProvinsi && ! this.selectP.active.length) {
      this.itemsP.forEach((item,index) => {
        if (item.id === this.dataSetting.idProvinsi) {
          this.selectP.active = [this.itemsP[index]];
          this.getKabupaten();
        }
      });
    }
    if (this.dataSetting.idKabupaten && ! this.selectK.active.length) {
      this.itemsK.forEach((item,index) => {
        if (item.id === this.dataSetting.idKabupaten) {
          this.selectK.active = [this.itemsK[index]];
          this.getLokasi();
        }
      });
    }
    if(this.dataSetting.idLokasiAset && ! this.selectL.active.length) {
      this.itemsL.forEach((item,index) => {
        if (item.id === this.dataSetting.idLokasiAset) {
          this.selectL.active = [this.itemsL[index]];
        }
      });
    }
  }

  refreshValueCC(e: SelectItem) {
    const old = this.dataSetting.idCompanyCode;
    this.dataSetting.idCompanyCode = parseInt(e.id);
    if (old !== this.dataSetting.idCompanyCode) {
      this.getBusinessArea();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.dataSetting.idCompanyCode;
    this.dataSetting.idCompanyCode = parseInt(e.id);
    if (old !== this.dataSetting.idCompanyCode) {
      this.getBusinessArea();
    }
  }

  removedCC(e: SelectItem) {
    this.itemsBA = [];
    this.selectCC.active = [];
    this.selectBA.active = [];
    this.dataSetting.idBusinessArea = null;
    this.dataSetting.idCompanyCode = null;
  }

  refreshValueBA(e: SelectItem) {
    const old = this.dataSetting.idBusinessArea;
    this.dataSetting.idBusinessArea = parseInt(e.id);
  }

  selectedBA(e: SelectItem) {
    const old = this.dataSetting.idBusinessArea;
    this.dataSetting.idBusinessArea = parseInt(e.id);
  }

  removedBA(e: SelectItem) {
    this.selectBA.active = [];
    this.dataSetting.idBusinessArea = null;
  }

  refreshValueP(e: SelectItem) {
    const old = this.dataSetting.idProvinsi;
    this.dataSetting.idProvinsi = parseInt(e.id);
    if (old !== this.dataSetting.idProvinsi) {
      this.getKabupaten();
    }
  }

  selectedP(e: SelectItem) {
    const old = this.dataSetting.idProvinsi;
    this.dataSetting.idProvinsi = parseInt(e.id);
    if (old !== this.dataSetting.idProvinsi) {
      this.getKabupaten();
    }
  }

  removedP(e: any) {
    this.itemsL = [];
    this.itemsK = [];
    this.selectP.active = [];
    this.selectK.active = [];
    this.selectL.active = [];
    this.dataSetting.idLokasiAset = null;
    this.dataSetting.idKabupaten = null;
    this.dataSetting.idProvinsi = null;
  }

  refreshValueK(e: SelectItem) {
    const old = this.dataSetting.idKabupaten;
    this.dataSetting.idKabupaten = parseInt(e.id);
    if (old !== this.dataSetting.idKabupaten) {
      this.getLokasi();
    }
  }

  selectedK(e: SelectItem) {
    const old = this.dataSetting.idKabupaten;
    this.dataSetting.idKabupaten = parseInt(e.id);
    if (old !== this.dataSetting.idKabupaten) {
      this.getLokasi();
    }
  }

  removedK(e: any) {
    this.itemsL = [];
    this.selectK.active = [];
    this.selectL.active = [];
    this.dataSetting.idLokasiAset = null;
    this.dataSetting.idKabupaten = null;
  }

  refreshValueL(e: SelectItem) {
    this.dataSetting.idLokasiAset = parseInt(e.id);
  }

  selectedL(e: SelectItem) {
    this.dataSetting.idLokasiAset = parseInt(e.id);
  }

  removedL(e: SelectItem) {
    this.selectL.active = [];
    this.dataSetting.idLokasiAset = null;
  }
}


