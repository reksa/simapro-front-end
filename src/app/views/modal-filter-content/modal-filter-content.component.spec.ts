import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFilterContentComponent } from './modal-filter-content.component';

describe('ModalFilterContentComponent', () => {
  let component: ModalFilterContentComponent;
  let fixture: ComponentFixture<ModalFilterContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFilterContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFilterContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
