import { Component, OnInit } from '@angular/core';
import { LoginSSOService } from '../../../services/auth';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-with-sso',
  templateUrl: './login-with-sso.component.html',
  styleUrls: ['./login-with-sso.component.scss']
})
export class LoginWithSsoComponent implements OnInit {

  constructor(
    private service: LoginSSOService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe(queryParams => {
      const success = queryParams.get('success');
      const token = queryParams.get('token');
      const message = queryParams.get('message');
      if(success) {
        this.service.saveSession(token);
      } else {
        alert('Login Gagal : ' + message);
      }
      window.close();
    })
  }

}
