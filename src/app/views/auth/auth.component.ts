import { environment } from "environments/environment";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { Errors } from "app/models/errors.model";
import { PenggunaService } from "app/services/data-services";
import { LoginSSOService } from "../../services/auth";
import { Subscription } from "rxjs";

const u =
  environment.sso_url + "?base64_callback=" + btoa(environment.callback_sso);

@Component({
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit {
  authType: string = "";
  title: string = "";
  errors: Errors = new Errors();
  invalidMessage = "";
  isSubmitting = false;
  authForm: FormGroup;
  invalidLogin: boolean = false;
  home = environment.server_url;
  private watcher: Subscription;

  showPassword = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: PenggunaService,
    private fb: FormBuilder,
    private loginSSOService: LoginSSOService
  ) {
    // use FormBuilder to create a form group
    this.authForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.watcher = this.loginSSOService.watchSession();
    this.route.url.subscribe(data => {
      // Get the last piece of the URL (it's either 'login' or 'register')
      this.authType = data[data.length - 1].path;

      if (this.authType === "logout") {
        this.authService.purgeAuth().subscribe(res => {
          if (res.success) {
            this.router.navigate(["/auth/login"]);
          }
        });
      }
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.errors = new Errors();

    const credentials = this.authForm.value;
    this.authService.attemptAuth(credentials).subscribe(
      res => {
        if (res.success) {
          const ref = decodeURI(
            this.route.snapshot.queryParamMap.get("ref") || "/"
          );
          if (this.watcher) {
            this.watcher.unsubscribe();
          }
          this.router.navigateByUrl(ref);
        } else {
          this.invalidMessage = res.message;
          this.invalidLogin = true;
          // @ts-ignore
          const timeout = res.timeout * 1000;
          // @ts-ignore
          if (res.reach_max_attempts) {
            setTimeout(() => {
              this.invalidLogin = false;
              this.invalidMessage = "";
              this.isSubmitting = false;
            }, timeout);
          } else {
            this.isSubmitting = false;
          }
        }
      },
      err => {
        const r = err.json();
        this.errors = err.json().message;
        if (r.message && typeof r.message == "string") {
          alert(r.message);
        }
        this.isSubmitting = false;
        this.invalidLogin = true;
      }
    );
  }

  loginWithSSO() {
    window.open(u);
  }

  goHome() {
    if (confirm("Kembali ke beranda ?")) {
      window.location.href = this.home;
    }
    return false;
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
    return false;
  }
}
