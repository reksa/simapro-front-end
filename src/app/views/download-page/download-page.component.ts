import { JsonService } from "./../../services/data-services/json.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-download-page",
  templateUrl: "./download-page.component.html",
  styleUrls: ["./download-page.component.scss"]
})
export class DownloadPageComponent implements OnInit {
  userGuides = [];
  constructor(private jsonService: JsonService) {}

  ngOnInit() {
    this.jsonService
      .getAll("./assets/user-guide.json")
      .subscribe(userGuides => (this.userGuides = userGuides));
  }
}
