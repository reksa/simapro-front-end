import { Observable } from 'rxjs/Rx';
import { TextToSlugPipe } from './../../../services/text-to-slug.service';
import { PenggunaService, RealisasiPekerjaanService } from 'app/services/data-services';
import { Realisasi, Pengguna } from 'app/models';
import { DataSetting, AppTableConfig, PaginationData } from 'app/components/pagination/page/page.component';
import { ModalFilterContentComponent } from './../../modal-filter-content/modal-filter-content.component';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-realisasi',
  templateUrl: './realisasi.component.html',
  styleUrls: ['./realisasi.component.scss']
})
export class RealisasiComponent implements OnInit {
  pageData: Object[] = [];
  dataSetting = new DataSetting();
  isLoading = true;
  root = 'realisasi/';
  tab = 'data';
  public tableConfig = new AppTableConfig();
  public pengguna = new Pengguna();
  public modalRef: BsModalRef;

  constructor(
    private realisasiPekerjaanService: RealisasiPekerjaanService,
    private penggunaService: PenggunaService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: BsModalService,
    private slugPipe: TextToSlugPipe
  ) {}

  ngOnInit() {
    this.penggunaService.getRoleForModule('realisasi').subscribe(pengguna => this.pengguna = pengguna);
    this.dataSetting.page = 1;
    this.dataSetting.dataPerPage = 5;


    this.route.fragment.subscribe(tab => {
      this.dataSetting = new DataSetting();
      this.tab = tab || this.tab;
      this._data();
    });
  }

  private _data() {
    let data: Subscription;
    if(data) {
      data.unsubscribe();
    }
    this.pageData = [];
    data = this.route.queryParamMap.switchMap((params) => {
      this.pageData = [];
      this.isLoading = true;
      const parameters = new URLSearchParams();
      parameters.append('query', params.get('query'));
      parameters.append('page', params.get('page'));
      parameters.append('dataPerPage', params.get('dataPerPage'));
      parameters.append('orderBy', params.get('orderBy'));
      parameters.append('orderDirection', params.get('orderDirection'));
      if (params.get('idBusinessArea')) {
        parameters.append('id_business_area', params.get('idBusinessArea'));
        this.dataSetting.idBusinessArea = parseInt(params.get('idBusinessArea'));
      }
      if (params.get('idCompanyCode')) {
        parameters.append('id_company_code', params.get('idCompanyCode'));
        this.dataSetting.idCompanyCode = parseInt(params.get('idCompanyCode'));
      }
      if (params.get('nama')) {
        parameters.append('nama', params.get('nama'));
        this.dataSetting.nama = params.get('nama');
      }
      if (params.get('nomor_anggaran')) {
        parameters.append('nomor_anggaran', params.get('nomor_anggaran'));
        this.dataSetting.nomor_anggaran = params.get('nomor_anggaran');
      }
      if (params.get('tahun_anggaran')) {
        parameters.append('tahun_anggaran', params.get('tahun_anggaran'));
        this.dataSetting.tahun_anggaran = parseInt(params.get('tahun_anggaran'));
      }

      let data: Observable<PaginationData>;
      switch(this.tab) {
        case 'data-perubahan': data = this.realisasiPekerjaanService.getPaginateNonValid(parameters);
          break;
        default: data = this.realisasiPekerjaanService.getPaginateValid(parameters);
      }
                                  
      return data;
    }).subscribe((res) => {
      this.dataSetting.dataPerPage = <number> res.per_page;
      this.pageData = res.data.map(d => {
        return new Realisasi(d);
      });
      this.tableConfig.current = res.current_page;
      this.tableConfig.limit = res.per_page;
      this.tableConfig.from = res.from;
      this.tableConfig.to = res.to;
      this.tableConfig.total = res.total;
      this.tableConfig.totalPages = res.last_page;
      this.tableConfig.generatePages();

      if (this.modalRef) {
        this.modalRef.hide();
      }

      this.isLoading = false;
    });
  }

  changeLimit(limit: number) {
    this.dataSetting.page = 1;
    this._navigate();
  }

  setPage(pageNumber: number) {
    this.dataSetting.page = pageNumber;
    this._navigate();
  }

  search() {
    this.dataSetting.page = 1;
    this._navigate();
  }

  private _navigate() {
    this.router.navigate([this.root, {}], {
      fragment: this.tab,
      queryParams: this.dataSetting      
    });
  }

  sortBy(column: string) {
    if (this.dataSetting.orderBy !== column) {
      this.dataSetting.orderBy = column;
      this.dataSetting.orderDirection = 'asc';
    } else if (this.dataSetting.orderDirection === 'asc') {
      this.dataSetting.orderDirection = 'desc';
    } else {
      this.dataSetting.orderDirection = 'asc';
    }
    this._navigate();
  }

  openModal() {
    const initialState = {
      isRealisasi: true,
      dataSetting: Object.assign({}, this.dataSetting)
    }
    this.modalRef = this.modalService.show(ModalFilterContentComponent, { initialState, class: 'modal-lg', ignoreBackdropClick: true });
    const content = <ModalFilterContentComponent> this.modalRef.content;
    const subscribeModal = this.modalService.onHidden.subscribe((reason: string) => {
      if (content.filter) {
        this.dataSetting = content.dataSetting;
        this.search();
      }
      subscribeModal.unsubscribe();
    })
  }

  show(realisasiPekerjaan: Realisasi) {
    const p = this.slugPipe.transform(realisasiPekerjaan.keterangan || 'detail-realisasi');
    this.router.navigate([`${this.root}show/${realisasiPekerjaan.id_realisasi}/${p}`]);
    return false;
  }
}
