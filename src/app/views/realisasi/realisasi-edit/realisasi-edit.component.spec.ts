import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealisasiEditComponent } from './realisasi-edit.component';

describe('RealisasiEditComponent', () => {
  let component: RealisasiEditComponent;
  let fixture: ComponentFixture<RealisasiEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealisasiEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealisasiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
