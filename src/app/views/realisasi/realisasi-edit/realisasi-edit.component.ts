import { AsetProperti } from './../../../models/aset-properti.model';
import { AnggaranService, RealisasiPekerjaanService, CompanyCodeService, BusinessAreaService } from 'app/services/data-services';
import { Anggaran, AsetTanah, Realisasi, AnggaranPekerjaan, RealisasiPekerjaan } from 'app/models';
import { Component, OnInit, ViewChild } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { SelectItem, SelectComponent } from 'ng2-select';
import { UpperCasePipe, Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-realisasi-edit',
  templateUrl: './realisasi-edit.component.html',
  styleUrls: ['./realisasi-edit.component.scss']
})
export class RealisasiEditComponent implements OnInit {
  title: string = '';
  subtitle: string = '';
  action: string;
  submitted: boolean = false;
  realisasi: Realisasi = new Realisasi();
  realisasiPekerjaan: RealisasiPekerjaan[] = [];

  tahunAnggaran: number = new Date().getFullYear();
  idBusinessArea: number = 0;
  idCompanyCode: number;
  anggaran: Anggaran;
  formRealisasi: FormGroup;
  byAnggaran: boolean = false;
  aset: AsetTanah | AsetProperti;
  pekerjaan: AnggaranPekerjaan;

  itemsCC = [];
  itemsBA = [];
  itemsA = [];

  @ViewChild('selectCC') selectCC: SelectComponent;
  @ViewChild('selectBA') selectBA: SelectComponent;
  @ViewChild('selectA') selectA: SelectComponent;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private realisasiPekerjaanService: RealisasiPekerjaanService,
    private anggaranService: AnggaranService,
    private companyCodeService: CompanyCodeService,
    private businessAreaService: BusinessAreaService,
    private _fb: FormBuilder,
    private _location: Location,
    private uppercasePipe: UpperCasePipe
  ) { }

  ngOnInit() {
    this._initForm();
    this.route.url.subscribe(url => {
      this.action = url[0].path;
      switch (this.action) {
        case 'revisi': this._revisi();
          break;
        case 'add':
        default: this._add();
      }
    });
  }
  
  private _add() {
    this.title = 'Tambah Realisasi Anggaran';
    this.subtitle = 'Tambah Realisasi Anggaran aset';
    this.realisasi = new Realisasi();
    this.getCompanyCode();
  }

  private _revisi() {
    this.title = 'Revisi Realisasi Anggaran';
    this.subtitle = 'Revisi Realisasi Anggaran';
    this.route.paramMap.subscribe(params => {
      const id = parseInt(params.get('idRealisasi'));
      this.realisasiPekerjaanService.getById(id).subscribe(realisasi => {
        this.realisasi = new Realisasi(realisasi);
        this.idCompanyCode = this.realisasi.id_company_code;
        this.idBusinessArea = this.realisasi.id_business_area;
        this.anggaran = this.realisasi.anggaran;
        this.realisasi.realisasi_pekerjaan.forEach(() => {
          this.addPekerjaan(true);
        });

        this.getCompanyCode();
        this.getAnggaran();
      })
    });
  }

  addPekerjaan(withoutModel?: boolean) {
    if(!withoutModel) {
      this.realisasi.realisasi_pekerjaan.push(new RealisasiPekerjaan());
    }
    (this.formRealisasi.get('detail') as FormArray).push(this._initFormDetail());
    return false;
  }

  private _initFormDetail() {
    return this._fb.group({
      aset: ['', [Validators.required]],
      nama_realisasi_pekerjaan: ['', [Validators.required]],
      id_anggaran_pekerjaan: ['', []],
      nilai_realisasi_pekerjaan: ['', [Validators.required, Validators.min(1000000)]]
    });
  }

  removePekerjaan(index: number) {
    this.realisasi.realisasi_pekerjaan.splice(index, 1);
    (this.formRealisasi.get('detail') as FormArray).removeAt(index);
  }

  private _initForm() {
    this.formRealisasi = this._fb.group({
      tahun_anggaran: ['', [Validators.required]],
      idCompanyCode: ['', [Validators.required]],
      anggaran: ['', [Validators.required]],
      tanggal_realisasi: ['', [Validators.required]],
      keterangan: ['', [Validators.maxLength(255)]],
      detail: this._fb.array([])
    });
  }
  
  changeAset(aset) {
    if(aset) {
      this.aset = aset;
      this.formRealisasi.get('aset').setValue(aset);
    }
  }

  log(obj: any) {
    // console.log(obj);
  }

  save() {
    if(this.anggaran.detailX.nilai_anggaran < this.totalRealisasi) {
      return false;
    }
    this.submitted = true;
    this.realisasi.path = '/realisasi/add';
    this.realisasi.id_anggaran = this.anggaran.id_anggaran;
    this.realisasi.id_anggaran_detail = this.anggaran.detailX.id_anggaran_detail;
    this.realisasi.id_company_code = this.anggaran.id_company_code;
    this.realisasi.id_business_area = this.anggaran.id_business_area;
    this.realisasi.total_realisasi = this.totalRealisasi;
    if(this.action === 'revisi') {
      this._revisiRealisasi();
    } else {
      this._addRealisasi();
    }
  }

  cancel() {
    if (confirm('Batal menambah Realisasi Anggaran?')) {
      this._location.back();
    }
    return false;
  }

  get totalRealisasi() {
    let total = 0;
    if(this.realisasi && this.realisasi.realisasi_pekerjaan) {
      this.realisasi.realisasi_pekerjaan.forEach(p => {
        const q = p.nilai_realisasi_pekerjaan ? parseFloat(p.nilai_realisasi_pekerjaan.toString()) : 0;
        total += q;
      })
    }
    return total;
  }

  private _addRealisasi() {
    this.realisasiPekerjaanService.save(this.realisasi)
      .subscribe(res => {
        if(res.message) {
          alert(res.message);
        }

        if(res.success) {
          this._location.back();
        } else {
          this.submitted = false;
        }
      });
  }

  private _revisiRealisasi() {
    this.realisasiPekerjaanService.revisi(this.realisasi)
      .subscribe(res => {
        if(res.message) {
          alert(res.message);
        }

        if(res.success) {
          this._location.back();
        } else {
          this.submitted = false;
        }
      });
  }

  private getCompanyCode() {
    this.itemsCC = [];
    this.companyCodeService.getAll().subscribe(companyCodes => {
      this.itemsCC = companyCodes.map(companyCode => {
        return {id: companyCode.id_company_code, text: `${companyCode.kode_company_code} - ${this.uppercasePipe.transform(companyCode.nama_company_code)}`};
      });
      if(this.idCompanyCode) {
        this.itemsCC.forEach((item,index) => {
          if (item.id === this.idCompanyCode) {
            this.selectCC.active = [this.itemsCC[index]];
          }
        });
      }
    });
  }

  // private getBusinessArea() {
  //   this.itemsBA = [];
  //   this.businessAreaService.getByIdCompanyCode(this.idCompanyCode).subscribe(businessAreas => {
  //     this.itemsBA = businessAreas.map(businessArea => {
  //       return {id: businessArea.id_business_area, text: `${businessArea.kode_business_area} - ${this.uppercasePipe.transform(businessArea.nama_business_area)}`};
  //     });
  //     if(this.idBusinessArea) {
  //       this.itemsBA.forEach((item,index) => {
  //         if (item.id == this.idBusinessArea) {
  //           this.selectBA.active = [this.itemsBA[index]];
  //         }
  //       });
  //     }
  //   });
  // }

  private getAnggaran() {
    this.itemsA = [];
    const params = new URLSearchParams();
    params.set('tahun_anggaran', this.tahunAnggaran.toString());
    params.set('id_company_code', this.idCompanyCode.toString());
    // params.set('id_business_area', this.idBusinessArea.toString());
    this.anggaranService.getAll(params).subscribe(anggarans => {
      this.itemsA = anggarans.map(anggaran => {
        anggaran = new Anggaran(anggaran);
        return {id: anggaran, text: this.uppercasePipe.transform(anggaran.nama_anggaran + ' - ' + anggaran.nomor_anggaran)};
        // return ;
      });
      if(this.anggaran && this.anggaran.id_anggaran) {
        this.itemsA.forEach((item,index) => {
          if (item.id.id_anggaran == this.anggaran.id_anggaran) {
            this.selectA.active = [this.itemsA[index]];
          }
        });
      }
    });
  }

  refreshValueCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getAnggaran();
    }
  }

  selectedCC(e: SelectItem) {
    const old = this.idCompanyCode;
    this.idCompanyCode = parseInt(e.id);
    if (old !== parseInt(e.id)) {
      this.getAnggaran();
    }
  }

  // refreshValueBA(e: SelectItem) {
  //   const old = this.idBusinessArea;
  //   this.idBusinessArea = parseInt(e.id);
  //   if (old !== parseInt(e.id)) {
  //     this.getAnggaran();
  //   }
  // }

  // selectedBA(e: SelectItem) {
  //   const old = this.idBusinessArea;
  //   this.idBusinessArea = parseInt(e.id);
  //   if (old !== parseInt(e.id)) {
  //     this.getAnggaran();
  //   }
  // }

  // refreshValueA(e) {
  //   this.anggaran = e.id;
  //   this.formRealisasi.get('anggaran').setValue(this.anggaran);
  // }

  selectedA(e) {
    this.anggaran = e.id;
    this.formRealisasi.get('anggaran').setValue(this.anggaran);
    this._initRealisasiPekerjaan();
  }

  private _initRealisasiPekerjaan() {
    const countRow = this.realisasi.realisasi_pekerjaan ? this.realisasi.realisasi_pekerjaan.length : 0;
    for(let i = 0; i < countRow; i++) {
      (this.formRealisasi.get('detail') as FormArray).removeAt(i);
    }
    this.realisasi.realisasi_pekerjaan = [];
    this.anggaran.detailX.alokasi.forEach(pekerjaan => {
      const r = new RealisasiPekerjaan();
      r.id_anggaran_pekerjaan = pekerjaan.id_anggaran_pekerjaan;
      r.nama_realisasi_pekerjaan = pekerjaan.nama_pekerjaan;
      r.nilai_realisasi_pekerjaan = pekerjaan.nilai_pekerjaan;
      r.realisasi_pekerjaanable_id = pekerjaan.anggaranable_id;
      r.realisasi_pekerjaanable_type = pekerjaan.anggaranable_type;
      r.realisasi_pekerjaanable = pekerjaan.anggaranable;
      this.realisasi.realisasi_pekerjaan.push(r);
      this.addPekerjaan(true);
    });
  }

}
