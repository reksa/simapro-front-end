import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RealisasiPekerjaan, AsetTanah, AsetProperti } from '../../../models';

@Component({
  selector: '[app-row-realisasi-pekerjaan]',
  templateUrl: './row-realisasi-pekerjaan.component.html',
  styleUrls: ['./row-realisasi-pekerjaan.component.scss']
})
export class RowRealisasiPekerjaanComponent implements OnInit {
  @Input('model') model: RealisasiPekerjaan;
  @Input('group') fGroup: FormGroup;
  @Input('editable') editable: boolean = true;
  @Input('index') index: number;
  @Input('idCompanyCode') idCompanyCode: number;
  @Input('idBusinessArea') idBusinessArea: number;
  @Input('removable') removable: boolean;
  @Output('onRemove') onRemove = new EventEmitter();
  nilai: number;

  constructor() { }

  ngOnInit() {
    if(this.model.realisasi_pekerjaanable && !this.model.realisasi_pekerjaanable.detail) {
      const aset = this.model.realisasi_pekerjaanable;
      this.model.realisasi_pekerjaanable = this.model.realisasi_pekerjaanable_type === 'App\\Tanah' ? new AsetTanah(aset) : new AsetProperti(aset);
    }
    if(!this.editable) {
      this.fGroup.get('aset').disable();
    }
    this.nilai = this.model.nilai_realisasi_pekerjaan;
  }

  remove(index) {
    this.onRemove.emit(index);
  }

  changeAset(e) {
    if(e && (e.id_tanah || e.id_properti)) {
      this.fGroup.get('aset').setValue(e);
      this.model.realisasi_pekerjaanable_id = e.id_tanah || e.id_properti;
      this.model.realisasi_pekerjaanable_type = e.id_tanah ? 'App\\Tanah' : 'App\\Properti';
    }
  }

}
