import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowRealisasiPekerjaanComponent } from './row-realisasi-pekerjaan.component';

describe('RowRealisasiPekerjaanComponent', () => {
  let component: RowRealisasiPekerjaanComponent;
  let fixture: ComponentFixture<RowRealisasiPekerjaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowRealisasiPekerjaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowRealisasiPekerjaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
