import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RealisasiComponent } from './realisasi/realisasi.component';
import { RealisasiEditComponent } from './realisasi-edit/realisasi-edit.component';
import { PermissionGuard, AuthGuard } from 'app/services/auth';
import { RealisasiDetailComponent } from './realisasi-detail/realisasi-detail.component';

const routes: Routes = [
  { path: '', component: RealisasiComponent },
  { path: 'add', component: RealisasiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'revisi/:idRealisasi/:nama', component: RealisasiEditComponent, canActivate: [AuthGuard, PermissionGuard] },
  { path: 'show/:idRealisasi/:nama', component: RealisasiDetailComponent, canActivate: [AuthGuard, PermissionGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RealisasiRoutingModule { }
