import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealisasiDetailComponent } from './realisasi-detail.component';

describe('RealisasiDetailComponent', () => {
  let component: RealisasiDetailComponent;
  let fixture: ComponentFixture<RealisasiDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealisasiDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealisasiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
