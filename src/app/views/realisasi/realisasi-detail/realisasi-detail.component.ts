import { DatatablesContent } from './../../../components/app-datatable/datatables-content.model';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { TemplateRef } from '@angular/core/src/linker/template_ref';
import { RealisasiPekerjaan } from 'app/models';
import { TextToSlugPipe } from 'app/services/text-to-slug.service';
import { PenggunaService, RealisasiPekerjaanService } from 'app/services/data-services';
import { Component, OnInit } from '@angular/core';
import { Realisasi, Pengguna } from '../../../models';
import { BsModalRef } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, UpperCasePipe, DecimalPipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-realisasi-detail',
  templateUrl: './realisasi-detail.component.html',
  styleUrls: ['./realisasi-detail.component.scss']
})
export class RealisasiDetailComponent implements OnInit {
  
  id: number;
  documentReady = false;
  realisasi: Realisasi;
  pengguna = new Pengguna();
  
  memo: string = '';
  modalRef: BsModalRef;
  subs: Subscription;

  historisDisposisi = []; 
  realisasiPekerjaan = new DatatablesContent();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private penggunaService: PenggunaService,
    private realisasiService: RealisasiPekerjaanService,
    private modalService: BsModalService,
    private _location: Location,
    private slugPipe: TextToSlugPipe,
    private numberPipe: DecimalPipe,
    private uppercasePipe: UpperCasePipe,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.penggunaService.getRoleForModule('realisasi')
      .subscribe(pengguna => {
        this.pengguna = pengguna;
        this.route.paramMap.subscribe(params => {
          this.id = parseInt(params.get('idRealisasi'), 10);
          this._init();
        });
      });
  }

  private _init() {
    this.spinner.show();
    if(this.subs) {
      this.subs.unsubscribe;
    }
    this.subs = this.realisasiService.getById(this.id)
      .subscribe(realisasi => {
        this.realisasi = new Realisasi(realisasi);
        this.documentReady = true
        this._mapDetailToRealisasiPekerjaan(realisasi.realisasi_pekerjaan || []);
        this.spinner.hide();
      });
  }

  showModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    return false;
  }

  showDisposisi(template: TemplateRef<any>, data: any[]) {
    this.historisDisposisi = data;
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
    return false;
  }

  validasi(status: boolean) {
    this.realisasiService.validasi(this.id, this.memo, status)
      .subscribe(res => {
        this.modalRef.hide();
        if (res.message) {
          alert(res.message);
        }

        if (res.success) {
          this.realisasi.action_button.button = false;
          this._init();
        }
      });
  }

  revisi() {
    const nama = this.slugPipe.transform(this.realisasi.keterangan || 'realisasi');
    this.router.navigate([`realisasi/revisi/${this.id}/${nama}`]);
    return false;
  }

  private _mapDetailToRealisasiPekerjaan(pekerjaan: RealisasiPekerjaan[]) {
    this.realisasiPekerjaan.columns = ['NO', 'NAMA ASET', 'NAMA PEKERJAAN', 'NILAI PEKERJAAN (Rp)'];
    this.realisasiPekerjaan.actions = [];
    this.realisasiPekerjaan.data.next([]);
    const data = [];
    let no = 0;

    pekerjaan.map((r: RealisasiPekerjaan) => {
      no++;
      // @ts-ignore
      const namaAset = this._toUppercase(r.nama_aset || '-');
      const namaPekerjaan = r.nama_realisasi_pekerjaan ? this._toUppercase(r.nama_realisasi_pekerjaan) : '-';
      const nilaiPekerjaan = r.nilai_realisasi_pekerjaan ?
        this.numberPipe.transform(r.nilai_realisasi_pekerjaan, '1.0-2') : '-';

      data.push({
        row: [ no, namaAset, namaPekerjaan, nilaiPekerjaan ],
        id: r.id_anggaran_pekerjaan
      });
    });

    this.realisasiPekerjaan.data.next(data);
  }

  private _toUppercase(str: string) {
    return this.uppercasePipe.transform(str || '');
  }

  back() {
    this._location.back();
    return false;
  }

}
