import { AppDatatableModule } from './../../components/app-datatable/app-datatable.module';
import { TabsModule } from 'ngx-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule, UpperCasePipe, CurrencyPipe, DecimalPipe } from '@angular/common';

import { RealisasiRoutingModule } from './realisasi-routing.module';
import { RealisasiComponent } from './realisasi/realisasi.component';
import { RealisasiEditComponent } from './realisasi-edit/realisasi-edit.component';
import { RealisasiDetailComponent } from './realisasi-detail/realisasi-detail.component';
import { PaginationModule } from '../../components/pagination/pagination.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { RealisasiPekerjaanService, CompanyCodeService, BusinessAreaService, AnggaranService } from '../../services/data-services';
import { TextToSlugPipe } from '../../services/text-to-slug.service';
import { SelectAsetModule } from '../select-aset/select-aset.module';
import { TimelineModule } from '../timeline/timeline.module';
import { RowRealisasiPekerjaanComponent } from './row-realisasi-pekerjaan/row-realisasi-pekerjaan.component';
import { InputNumberModule } from '../input-number/input-number.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RealisasiRoutingModule,
    PaginationModule,
    TabsModule,
    SelectModule,
    SelectAsetModule,
    TimelineModule,
    AppDatatableModule,
    InputNumberModule
  ],
  declarations: [RealisasiComponent, RealisasiEditComponent, RealisasiDetailComponent, RowRealisasiPekerjaanComponent],
  providers: [
    RealisasiPekerjaanService, AnggaranService, CompanyCodeService, BusinessAreaService,
    TextToSlugPipe, UpperCasePipe, CurrencyPipe, DecimalPipe
  ]
})
export class RealisasiModule { }
