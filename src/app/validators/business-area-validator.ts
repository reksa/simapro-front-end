import { BusinessAreaService } from 'app/services/data-services';
import { Observable } from 'rxjs/Rx';
import { ValidationErrors, AbstractControl, AsyncValidator, FormControl } from "@angular/forms";
import { Injectable } from '@angular/core';

@Injectable()
export class BusinessAreaValidator {
	id: number = null;
	debouncerKode: any;
	debouncerNama: any;
 
  constructor(private businessAreaService: BusinessAreaService){
 
  }
 
  uniqueKodeBusinessArea(control: FormControl): any {
 
    clearTimeout(this.debouncerKode);
 
    return new Promise(resolve => {
 
      this.debouncerKode = setTimeout(() => {
 
        this.businessAreaService.isUnique({
          column: 'kode_business_area',
          value : control.value,
          id_business_area: this.id
        }).subscribe((res) => {
          if(res.unique){
            resolve(null);
          }
        }, (err) => {
          resolve({'kodeBusinessAreaInUse': true});
        });
 
      }, 1000);     
 
    });
  }
 
  uniqueNamaBusinessArea(control: FormControl): any {
 
    clearTimeout(this.debouncerNama);
 
    return new Promise(resolve => {
 
      this.debouncerNama = setTimeout(() => {
 
        this.businessAreaService.isUnique({
          column: 'nama_business_area',
          value: control.value,
          id_business_area: this.id
        }).subscribe((res) => {
          if(res.unique){
            resolve(null);
          }
        }, (err) => {
          resolve({'namaBusinessAreaInUse': true});
        });
 
      }, 1000);     
 
    });
  }
 

}
