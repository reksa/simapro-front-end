import { CompanyCodeService } from 'app/services/data-services';
import { Observable } from 'rxjs/Rx';
import { ValidationErrors, AbstractControl, AsyncValidator, FormControl } from "@angular/forms";
import { Injectable } from '@angular/core';

@Injectable()
export class CompanyCodeValidator {
  id: number = null;
	
	debouncerKode: any;
	debouncerNama: any;
	debouncerNamaCC: any;
 
  constructor(private companyCodeService: CompanyCodeService){
 
  }
 
  uniqueKodeCompanyCode(control: FormControl): any {
 
    clearTimeout(this.debouncerKode);
 
    return new Promise(resolve => {
 
      this.debouncerKode = setTimeout(() => {
 
        this.companyCodeService.isUnique({
          column: 'kode_company_code',
          value : control.value,
          id_company_code: this.id
        }).subscribe((res) => {
          if(res.unique){
            resolve(null);
          }
        }, (err) => {
          resolve({'kodeCompanyCodeInUse': true});
        });
 
      }, 1000);     
 
    });
  }
 
  uniqueNama(control: FormControl): any {
 
    clearTimeout(this.debouncerNama);
 
    return new Promise(resolve => {
 
      this.debouncerNama = setTimeout(() => {
 
        this.companyCodeService.isUnique({
          column: 'nama',
          value: control.value,
          id_company_code: this.id
        }).subscribe((res) => {
          if(res.unique){
            resolve(null);
          }
        }, (err) => {
          resolve({'namaInUse': true});
        });
 
      }, 1000);     
 
    });
  }
 
  uniqueNamaCompanyCode(control: FormControl): any {
 
    clearTimeout(this.debouncerNamaCC);
 
    return new Promise(resolve => {
 
      this.debouncerNamaCC = setTimeout(() => {
 
        this.companyCodeService.isUnique({
          column: 'nama_company_code',
          value: control.value,
          id_company_code: this.id
        }).subscribe((res) => {
          if(res.unique){
            resolve(null);
          }
        }, (err) => {
          resolve({'namaCompanyCodeInUse': true});
        });
 
      }, 1000);     
 
    });
  }
 

}
