export class Koordinat {
    type: string;
    coordinates: number[] = [-6.2357278, 106.818673];
  
    constructor(lat?, lng?) {
      if (lat && lng) {
        this.coordinates = [lat, lng];
      }
    }
  }