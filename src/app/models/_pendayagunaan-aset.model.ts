export class _PendayagunaanAset {
  path: string;
  no_urut: number;
  
  id_jenis_dayaguna: number;
  pendayagunaanable_type: string;
  kode_aset: string;
  nama_aset: string;
  kode_business_area: string;
  kontrak: string;
  keterangan: string;
  nilai_pendayagunaan: number;
  start_date_pendayagunaan: Date;
  end_date_pendayagunaan: Date;
  is_use: boolean;
  is_active: boolean;

  constructor(_t: any[]) {
    this.no_urut= _t['0'] || null;
    this.pendayagunaanable_type= _t['1'] == 'TANAH' ? 'App\\Tanah' : 'App\\Properti';
    this.kode_aset= _t['2'] || null; //2
    this.nama_aset= _t['3'] || null; //2
    this.kode_business_area= _t['4'] || null; //4
    this.kontrak= _t['5'] || null;
    this.keterangan= _t['6'] || null;
    this.nilai_pendayagunaan= _t['7'] || 0;
    this.id_jenis_dayaguna= _t['8'] || null;
    this.start_date_pendayagunaan= _t['9'] ? new Date(_t['9']) : null;
    this.end_date_pendayagunaan= _t['10'] ? new Date(_t['10']) : null;
    this.is_active = true;
    this.is_use = true;
  }
}
