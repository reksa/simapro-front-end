import { OrganisasiLevel } from './organisasi-level.model';
import { Permission } from './permission.model';
import { Pengguna } from './pengguna.model';

export class Role {
  id_role: number;
  nama: string;
  description?: string;
  is_active = true;
  members?: Pengguna[] = [];
  permission?: Permission[] = [];
  permission_list: string[];
  created_on: Date;
  updated_on: Date;
  id_organisasi_level: number;

  organisasi_level: OrganisasiLevel;
}
