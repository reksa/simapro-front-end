import { LatLng } from 'leaflet';
export class _Tanah{
    no_urut: number;
    kode_lokasi: number;
    kode_business_area: string;
    id_sumber_perolehan: number;
    nama_tanah: string;
    alamat_tanah: string;
    luas_tanah: number;
    nilai_perolehan: number;
    tahun_perolehan: string;
    nilai_tanah: number;
    tahun_penilaian: string;
    tanggal_penilaian: Date;
    no_pbb: string;
    no_bphtb: string;
    id_ijin: number;
    no_sertifikat: string;
    start_date_sertifikat?: Date;
    end_date_sertifikat?: Date;
    polygon: LatLng[][];
    is_active: boolean;
    path = '/aset-tanah/add';
    deskripsi: string;
    nilai_potensi: number;
    start_date_potensi: Date;
    end_date_potensi: Date;
    no_sap: string;

    constructor(_t: any[]) {
        this.no_urut = _t['0'] || null;
        this.kode_lokasi = _t['1'] || null;
        this.kode_business_area = _t['2'] || null;
        this.nama_tanah = _t['3'] || null;
        this.luas_tanah = _t['4'] || null;
        this.nilai_perolehan = _t['5'] || null;
        this.tahun_perolehan = _t['6'] || null;
        this.nilai_tanah = _t['7'] || null;
        this.tanggal_penilaian = _t['8'] || null;
        this.tahun_penilaian = _t['8'] || null;
        this.no_sertifikat = _t['9'] || null;
        this.start_date_sertifikat = _t['10'] || null;
        this.end_date_sertifikat = _t['11'] || null;
        this.no_pbb = _t['12'] || null;
        this.no_bphtb = _t['13'];
        this.id_ijin = _t['14'] || null;
        this.id_sumber_perolehan = _t['16'] || null;
        this.alamat_tanah = _t['17'] || null;
        this.polygon = _t['18'] ? [this.mapToPolygon(_t['18'])] : null;
        this.deskripsi = _t['19'] || null;
        this.nilai_potensi = _t['20'] || null;
        this.start_date_potensi = _t['21'] || null;
        this.end_date_potensi = _t['22'] || null;
        this.no_sap = _t['23'] || null;
    }

    private mapToPolygon(str: string): LatLng[] {
        const arr = str.split(',');
        return arr.map(latlngString => {
            const latlngArray = latlngString.trim().split(' ');
            const lat = latlngArray[0] ? parseFloat(latlngArray[0]): 0;
            const lng = latlngArray[1] ? parseFloat(latlngArray[1]): 0; 
            return new LatLng(lat, lng);
        });
    }
}