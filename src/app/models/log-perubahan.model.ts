import { Pemeliharaan } from './pemeliharaan.model';
// import { Perizinan } from './perizinan.model';
// import { Operasi } from './operasi.model';
import { Pengguna } from './pengguna.model';

export class LogPerubahan {
  id: number;
  // perubahan: Partial<Kondisi>;
  happen_on: Date;
  disetujui_oleh: Pengguna;
  // sebab: Pekerjaan | Perizinan | Operasi;
}
