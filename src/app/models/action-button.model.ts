export class ActionButton {
  type: string;
  button: boolean;
  is_last_approvement?: boolean;
}