import { Regional } from './regional.model';
import { Pengguna } from './pengguna.model';
export class Headquarter {
  id_head_quarters: number;
  nama_head_quarters: string;
  regional: Regional[];
  is_active: boolean;
  created_on: Date;
  changed_on: Date;
  deleted_on: Date;
}
