import { BusinessArea } from './business-area.model';
import { Pengguna } from './pengguna.model';
import { AsetTanah } from './aset-tanah.model';
import { AsetProperti } from './aset-properti.model';
import { ActionButton } from './action-button.model';
import { WorkflowProgress } from './workflow.model';

export class Anggaran {
  path: string;
  nilai_anggaran: number;

  id_anggaran: number;
  id_company_code: number;
  id_business_area: number;
  nama_anggaran: string;
  tahun_anggaran: number;
  nomor_anggaran: string;
  deskripsi: string;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  cranged_by?: Pengguna;
  nomor_pengajuan_anggaran: string;

  // relation
  pekerjaan?: AnggaranPekerjaan[];
  alokasi?: AnggaranPekerjaan[];
  detail_aktif: AnggaranDetail[];
  detail: AnggaranDetail[];
  detail_non_valid: AnggaranDetail[];
  business_area: BusinessArea;
  
  get detailX(): AnggaranDetail {
    return this.detail_aktif && this.detail_aktif.length ? this.detail_aktif[0] : null; 
  }

  get detail_perubahan(): AnggaranDetail {
    return this.detail_non_valid && this.detail_non_valid.length ? this.detail_non_valid[0] : null; 
  }

  set detailX(detail: AnggaranDetail) {
    this.detail_aktif = [detail];
  }

  set detail_perubahan(detail: AnggaranDetail) {
    this.detail_non_valid = [detail];
  }

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        this[prop] = obj[prop];
      }
    }
  }
}

export class AnggaranPekerjaan {
  path: string;

  id_anggaran: number;
  id_anggaran_pekerjaan: number;
  id_anggaran_detail: number;
  anggaranable_type: string;
  anggaranable_id: number;
  nama_pekerjaan: string;
  nilai_pekerjaan: number;
  keterangan: string;
  nomor_pengajuan_anggaran_pekerjaan: string;

  anggaranable: AsetTanah | AsetProperti; 
}

export class AnggaranDetail {
  path: string;
  action_button: ActionButton;

  id_anggaran: number;
  id_anggaran_detail: number;
  nilai_anggaran: number;
  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  id_workflow: number;
  id_current_role_workflow: number;
  id_status_validasi: boolean;
  item_alokasi: number;
  created_on: Date;
  created_by: Pengguna;
  changed_on: Date;
  changed_by: Pengguna;

  // relation
  tanggal_approve?: string;
  alokasi?: AnggaranPekerjaan[];
  approval_level?: string;

  pekerjaan: AnggaranPekerjaan[] = [];
  workflow_progress: WorkflowProgress[];
}
