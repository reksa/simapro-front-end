export class JenisAsetProperti {
  id_jenis_aset: number;
  sub_jenis_aset: SubJenisAsetProperti[];
  nama: string;
}

export class SubJenisAsetProperti {
  id_sub_jenis_aset: number;
  id_jenis_aset: number;
  jenis_aset: JenisAsetProperti;
  nama: string;
}
