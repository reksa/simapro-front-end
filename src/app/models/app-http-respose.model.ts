export class AppHttpResponse {
  success: boolean;
  message: string;
  result: Object;
  total?: number;
}
