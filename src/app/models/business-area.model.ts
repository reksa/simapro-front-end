import { CompanyCode } from './company-code.model';
import { Pengguna } from './pengguna.model';
export class BusinessArea {
  id_business_area: number;
  id_company_code: number;
  kode_business_area: string;
  nama_business_area: string;
  id_busare: string;
  is_active = true;
  company_code: CompanyCode;
  created_on: Date;
  changed_on: Date;
  created_by: Pengguna;
  changed_by: Pengguna;
}
