import { LatLng } from "leaflet";

export class _Properti {
    no_urut: number;
    kode_aset_tanah: string;
    kode_business_area: string; //2
    nama_properti: string; //4
    luas_properti: number;
    nilai_perolehan: number;
    tahun_perolehan: string;
    nilai_properti: number;
    tahun_penilaian: string;
    no_imb: string;
    no_sap: string;
    tanggal_terbit_imb: Date;
    id_sumber_perolehan: number;
    id_jenis_aset: string;
    id_sub_jenis_aset: string;
    alamat_properti: string;
    polygon: LatLng[][];
    is_active: boolean = true;
    deskripsi: string;
    nilai_potensi: number;
    start_date_potensi: Date;
    end_date_potensi: Date;

    constructor(_t: any[]) {
        this.no_urut= _t['0'] || null;
        this.kode_aset_tanah= _t['1'] || null;
        this.kode_business_area= _t['2'] || null; //2
        this.nama_properti= _t['4'] || null; //4
        this.luas_properti= _t['5'] || null;
        this.nilai_perolehan= _t['6'] || null;
        this.tahun_perolehan= _t['7'] ? _t['7'] : null;
        this.nilai_properti= _t['8'] || null;
        this.tahun_penilaian= _t['9'] ? _t['9'] : null;
        this.no_imb= _t['10'] || null;
        this.tanggal_terbit_imb= _t['11'] || null;
        this.id_sumber_perolehan= _t['12'] || null;
        this.id_jenis_aset= _t['13'] || null;
        this.id_sub_jenis_aset= _t['14'] || null;
        this.alamat_properti= _t['15'] || null;
        this.polygon = _t['16'] ? [this.mapToPolygon(_t['16'])] : null;
        this.deskripsi = _t['17'] || null;
        this.nilai_potensi = _t['18'] || null;
        this.start_date_potensi = _t['19'] || null;
        this.end_date_potensi = _t['20'] || null;
        this.no_sap = _t['21'] || null;
    }

    private mapToPolygon(str: string): LatLng[] {
        const arr = str.split(',');
        return arr.map(latlngString => {
            const latlngArray = latlngString.trim().split(' ');
            const lat = latlngArray[0] ? parseFloat(latlngArray[0]): 0;
            const lng = latlngArray[1] ? parseFloat(latlngArray[1]): 0; 
            return new LatLng(lat, lng);
        });
    }
}