import { AsetProperti } from './aset-properti.model';
import { Pengguna } from './pengguna.model';
import { AsetTanah, PendayagunaanAset } from 'app/models';
import { Disposisi } from 'app/models/disposisi.model';
import { ActionButton } from './action-button.model';

export class PotensiAset {
  path: string;
  action_button: ActionButton;
  is_pendayagunaanable: boolean;

  id_potensi: number;
  deskripsi: string;
  nilai_potensi: number;
  benefit_potensi: number;
  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  start_date_potensi: Date;
  end_date_potensi: Date;
  potensiable: AsetTanah | AsetProperti;
  potensiable_id: number;
  potensiable_type: string;
  disposisi: Disposisi[];
  disposisi_pending: Disposisi[];
  pendayagunaan: PendayagunaanAset[];
  jenis_potensi: string;
  is_editable_potensi: boolean;
  id_status_validasi: number;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  cranged_by?: Pengguna;
  pendayagunaan_aktif: PendayagunaanAset[];

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        this[prop] = obj[prop];
      }
    }
  }
}
