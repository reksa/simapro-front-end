import { Regional } from './regional.model';
import { BusinessArea } from './business-area.model';
import { Pengguna } from './pengguna.model';
export class CompanyCode {
  id_company_code: number;
  id_regional: number;
  regional: Regional;
  nama: string;
  kode_company_code: string;
  nama_company_code: string;
  grup: string;
  id_pusare: string;
  id_pare: string;
  id_psuare: string;
  members?: Pengguna[];
  business_area: BusinessArea[];
  is_active: boolean;
  created_on: Date;
  created_by?: Pengguna;
  changed_on: Date;
  changed_by?: Pengguna;
  deleted_on?: Date;
}
