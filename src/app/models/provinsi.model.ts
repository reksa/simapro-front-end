import { Pulau } from './pulau.model';
import { Kabupaten } from './kabupaten.model';
import { Pengguna } from './pengguna.model';

export class Provinsi {
  id_provinsi: number;
  id_pulau: number;
  pulau: Pulau;
  nama: string;
  kabupatens?: Kabupaten[];
  is_active = true;
  created_on: Date;
  created_by: Pengguna;
  changed_on: Date;
  changed_by: Pengguna;
}
