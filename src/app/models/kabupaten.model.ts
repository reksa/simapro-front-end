import { Lokasi } from './lokasi.model';
import { Provinsi } from './provinsi.model';
import { Pengguna } from './pengguna.model';

export class Kabupaten {
  id_kabupaten: number;
  id_provinsi: number;
  kode_kabupaten: number;
  nama: string;
  is_active = true;
  provinsi?: Provinsi = new Provinsi();
  lokasis?: Lokasi[];
  lokasi?: Lokasi[];
  created_on: Date;
  created_by: Pengguna;
  changed_on: Date;
  changed_by: Pengguna;
}
