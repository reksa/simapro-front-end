import { Headquarter } from './headquarter.model';
import { Pengguna } from './pengguna.model';
import { CompanyCode } from './company-code.model';
export class Regional {
 id_regional: number;
 id_head_quarters: number;
 nama_regional: string;
 is_active: Boolean;
 created_on: Date;
 changed_on: Date;
 created_by: Pengguna;
 changed_by: Pengguna;

 head_quarters: Headquarter;
 company_code: CompanyCode[];
}
