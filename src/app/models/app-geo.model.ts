import { LatLng } from 'leaflet';

export class AppGeoPolygon {
  area: {
    coordinates: number[][];
    type: string;
  }
}
