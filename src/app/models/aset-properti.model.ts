import { WorkflowProgress } from './workflow.model';
import { JenisAsetProperti, SubJenisAsetProperti } from './jenis-aset-properti.model';
import { PotensiAset } from './potensi-aset.model';
import { CompanyCode } from './company-code.model';
import { Lokasi } from './lokasi.model';
import { Polygon, Point, LatLng } from 'leaflet';
import { BusinessArea, AsetTanah, JenisPenghapusan, PendayagunaanAset, AppGeoPolygon, SumberPerolehan } from 'app/models';
import { Pengguna } from 'app/models/pengguna.model';
import { Disposisi } from './disposisi.model';
import { ActionButton } from './action-button.model';

export class AsetProperti {
  // untuk submit form
  path?: string;
  nama_pendayagunaan: string;
  type: string = 'App\\Properti';
  is_potensiable: boolean;
  is_nilaiable: boolean;
  
  id_tanah?: number;
  id_business_area?: number;
  nama_properti: string;
  alamat_properti: string;
  luas_properti: number;
  foto: string;
  no_imb: string;
  no_sap?: string;
  tanggal_terbit_imb: Date;
  is_active: boolean;
  is_use: boolean;
  start_date_properti: Date;


  id_properti: number;
  id_jenis_aset: number;
  id_sub_jenis_aset: number;

  detail_aktif: AsetPropertiDetail[];
  detail_non_valid?: AsetPropertiDetail[] = [];
  histori_detail?: AsetPropertiDetail[] = [];

  perpindahan_aktif: PerpindahanAsetProperti[] = [];
  perpindahan_non_valid: PerpindahanAsetProperti[] = [];
  histori_perpindahan: PerpindahanAsetProperti[] = [];

  potensi_aktif: PotensiAset[];
  // potensi: PotensiAset[];
  histori_potensi: PotensiAset[];
  potensi_non_valid: PotensiAset[];
  dayaguna: PendayagunaanAset[];
  pendayagunaan_aktif: PendayagunaanAset[];
  histori_dayaguna: PendayagunaanAset[];

  nilai_perolehan: number;
  tahun_perolehan: number;
  sumber_perolehan?: SumberPerolehan;

  nilai_aktif: NilaiAsetProperti[];
  nilai_non_valid: NilaiAsetProperti[];
  histori_nilai: NilaiAsetProperti[];

  jenis_aset: JenisAsetProperti;

  is_editable_detail = false;
  is_editable_nilai = false;
  is_editable_perpindahan = false;

  sub_jenis_aset: SubJenisAsetProperti;
  polygon?: LatLng[][] | Polygon;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;


  get detail(): AsetPropertiDetail {
    return this.detail_aktif && this.detail_aktif.length ? this.detail_aktif[0] : null;
  }

  get detail_perubahan() {
    return this.detail_non_valid && this.detail_non_valid.length ? this.detail_non_valid[0] : null;
  }
  
  get perpindahan(): PerpindahanAsetProperti {
    return this.perpindahan_aktif && this.perpindahan_aktif.length ? this.perpindahan_aktif[0] : null;
  }

  get perpindahan_perubahan() {
    return this.perpindahan_non_valid && this.perpindahan_non_valid.length ? this.perpindahan_non_valid[0] : null;
  }

  get nilai(): NilaiAsetProperti {
    return this.nilai_aktif && this.nilai_aktif.length ? this.nilai_aktif[0] : null;
  }

  get nilai_perubahan() {
    return this.nilai_non_valid && this.nilai_non_valid.length ? this.nilai_non_valid[0] : null;
  }

  get potensi(): PotensiAset {
    return this.potensi_aktif && this.potensi_aktif.length ? this.potensi_aktif[0] : null;
  }

  get potensi_perubahan(): PotensiAset {
    return this.potensi_non_valid && this.potensi_non_valid.length ? this.potensi_non_valid[0] : null;
  }

  get id(): number {
    return this.id_properti;
  }

  set detail(detail: AsetPropertiDetail) {
    this.detail_aktif = [detail];
  }  

  set perpindahan(perpindahan: PerpindahanAsetProperti) {
    this.perpindahan_aktif = [perpindahan];
  }

  set nilai(nilai: NilaiAsetProperti) {
    this.nilai_aktif = [nilai];
  }

  set potensi(potensi: PotensiAset) {
    this.potensi_aktif = [potensi];
  }

  set potensi_perubahan(potensi: PotensiAset) {
    this.potensi_non_valid = [potensi];
  }

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        this[prop] = obj[prop];
      }
    }
  }

  workflow_progress: WorkflowProgress[];
}

export class AsetPropertiDetail {
  // untuk submit form
  path?: string;
  nama_tanah?: string; // untuk Polymorfisme
  nama_aset:string; // untuk polymorfisme

  id_properti_detail: number;
  id_properti: number;
  id_polygon: number;
  id_jenis_aset: number;
  id_sub_jenis_aset: number;
  nama_properti: string;
  alamat_properti: string;
  luas_properti: number;
  foto: string;
  no_imb: string;
  no_sap: string;
  tanggal_terbit_imb: Date;
  is_active: boolean;
  is_use: boolean;
  start_date_properti: Date;
  end_date_properti: Date;

  disposisi: Disposisi[];
  is_history: boolean;
  is_editable_detail = false;
  action_button: ActionButton;

  properti: AsetProperti;
  // polygon_active: AppGeoPolygon;
  polygon?: any;
  Workflow_progress: WorkflowProgress[];

  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;
}

export class PerpindahanAsetProperti {
  // untuk submit form
  path?: string;

  id_perpindahan_properti: number;
  id_properti: number;
  id_tanah: number;
  id_lokasi: number;
  id_business_area: number;
  id_company_code: number;
  kode_properti: string;
  kode_properti_lama: string;
  keterangan: string;
  start_date_perpindahan: Date;
  end_date_perpindahan: Date;
  is_use: boolean;

  properti: AsetProperti;
  // aset_tanah: AsetTanah;
  tanah: AsetTanah;
  disposisi: Disposisi[];
  is_history: boolean;
  is_editable_perpindahan = false;
  action_button: ActionButton;

  business_area: BusinessArea;
  company_code: CompanyCode;
  Workflow_progress: WorkflowProgress[];

  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;
}

export class NilaiAsetProperti {
  // untuk submit form
  path?: string;

  id_nilai_properti: number;
  id_properti: number;
  // id_status_kelayakan: number;
  nilai_properti: number;
  tahun_penilaian: number;
  biaya_penyusutan: number;
  kualitas_properti: number;
  lifetime_aset: number;
  keterangan: string;
  is_use: boolean;
  is_active: boolean;

  is_history: boolean;
  action_button: ActionButton;
  is_editable_nilai = false;

  disposisi: Disposisi[];
  Workflow_progress: WorkflowProgress[];
  
  start_date_nilai: Date;
  end_date_nilai: Date;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;
}
