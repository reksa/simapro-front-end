import { Pengguna } from './pengguna.model';
import { Kabupaten } from './kabupaten.model';
import { LatLng } from 'leaflet';

export class Lokasi {
  id_lokasi: number;
  id_kabupaten: number;
  kode_lokasi: number;
  nama: string;
  is_active = true;
  kabupaten?: Kabupaten;
  created_on: Date;
  created_by: Pengguna;
  changed_on: Date;
  changed_by: Pengguna;
}
