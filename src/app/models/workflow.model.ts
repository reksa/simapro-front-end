import { Role } from './role.model';
import { Pengguna } from './pengguna.model';
import { Permission } from './permission.model';

export class Workflow {
  id_workflow: number;
  nama_workflow: string;
  start_date: Date;
  end_date: Date;
  is_active: Boolean;
  created_by: Pengguna;
  created_on: Date;
  updated_by: Pengguna;
  updated_on: Date;
  deleted_by: Pengguna;
  deleted_on: Pengguna;

  role_workflow: RoleWorkflow[] = [];
}

export class RoleWorkflow {
  id_role_workflow: number;
  id_workflow: number;
  id_role: number;
  id_permission: number;
  type_workflow: TypeWorkflow;
  prev: number;
  next: number;
  is_start: Boolean;
  is_finish: Boolean;

  workflow: Workflow;
  role: Role;
  permission: Permission;
  prev_step: RoleWorkflow;
  next_step: RoleWorkflow;
}

export enum TypeWorkflow {
  'submit',
  'create',
  'approve',
  'reject',
  'finish'
}

export class WorkflowProgress {
  name: string;
  time: string;
  current: boolean;
  permission: string;
  memo: string;
  is_terima: boolean;
  is_tolak: boolean;
}
