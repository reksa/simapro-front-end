import { Pengguna } from './pengguna.model';

export class Disposisi {
  id: number;
  from: Pengguna;
  created_on: Date;
  role_asal: string;
  role_tujuan: string;
  pengguna: string;
  validasi: string;
  memo: string;
  workflow: string
  permission: string;
}