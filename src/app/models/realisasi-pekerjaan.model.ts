import { ActionButton } from './action-button.model';
import { AsetProperti } from './aset-properti.model';
import { AsetTanah } from './aset-tanah.model';
import { RoleWorkflow, WorkflowProgress } from './workflow.model';
import { CompanyCode } from './company-code.model';
import { BusinessArea } from './business-area.model';
import { Anggaran, AnggaranDetail, AnggaranPekerjaan } from './anggaran.model';
import { Pengguna } from './pengguna.model';
import { Disposisi } from './disposisi.model';
export class Realisasi {
  path: string;
  action_button: ActionButton;

  id_realisasi: number;
  id_anggaran: number;
  id_anggaran_detail: number;
  id_business_area: number;
  id_company_code: number;
  id_workflow: number;
  id_current_role_workflow: number;

  realisasi_pekerjaanable_id: number;
  realisasi_pekerjaanable_type: string;

  total_realisasi: number;
  tanggal_realisasi: Date;
  keterangan: string;
  is_active: boolean;
  is_use: boolean;
  is_history: boolean;

  created_on: Date;
  changed_on: Date;
  created_by: Pengguna;
  changed_by: Pengguna;

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        if (prop === 'realisasi_pekerjaanable' && obj['realisasi_pekerjaanable_type']) {
          if (obj['realisasi_pekerjaanable_type'] === 'App\\Tanah') {
            this[prop] = new AsetTanah(obj[prop]);
          } else {
            this[prop] = new AsetProperti(obj[prop]);
          }
        } else {
          this[prop] = obj[prop];
        }
      }
    }
  }

  // Relation

  realisasi_pekerjaan: RealisasiPekerjaan[];
  anggaran?: Anggaran;
  anggaran_detail?: AnggaranDetail;
  pekerjaan?: AnggaranPekerjaan;
  business_area: BusinessArea;
  company_code: CompanyCode;
  role_workflow: RoleWorkflow[];
  realisasi_pekerjaanable: AsetTanah | AsetProperti;
  historiDisposisi: Disposisi[];
  workflow_progress: WorkflowProgress[];
}

export class RealisasiPekerjaan {
  id_realisasi_pekerjaan: number;
  id_realisasi: number;
  id_anggaran_pekerjaan: number;
  nama_realisasi_pekerjaan: string;
  nilai_realisasi_pekerjaan: number;
  realisasi_pekerjaanable_id: number;
  realisasi_pekerjaanable_type: string;

  // Relation

  realisasi?: Realisasi;
  anggaran_detail?: AnggaranDetail;
  pekerjaan?: AnggaranPekerjaan;
  realisasi_pekerjaanable: AsetTanah | AsetProperti;
  realisasi_type: string;

}