import { Pemeliharaan } from './pemeliharaan.model';
// import { Perizinan } from './perizinan.model';
// import { Operasi } from './operasi.model';
import { Pengguna } from './pengguna.model';

export class ProgresPekerjaan {
  id_progres_pekerjaan: number;
  nama: string;
  pemeliharaan: Pemeliharaan;
  plan_start_date: Date;
  plan_end_date: Date;
  start_date?: Date;
  end_date?: Date;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  cranged_by?: Pengguna;
}
