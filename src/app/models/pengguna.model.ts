import { BusinessArea } from './business-area.model';
import { CompanyCode } from './company-code.model';
import { Role, Notifikasi } from 'app/models';

export class Pengguna {
  id_pengguna: number;
  id_regional: number;
  id_business_area: number;
  username: string; // 50
  nama: string; // 50
  email: string; // 100
  foto: string | any;
  company: string; // 50
  department: string; // 50
  title: string; // 50
  employee_number: string; // 50
  active_directory: boolean;
  is_active: boolean;

  password?: string;

  role_pengguna?: Role[];
  role?: Role[] = [];
  // company_code: CompanyCode;
  business_area: BusinessArea;

  // id_company_code: number;
  get company_code(): CompanyCode {
    if(!this.business_area) this.business_area = new BusinessArea();
    return this.business_area.company_code;
  }

  set company_code(companyCode: CompanyCode) {
    if(!this.business_area) this.business_area = new BusinessArea();
    this.business_area.company_code = companyCode;
  }

  get id_company_code(): number {
    if(!this.business_area) this.business_area = new BusinessArea();
    return this.business_area.id_company_code;
  }

  set id_company_code(idCompanyCode: number) {
    if(!this.business_area) this.business_area = new BusinessArea();
    this.business_area.id_company_code = idCompanyCode;
  }
  
  is_validator_unit = false;
  is_validator_pusat = false;
  is_user_input = false;
  token?: string;

  notifikasi?: Notifikasi[];
  created_by: Pengguna;
  created_on: Date;
  updated_on: Date;
  deleted_on: Date;
}
