import { Pengguna } from './pengguna.model';
import { Anggaran } from './anggaran.model';
import { AsetProperti } from './aset-properti.model';

export class Pemeliharaan {
  id_pemeliharaan: number;
  nama: string;
  anggaran?: Anggaran;
  asetProperti: AsetProperti;
  nilai: number;
  plan_start_date: Date;
  plan_end_date: Date;
  start_date?: Date;
  end_date?: Date;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  cranged_by?: Pengguna;
}
