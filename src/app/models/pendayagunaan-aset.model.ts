import { ActionButton } from './action-button.model';
import { Pengguna } from './pengguna.model';
import { PotensiAset } from './potensi-aset.model';
import { JenisDayaGuna, AsetTanah, AsetProperti } from '.';
import { Disposisi } from './disposisi.model';

export class PendayagunaanAset {
  path: string;
  action_button: ActionButton;

  id_pendayagunaan: number;
  id_potensi: number;
  id_jenis_dayaguna: number;
  id_business_area: number;
  id_company_code: number;
  pendayagunaanable_id: number;
  pendayagunaanable_type: string;
  keterangan: string;
  kontrak: string;
  nilai_pendayagunaan: number;
  // pihak_terkait: string;
  start_date_pendayagunaan: Date;
  end_date_pendayagunaan: Date;
  is_use: boolean;
  is_active: boolean;
  jenis_dayaguna: JenisDayaGuna;
  jenis_pendayagunaan: string;
  pendayagunaanable: AsetTanah | AsetProperti;
  potensi: PotensiAset;

  disposisi: Disposisi[];
  disposisi_pending: Disposisi[];

  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        this[prop] = obj[prop];
      }
    }
  }
}
