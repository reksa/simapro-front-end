import { Pengguna } from './pengguna.model';

export class LogAktifitas {
  id_log_aktifitas: number;
  pengguna?: Pengguna;
  aktifitas: string;
  created_on: Date;
}
