import { Pengguna } from './pengguna.model';

export class AccessToken {
  token: string;
  expires_in: number
}

export class DecodedToken {
  iss: string;
  iat: number;
  exp: number
  nbf: number
  jti: string;
  sub: number
  prv: string
  pengguna: Pengguna;
}
