import { Pengguna } from './pengguna.model';

export class Notifikasi {
  id: string;
  pengirim: Pengguna;
  penerima: Pengguna;
  url: string;
  category: string;
  date: string;
  date_diff: string;
  keterangan: string;
  notifiable_id: number;
  notifiable_type: string;
  created_at: Date;
  updated_at: Date;
  read_at: Date;
}
