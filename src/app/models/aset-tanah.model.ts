import { WorkflowProgress } from './workflow.model';
import { CompanyCode } from 'app/models';
import { PotensiAset } from './potensi-aset.model';
import { JenisPenghapusan, SumberPerolehan, BusinessArea, JenisPerizinan, OpiniLegal, AppGeoPolygon, PendayagunaanAset } from 'app/models';
import { Pengguna } from './pengguna.model';
import { Lokasi } from './lokasi.model';
import { Point, LatLng } from 'leaflet';
import { Disposisi } from './disposisi.model';
import { ActionButton } from './action-button.model';

export class AsetTanah {
  id_properti:number; // untuk polymorfisme
  // untuk submit form
  path?: string;
  nama_pendayagunaan: string;
  type: string = 'App\\Tanah';
  has_sertifikat: boolean;
  is_potensiable: boolean;
  is_editable_sertifikat: boolean;

  id_company_code?: number;
  id_business_area?: number;
  id_lokasi?: number;
  id_master_progress?: number;
  id_ijin?: number;
  nama_tanah?: string;
  alamat_tanah?: string;
  luas_tanah?: number;
  no_bphtb?: number;
  no_sap?: string;
  nama_progress?: string;
  tanggal_progress?: Date;
  keterangan: string; // tahap dokumen legal
  no_sertifikat?: string;
  no_pbb?: string;
  biaya_pbb?: number;
  start_date_sertifikat?: Date;
  end_date_sertifikat?: Date;
  status_progress?: boolean;

  id_tanah: number;
  id_sumber_perolehan: number;
  nilai_perolehan: number;
  luas: number;
  is_active?: boolean;
  is_use: boolean;

  harga_perolehan: number;
  tahun_perolehan: number;
  sumber_perolehan?: SumberPerolehan;

  // detail?: AsetTanahDetail;
  detail_non_valid?: AsetTanahDetail[];
  detail_aktif: AsetTanahDetail[];
  // detail_non_valid: AsetTanahDetail[];
  histori_detail?: AsetTanahDetail[];

  perpindahan_non_valid?: PerpindahanAsetTanah[];
  perpindahan_aktif?: PerpindahanAsetTanah[] = [];
  // perpindahan_non_valid: PerpindahanAsetTanah[] = []
  histori_perpindahan?: PerpindahanAsetTanah[] = [];

  sertifikat_aktif?: DokumenLegal[] = [];
  // sertifikat_aktif: DokumenLegal[];
  sertifikat_non_valid: DokumenLegal[];
  sertifikat_on_progress?: DokumenLegal[];
  histori_sertifikat?: DokumenLegal[] = [];

  nilai_non_valid: NilaiAsetTanah[];
  nilai_aktif: NilaiAsetTanah[];
  // nilai_non_valid: NilaiAsetTanah[];
  histori_nilai: NilaiAsetTanah[];

  // potensi?: PotensiAset[] = [];
  potensi_aktif: PotensiAset[];
  potensi_non_valid: PotensiAset[];
  histori_potensi: PotensiAset[];
  dayaguna: PendayagunaanAset[];
  pendayagunaan_aktif: PendayagunaanAset[];
  histori_dayaguna: PendayagunaanAset[];

  jenis_aset: string; // App\AsetTanah OR App\AsetProperti

  polygon?: LatLng[][] | AppGeoPolygon;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  get detail(): AsetTanahDetail {
    return this.detail_aktif && this.detail_aktif.length ? this.detail_aktif[0] : null;
  }

  get detail_perubahan() {
    return this.detail_non_valid && this.detail_non_valid.length ? this.detail_non_valid[0] : null;
  }
  
  get perpindahan(): PerpindahanAsetTanah {
    return this.perpindahan_aktif && this.perpindahan_aktif.length ? this.perpindahan_aktif[0] : null;
  }

  get perpindahan_perubahan() {
    return this.perpindahan_non_valid && this.perpindahan_non_valid.length ? this.perpindahan_non_valid[0] : null;
  }
  get dokumen(): DokumenLegal {
    return this.sertifikat_aktif && this.sertifikat_aktif.length ? this.sertifikat_aktif[0] : null;
  }

  get dokumen_perubahan() {
    return this.sertifikat_non_valid && this.sertifikat_non_valid.length ? this.sertifikat_non_valid[0] : null;
  }

  get dokumen_on_progress(): DokumenLegal {
    return this.sertifikat_on_progress && this.sertifikat_on_progress.length ? this.sertifikat_on_progress[0] : null;
  }

  get nilai(): NilaiAsetTanah {
    return this.nilai_aktif && this.nilai_aktif.length ? this.nilai_aktif[0] : null;
  }

  get nilai_perubahan() {
    return this.nilai_non_valid && this.nilai_non_valid.length ? this.nilai_non_valid[0] : null;
  }

  get potensi(): PotensiAset {
    return this.potensi_aktif && this.potensi_aktif.length ? this.potensi_aktif[0] : null;
  }

  get potensi_perubahan(): PotensiAset {
    return this.potensi_non_valid && this.potensi_non_valid.length ? this.potensi_non_valid[0] : null;
  }

  get id(): number {
    return this.id_tanah;
  }

  set detail(detail: AsetTanahDetail) {
    this.detail_aktif = [detail];
  }

  set perpindahan(perpindahan: PerpindahanAsetTanah) {
    this.perpindahan_aktif = [perpindahan];
  }

  set dokumen(sertifikat: DokumenLegal) {
    this.sertifikat_aktif = [sertifikat];
  }

  set nilai(nilai: NilaiAsetTanah) {
    this.nilai_aktif = [nilai];
  }

  set potensi(potensi: PotensiAset) {
    this.potensi_aktif = [potensi];
  }

  set potensi_perubahan(potensi: PotensiAset) {
    this.potensi_non_valid = [potensi];
  }

  constructor(obj?: Object) {
    if(obj) {
      for(let prop in obj) {
        this[prop] = obj[prop];
      }
    }
  }

  is_nilaiable: boolean;
  workflow_progress: WorkflowProgress[];

}

export class AsetTanahDetail {
  // untuk submit form
  path?: string;
  nama_properti?: string; // untuk Polymorfisme
  nama_aset:string; // untuk polymorfisme

  id_tanah_detail: number;
  id_tanah: number;
  id_jenis_penghapusan: number;
  id_polygon: number;
  id_status_validasi: number;
  id_workflow: number;
  id_current_role_workflow: number;
  nama_tanah: string;
  alamat_tanah: string;
  foto: string;
  luas_tanah: number;
  no_bphtb: string;
  biaya_bphtb: number;
  no_sap: string;

  polygon?: any;
  point?: Point;


  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  end_date_detail: Date;
  start_date_detail: Date;

  jenis_penghapusan?: JenisPenghapusan;
  action_button: ActionButton;
  is_editable_detail = false;
  tanah?: AsetTanah;
  Workflow_progress: WorkflowProgress[];

  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  // getter
  // get nama() {
  //   return this.nama_tanah;
  // }
  // get alamat() {
  //   return this.alamat_tanah;
  // }

  // set nama(nama: string) {
  //   this.nama_tanah = nama;
  // }
  // set alamat(alamat: string) {
  //   this.alamat_tanah = alamat;
  // }

  workflow_progress: WorkflowProgress[];
}

export class PerpindahanAsetTanah {
  // untuk submit form
  path?: string;

  id_perpindahan_tanah: number;
  id_tanah: number;
  id_lokasi: number;
  id_company_code: number;
  id_business_area: number;
  id_workflow: number;
  id_current_role_workflow: number;
  id_status_validasi: number;
  kode_tanah: string;
  kode_tanah_lama: string;
  keterangan: string;
  start_date_perpindahan: Date;
  end_date_perpindahan: Date;
  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  is_editable_perpindahan = false;

  // aset_tanah: AsetTanah;
  lokasi: Lokasi;
  business_area: BusinessArea;
  company_code: CompanyCode;
  Workflow_progress: WorkflowProgress[];
  // disposisi: Disposisi[];

  tanah: AsetTanah;
  action_button: ActionButton;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  workflow_progress: WorkflowProgress[];
}

export class DokumenLegal {
  // untuk submit form
  path?: string;
  id_master_progress: number;
  tanggal_progress: Date;
  keterangan: string;

  id_sertifikat_tanah: number;
  id_tanah: number;
  id_ijin: number;
  id_opini: number;
  id_workflow: number;
  id_current_role_workflow: number;
  id_status_validasi: number;
  no_sertifikat: string;
  no_pbb: string;
  biaya_pbb: number;
  start_date_sertifikat: Date;
  end_date_sertifikat: Date;
  
  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  status_progress: boolean;
  is_editable_sertifikat = false;


  ijin: JenisPerizinan;
  aset_tanah?: AsetTanah;
  lokasi_aset?: Lokasi;
  action_button: ActionButton;
  businessArea?: BusinessArea;
  tahap_sertifikat?: TahapDokumenLegal[] = [];
  disposisi: Disposisi[];
  Workflow_progress: WorkflowProgress[];

  opini_legal?: OpiniLegal;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  workflow_progress: WorkflowProgress[];
}

export class TahapDokumenLegal {
  path: string;
  is_terima: boolean;

  no_sertifikat?: string;
  no_pbb?: string;
  start_date_sertifikat: Date;
  end_date_sertifikat: Date;

  id_sertifikat_tanah: number;
  id_master_progress: number;
  nama_progress: string;
  tanggal_progress: Date;
  is_finish: boolean;
  keterangan: string;

  sertifikat: DokumenLegal;
  action_button: ActionButton;

  // get dokumen(): DokumenLegal {
  //   return this.sertifikat;
  // }

  // set dokumen(dokumen: DokumenLegal) {
  //   this.sertifikat = dokumen;
  // }

  master_progress: MasterProgress;
  // keterangan: string;
  // status_tahap_dokumen: number;
  // start_date_tahap_dokumen: Date;
  // end_date_tahap_dokumen: Date;
  // is_active: boolean;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  workflow_progress: WorkflowProgress[];
}

export class NilaiAsetTanah {
  // untuk submit form
  path?: string;
  
  id_tanah: number;
  id_nilai_tanah: number;
  id_workflow: number;
  id_current_role_workflow: number;
  id_status_validasi: number;
  nilai_tanah: number;
  tahun_penilaian: number;
  keterangan: string;
  disposisi: Disposisi[];
  start_date_nilai: Date;
  end_date_nilai: Date;

  tanah?: AsetTanah;
  Workflow_progress: WorkflowProgress[];
  action_button: ActionButton;
  is_editable_nilai = false;

  is_active: boolean;
  is_use: boolean;
  is_history: boolean;
  created_on: Date;
  changed_on: Date;
  created_by?: Pengguna;
  changed_by?: Pengguna;

  workflow_progress: WorkflowProgress[];
}

export class MasterProgress {
  id_master_progress: number;
  is_finish: boolean;
  nama_master_progress: string;
  deskripsi: string;
}
