export class JadwalInput {
  id_jadwal: number
  start_date_jadwal: Date;
  end_date_jadwal: Date;
  keterangan: string;
}
