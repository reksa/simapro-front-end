import { Role } from './role.model';

export class Permission {
  id_permission: number;
  nama: string;
  path: string;
  description?: string;
  enabled: boolean;
  roles?: Role[];
  created_on: Date;
  updated_on: Date;
}

export class Modul {
  id_modul: number;
  nama_modul: string;
  permission: Permission[];
}
