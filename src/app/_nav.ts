export const navigation = [
  {
    name: 'Master Data',
    url: '/aset',
    icon: 'icon-flag',
    children: [
      {
        name: 'Organisasi',
        url: '/master-data/organisasi',
        icon: 'icon-flag'
      },
      {
        name: 'Wilayah',
        url: '/master-data/wilayah',
        icon: 'icon-flag'
      },
      {
        name: 'Manajemen Pengguna',
        url: '/master-data/manajemen-pengguna',
        icon: 'fa fa-users'
      }
    ]
  },
  {
    name: 'Workflow',
    url: '/workflow',
    icon: 'fa fa-calendar'
  },
  {
    name: 'Jadwal Input Data',
    url: '/master-data/jadwal-input',
    icon: 'fa fa-calendar'
  },
  {
    name: 'Import',
    url: '/import',
    icon: 'fa fa-calendar',
    children: [
      {
        name: 'Import Tanah',
        url: '/import/tanah',
        icon: 'icon-flag'
      },
      {
        name: 'Import Properti',
        url: '/import/properti',
        icon: 'icon-building'
      },
      {
        name: 'Import Pendayagunaan',
        url: '/import/pendayagunaan',
        icon: 'icon-building'
      }
    ]
  },
  {
    name: 'Aset',
    url: '/aset',
    icon: 'icon-star',
    children: [
      // {
      //   name: 'Lokasi Aset',
      //   url: '/lokasi',
      //   icon: 'fa fa-map-pin'
      // },
      {
        name: 'Aset Tanah',
        url: '/aset-tanah',
        icon: 'icon-flag'
      },
      {
        name: 'Aset Properti',
        url: '/aset-properti',
        icon: 'icon-building'
      }
    ]
  },
  {
    name: 'Anggaran dan Realisasi',
    url: '/ophar',
    icon: 'icon-directions',
    children: [
      {
        name: 'Anggaran',
        url: '/anggaran',
        icon: 'fa fa-money'
      },
      {
        name: 'Realisasi',
        url: '/realisasi',
        icon: 'fa fa-wrench'
      }
    ]
  },
  {
      name: 'Potensi & Pendayagunaan',
      url: '/potensi-aset',
      icon: 'icon-directions',
      children: [
        {
          name: 'Potensi',
          url: '/potensi-aset',
          icon: 'icon-directions',
        },
        {
          name: 'Pendayagunaan',
          url: '/pendayagunaan-aset',
          icon: 'fa fa-wrench'
        }
      ]
  },
  // {
  //   name: 'Pendayagunaan',
  //   url: '/potensi-pendayagunaan',
  //   icon: 'icon-directions',
  //   children: [
  //     {
  //       name: 'Potensi Aset',
  //       url: '/potensi-aset',
  //       icon: 'fa fa-retweet'
  //     },
  //     {
  //       name: 'Pendayagunaan',
  //       url: '/pendayagunaan-aset',
  //       icon: 'fa fa-server'
  //     }
  //   ]
  // },
  {
    name: 'Laporan',
    url: '/laporan',
    icon: 'icon-chart',
    children: [
      {
        name: 'Data Aset',
        url: '/laporan/aset',
        icon: 'icon-directions',
      },
      {
        name: 'Pendapatan',
        url: '/laporan/pendapatan',
        icon: 'fa fa-wrench'
      },
      {
        name: 'Realisasi',
        url: '/laporan/realisasi',
        icon: 'fa fa-wrench'
      }
    ]
  }
];
