import { AppError } from './app-error';
import { environment } from './../../environments/environment';

export class BadInputError extends AppError {
  handleError() {
    console.log('Validation Error');
    if (!environment.production) {
      console.log('Error 400 :', this.error);
    }
  }
}
