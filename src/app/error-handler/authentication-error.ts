import { AppError } from './app-error';
import { environment } from './../../environments/environment';

export class AuthenticationError extends AppError {
  handleError() {
    if (!environment.production) {
      console.log('Error 401 :', this.error);
    }
    // const loc = window.location;
    // window.location.replace(loc.host + loc.pathname + '#/auth/logout');
  }
}
