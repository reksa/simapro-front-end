import { AppError } from './app-error';
import { environment } from './../../environments/environment';

export class ForbidenAccessError extends AppError {
  handleError() {
    console.log('Forbiden Access');
    if (!environment.production) {
      console.log('Error 403 :', this.error);
    }
  }
}
