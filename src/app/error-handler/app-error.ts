import { environment } from './../../environments/environment';

export class AppError {
  constructor(
    public error: any
  ) {
      // alert(error.message);
      this.handleError();
  }

  handleError() {
    console.log('Unexpected Errors');
    if (!environment.production) {
      console.log('Unexpected Errors :', this.error);
    }
  }
}
