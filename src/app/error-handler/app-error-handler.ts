import { ErrorHandler } from '@angular/core';

export class AppErrorHandler implements ErrorHandler {

  handleError(error) {
    const message = typeof error !== 'undefined' && error && error.error && error.error.message ? error.error.message : 'Unexpected error';
      // alert(message);
      console.log('An unexpected error occured', error);
  }
}
