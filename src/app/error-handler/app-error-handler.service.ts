// import { AlertService, AlertMessage } from 'app/services/alert.service';
import { Observable } from 'rxjs/Rx';
import { AppError, BadInputError, NotFoundError, InternalServerError, ForbidenAccessError, AuthenticationError } from 'app/error-handler';
// import { Injectable } from '@angular/core';

// @Injectable()
export class AppErrorHandlerService {
  static handle(error: Response) {
    const e = typeof error !== 'undefined' ? error.json() : {};
    switch (error.status) {
      case 400: return Observable.throw(new BadInputError(e));
      case 401: return Observable.throw(new AuthenticationError(e));
      case 403: return Observable.throw(new ForbidenAccessError(e));
      case 404: return Observable.throw(new NotFoundError(e));
      case 500: return Observable.throw(new InternalServerError(e));
      default:
    }
    return Observable.throw(new AppError(e));
  }

  // constructor(private alertService: AlertService) {
  // }

  // showError(alert: AlertMessage) {
  //   this.alertService.alert(alert);
  // }
}
