import { AppError } from './app-error';
import { environment } from './../../environments/environment';

export class NotFoundError extends AppError {
  handleError() {
    console.log('Resource Not Found');
    if (!environment.production) {
      console.log(this.error);
    }
  }
}
