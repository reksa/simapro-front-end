import { AppError } from './app-error';
import { environment } from './../../environments/environment';

export class InternalServerError extends AppError {
  handleError() {
    console.log('Internal Server Errors');
    if (!environment.production) {
      console.log('Error 500 :', this.error);
    }
  }
}
