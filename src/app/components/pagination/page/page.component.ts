import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-pagination]',
  templateUrl: './page.component.html'
})
export class PageComponent {
  @Input('config') config: AppTableConfig;
  @Output('first') first = new EventEmitter();
  @Output('last') last = new EventEmitter();
  @Output('onPageClick') onPageClick = new EventEmitter();
  @Output('click') click = new EventEmitter();

  constructor() {}

  setPage(pageNumber: number) {
    this.config.current = pageNumber;
  }

  onFirstClick($event) {
    this.first.emit($event);
    return false;
  }

  onLastClick($event) {
    this.last.emit($event);
    return false;
  }

  onClick($event) {
    this.onPageClick.emit($event);
  }

}

export class AppTableConfig {
  current = 1;
  limit = 5;
  total = 0;
  baseuri: string;
  from = 0;
  to = 0;
  show = 3;
  totalPages = 0;
  pages: {pageNumber: number, text: any}[] = [];

  generatePages(): any {
    const halfShow = Math.floor(this.show / 2);
    const pages = [];
    if (this.totalPages > this.show) {
      const lastShown = (this.current + this.show - halfShow - 1);
      if (this.current > halfShow && this.totalPages > lastShown) {
        if (this.current - halfShow > 1) {
          pages.push({pageNumber: (this.current - halfShow - 1), text: '..'});
        }
        for (let i = (this.current - halfShow); i <= lastShown; i++) {
          pages.push({pageNumber: i, text: i});
        }
        if (lastShown < this.totalPages) {
          pages.push({pageNumber: (lastShown + 1), text: '..'});
        }
      } else if (this.current - halfShow > 0) {
        for (let i = this.totalPages ; i > (this.totalPages - this.show); i--) {
          pages.unshift({pageNumber: i, text: i});
        }
        pages.unshift({pageNumber: (this.totalPages - this.show), text: '..'});
      } else {
        for (let i = 1; i <= this.show; i++) {
          pages.push({pageNumber: i, text: i});
        }
        pages.push({pageNumber: (this.show + 1), text: '..'});
      }
    } else {
      for (let i = 1; i <= this.totalPages; i++) {
        pages.push({pageNumber: i, text: i});
      }
    }
    this.pages =  pages;
  }
}

export class DataSetting  {
  query: string;
  orderBy: string;
  orderDirection: string;
  page: number;
  dataPerPage: number;
  idProvinsi: number;
  idKabupaten: number;
  idCompanyCode: number;
  idBusinessArea: number;
  nama: string;
  idLokasiAset: number;
  idAsetTanah: number;
  idJenisPendayagunaan: number;
  type: string;
  progress_sertifikat: string;
  nomor_sertifikat: string;
  no_imb: string;
  nomor_anggaran: string;
  nama_anggaran: string;
  tahun_anggaran: number;
  luas: number;
  start_range_luas: number;
  end_range_luas: number;
}

export interface PaginationData {
  current_page: number;
  data: Array<Object>;
  first_page: number;
  last_page: number;
  per_page: number;
  from: number;
  to: number;
  total: number;
}
