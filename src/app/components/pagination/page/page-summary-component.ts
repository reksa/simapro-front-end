import { AppTableConfig } from './page.component';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-pagination-summary',
    templateUrl: './page-summary.component.html'
})
export class PageSummaryComponent {
  @Input('config') config: AppTableConfig;
}
