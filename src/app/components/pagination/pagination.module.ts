import { PageSummaryComponent } from './page/page-summary-component';
import { PageComponent } from './page/page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PageComponent,
    PageSummaryComponent
  ],
  exports: [
    PageComponent,
    PageSummaryComponent
  ]
})
export class PaginationModule {}
