import { Component } from '@angular/core';

@Component({
  selector: 'app-sidebar-header',
  templateUrl: './app-sidebar-header.component.html',
  styleUrls: ['./app-sidebar-header.component.scss']
})
export class AppSidebarHeaderComponent { }
