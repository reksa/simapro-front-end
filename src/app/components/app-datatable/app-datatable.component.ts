import { RowData } from './datatables-content.model';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-datatable',
  templateUrl: './app-datatable.component.html',
  styleUrls: ['./app-datatable.component.scss']
})
export class AppDatatableComponent implements OnInit {
  @Input('columns') columns: string[] = [];
  @Input('data') data = new ReplaySubject<RowData[]>(1);
  @Input('actions') actions: string[] = [];
  @Output('show') show = new EventEmitter();
  @Output('edit') edit = new EventEmitter();
  @Output('delete') delete = new EventEmitter();
  @Output('validate') validate = new EventEmitter();
  @Output('approve') approve = new EventEmitter();

  dataStore: RowData[] = [];
  dataToShow: RowData[] = [];
  filter = '';
  page = 1;
  dataPerPage = 5;
  countFiltered = 0;
  from = 0;
  to = 0;
  pages = [];
  totalPages = 0;
  constructor() { }

  ngOnInit() {
    this.data.asObservable().subscribe(data => {
      this.dataStore = data;
      this.filterData();
    });
  }

  filterData() {
    const filter = this.filter.toLowerCase();
    const regexp = new RegExp(filter);
    this.dataToShow = [];
    this.countFiltered = 0;

    this.dataStore.map((rowData: RowData) => {
      const row = rowData.row;
      for (let i = 0; i < row.length; i++) {
        if (regexp.test(row.toString().toLowerCase())) {
          this.dataToShow.push(rowData);
          this.countFiltered++;
          break;
        }
      }
    });

    this.from = ((this.page - 1) * this.dataPerPage) + 1;
    this.to = (this.page * this.dataPerPage) <= this.countFiltered ? (this.page * this.dataPerPage) : this.countFiltered;
    this.dataToShow = this.dataToShow.slice(this.from - 1, this.to);
    this.generatePages();
  }

  filterDataP() {
    this.page = 1;
    this.filterData();
  }

  generatePages(): any {
    const show = 6;
    const halfShow = 3;
    const totalPages = Math.ceil(this.countFiltered / this.dataPerPage);
    this.totalPages = totalPages;
    const pages = [];
    if (totalPages > show) {
      const lastShown = (this.page + show - halfShow - 1);
      if (this.page > halfShow && totalPages > lastShown) {
        if (this.page - halfShow > 1) {
          pages.push({pageNumber: (this.page - halfShow - 1), text: '..'});
        }
        for (let i = (this.page - halfShow); i <= lastShown; i++) {
          pages.push({pageNumber: i, text: i});
        }
        if (lastShown < totalPages) {
          pages.push({pageNumber: (lastShown + 1), text: '..'});
        }
      } else if (this.page - halfShow > 0) {
        for (let i = totalPages ; i > (totalPages - show); i--) {
          pages.unshift({pageNumber: i, text: i});
        }
        pages.unshift({pageNumber: (totalPages - show), text: '..'});
      } else {
        for (let i = 1; i <= show; i++) {
          pages.push({pageNumber: i, text: i});
        }
        pages.push({pageNumber: (show + 1), text: '..'});
      }
    } else {
      for (let i = 1; i <= totalPages; i++) {
        pages.push({pageNumber: i, text: i});
      }
    }
    this.pages =  pages;
  }

  private getPages() {
    this.pages = [];
    const length = Math.ceil(this.countFiltered / this.dataPerPage);
    for (let i = 0; i < length; i++) {
      this.pages[i] = i + 1;
    }
  }

  setPage(page) {
    this.page = page;
    this.filterData();
  }

  lastPage() {
    this.page = this.totalPages;
    this.filterData();
  }

  action(type: string, id: number, nama?: string) {
    switch (type) {
      case 'show' : this.show.emit(id);
        break;
      case 'edit' : this.edit.emit(id);
        break;
      case 'delete' : this.delete.emit(id);
        break;
      case 'validate' : this.validate.emit(id);
        break;
      case 'approve' : this.approve.emit(id);
        break;
      default: return false;
    }
  }

}
