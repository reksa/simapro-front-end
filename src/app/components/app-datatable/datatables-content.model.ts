import { ReplaySubject } from 'rxjs/ReplaySubject';

export class DatatablesContent {
  columns: string[] = [];
  data: ReplaySubject<RowData[]> = new ReplaySubject<RowData[]>(1);
  actions: string[] = [];
}

export class RowData {
  row: any[];
  id: number
}
