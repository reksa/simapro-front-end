import { AppDatatableComponent } from './app-datatable.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    AppDatatableComponent
  ],
  exports: [
    AppDatatableComponent
  ]
})
export class AppDatatableModule { }
