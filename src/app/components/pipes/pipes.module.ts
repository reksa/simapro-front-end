import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextToSlugPipe } from '../../services/text-to-slug.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TextToSlugPipe],
  exports: [TextToSlugPipe]
})
export class PipesModule { }
