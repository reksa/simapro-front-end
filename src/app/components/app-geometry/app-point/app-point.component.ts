import { Position } from 'geojson';
import { FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';
import { LatLng } from 'leaflet';

@Component({
  selector: 'app-point',
  templateUrl: './app-point.component.html',
  styleUrls: ['./app-point.component.scss']
})
export class AppPointComponent {
  @Input('group') group: FormGroup;
  @Input('point') point: LatLng;
}
