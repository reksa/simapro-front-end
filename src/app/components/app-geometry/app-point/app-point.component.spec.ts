import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPointComponent } from './app-point.component';

describe('AppPointComponent', () => {
  let component: AppPointComponent;
  let fixture: ComponentFixture<AppPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
