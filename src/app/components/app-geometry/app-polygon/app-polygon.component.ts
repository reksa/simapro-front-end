import { FormGroup } from '@angular/forms';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { LatLng } from 'leaflet';

@Component({
  selector: 'app-polygon',
  templateUrl: './app-polygon.component.html',
  styleUrls: ['./app-polygon.component.scss']
})
export class AppPolygonComponent {
  @Input('group') group: FormGroup;
  @Input('polygon') polygon: LatLng[];
  @Output('onRemove') onRemove = new EventEmitter();

  removePoint(index) {
    this.onRemove.emit(index);
  }
}
