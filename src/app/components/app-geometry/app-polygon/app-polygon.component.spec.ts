import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPolygonComponent } from './app-polygon.component';

describe('AppPolygonComponent', () => {
  let component: AppPolygonComponent;
  let fixture: ComponentFixture<AppPolygonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppPolygonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPolygonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
