import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppPolygonComponent } from './app-polygon/app-polygon.component';
import { AppPointComponent } from './app-point/app-point.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AppPolygonComponent, AppPointComponent],
  exports: [AppPolygonComponent, AppPointComponent]
})
export class AppGeometryModule { }
