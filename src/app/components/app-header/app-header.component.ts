import { environment } from "./../../../environments/environment";
import { Component, OnInit } from "@angular/core";
import { navigation } from "app/_nav";
import { Pengguna, Role } from "../../models";
import {
  PenggunaService,
  NotifikasiService
} from "../../services/data-services";
import { JwtService } from "../../services/jwt.service";

@Component({
  selector: "app-header",
  templateUrl: "./app-header.component.html",
  styleUrls: ["./app-header.component.css"]
})
export class AppHeaderComponent implements OnInit {
  public navigation = navigation;
  public user: Pengguna = new Pengguna();
  public menus: NavMenu[];
  public permissions: string[] = [];
  public unread = 0;
  public foto;
  public uploadURL = environment.upload_url;
  public activeUrl;

  constructor(
    private penggunaService: PenggunaService,
    public notifikasiService: NotifikasiService,
    public jwtService: JwtService
  ) {
    this.penggunaService.getRoleForModule("all").subscribe(pengguna => {
      this.user = pengguna;
      this.foto = pengguna.foto
        ? this.uploadURL + "/pengguna/" + pengguna.foto
        : "assets/img/anonymous.png";
      this.user.role.map((role: Role) => {
        this.permissions = this.permissions.concat(role.permission_list);
      });
      this.menus = this._checkIfAllowed(navigation);
    });
  }

  ngOnInit() {
    this.notifikasiService.unread
      .asObservable()
      .subscribe(unread => (this.unread = unread));
  }

  private _checkIfAllowed(navMenus: NavMenu[]): NavMenu[] {
    const allowedMenus: NavMenu[] = [];
    navMenus.map((nav: NavMenu) => {
      if (nav.children && nav.children.length) {
        const child = this._checkIfAllowed(nav.children);
        if (child.length) {
          nav.children = child;
          allowedMenus.push(nav);
        }
      } else if (this.permissions.indexOf(nav.url) > -1) {
        allowedMenus.push(nav);
      }
    });
    return allowedMenus;
  }
}

class NavMenu {
  name: string;
  url: string;
  icon: string;
  children?: NavMenu[];
}
