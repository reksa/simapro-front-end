import { environment } from './../../../environments/environment';
import { Component } from '@angular/core';
import { NotifikasiService } from '../../services/data-services';
import { Router } from '@angular/router';
import { Notifikasi } from '../../models';

@Component({
  selector: 'app-aside',
  templateUrl: './app-aside.component.html'
})
export class AppAsideComponent {
  notifications: Notifikasi[] = [];
  fotoUrl = environment.upload_url + '/pengguna/'
  unread = 0;
  total = 0;

  constructor(
    private notifikasiService: NotifikasiService,
    private router: Router
  ) {
    this.notifikasiService.getNotification().subscribe(notifications => (this.notifications = notifications));
    this.notifikasiService.unread.asObservable().subscribe(unread => (this.unread = unread));
    this.notifikasiService.total.asObservable().subscribe(total => (this.total = total));
    // this._getNotification();
    // this.notifikasiService.triggerGetLast.asObservable().subscribe((get: number) => {
    //   if (get) {
    //     get = get < 10 ? get : 10;
    //     this._getNotification(0, get);
    //   }
    // })
  }

  // private _getNotification(start?: number, limit?: number) {
  //   this.notifikasiService.get(start, limit).subscribe( notifications => {
  //     this.notifikasiService.start += this.notifikasiService.limit;
  //     this.notifications = notifications;
  //   });
  // }

  _showRelated(notifikasi) {
    const url = this.notifikasiService.getUrl(notifikasi);
    if (url) {
      this.router.navigate([url]);
    }
    return false;
  }

  public loadMoreNotifications() {
    this.notifikasiService.getMore();
  }

}
