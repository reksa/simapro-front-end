import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppListErrorsComponent } from './app-list-errors.component';

describe('ListErrorsComponent', () => {
  let component: AppListErrorsComponent;
  let fixture: ComponentFixture<AppListErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppListErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppListErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
