import { Component, Input } from '@angular/core';

import { Errors } from 'app/models/errors.model';

@Component({
  selector: 'app-list-errors',
  templateUrl: './app-list-errors.component.html'
})
export class AppListErrorsComponent {
  formattedErrors: Array<string> = [];

  @Input()
  set errors(errorList: Errors) {
    this.formattedErrors = [];

    if (errorList.errors) {
      // tslint:disable-next-line:forin
      for (const field in errorList.errors) {
        this.formattedErrors.push(`${field} ${errorList.errors[field]}`);
      }
    }
  };

  get errorList() { return this.formattedErrors; }


}
