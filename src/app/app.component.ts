import { Component, OnInit } from '@angular/core';
import { PenggunaService } from './services/data-services';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(
    private authService: PenggunaService
  ) {}

  ngOnInit() {
    this.authService.populate();
  }
}
