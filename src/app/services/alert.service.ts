import { Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertService {
  // private alertSource = new Subject<AlertMessage>();

  // alerts = this.alertSource.asObservable();

  // add(alert: AlertMessage) {
  //   this.alertSource.next(alert);
  // }

  // delete(index: string) {
  //   // this.alertSource.
  //   // this.missionConfirmedSource.next(astronaut);
  // }
  private subject = new Subject<AlertMessage>();
  private keepAfterRouteChange = false;

  constructor(
    router: Router
  ) {
      // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
      router.events.subscribe(event => {
          if (event instanceof NavigationStart) {
              if (this.keepAfterRouteChange) {
                  // only keep for a single route change
                  this.keepAfterRouteChange = false;
              } else {
                  // clear alert messages
                  this.clear();
              }
          }
      });
  }

  getAlert(): Observable<any> {
      return this.subject.asObservable();
  }

  alert(alert: AlertMessage, keepAfterRouteChange = false) {
      // this.keepAfterRouteChange = keepAfterRouteChange;
      this.subject.next(alert);
  }

  clear() {
      // clear alerts
      this.subject.next();
  }
}

export interface AlertMessage {type: string, msg: string}
