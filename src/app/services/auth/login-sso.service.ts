import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Injectable } from "@angular/core";
import { PenggunaService, ApiService, AuthReply } from "../data-services";
import { interval } from 'rxjs/observable/interval';
import { Subscription, Observable } from "rxjs";

@Injectable()
export class LoginSSOService {
  constructor(
    private penggunaService: PenggunaService,
    private apiService: ApiService
  ) {}

  saveSession(token: string) {
    if(this.penggunaService.setAuth(token)) {
      window.localStorage['sessionUpdated'] = true;
    }
  }

  watchSession(): Subscription {
    const i = interval(1000);
    return i.subscribe(() => {
      if(window.localStorage['sessionUpdated'] && window.localStorage['sessionUpdated'] === 'true') {
        window.localStorage['sessionUpdated'] = false;
        window.location.reload();
      }
    });
  }

  getSession(code: string): Observable<AuthReply> {
    const credentials = {
      code: code
    }
    return this.apiService
      .post('/auth/login-with-iam-pln', credentials)
      .map((data: AuthReply) => data)
      .catch(AppErrorHandlerService.handle)
  }

}