import { ReplaySubject } from 'rxjs/ReplaySubject';
import { PermissionService } from './../data-services/permission.service';
// import { PenggunaService } from './../data-services/pengguna.service';
// import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { LocationStrategy } from '@angular/common';

@Injectable()
export class PermissionGuard implements CanActivate {
  isPermitted = new ReplaySubject<boolean>();
  constructor(
    private router: Router,
    private permissionService: PermissionService,
    private url: LocationStrategy
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    // let isAuthorized = true;

    // Check if the user is authenticated & authorized
    // this.authService.isAuthenticated.take(1).subscribe((isLoggedIn) => {
    //   const page = btoa(state.url);
    //   // isAuthorized = (isLoggedIn && this.authService.getCurrentUser().canAccess(page));
    //   // this.authService.isAuthorized(page).subscribe( isAuthorized => {
    //   //   // If the user is not authenticated or not authorized, redirect to Error 403 Page
    //   //   // if (! isAuthorized) {
    //   //   //     this.router.navigate(['/error/403']);
    //   //   // }
    //   // });
    // });

    // If the user is not authenticated or not authorized, redirect to Error 403 Page
    // if (! isAuthorized) {
    //     this.router.navigate(['/error/403']);
    // }
    let url = state.url;

    // Remove Fragment & Query parameters
    const end = url.indexOf('?') > -1 ? url.indexOf('?') :
        ( url.indexOf('#') > -1 ? url.indexOf('#') : url.length);
    url =  url.substring(0, end);

    this.permissionService.checkPermission(url).subscribe(res => {
      if(!res) {
        // alert('Anda tidak memiliki akses!');
        this.router.navigate(['error/403']);
      }
      this.isPermitted.next(res);
    });

    return this.isPermitted.asObservable();

  }
}
