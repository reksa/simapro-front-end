import { JwtHelper } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';

import { ApiService } from './../data-services/api.service';
import { JwtService } from './../jwt.service';


@Injectable()
export class AuthService {
  private currentUserSubject = new BehaviorSubject<AuthenticatedUser>(new AuthenticatedUser());
  public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
  private path = '/authenticated-user/';

  constructor (
    private apiService: ApiService,
    private http: Http,
    private jwtService: JwtService
  ) {}

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  populate() {
    // If JWT detected, attempt to get & store user's info
    const token = this.jwtService.getToken();
    const jwtHelper = new JwtHelper();
    if (token && !jwtHelper.isTokenExpired(token)) {
      const user = jwtHelper.decodeToken(token);
      user['token'] = token;
      this.setAuth(user);
      // this.apiService.get(this.path)
      // .subscribe(
      //   data => this.setAuth(data.user),
      //   err => this.purgeAuth()
      // );
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
  }

  setAuth(user: AuthenticatedUser) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.token);
    // Set current user data into observable
    this.currentUserSubject.next(new AuthenticatedUser(user));
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
    // this.isAuthenticated = true;
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next(new AuthenticatedUser());
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
    // this.isAuthenticated = false;
  }

  attemptAuth(type, credentials): Observable<AuthenticatedUser> {
    const route = (type === 'login') ? '/auth/login' : '';
    return this.apiService.post(route, credentials)
    .map(
      (data) => {
        this.setAuth(data);
        return data;
      }
    );
  }

  getCurrentUser(): AuthenticatedUser {
    return this.currentUserSubject.value;
  }

  // Update the user on the server (email, pass, etc)
  update(user): Observable<AuthenticatedUser> {
    return this.apiService
    .put('/user', { user })
    .map(data => {
      // Update the currentUser observable
      this.currentUserSubject.next(new AuthenticatedUser(data.user));
      return data.user;
    });
  }

  isAuthorized(url: string): Observable<boolean> {
    return this.apiService
      .get('/is-authorize')
      .map( data => {
        return data.result;
      });
  }
}

class AuthenticatedUser {
  public username: string;
  public name: string;
  public id: number;
  public foto: number;
  public superuser: false;
  private access: any[];
  public token: string;

  constructor(user?) {
    user = typeof user === 'undefined' ? {} : user;
    this.username = user.username;
    this.name = user.username;
    this.id = user.username;
    this.foto = user.foto;
    this.superuser = user.username;
    this.access = user.username;
    this.token = user.username;
  }

  canAccess(id) {
    return true; // (this.access.indexOf(id) !== -1);
  }
}
