import { AlertService } from './../alert.service';
import { Observable } from 'rxjs/Observable';
import { OnProgressService } from './../on-progress.service';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ImportTanahComponent } from '../../views/import/import-tanah/import-tanah.component';

@Injectable()
export class OnProgressGuardService implements CanDeactivate<ImportTanahComponent> {

  constructor(
    private onProgressService: OnProgressService,
    private alertService: AlertService
  ) { }

  canDeactivate(
    component: ImportTanahComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> {
    return this.onProgressService.isOnProgress.take(1).map(onProgress => {
      if (onProgress) {
        this.alertService.alert({
          type: 'info',
          msg: 'Sistem sedang memproses permintaan'
        })
      }
      return !onProgress;
    });
  }
}
