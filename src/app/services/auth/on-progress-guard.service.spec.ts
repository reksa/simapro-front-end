import { TestBed, inject } from '@angular/core/testing';

import { OnProgressGuardService } from './on-progress-guard.service';

describe('OnProgressGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnProgressGuardService]
    });
  });

  it('should be created', inject([OnProgressGuardService], (service: OnProgressGuardService) => {
    expect(service).toBeTruthy();
  }));
});
