import { ReplaySubject } from 'rxjs/ReplaySubject';
import { PermissionService } from './../data-services/permission.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class EditPermissionGuard implements CanActivate {
  isPermitted = new ReplaySubject<boolean>();
  constructor(
    private router: Router,
    private permissionService: PermissionService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    let url = state.url;

    // Remove Fragment & Query parameters
    const end = url.indexOf('?') > -1 ? url.indexOf('?') :
        ( url.indexOf('#') > -1 ? url.indexOf('#') : url.length);
    url =  url.substring(0, end);

    this.permissionService.checkEditPermission(url).subscribe(res => {
      if(!res) {
        this.router.navigate(['error/403']);
      }
      this.isPermitted.next(res);
    });

    return this.isPermitted.asObservable();

  }
}
