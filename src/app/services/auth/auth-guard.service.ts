import { PenggunaService } from 'app/services/data-services';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: PenggunaService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {

    let isAuthenticated = false;

    // Check if the user is authenticated
    this.authService.isAuthenticated.take(1).subscribe((isLoggedIn) => {
      isAuthenticated = isLoggedIn;
    });

    // If the user is not authenticated, redirect to Login page
    if (! isAuthenticated) {
        this.router.navigate(['/auth/login'], {queryParams : {ref : state.url}});
    }

    return this.authService.isAuthenticated.take(1);

  }
}
