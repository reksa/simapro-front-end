import { TestBed, inject } from '@angular/core/testing';

import { ObjectComparatorService } from './object-comparator.service';

describe('ObjectComparatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObjectComparatorService]
    });
  });

  it('should be created', inject([ObjectComparatorService], (service: ObjectComparatorService) => {
    expect(service).toBeTruthy();
  }));
});
