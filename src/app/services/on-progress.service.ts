import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class OnProgressService {
    onProgress = new ReplaySubject<boolean>(1);
    isOnProgress = this.onProgress.asObservable();

    constructor() {
        this.onProgress.next(false);
    }

}