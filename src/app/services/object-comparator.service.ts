export class ObjectComparator {

  static compare(obj1: object, obj2: object, id: string = 'id'): boolean {
    return obj1[id] === obj2[id];
  }

  static indexOf(array: object[], obj2: object, id: string = 'id'): number {
    for (let i = 0; i < array.length; i++) {
      if (array[i][id] === obj2[id]) {
        return i;
      }
    }
    return -1;
  }

  static findById(array: Object[], id: any, idName = 'id' ) {
    for (let i = 0; i < array.length; i++) {
      if (array[i][idName] === id) {
        return array[i];
      }
    }
    return null;
  }

}
