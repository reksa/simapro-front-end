import { Injectable } from '@angular/core';

@Injectable()
export class OptionButtonService {
  style = {
    edit: {
      class: 'btn btn-warning',
      label: 'Ubah',
      icon: '',
      onClickAction: 'edit({id})'
    },
    delete: {
      class: 'btn btn-danger',
      label: 'Hapus',
      icon: '',
      onClickAction: 'delete({id})'
    },
    view: {
      class: 'btn btn-info',
      label: 'Lihat',
      icon: '',
      onClickAction: 'show({id})'
    }
  };
  options = {
    label: true,
    icon: false,
    size: ''
  };

  constructor() { }

  generate(type: string, id: number | string) {
    const action = this.style[type].onClickAction;
    action.replace('{id}', (typeof id === 'number' ? id : `'${id}'`));
    console.log(action, (typeof id === 'number' ? id : `'${id}'`));
    const buttons = `<button class="${this.style[type].class}" (click)="${action}">
      ${this.style[type].label}</button>`;
    return buttons;
  }

}
