import { Pipe, PipeTransform } from '@angular/core';
/*
 * Transform text to url friendly text
*/
@Pipe({name: 'slug'})
export class TextToSlugPipe implements PipeTransform {
  transform(Text: string): string {
    return Text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
  }
}
