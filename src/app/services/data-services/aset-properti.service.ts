import { _Properti } from './../../models/_properti.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { AsetProperti, AsetPropertiDetail, NilaiAsetProperti, PerpindahanAsetProperti } from 'app/models';
import { AppHttpResponse } from 'app/models/app-http-respose.model';

@Injectable()
export class AsetPropertiService {
  private path = '/properti';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(params: URLSearchParams): Observable<AsetProperti[]> {
    return this.apiService.get(this.path, params)
      .map((asetPropertis: AsetProperti[]) => asetPropertis)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-properti-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-properti-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginatePenerimaan(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/penerimaan', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<AsetProperti> {
    return this.apiService.get(this.path + '/' + id)
      .map(asetProperti => asetProperti)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<AsetProperti> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(asetProperti: AsetProperti): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    if (asetProperti.id_properti) {
      return this.apiService.post(this.path + '/revisi/' + asetProperti.id_properti, asetProperti)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, asetProperti)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  saveDataStatis(asetProperti: AsetProperti) {
    return this.apiService.put(this.path + '/update-data-statis/' + asetProperti.id_properti, asetProperti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveDetail(asetPropertiDetail: AsetPropertiDetail) {
    // asetPropertiDetail.path = '/aset-properti/edit/1/edit';
    return this.apiService.post(this.path + '/add-detail/' + asetPropertiDetail.id_properti, asetPropertiDetail)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveNilaiAset(nilaiAsetProperti: NilaiAsetProperti) {
    // nilaiAsetProperti.path = '/aset-properti/edit/1/edit';
    return this.apiService.post(this.path + '/add-nilai/' + nilaiAsetProperti.id_properti, nilaiAsetProperti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveMutasi(perpindahanAsetProperti: PerpindahanAsetProperti) {
    // perpindahanAsetProperti.path = '/aset-properti/edit/1/edit';
    return this.apiService.post(this.path + '/add-perpindahan/' + perpindahanAsetProperti.id_properti, perpindahanAsetProperti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiDetail(asetPropertiDetail: AsetPropertiDetail) {
    // asetPropertiDetail.path = '/aset-properti/revisi/1/edit';
    return this.apiService.put(this.path + '/revisi-detail/' + asetPropertiDetail.id_properti, asetPropertiDetail)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiNilaiAset(nilaiAsetProperti: NilaiAsetProperti, isExternal?: boolean) {
    // nilaiAsetProperti.path = '/aset-properti/revisi/1/edit';
    if(isExternal) {
      nilaiAsetProperti.path += '/eksternal';
    }
    return this.apiService.put(this.path + '/revisi-nilai/' + nilaiAsetProperti.id_properti, nilaiAsetProperti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiMutasi(perpindahanAsetProperti: PerpindahanAsetProperti) {
    // perpindahanAsetProperti.path = '/aset-properti/revisi/1/edit';
    return this.apiService.put(this.path + '/revisi-perpindahan/' + perpindahanAsetProperti.id_properti, perpindahanAsetProperti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  destroy(id): Observable<AppHttpResponse> {
    const params = {
      path: '/aset-properti/show/1/delete'
    }
    return this.apiService.post(this.path + '/' + id, params)
      .catch(AppErrorHandlerService.handle);
  }

  validasi(idAsetProperti: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi/` + idAsetProperti, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiDetail(idAsetPropertiDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-detail/` + idAsetPropertiDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiNilai(idAsetPropertiDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-nilai/` + idAsetPropertiDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiPerpindahan(idAsetPropertiDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-perpindahan/` + idAsetPropertiDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  import(properti: _Properti[]) {
    return this.apiService.post(this.path + '/import', properti)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  };

  // tolakUsulanDetail(idAsetPropertiDetail: number, memo, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/tolak-validasi-detail-${asValidator}/` + idAsetPropertiDetail, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanDetail(idAsetPropertiDetail: number, memo, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/terima-validasi-detail-${asValidator}/` + idAsetPropertiDetail, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // tolakUsulanMutasi(idPerpindahanProperti: number, memo, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/tolak-validasi-perpindahan-${asValidator}/` + idPerpindahanProperti, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanMutasi(idPerpindahanProperti: number, memo, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/terima-validasi-perpindahan-${asValidator}/` + idPerpindahanProperti, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // tolakUsulanNilai(idNilai: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/nilai-aset-properti/action/tolak-validasi-properti-${asValidator}/` + idNilai, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanNilai(idNilai: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/nilai-aset-properti/action/terima-validasi-properti-${asValidator}/` + idNilai, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

}
