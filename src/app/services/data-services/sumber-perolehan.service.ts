import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { SumberPerolehan } from 'app/models';

@Injectable()
export class SumberPerolehanService {
  private path = '/sumber-perolehan';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<SumberPerolehan[]> {
    return this.apiService.get(this.path)
      .map((sumberPerolehan: SumberPerolehan[]) => sumberPerolehan)
      .catch(AppErrorHandlerService.handle);
  }

}
