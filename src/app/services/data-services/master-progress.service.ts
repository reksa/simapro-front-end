import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Rx';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { MasterProgress } from 'app/models';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class MasterProgressService {
  private path = '/master-progress';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(params?: URLSearchParams): Observable<MasterProgress[]> {
    return this.apiService.get(this.path, params)
      .map((masterProgress: MasterProgress[]) => masterProgress)
      .catch(AppErrorHandlerService.handle);
  }

}
