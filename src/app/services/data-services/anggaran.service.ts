import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Anggaran, AnggaranDetail, AnggaranPekerjaan } from 'app/models/anggaran.model';

@Injectable()
export class AnggaranService {
  private path = '/anggaran';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(params?): Observable<Anggaran[]> {
    return this.apiService.get(this.path, params)
      .map((anggarans: Anggaran[]) => anggarans)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-anggaran-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-anggaran-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Anggaran> {
    return this.apiService.get(this.path + '/' + id)
      .map(anggaran => anggaran)
      .catch(AppErrorHandlerService.handle);
  }

  getAlokasi(id: number): Observable<AnggaranPekerjaan[]> {
    return this.apiService.get(this.path + '/alokasi/' + id)
      .map(alokasi => alokasi)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Anggaran> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  /*

		Route::post('', 'AnggaranCtrl@store');
		Route::post('action/validasi/{id}', 'AnggaranCtrl@validasiAnggaran');
		Route::post('revisi/{id}', 'AnggaranCtrl@revisi');
		Route::post('approve/{id}', 'AnggaranCtrl@approve');
		Route::get('{id}', 'AnggaranCtrl@show');
	});
  */

  save(anggaran: Anggaran): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    // if (anggaran.id_anggaran) {
    //   return this.apiService.put(this.path + '/' + anggaran.id_anggaran, anggaran)
    //     .map(data => data)
    //     .catch(AppErrorHandlerService.handle);

    // // Otherwise, create a new menu
    // } else {
      return this.apiService.post(this.path, anggaran)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    // }
  }

  revisi(anggaran: Anggaran): Observable<{success: boolean, message?: string}> {
    return this.apiService.post(this.path + '/revisi/' + anggaran.id_anggaran, anggaran)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  approve(anggaranDetail: AnggaranDetail): Observable<{success: boolean, message?: string}> {
    return this.apiService.post(this.path + '/approve/' + anggaranDetail.id_anggaran_detail, anggaranDetail)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasi(id: number, memo: string, selectedOption: boolean): Observable<{success: boolean, message?: string}> {
    const params = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + '/action/validasi/' + id, params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  destroy(id): Observable<Anggaran> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
