import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { JenisAsetProperti } from 'app/models';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';

@Injectable()
export class JenisPropertiService {
  private path = '/jenis-aset-properti';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<JenisAsetProperti[]> {
    return this.apiService.get(this.path)
      .map((jenisPropertis: JenisAsetProperti[]) => jenisPropertis)
      .catch(AppErrorHandlerService.handle);
  }
}
