import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Headers, Response, URLSearchParams, Http, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { JwtService } from './../jwt.service';

import { environment } from '../../../environments/environment';

import { AccessToken } from 'app/models/access-token.model';

@Injectable()
export class ApiService {
  constructor(
    private http: Http,
    private jwtService: JwtService,
    private router: Router
  ) {}

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
    if (error && error.status && error.status === 401) {
      return Observable.of(error);
    }
    return Observable.throw(error);
  }

  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    return this.http.get(`${environment.webservice_url}${path}`, { headers: this.setHeaders(), search: params })
    .catch(this.formatErrors)
    .map((res: Response) => {
      this._checkToken();
      this._isUnAuthenticated(res);
      return res.json();
    });
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${environment.webservice_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map((res: Response) => {
      this._checkToken();
      this._isUnAuthenticated(res);
      return res.json();
    });
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${environment.webservice_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map((res: Response) => {
      this._checkToken();
      this._isUnAuthenticated(res);
      return res.json();
    });
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.webservice_url}${path}`,
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map((res: Response) => {
      this._checkToken();
      this._isUnAuthenticated(res);
      return res.json();
    });
  }

  download(path, params) {
    return this.http.get(
      `${environment.webservice_url}${path}`, {
        responseType: ResponseContentType.Blob,
        search: params,
        headers: this.setHeaders()
      })
      .catch(this.formatErrors)
      .map(res => res);
  }

  private _checkToken() {
    if (this.jwtService.isNearlyExpired()) {
      this.jwtService.tokenNearlyExpired.next(true);
    }
  }

  private _isUnAuthenticated(error) {
    if (error.status && error.status === 401) {
      this.router.navigate(['auth/logout']);
      return Observable.throw(error);
    }
  }
}
