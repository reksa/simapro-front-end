import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { JenisDayaGuna } from 'app/models';

@Injectable()
export class JenisDayaGunaService {
  private path = '/jenis-daya-guna';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<JenisDayaGuna[]> {
    return this.apiService.get(this.path)
      .map((jenisDayaGunas: JenisDayaGuna[]) => jenisDayaGunas)
      .catch(AppErrorHandlerService.handle);
  }
}
