import { _PendayagunaanAset } from './../../models/_pendayagunaan-aset.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { AsetProperti, AsetTanah, PendayagunaanAset } from 'app/models';
import { AppHttpResponse } from 'app/models/app-http-respose.model';

@Injectable()
export class PendayagunaanAsetService {
  private path = '/pendayagunaan';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<PendayagunaanAset[]> {
    return this.apiService.get(this.path)
      .map((pendayagunaanAsets: PendayagunaanAset[]) => pendayagunaanAsets)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-dayaguna-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-dayaguna-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<PendayagunaanAset> {
    return this.apiService.get(this.path + '/' + id)
      .map(pendayagunaanAset => pendayagunaanAset)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<PendayagunaanAset> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(aset: | AsetTanah | AsetProperti): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    // if (aset.jenis_aset === 'App') {
    //   return this.apiService.put(this.path + pendayagunaanAset.id, pendayagunaanAset)
    //     .map(data => data)
    //     .catch(AppErrorHandlerService.handle);

    // // Otherwise, create a new menu
    // } else {

    /**
     * 
                $jenisDayaguna = JenisDayaguna::find($request->id_jenis_dayaguna[$i]);
                
                $pendayagunaan = new Pendayagunaan();
                $pendayagunaan->nama_pendayagunaan = $request->nama_pendayagunaan;
                $pendayagunaan->kontrak = $request->kontrak[$i];
                $pendayagunaan->nilai_pendayagunaan = $request->nilai_pendayagunaan[$i];
                $pendayagunaan->keterangan = $request->keterangan[$i];
                $pendayagunaan->start_date_pendayagunaan = $request->start_date_pendayagunaan[$i];
                $pendayagunaan->end_date_pendayagunaan = $request->end_date_pendayagunaan[$i];
                $pendayagunaan->id_status_validasi = $statusValidasi;
     */
      const obj = {
        pendayagunaanable_id: aset.id,
        // @ts-ignore
        jenis: aset.id_properti ? 'Properti' : 'Tanah',
        nama_pendayagunaan: aset.nama_pendayagunaan,
        dayaguna: aset.dayaguna,
        path: aset.path
      }

      return this.apiService.post(this.path, obj)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    // }
  }

  update(pendayagunaanAset: PendayagunaanAset): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    // if (aset.jenis_aset === 'App') {
      return this.apiService.post(this.path + '/revisi/' + pendayagunaanAset.id_pendayagunaan, pendayagunaanAset)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
  }

  destroy(pendayagunaanAset: PendayagunaanAset): Observable<AppHttpResponse> {
    const params = {
      path: pendayagunaanAset.path
    }
    return this.apiService.put(this.path + '/' + pendayagunaanAset.id_pendayagunaan, params)
      .catch(AppErrorHandlerService.handle);
  }

  validasi(idPendayagunaanAset: number, decision: boolean, memo: string) {
    const args = {
      path: '/pendayagunaan-aset/show/1/show',
      memo: memo,
      is_terima: decision
    }
    return this.apiService.post(this.path + `/action/validasi/` + idPendayagunaanAset, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  import(pendayagunaan: _PendayagunaanAset[]) {
    return this.apiService.post(this.path + '/import', pendayagunaan)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  };

  // tolakUsulanPendayagunaan(idPendayagunaanAset: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/tolak-validasi-dayaguna-${asValidator}/` + idPendayagunaanAset, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanPendayagunaan(idPendayagunaanAset: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/terima-validasi-dayaguna-${asValidator}/` + idPendayagunaanAset, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

}
