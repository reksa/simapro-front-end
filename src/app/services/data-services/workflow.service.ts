import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Workflow } from 'app/models';

@Injectable()
export class WorkflowService {
  private path = '/workflow';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Workflow[]> {
    return this.apiService.get(this.path)
      .map((workflows: Workflow[]) => workflows)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Workflow> {
    return this.apiService.get(this.path + '/' + id)
      .map(workflow => workflow)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Workflow> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }
  
  isUnique(field: {}): Observable<{success: boolean, message: string, unique: boolean}> {
    return this.apiService.post(this.path + '/uniqueness', field)
      .map(workflow => workflow)
      // .catch(AppErrorHandlerService.handle);
  }

  save(workflow: Workflow): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (workflow.id_workflow) {
      return this.apiService.put('/role-workflow' + '/' + workflow.id_workflow, workflow)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post('/role-workflow', workflow)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
