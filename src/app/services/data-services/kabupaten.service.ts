import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Kabupaten } from 'app/models/kabupaten.model';

@Injectable()
export class KabupatenService {
  private path = '/kabupaten';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Kabupaten[]> {
    return this.apiService.get(this.path)
      .map((kabupatens: Kabupaten[]) => kabupatens)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Kabupaten> {
    return this.apiService.get(this.path + '/' + id)
      .map(kabupaten => kabupaten)
      .catch(AppErrorHandlerService.handle);
  }

  getByIdProvinsi(id: number): Observable<Kabupaten[]> {
    const params = new URLSearchParams();
    params.append('id_provinsi', id.toString());
    return this.apiService.get(this.path, params)
      .map(kabupaten => kabupaten)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Kabupaten> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(kabupaten: Kabupaten): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (kabupaten.id_kabupaten) {
      return this.apiService.put(this.path + '/' + kabupaten.id_kabupaten, kabupaten)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, kabupaten)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
