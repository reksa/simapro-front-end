import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Provinsi } from 'app/models/provinsi.model';
import { AppHttpResponse } from '../../models/app-http-respose.model';

@Injectable()
export class ProvinsiService {
  private path = '/provinsi';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Provinsi[]> {
    return this.apiService.get(this.path)
      .map((provinsis: Provinsi[]) => provinsis)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Provinsi> {
    return this.apiService.get(this.path + '/' + id)
      .map(provinsi => provinsi)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Provinsi> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(provinsi: Provinsi): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (provinsi.id_provinsi) {
      return this.apiService.put(this.path + '/' + provinsi.id_provinsi, provinsi)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, provinsi)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
