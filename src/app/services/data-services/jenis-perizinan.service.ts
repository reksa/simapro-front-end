import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { JenisPerizinan } from 'app/models';

@Injectable()
export class JenisPerizinanService {
  private path = '/izin';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<JenisPerizinan[]> {
    return this.apiService.get(this.path)
      .map((jenisPerizinan: JenisPerizinan[]) => jenisPerizinan)
      .catch(AppErrorHandlerService.handle);
  }

}
