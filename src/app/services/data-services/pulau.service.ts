import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Pulau } from 'app/models';

@Injectable()
export class PulauService {
  private path = '/pulau';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Pulau[]> {
    return this.apiService.get(this.path)
      .map((pulaus: Pulau[]) => pulaus)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Pulau> {
    return this.apiService.get(this.path + '/' + id)
      .map(pulau => pulau)
      .catch(AppErrorHandlerService.handle);
  }

  save(pulau: Pulau): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (pulau.id_pulau) {
      return this.apiService.put(this.path + '/' + pulau.id_pulau, pulau)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, pulau)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
