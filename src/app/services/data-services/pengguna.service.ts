import { interval } from "rxjs/observable/interval";
import { Injectable } from "@angular/core";
import { URLSearchParams } from "@angular/http";

import { PaginationData } from "app/components/pagination/page/page.component";

import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { ReplaySubject } from "rxjs/ReplaySubject";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { JwtHelper, tokenNotExpired } from "angular2-jwt";

import { AppErrorHandlerService } from "app/error-handler/app-error-handler.service";
import { ApiService } from "./api.service";
import { JwtService } from "./../jwt.service";
import { NotifikasiService } from "./notifikasi.service";

import { Pengguna } from "app/models";
import { AccessToken, DecodedToken } from "app/models/access-token.model";
import { AppHttpResponse } from "app/models/app-http-respose.model";

@Injectable()
export class PenggunaService {
  private currentUserSubject = new BehaviorSubject<Pengguna>(new Pengguna());
  public currentUser = this.currentUserSubject
    .asObservable()
    .distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
  private path = "/pengguna";

  private jwtHelper = new JwtHelper();

  constructor(
    private apiService: ApiService,
    private jwtService: JwtService,
    private errorHandler: AppErrorHandlerService,
    private notifikasiService: NotifikasiService
  ) {}

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  populate() {
    // If JWT detected, attempt to get & store user's info
    if (tokenNotExpired(this.jwtService.tokenName)) {
      this.setAuth(this.jwtService.getToken());
      this.jwtService.tokenNearlyExpired
        .asObservable()
        .subscribe(nearlyExpired => {
          if (nearlyExpired && !this.jwtService.tokenIsBeingRefreshed) {
            this.jwtService.tokenIsBeingRefreshed = true;
            this.refreshToken().subscribe(res => {
              if (res) {
                this.jwtService.tokenNearlyExpired.next(false);
                this.jwtService.tokenIsBeingRefreshed = false;
              } else {
                this.isAuthenticatedSubject.next(false);
              }
            });
          }
        });
    } else {
      // Remove any potential remnants of previous auth states
      if (this.notifikasiService.isSubscribed) {
        setTimeout(() => {
          window.location.reload();
        }, 100);
        this.notifikasiService.unsubscribe();
      }
      this.jwtService.destroyToken();
      this.isAuthenticatedSubject.next(false);
    }
  }

  setAuth(token: string): boolean {
    const jwtHelper = new JwtHelper();
    const decodedToken: DecodedToken = jwtHelper.decodeToken(token);
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(token);
    // Set current user data into observable
    this.currentUserSubject.next(decodedToken.pengguna);
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
    this.notifikasiService.subscribe();
    return true;
  }

  purgeAuth(): Observable<AppHttpResponse> {
    return this.apiService.post("/auth/logout").map(res => {
      if (res.success) {
        if (this.notifikasiService.isSubscribed) {
          setTimeout(() => {
            window.location.reload();
          }, 100);
          this.notifikasiService.unsubscribe();
        }
        // Remove JWT from localstorage
        this.jwtService.destroyToken();
        // Set current user to an empty object
        this.currentUserSubject.next(new Pengguna());
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
      }
      return res;
    });
  }

  attemptAuth(credentials): Observable<AppHttpResponse> {
    return this.apiService.post("/auth/login", credentials).map(data => {
      if (data.access_token) {
        const res = new AppHttpResponse();
        res.success = true;
        if (this.setAuth(data.access_token)) {
          return res;
        } else {
          res.success = false;
          res.message =
            "Gagal menyimpan session, silahkan ulangi proses login!";
          return res;
        }
      } else {
        return data;
      }
    });
  }

  getCurrentUser(): Pengguna {
    // return this.currentUserSubject.value;
    return this.jwtHelper.decodeToken(this.jwtService.getToken()).pengguna;
  }

  // Update the user on the server (email, pass, etc)
  update(user): Observable<Pengguna> {
    return this.apiService.put(this.path, { user }).map(data => {
      // Update the currentUser observable
      this.currentUserSubject.next(data.user);
      return data.user;
    });
  }

  getAll(params?: URLSearchParams): Observable<Pengguna[]> {
    return this.apiService
      .get(this.path, params)
      .map((users: Pengguna[]) => users)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService
      .get(this.path + "/paginate", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Pengguna> {
    return this.apiService
      .get(this.path + "/" + id)
      .map(user => user)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: object): Observable<Pengguna> {
    let params = "";
    for (const key in resource) {
      if (resource[key] !== null) {
        params += "/" + key + "/" + resource[key];
      }
    }
    return this.apiService
      .get(this.path + "/" + params)
      .map(res => res)
      .catch(AppErrorHandlerService.handle);
  }

  getFromLDAP(sameaccountname: string): Observable<any> {
    return this.apiService
      .post(this.path + "/cari-pengguna-ldap", { username: sameaccountname })
      .map((user: any) => user)
      .catch(AppErrorHandlerService.handle);
  }

  save(user: Pengguna): Observable<AppHttpResponse> {
    // If we're updating an existing user
    if (user.id_pengguna) {
      return this.apiService
        .put(this.path + "/" + user.id_pengguna, user)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

      // Otherwise, create a new user
    } else {
      return this.apiService
        .post(this.path, user)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  saveAll(users: Pengguna[], args?: any): Observable<AppHttpResponse> {
    const params = { users: users, args: args };
    return this.apiService
      .put(this.path + "/all", params)
      .map(result => result)
      .catch(AppErrorHandlerService.handle);
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService
      .delete(this.path + "/" + id)
      .catch(AppErrorHandlerService.handle);
  }

  uploadFoto(fileToUpload: any) {
    const input = new FormData();
    input.append("file", fileToUpload);

    return this.apiService.post("/api/upload-file", input);
  }

  refreshToken(): Observable<boolean> {
    return this.apiService
      .post("/auth/refresh")
      .map((accessToken: AuthReply) => {
        // return accessToken;
        return this.setAuth(accessToken.access_token);
      });
  }

  getRoleForModule(moduleName: string): Observable<Pengguna> {
    const params = new URLSearchParams();
    params.append("modul", moduleName);
    return this.apiService
      .get("/auth/self", params)
      .map(pengguna => pengguna)
      .catch(AppErrorHandlerService.handle);
  }

  getLogActivity(
    id: number,
    params: URLSearchParams
  ): Observable<PaginationData> {
    return this.apiService
      .get(`${this.path}/log/${id}`, params)
      .map(log => log)
      .catch(AppErrorHandlerService.handle);
  }
}

export class AuthReply {
  access_token: string;
  expires_in: number;
  token_type: string;
}
