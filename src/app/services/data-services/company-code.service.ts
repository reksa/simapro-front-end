import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { CompanyCode } from 'app/models';
import { Location } from '@angular/common';

@Injectable()
export class CompanyCodeService {
  private path = '/company-code';
    defaultParams = new URLSearchParams();
    
  constructor (
    private apiService: ApiService,
    private _location: Location
  ) {
    this.defaultParams.set('level_access', 'true');
  }

  getAll(params?: URLSearchParams, all?: boolean): Observable<CompanyCode[]> {
    // params = params || new URLSearchParams();
    if (!all) {
      params = new URLSearchParams();
      if(!params.get('path')) {
        let path = this._location.path();
        const indexOfQueryParametersSign = path.indexOf('?');
        const indexOfFragment = path.indexOf('#');
        path = indexOfQueryParametersSign > -1 ? path.substring(0, indexOfQueryParametersSign) : path;
        path = indexOfFragment > -1 ? path.substring(0, indexOfFragment) : path;
        params.append('path', path);
      }
      return this.apiService.get(this.path, params)
        .map((companyCodes: CompanyCode[]) => companyCodes)
        .catch(AppErrorHandlerService.handle);
    } else {
      return this.apiService.get(this.path + '/all')
        .map((companyCodes: CompanyCode[]) => companyCodes)
        .catch(AppErrorHandlerService.handle);
    }
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<CompanyCode> {
    return this.apiService.get(this.path + '/' + id)
      .map(companyCode => companyCode)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<CompanyCode> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }
  
  isUnique(field: {}): Observable<{success: boolean, message: string, unique: boolean}> {
    return this.apiService.post(this.path + '/uniqueness', field)
      .map(companyCode => companyCode)
      // .catch(AppErrorHandlerService.handle);
  }

  save(companyCode: CompanyCode): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (companyCode.id_company_code) {
      return this.apiService.put(this.path + '/' + companyCode.id_company_code, companyCode)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, companyCode)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
