import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Role} from 'app/models';

@Injectable()
export class RoleService {
  private path = '/role';

  constructor (
    private apiService: ApiService
  ) {
  }

  // customGet(url): Observable<Role> {
  //   return this.http.get(url)
  //   .map(res => res.json())
  //     .catch(AppErrorHandlerService.handle);
  // }

  getAll(): Observable<Role[]> {
    return this.apiService.get(this.path)
      .map((roles: Role[]) => roles)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Role> {
    return this.apiService.get(this.path + '/' + id)
      .map(role => role)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Role> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(role: Role): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (role.id_role) {
      return this.apiService.put(this.path + '/' + role.id_role, role)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, role)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
