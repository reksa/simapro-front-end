import { Pemeliharaan } from './../../models/pemeliharaan.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { ProgresPekerjaan } from 'app/models/progres-pekerjaan.model';

@Injectable()
export class ProgresPekerjaanService {
  private path = '/progres-pekerjaan';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(pemeliharaan: Pemeliharaan): Observable<ProgresPekerjaan[]> {
    return this.apiService.get(this.path)
      .map((progresPekerjaans: ProgresPekerjaan[]) => progresPekerjaans)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(pemeliharaan: Pemeliharaan, params?: URLSearchParams): Observable<PaginationData> {
    params.append('pemeliharaan_id', pemeliharaan.id_pemeliharaan.toString());
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<ProgresPekerjaan> {
    return this.apiService.get(this.path + '/' + id)
      .map(progresPekerjaan => progresPekerjaan)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<ProgresPekerjaan> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(progresPekerjaan: ProgresPekerjaan): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    if (progresPekerjaan.id_progres_pekerjaan) {
      return this.apiService.put(this.path + '/' + progresPekerjaan.id_progres_pekerjaan, progresPekerjaan)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, progresPekerjaan)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<ProgresPekerjaan> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
