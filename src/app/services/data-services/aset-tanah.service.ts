import { _Tanah } from './../../models/_tanah.model';
import { TahapDokumenLegal } from './../../models/aset-tanah.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { AsetTanah, AsetTanahDetail, DokumenLegal, NilaiAsetTanah, PerpindahanAsetTanah } from 'app/models';
import { AppHttpResponse } from '../../models/app-http-respose.model';

@Injectable()
export class AsetTanahService {
  private path = '/tanah';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(params: URLSearchParams): Observable<AsetTanah[]> {
    return this.apiService.get(this.path, params)
      .map((asetTanahs: AsetTanah[]) => asetTanahs)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-tanah-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-tanah-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginatePenerimaan(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/penerimaan', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<AsetTanah> {
    return this.apiService.get(this.path + '/' + id)
      .map(asetTanah => asetTanah)
      .catch(AppErrorHandlerService.handle);
  }

  getByIdLokasi(idLokasi: number): Observable<AsetTanah[]> {
    const params = new URLSearchParams();
    params.set('id_lokasi', idLokasi.toString());
    return this.apiService.get(this.path, params)
      .map(asetTanahs => asetTanahs)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<AsetTanah> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(asetTanah: AsetTanah): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    if (asetTanah.id_tanah) {
      return this.apiService.post(this.path + '/revisi/' + asetTanah.id_tanah, asetTanah)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, asetTanah)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  saveDataStatis(asetTanah: AsetTanah) {
    return this.apiService.put(this.path + '/update-data-statis/' + asetTanah.id_tanah, asetTanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveDetail(asetTanahDetail: AsetTanahDetail) {
    return this.apiService.post(this.path + '/add-detail/' + asetTanahDetail.id_tanah, asetTanahDetail)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveDokumenLegal(dokumenLegal: DokumenLegal) {
    return this.apiService.post(this.path + '/add-sertifikat/' + dokumenLegal.id_tanah, dokumenLegal)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveNilaiAset(nilaiAsetTanah: NilaiAsetTanah) {
    return this.apiService.post(this.path + '/add-nilai/' + nilaiAsetTanah.id_tanah, nilaiAsetTanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveMutasi(perpindahanAsetTanah: PerpindahanAsetTanah) {
    return this.apiService.post(this.path + '/add-perpindahan/' + perpindahanAsetTanah.id_tanah, perpindahanAsetTanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  saveProgressDokumen(tahapDokumenLegal: TahapDokumenLegal, idTanah: number) {
    // const lastIndex = dokumenLegal.tahap_sertifikat.length - 1;
    return this.apiService.post(this.path + '/add-progress-dokumen/' + idTanah, tahapDokumenLegal)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiDetail(asetTanahDetail: AsetTanahDetail) {
    return this.apiService.post(this.path + '/revisi-detail/' + asetTanahDetail.id_tanah, asetTanahDetail)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiDokumenLegal(dokumenLegal: DokumenLegal) {
    return this.apiService.post(this.path + '/revisi-sertifikat/' + dokumenLegal.id_tanah, dokumenLegal)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiNilaiAset(nilaiAsetTanah: NilaiAsetTanah) {
    return this.apiService.post(this.path + '/revisi-nilai/' + nilaiAsetTanah.id_tanah, nilaiAsetTanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  revisiMutasi(perpindahanAsetTanah: PerpindahanAsetTanah) {
    return this.apiService.post(this.path + '/revisi-perpindahan/' + perpindahanAsetTanah.id_tanah, perpindahanAsetTanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + id)
      .catch(AppErrorHandlerService.handle);
  }

  validasi(idAsetTanah: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi/` + idAsetTanah, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiDetail(idAsetTanahDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-detail/` + idAsetTanahDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiSertifikat(idAsetTanahDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-sertifikat/` + idAsetTanahDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiNilai(idAsetTanahDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-nilai/` + idAsetTanahDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasiPerpindahan(idAsetTanahDetail: number, memo: string, selectedOption: boolean): Observable<any> {
    const args = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + `/action/validasi-perpindahan/` + idAsetTanahDetail, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  import(tanah: _Tanah[]) {
    return this.apiService.post(this.path + '/import', tanah)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  };

  // tolakUsulanDetail(idAsetTanahDetail: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/tolak-validasi-detail-${asValidator}/` + idAsetTanahDetail, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanDetail(idAsetTanahDetail: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/terima-validasi-detail-${asValidator}/` + idAsetTanahDetail, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // tolakUsulanMutasi(idPerpindahanTanah: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/tolak-validasi-perpindahan-${asValidator}/` + idPerpindahanTanah, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanMutasi(idPerpindahanTanah: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(this.path + `/action/terima-validasi-perpindahan-${asValidator}/` + idPerpindahanTanah, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // tolakUsulanNilai(idNilai: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/nilai-aset-tanah/action/tolak-validasi-nilai-${asValidator}/` + idNilai, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanNilai(idNilai: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/nilai-aset-tanah/action/terima-validasi-nilai-${asValidator}/` + idNilai, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // tolakUsulanLegal(idDokumenLegal: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/dokumen-legal/action/tolak-validasi-dokumen-${asValidator}/` + idDokumenLegal, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

  // terimaUsulanLegal(idDokumenLegal: number, memo: string, asValidator: string): Observable<any> {
  //   const args = {
  //     memo: memo
  //   }
  //   return this.apiService.post(`/dokumen-legal/action/terima-validasi-dokumen-${asValidator}/` + idDokumenLegal, args)
  //     .map(data => data)
  //     .catch(AppErrorHandlerService.handle);
  // }

}
