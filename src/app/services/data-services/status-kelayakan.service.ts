import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Observable';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { StatusKelayakan } from 'app/models';

@Injectable()
export class StatusKelayakanService {
  private path = '/status-kelayakan';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<StatusKelayakan[]> {
    return this.apiService.get(this.path)
      .map((statusKelayakan: StatusKelayakan[]) => statusKelayakan)
      .catch(AppErrorHandlerService.handle);
  }

}
