import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Regional } from 'app/models';

@Injectable()
export class RegionalService {
  private path = '/regional';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Regional[]> {
    return this.apiService.get(this.path)
      .map((regionals: Regional[]) => regionals)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Regional> {
    return this.apiService.get(this.path + '/' + id)
      .map(regional => regional)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Regional> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }
  
  isUnique(field: {}): Observable<{success: boolean, message: string, unique: boolean}> {
    return this.apiService.post(this.path + '/uniqueness', field)
      .map(regional => regional)
      // .catch(AppErrorHandlerService.handle);
  }

  save(regional: Regional): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (regional.id_regional) {
      return this.apiService.put(this.path + '/' + regional.id_regional, regional)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, regional)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
