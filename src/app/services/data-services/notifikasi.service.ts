import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Injectable, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Notifikasi } from '../../models/notifikasi.model';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { URLSearchParams } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { Pengguna } from '../../models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { interval } from 'rxjs/observable/interval';
import { ObjectComparator } from '../object-comparator.service';

@Injectable()
export class NotifikasiService {
  private path = '/notifikasi';
  public start = 0;
  public limit = 10;
  public notifications = new BehaviorSubject<NotificationRequest>(new NotificationRequest());
  public unread = new BehaviorSubject<number>(0);
  public total = new BehaviorSubject<number>(0);
  public newUnread = new BehaviorSubject<number>(0);
  public sorted: Notifikasi[] = [];
  private _unread = 0;
  private isFirst = true;
  private _observableUnread: Subscription;
  private _observableNotification: Subscription;
  private timer: Subscription;
  private x: Subscription;
  public isSubscribed = false;
  public needToCheck = true;

  constructor (
    private apiService: ApiService
  ) {}

  getMore() {
    return this.get(this.start).subscribe(notifications => {
        this.start = this.start + notifications.length;
        return this._addToNotification(notifications);
      });
  }

  private _addToNotification(notifications, start?: number): boolean {
    const req = new NotificationRequest();
    req.limit = this.limit;
    req.start = start || this.start;
    req.notification = notifications
    this.notifications.next(req);
    return true;
  }

  getNotification(): Observable<Notifikasi[]> {
    return this.notifications.asObservable().map((request: NotificationRequest) => {
      return this.sorted;
    });
  }

  get(start?: number, limit?: number): Observable<Notifikasi[]> {
    start = start || this.start;
    limit = limit || this.limit;
    const params = new URLSearchParams();
    params.append('offset', start.toString());
    params.append('limit', limit.toString());
    return this.apiService.get(this.path, params)
      .map((notifikasi: Notification[]) => notifikasi)
      .catch(AppErrorHandlerService.handle);
  }

  getUnread(): Observable<AppHttpResponse> {
    return this.apiService.get(this.path + '/unread')
      .map( (respose: AppHttpResponse) => respose)
      .catch(AppErrorHandlerService.handle);
  }

  setReaded(notifikasi: Notifikasi): Observable<AppHttpResponse> {
    return this.apiService.put(this.path + '/' + notifikasi.id)
      .map(response => response)
      .catch(AppErrorHandlerService.handle);
  }

  getUrl(notifikasi: Notifikasi): string {
    if (!notifikasi.read_at) {
      const x = this.setReaded(notifikasi).subscribe(res => {
        this._unread -= 1;
        this.unread.next(this._unread);
        x.unsubscribe();
      });
      notifikasi.read_at = new Date();
    }
    return notifikasi.url;
    // const moduls = ['Lokasi', 'Tanah', 'Properti', 'Potensi', 'Pendayagunaan', 'Anggaran', 'Pekerjaan'];
    // let index = -1;
    // for (let i = 0; i < moduls.length; i++) {
    //   if (notifikasi.notifiable_type.search(moduls[i]) > -1) {
    //     index = i;
    //     break;
    //   }
    // }
    // let modul = '';

    // switch (index) {
    //   case 0: modul = 'lokasi';
    //     break;
    //   case 1: modul = 'aset-tanah';
    //     break;
    //   case 2: modul = 'aset-properti';
    //     break;
    //   case 3: modul = 'potensi-aset';
    //     break;
    //   case 4: modul = 'pendayagunaan-aset';
    //     break;
    //   case 5: modul = 'anggaran';
    //     break;
    //   case 6: modul = 'pekerjaan';
    //     break;
    //   default: modul = null;
    // }
    // return modul ? modul + `/show/${notifikasi.notifiable_id}/ref-notifikasi` : null;
  }

  public checkNotification(time = 30000) {
    if (this.isFirst) {
      this._check();
      this.isFirst = false;
    }
    const t = interval(time);

    this.timer = t.subscribe(n => {
      if(this.needToCheck) {
        if(this.x) {
          this.x.unsubscribe();
        }
        this.x = this.getUnread().subscribe(res => {
          const unread = parseInt(res.message)
          if (this._unread !== unread) {
            this.newUnread.next(unread - this._unread);
            this.unread.next(unread);
            this._unread = unread;
            this.total.next(res.total);
          }
          // this.x.unsubscribe();
        });
      }
    })
  }

  private _check() {
    return this.getUnread().subscribe(res => {
      const unread = parseInt(res.message)
      if (this._unread !== unread) {
        this.newUnread.next(unread - this._unread);
        this.unread.next(unread);
        this._unread = unread;
        this.total.next(res.total);
      }
    });
  }

  public subscribe() {
    this.isSubscribed = true;
    this._observableNotification = this.notifications.asObservable().subscribe((request: NotificationRequest) => {
      if (request.notification) {
        for (const notifikasi of request.notification) {
          if (ObjectComparator.indexOf(this.sorted, notifikasi)) {
            this.sorted.push(notifikasi);
          }
        }
        this.sorted.sort((a, b) => (b.created_at < a.created_at ? -1 : 1));
      }
    });
    this._observableUnread = this.newUnread.asObservable().subscribe(newUnread => {
      const limit = newUnread <= this.limit ? newUnread : this.limit;
      this.get(this.start, limit).subscribe(notifications => this._addToNotification(notifications));
    });
    this.checkNotification();
  }

  public unsubscribe() {
    this.isSubscribed = false;
    if (this._observableNotification) {
      this._observableNotification.unsubscribe();
    }
    if (this._observableUnread) {
      this._observableUnread.unsubscribe();
    }
    if (this.timer) {
      this.timer.unsubscribe();
    }
    this.start = 0;
    this.sorted = [];
    this._unread = 0;
    this.unread.next(0);
    this.newUnread.next(0);
  }

}

class NotificationRequest {
  start: number;
  limit: number;
  notification: Notifikasi[] = [];
}
