import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Lokasi } from 'app/models/lokasi.model';

@Injectable()
export class LokasiService {
  private path = '/lokasi';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Lokasi[]> {
    return this.apiService.get(this.path)
      .map((lokasis: Lokasi[]) => lokasis)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Lokasi> {
    return this.apiService.get(this.path + '/' + id)
      .map(lokasi => lokasi)
      .catch(AppErrorHandlerService.handle);
  }

  getByIdKabupaten(id: number): Observable<Lokasi[]> {
    const params = new URLSearchParams();
    params.append('id_kabupaten', id.toString());
    return this.apiService.get(this.path, params)
      .map(lokasi => lokasi)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Lokasi> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(lokasi: Lokasi): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (lokasi.id_lokasi) {
      return this.apiService.put(this.path + '/' + lokasi.id_lokasi, lokasi)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, lokasi)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
