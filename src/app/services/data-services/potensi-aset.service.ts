import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { PotensiAset } from 'app/models/potensi-aset.model';
import { AppHttpResponse } from 'app/models/app-http-respose.model';

@Injectable()
export class PotensiAsetService {
  private path = '/potensi';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<PotensiAset[]> {
    return this.apiService.get(this.path)
      .map((potensiAsets: PotensiAset[]) => potensiAsets)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-potensi-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-potensi-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<PotensiAset> {
    return this.apiService.get(this.path + '/' + id)
      .map(potensiAset => potensiAset)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<PotensiAset> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(potensiAset: PotensiAset, isRevisi?: boolean): Observable<{success: boolean, message?: string}> {
    isRevisi = isRevisi || false;
    // If we're updating an existing menu
    if (isRevisi) {
      return this.apiService.post(this.path + '/revisi/' + potensiAset.id_potensi, potensiAset)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, potensiAset)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }
  
  destroy(potensiAset: PotensiAset): Observable<AppHttpResponse> {
    const params = {
      path: potensiAset.path
    }
    return this.apiService.put(this.path + '/' + potensiAset.id_potensi, params)
      .catch(AppErrorHandlerService.handle);
  }

  tolakUsulanPotensi(idPotensiAset: number, memo: string, asValidator?: string): Observable<any> {
    const args = {
      path: '/potensi-aset/show/1/show',
      is_terima: false,
      memo: memo
    }
    return this.apiService.post(this.path + `/action/validasi/` + idPotensiAset, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  terimaUsulanPotensi(idPotensiAset: number, memo: string, asValidator?: string): Observable<any> {
    const args = {
      path: '/potensi-aset/show/1/show',
      is_terima: true,
      memo: memo
    }
    return this.apiService.post(this.path + `/action/validasi/` + idPotensiAset, args)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

}
