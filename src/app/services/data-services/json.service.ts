import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { AppErrorHandlerService } from "app/error-handler/app-error-handler.service";
import { Http } from "@angular/http";

@Injectable()
export class JsonService {
  constructor(private http: Http) {}

  getAll(url): Observable<any[]> {
    return this.http
      .get(url)
      .map(arr => {
        return arr.json();
      })
      .catch(AppErrorHandlerService.handle);
  }

  get(url): Observable<any> {
    return this.http
      .get(url)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }
}
