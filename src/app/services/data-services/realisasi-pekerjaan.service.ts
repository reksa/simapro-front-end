import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Realisasi } from 'app/models';

@Injectable()
export class RealisasiPekerjaanService {
  private path = '/realisasi';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(params?): Observable<Realisasi[]> {
    return this.apiService.get(this.path, params)
      .map((realisasiPekerjaans: Realisasi[]) => realisasiPekerjaans)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-realisasi-valid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginateNonValid(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/filter-realisasi-nonvalid', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Realisasi> {
    return this.apiService.get(this.path + '/' + id)
      .map(realisasiPekerjaan => realisasiPekerjaan)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Realisasi> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(realisasiPekerjaan: Realisasi): Observable<{success: boolean, message?: string}> {
      return this.apiService.post(this.path, realisasiPekerjaan)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    // }
  }

  revisi(realisasiPekerjaan: Realisasi): Observable<{success: boolean, message?: string}> {
    return this.apiService.post(this.path + '/revisi/' + realisasiPekerjaan.id_realisasi, realisasiPekerjaan)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  validasi(id: number, memo: string, selectedOption: boolean): Observable<{success: boolean, message?: string}> {
    const params = {
      memo: memo,
      is_terima: selectedOption
    }
    return this.apiService.post(this.path + '/action/validasi/' + id, params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  destroy(id): Observable<Realisasi> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
