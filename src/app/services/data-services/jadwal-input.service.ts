import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { JadwalInput } from 'app/models';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { AppHttpResponse } from '../../models/app-http-respose.model';

@Injectable()
export class JadwalInputService {
  private path = '/jadwal';

  constructor (
    private apiService: ApiService
  ) {}

  get(): Observable<JadwalInput[]> {
    return this.apiService.get(this.path)
      .map((jadwalInput: JadwalInput[]) => jadwalInput)
      .catch(AppErrorHandlerService.handle);
  }

  save(jadwalInput: JadwalInput): Observable<AppHttpResponse> {
    return this.apiService.put(this.path + '/' + jadwalInput.id_jadwal, jadwalInput)
      .map(response => response)
      .catch(AppErrorHandlerService.handle);
  }
}
