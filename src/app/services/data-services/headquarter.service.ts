import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Headquarter } from 'app/models';

@Injectable()
export class HeadquarterService {
  private path = '/head-quarters';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Headquarter[]> {
    return this.apiService.get(this.path)
      .map((headquarters: Headquarter[]) => headquarters)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Headquarter> {
    return this.apiService.get(this.path + '/' + id)
      .map(headquarter => headquarter)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Headquarter> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(headquarter: Headquarter): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (headquarter.id_head_quarters) {
      return this.apiService.put(this.path + '/' + headquarter.id_head_quarters, headquarter)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, headquarter)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
