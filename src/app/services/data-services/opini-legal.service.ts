import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Observable';
import { OpiniLegal } from 'app/models';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class OpiniLegalService {
  private path = '/opini-legal';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<OpiniLegal[]> {
    return this.apiService.get(this.path)
      .map((opiniLegal: OpiniLegal[]) => opiniLegal)
      .catch(AppErrorHandlerService.handle);
  }

}
