import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { Observable } from 'rxjs/Observable';
import { JenisPenghapusan } from 'app/models';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class JenisPenghapusanService {
  private path = '/jenis-penghapusan';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<JenisPenghapusan[]> {
    return this.apiService.get(this.path)
      .map((jenisPenghapusan: JenisPenghapusan[]) => jenisPenghapusan)
      .catch(AppErrorHandlerService.handle);
  }
}
