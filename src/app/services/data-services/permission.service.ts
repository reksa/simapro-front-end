import { NotifikasiService } from './notifikasi.service';
import { AppHttpResponse } from './../../models/app-http-respose.model';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Permission, Modul } from 'app/models';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
// import { BadInputError, NotFoundError, AppError } from 'app/error-handler';
// import { AlertService } from 'app/services/alert.service';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class PermissionService {
  private path = '/permission';

  constructor (
    private apiService: ApiService,
    private notifikasiService: NotifikasiService
  ) {
  }

  get(): Observable<Permission[]> {
    return this.apiService.get(this.path)
      .map((data: {permissions: Permission}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  // check permission ke halaman tertentu
  checkPermission(route: string): Observable<boolean> {
    const params = new URLSearchParams();
    params.append('route', route);
    this.notifikasiService.needToCheck = false;
    return this.apiService.get(this.path + '/check-permission', params)
      .map((data: AppHttpResponse) =>{
        this.notifikasiService.needToCheck = true;
        return data.success
      })
      .catch(AppErrorHandlerService.handle);
  }

  // check edit permission
  checkEditPermission(route: string): Observable<boolean> {
    const params = new URLSearchParams();
    params.append('route', route);
    return this.apiService.get(this.path + '/check-edit-permission', params)
      .map((data: AppHttpResponse) => data.success)
      .catch(AppErrorHandlerService.handle);
  }

  checkRole(type: string): Observable<boolean> {
    const params = new URLSearchParams();
    params.append('type', type);
    this.notifikasiService.needToCheck = false;
    return this.apiService.get(this.path + '/check-role', params)
      .map((data: AppHttpResponse) => {
        this.notifikasiService.needToCheck = true;
        return data.success
      })
      .catch(AppErrorHandlerService.handle);

  }

  getModul(): Observable<Modul[]> {
    this.notifikasiService.needToCheck = false;
    return this.apiService.get('/modul')
      .map((data: {permissions: Modul}) => {
        this.notifikasiService.needToCheck = true;
        return data
      })
      .catch(AppErrorHandlerService.handle);
  }

}
