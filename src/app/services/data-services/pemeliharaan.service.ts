import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { Pemeliharaan } from 'app/models/pemeliharaan.model';

@Injectable()
export class PemeliharaanService {
  private path = '/pemeliharaan';

  constructor (
    private apiService: ApiService
  ) {
  }

  getAll(): Observable<Pemeliharaan[]> {
    return this.apiService.get(this.path)
      .map((pemeliharaans: Pemeliharaan[]) => pemeliharaans)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<Pemeliharaan> {
    return this.apiService.get(this.path + '/' + id)
      .map(pemeliharaan => pemeliharaan)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<Pemeliharaan> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  save(pemeliharaan: Pemeliharaan): Observable<{success: boolean, message?: string}> {
    // If we're updating an existing menu
    if (pemeliharaan.id_pemeliharaan) {
      return this.apiService.put(this.path + pemeliharaan.id_pemeliharaan, pemeliharaan)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, pemeliharaan)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<Pemeliharaan> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
