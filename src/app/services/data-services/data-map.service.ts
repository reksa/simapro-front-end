import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { LatLngBounds } from 'leaflet';

@Injectable()
export class DataMapService {
  private path = '/map';

  constructor (
    private apiService: ApiService
  ) {}

  get(zoom: number, bounds: LatLngBounds, type: string[], otherParams?): Observable<any> {
    const params = otherParams || {};
    params['bounds'] = bounds;
    params['type'] = type;
    
    return this.apiService.post(this.path + '/' + zoom, params)
      .map(res => res)
      .catch(AppErrorHandlerService.handle);
  }

}
