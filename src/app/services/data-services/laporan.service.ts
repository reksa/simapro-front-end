import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { PaginationData } from "app/components/pagination/page/page.component";
import { AppErrorHandlerService } from "app/error-handler/app-error-handler.service";
import { ApiService } from "./api.service";
import { URLSearchParams } from "@angular/http";
import { resetFakeAsyncZone } from "@angular/core/testing";

@Injectable()
export class LaporanService {
  constructor(private apiService: ApiService) {}

  dashboard(params?: URLSearchParams): Observable<any> {
    return this.apiService
      .get("/dashboard", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  tanah(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService
      .get("/laporan/tanah", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  properti(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService
      .get("/laporan/properti", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  pendayagunaan(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService
      .get("/laporan/pendayagunaan", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  realisasi(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService
      .get("/laporan/realisasi", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  downloadExcel(
    type: string,
    params: URLSearchParams,
    filename: string = "download.xlsx"
  ) {
    params.append("jenis", type);
    return this.apiService
      .download("/laporan/excel", params)
      .map(res => {
        return {
          filename: filename,
          data: res.blob()
        };
      })
      .catch(AppErrorHandlerService.handle);
  }

  public getInbox(params?: URLSearchParams): Observable<any> {
    return this.apiService
      .get("/inbox", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }

  public getSertifikatWillBeExpired(params?: URLSearchParams): Observable<any> {
    return this.apiService
      .get("/tanah/sertifikat-akan-habis", params)
      .map(data => data)
      .catch(AppErrorHandlerService.handle);
  }
}
