import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { OrganisasiLevel } from 'app/models';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';

@Injectable()
export class OrganisasiLevelService {
  private path = '/organisasi-level';

  constructor (
    private apiService: ApiService
  ) {}

  getAll(): Observable<OrganisasiLevel[]> {
    return this.apiService.get(this.path)
      .map((organisasiLevels: OrganisasiLevel[]) => organisasiLevels)
      .catch(AppErrorHandlerService.handle);
  }
}
