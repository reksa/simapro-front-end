import { AppHttpResponse } from './../../models/app-http-respose.model';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { PaginationData } from 'app/components/pagination/page/page.component';
import { AppErrorHandlerService } from 'app/error-handler/app-error-handler.service';
import { ApiService } from './api.service';
import { BusinessArea } from 'app/models';
import { Location } from '@angular/common';

@Injectable()
export class BusinessAreaService {
  private path = '/business-area';

  constructor (
    private apiService: ApiService,
    private _location: Location
  ) {
  }

  getAll(params: URLSearchParams): Observable<BusinessArea[]> {
    if(!params.get('path')) {
      let path = this._location.path();
      const indexOfQueryParametersSign = path.indexOf('?');
      const indexOfFragment = path.indexOf('#');
      path = indexOfQueryParametersSign > -1 ? path.substring(0, indexOfQueryParametersSign) : path;
      path = indexOfFragment > -1 ? path.substring(0, indexOfFragment) : path;
      params.append('path', path);
    }
    params = params || new URLSearchParams();
    return this.apiService.get(this.path)
      .map((businessAreas: BusinessArea[]) => businessAreas, params)
      .catch(AppErrorHandlerService.handle);
  }

  getPaginate(params?: URLSearchParams): Observable<PaginationData> {
    return this.apiService.get(this.path + '/paginate', params)
      .map((data: {data: PaginationData}) => data)
      .catch(AppErrorHandlerService.handle);
  }

  getById(id: number): Observable<BusinessArea> {
    return this.apiService.get(this.path + '/' + id)
      .map(businessArea => businessArea)
      .catch(AppErrorHandlerService.handle);
  }

  getByIdCompanyCode(id: number, params?: URLSearchParams): Observable<BusinessArea[]> {
    params = params || new URLSearchParams();
    if(!params.get('path')) {
      let path = this._location.path();
      const indexOfQueryParametersSign = path.indexOf('?');
      const indexOfFragment = path.indexOf('#');
      path = indexOfQueryParametersSign > -1 ? path.substring(0, indexOfQueryParametersSign) : path;
      path = indexOfFragment > -1 ? path.substring(0, indexOfFragment) : path;
      params.append('path', path);
    }
    params.set('id_company_code', id.toString());
    // params.set('level_access', '1');
    return this.apiService.get(this.path, params)
      .map(businessArea => businessArea)
      .catch(AppErrorHandlerService.handle);
  }

  get(resource: Object): Observable<BusinessArea> {
    let params = '';
    for (const key in resource) {
      if (resource[key] !== null) {
        params += '/' + key + '/' + resource[key];
      }
    }
    return this.apiService.get(this.path + '/' + params)
      .map(res => res.json())
      .catch(AppErrorHandlerService.handle);
  }

  isUnique(field: {}): Observable<{success: boolean, message: string, unique: boolean}> {
    return this.apiService.post(this.path + '/uniqueness', field)
      .map(businessArea => businessArea);
  }

  save(businessArea: BusinessArea): Observable<AppHttpResponse> {
    // If we're updating an existing menu
    if (businessArea.id_business_area) {
      return this.apiService.put(this.path + '/' + businessArea.id_business_area, businessArea)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);

    // Otherwise, create a new menu
    } else {
      return this.apiService.post(this.path, businessArea)
        .map(data => data)
        .catch(AppErrorHandlerService.handle);
    }
  }

  destroy(id): Observable<AppHttpResponse> {
    return this.apiService.delete(this.path + '/' + id)
      .catch(AppErrorHandlerService.handle);
  }

}
