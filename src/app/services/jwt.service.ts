import { environment } from 'environments/environment';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { DecodedToken } from './../models/access-token.model';
import { JwtHelper } from 'angular2-jwt';
import { AccessToken } from 'app/models/access-token.model';
import { Injectable } from '@angular/core';


@Injectable()
export class JwtService {
  public tokenNearlyExpired = new ReplaySubject<boolean>(1);
  public tokenIsBeingRefreshed = false;
  jwtHelper: JwtHelper = new JwtHelper();
  tokenName = 'jwtToken';

  getToken(): string {
    return window.localStorage[this.tokenName];
  }

  saveToken(token: string) {
    const decodedToken: DecodedToken = this.jwtHelper.decodeToken(token);
    window.localStorage[this.tokenName] = token;
    window.localStorage['jwtExpiresOn'] = decodedToken.exp;
  }

  saveAppData(data: Object) {
    window.localStorage['appData'] = JSON.stringify(data);
  }

  getAppData(): any {
    return JSON.parse(window.localStorage['appData']);
  }

  destroyToken() {
    window.localStorage.removeItem(this.tokenName);
    window.localStorage.removeItem('jwtExpiresOn');
  }

  isNearlyExpired(): boolean {
    let batas = environment.jwt_refresh_time_before_expires;
    const expires = parseInt(window.localStorage['jwtExpiresOn'], 10);
    const now = new Date();
    batas = Math.floor(now.getTime() / 1000) + batas;

    // Will Expires in 1 Minutes or less ?
    return expires < batas;
  }

  timeUntilExpired(padding: number): number {
    const expires = parseInt(window.localStorage['jwtExpiresOn'], 10);
    const timeInSecond = Math.floor(new Date().getTime() / 1000);

    // Will Expires < 5 Minutes ?
    return (expires - timeInSecond) - padding ;
  }

}
