import { TestBed, inject } from '@angular/core/testing';

import { OptionButtonService } from './option-button.service';

describe('OptionButtonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OptionButtonService]
    });
  });

  it('should be created', inject([OptionButtonService], (service: OptionButtonService) => {
    expect(service).toBeTruthy();
  }));
});
