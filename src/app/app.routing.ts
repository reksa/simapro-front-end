import { PermissionGuard } from "./services/auth/permission-guard.service";
import { CreditsComponent } from "./views/credits/credits.component";
import { AuthComponent } from "./views/auth/auth.component";
import { NoAuthGuard, AuthGuard } from "./services/auth";
import {
  P401Component,
  P403Component,
  P404Component,
  P500Component
} from "./views/error-pages";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import Containers
import { FullLayoutComponent, SimpleLayoutComponent } from "./containers";
import { LoginWithSsoComponent } from "./views/auth/login-with-sso/login-with-sso.component";
import { FaqComponent } from "./views/faq/faq.component";
import { DownloadPageComponent } from "./views/download-page/download-page.component";

export const routes: Routes = [
  {
    path: "",
    component: FullLayoutComponent,
    data: {
      title: "Home"
    },
    children: [
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
        canActivate: [AuthGuard]
      },
      {
        path: "credits",
        component: CreditsComponent,
        canActivate: [AuthGuard]
      },
      { path: "faq", component: FaqComponent, canActivate: [AuthGuard] },
      {
        path: "frequently-asked-question",
        component: FaqComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "download",
        component: DownloadPageComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "dashboard",
        loadChildren: "./views/dashboard/dashboard.module#DashboardModule"
      },
      {
        path: "aset-tanah",
        loadChildren: "./views/aset-tanah/aset-tanah.module#AsetTanahModule"
      },
      {
        path: "aset-properti",
        loadChildren:
          "./views/aset-properti/aset-properti.module#AsetPropertiModule"
      },
      {
        path: "anggaran",
        loadChildren: "./views/anggaran/anggaran.module#AnggaranModule"
      },
      {
        path: "realisasi",
        loadChildren: "./views/realisasi/realisasi.module#RealisasiModule"
      },
      {
        path: "potensi-aset",
        loadChildren:
          "./views/potensi-aset/potensi-aset.module#PotensiAsetModule"
      },
      {
        path: "pendayagunaan-aset",
        loadChildren:
          "./views/pendayagunaan-aset/pendayagunaan-aset.module#PendayagunaanAsetModule"
      },
      {
        path: "master-data",
        loadChildren: "./views/master-data/master-data.module#MasterDataModule"
      },
      {
        path: "import",
        loadChildren: "./views/import/import.module#ImportModule"
      },
      {
        path: "workflow",
        loadChildren: "./views/workflow/workflow.module#WorkflowModule"
      },
      {
        path: "laporan",
        loadChildren: "./views/laporan/laporan.module#LaporanModule"
      },
      {
        path: "user",
        loadChildren:
          "./views/master-data/manajemen-pengguna/pengguna/pengguna.module#PenggunaModule"
      }
    ]
  },
  {
    path: "error",
    component: SimpleLayoutComponent,
    children: [
      { path: "401", component: P401Component },
      { path: "403", component: P403Component },
      { path: "404", component: P404Component },
      { path: "500", component: P500Component }
    ]
  },
  { path: "auth/login", component: AuthComponent, canActivate: [NoAuthGuard] },
  { path: "auth/logout", component: AuthComponent },
  { path: "auth/login-with-sso", component: LoginWithSsoComponent },
  { path: "**", redirectTo: "error/404" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
