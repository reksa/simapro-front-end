
const server_url = window.location.origin;

export const environment = {
  production: true,
  max_file_upload_size: 5000000, // 5 MB
  jwt_refresh_time_before_expires: 60, // 60 detik / 1 menit
  server_url: server_url,
  webservice_url: server_url + '/api', //'http://192.168.100.5/simapro/public/api',
  upload_url: server_url + '/assets/img', //'http://192.168.100.5/simapro/public/upload',
  sso_url: server_url + '/login-with-sso',
  callback_sso: 'https://simapro.pln.co.id/simapro/#/auth/login-with-sso', //'http://192.168.100.5/simapro/public/upload',
  map: {
    tile_server_url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', // http://192.168.100.18/tile-server.php?z={z}&x={x}&y={y}'
    marker: {
      lokasi: {
        iconAnchor: [6, 30],
        iconSize: [30, 30],
        popupAnchor: [0, -25],
        className: '',
        iconUrl: 'assets/img/maps/marker-icon/036-goal-1.png',
        //shadowUrl:''
      },
      lokasiClustered: {
        iconAnchor: [6, 30],
        iconSize: [30, 30],
        popupAnchor: [0, -25],
        className: '',
        iconUrl: 'assets/img/maps/marker-icon/045-flag-3.png',
        //shadowUrl:''
      },
      asetTanah: {
        pin: {
          iconAnchor: [10, 20],
          iconSize: [20, 20],
          popupAnchor: [0, -25],
          className: '',
          iconUrl: 'assets/img/maps/marker-icon/121-push-pin.png',
          //shadowUrl:''
        },
        pointClustered: {
          iconAnchor: [20, 40],
          iconSize: [40, 40],
          popupAnchor: [0, -25],
          className: '',
          iconUrl: 'assets/img/maps/marker-icon/if_gpsmapicons07_68010.png',
          //shadowUrl:''
        },
        point: {
          iconAnchor: [15, 30],
          iconSize: [30, 30],
          popupAnchor: [0, -25],
          shadowSize: [0,0],
          className: '',
          iconUrl: 'assets/img/maps/marker-icon/if_Map-Marker-Ball-Pink_73017.png',
          //shadowUrl:''
        },
        polygon: {
          color: 'red',
          weight: 1
        },
        polygonNonValid: {
          color: 'yellow',
          weight: 1
        }
      },
      asetProperti: {
        pin: {
          iconAnchor: [10, 20],
          iconSize: [20, 20],
          popupAnchor: [0, -25],
          className: '',
          shadowSize: [0,0],
          iconUrl: 'assets/img/maps/marker-icon/121-push-pin.png',
          //shadowUrl:''
        },
        pointClustered: {
          iconAnchor: [20, 40],
          iconSize: [40, 40],
          popupAnchor: [0, -25],
          className: '',
          shadowSize: [0,0],
          iconUrl: 'assets/img/maps/marker-icon/if_gpsmapicons07_101894.png',
          // shadowUrl:'http://plantationcounseling.com/wp-content/uploads/2015/05/Bullet-Arrow-BLANK.png'
        },
        point: {
          iconAnchor: [15, 30],
          iconSize: [30, 30],
          popupAnchor: [0, -25],
          className: '',
          shadowSize: [0,0],
          iconUrl: 'assets/img/maps/marker-icon/if_Map-Marker-Ball-Azure_73012.png',
          //shadowUrl:''
        },
        polygon: {
          color: 'blue',
          weight: 1
        },
        polygonNonValid: {
          color: 'yellow',
          weight: 1
        }
      },
      asetBangunan: {
        iconAnchor: [10, 56],
        iconSize: [50, 50],
        popupAnchor: [0, -25],
        className: '',
        iconUrl: 'assets/img/maps/marker-icon/040-goal.png',
        //shadowUrl:''
      },
      asetWisma: {
        iconAnchor: [10, 56],
        iconSize: [50, 50],
        popupAnchor: [0, -25],
        className: '',
        iconUrl: 'assets/img/maps/marker-icon/040-goal.png',
        //shadowUrl:''
      },
      asetKantor: {
        iconAnchor: [10, 56],
        iconSize: [50, 50],
        popupAnchor: [0, -25],
        className: '',
        iconUrl: 'assets/img/maps/marker-icon/040-goal.png',
        //shadowUrl:''
      },
    }
  }
};
