______________________________________________________________________________

Cara penggunaan :
______________________________________________________________________________
/**
| Requirements :
| * PHP ^7.0
| * Composer
| * untuk windows : PHP telah dimasukkan pada PATH system environment
**/
---------
Server :
---------
1. Clone Repositori Simapro-webservice : https://sandifebrian@bitbucket.org/simaprodevteam/simapro-webservice.git
2. Buka folder dari repositori simapro-webservice melalui console (cmd, terminal, dll)
3. Ketikkan perintah Composer Install
4. Import database (file simapro_v3.sql) ke DBMS MySQL
5. sesuaikan pengaturan database (DB_DATABASE, DB_USERNAME, DB_PASSWORD) pada file .env
4. Ketikkan perintah "php artisan serve"

---------
Client :
---------
1. Clone Repositori Simapro-front-end : https://sandifebrian@bitbucket.org/simaprodevteam/simapro-web.git
2. Copy File dari folder "/dist" ke folder www / htdocs di server
3. Jalankan web server

Keterangan :
* Server simapro harus berjalan pada development localhost:8000. Jika server 
	menggunakan alamat lainnya, maka Client harus di Build ulang